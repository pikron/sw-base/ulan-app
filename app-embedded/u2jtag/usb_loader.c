#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <endian.h>
#include <usb/usb.h>
#include <JTAGfunc.h>
#include <LowLevelFunc.h>
#include <usb/usb_loader.h>

unsigned char loader_buffer[USB_MAX_PACKET0];

unsigned long msp430_flash_minaddr=0x1100;
unsigned long msp430_flash_maxaddr=0xffff;

/* forward declarations */
int jtag_check_init(int force);

int usb_msp430_pkt_wrmsp(struct usb_ep_t *ep, int len, int code)
{
  unsigned char *ptr=ep->ptr-len;
#if __BYTE_ORDER == __BIG_ENDIAN
  unsigned char c;
#endif
  int i;
  /*printf("usb_msp430_pkt_wrmsp bytes %d, loc %lx -> msp %lx\n", len, (long)ptr, ep->user_data);*/

  if(!(ep->user_data&1)) {
    i = len/2;
#if __BYTE_ORDER == __BIG_ENDIAN
    while(i--){
      /* swap endianning */
     c = ptr[0]; ptr[0] = ptr[1]; ptr[1] = c;
      ptr += 2;
    }
    ptr -= len & ~1;
#endif
    if( (ep->user_data<msp430_flash_minaddr) ||
        (ep->user_data>msp430_flash_maxaddr) ) {
      WriteMemQuick(ep->user_data, len/2, (word*)ptr);
    } else {
      WriteFLASH(ep->user_data, len/2, (word*)ptr);
      if(VerifyMem(ep->user_data, len/2, (word*)ptr) != STATUS_OK) {
	ep->ptr=loader_buffer;
	return USB_COMPLETE_FAIL;
      }
    }
#if __BYTE_ORDER != __BIG_ENDIAN
    ptr += len & ~1;
#endif
    ep->user_data += len & ~1;
    len &= 1;
  }

  while(len--){
    /*F_BYTE or F_WORD*/
    WriteMem(F_BYTE, ep->user_data++, *(ptr++));
  }

  ep->ptr=loader_buffer;
  return USB_COMPLETE_OK;
}

int usb_msp430_pkt_rdmsp(struct usb_ep_t *ep, int len, int code)
{
  unsigned char *ptr;
#if __BYTE_ORDER == __BIG_ENDIAN
  unsigned char c;
#endif
  int i;
  /* printf("usb_msp430_pkt_rdmsp bytes %d\n", len);  */
  ptr = ep->ptr = loader_buffer;
  
  if(!(ep->user_data&1)) {
    i = len/2;
    ReadMemQuick(ep->user_data, i, (word*)ptr);
#if __BYTE_ORDER == __BIG_ENDIAN
    while(i--){
      /* swap endianning */
      c = ptr[0]; ptr[0] = ptr[1]; ptr[1] = c;
      ptr += 2;
    }
#else
    ptr += len & ~1;
#endif
    ep->user_data += len & ~1;
    len &= 1;
  }

  while(len--){
    /*F_BYTE or F_WORD*/
    *(ptr++) = ReadMem(F_BYTE, ep->user_data++);
  }

  return USB_COMPLETE_OK;
}

/*

ERASE_MASS or ERASE_MAIN or ERASE_SGMT
void EraseFLASH(word EraseMode, word EraseAddr)

*/

int msp430_erase(unsigned addr, unsigned len)
{
  unsigned end = addr + len - 1; 
  if(addr<0x1000)
    return -1;
  
  while(addr < end) {
    EraseFLASH( ERASE_SGMT, addr);
    if(addr<0x1100) addr = (addr & ~0x7f) + 0x80;
    else addr = (addr & ~0x1ff) + 0x200;
  }
  return 0;
}

int msp430_mass_erase(unsigned mode, unsigned addr)
{
  if(mode == 0) mode = ERASE_MASS;
  EraseFLASH( mode, addr);
  return 0;
}

void msp430_goto(unsigned address)
{
  /*SetPC(address);*/
  ReleaseDevice(address);
}

int usb_msp430_loader(usb_device_t *udev)

{
  unsigned long addr;
  unsigned len;
  USB_DEVICE_REQUEST *dreq;

  dreq=&udev->request;
  switch ( dreq->bRequest & USB_VENDOR_MASK) {

    case USB_VENDOR_GET_CAPABILITIES:
//      printf("GET_CAPABILITIES\n");
      loader_buffer[0] = 0xAA; // test
      usb_send_control_data(udev, loader_buffer, 1);
      return 1;

    case USB_VENDOR_RESET_DEVICE:
//      printf("RESET_DEVICE\n");
      InitController();
      InitTarget();
      if (!GetDevice()) {
        return -1;
      }
      usb_send_control_data(udev,NULL,0);
      return 1;

    case USB_VENDOR_GOTO:
//      printf( "GOTO 0x%X\n", dreq->wValue);
      usb_send_control_data(udev,NULL,0);
      msp430_goto(dreq->wValue);
      return 1;

    case USB_VENDOR_ERASE_MEMORY:
//      printf( "ERASE 0x%X 0x%X\n", dreq->wValue, dreq->wIndex);
      usb_send_control_data(udev,NULL,0);
      msp430_erase(dreq->wValue, dreq->wIndex);
      return 1;

    case USB_VENDOR_MASS_ERASE:
//      printf( "MASSERASE 0x%X 0x%X\n", dreq->wIndex, dreq->wValue);
      usb_send_control_data(udev,NULL,0);
      msp430_mass_erase(dreq->wIndex, dreq->wValue);
      return 1;
      
    case USB_VENDOR_GET_SET_MEMORY:
      addr = (dreq->wValue & 0xffff) | (((unsigned long)dreq->wIndex&0xffff)<<16);
      len = dreq->wLength;
//      printf("GET_SET_MEMORY, addr=0x%lx, len=%d\n", (long)addr, len);
      if (( dreq->bmRequestType & USB_DATA_DIR_MASK) == USB_DATA_DIR_FROM_HOST) {
        // read from HOST ???
        switch( dreq->bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_ADAPTER:
	  	udev->ep0.ptr = (void*)addr; break;

          case USB_VENDOR_TARGET_MSP430:
	        if (jtag_check_init(0)<0) {
//                  printf("MSP430 JTAG init ERROR\n");
                  return -1;
                }
	  	udev->ep0.next_pkt_fnc = usb_msp430_pkt_wrmsp;
		udev->ep0.user_data=addr;
	  	udev->ep0.ptr = loader_buffer; break;

	  default: return -1;
        }
        if ( len) usb_set_control_endfnc(udev, usb_ack_setup);
        else usb_send_control_data(udev,NULL,0);
	return 1;
      } else {
        switch( dreq->bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_ADAPTER:
	  	usb_send_control_data(udev, (void*)addr, len); break;

          case USB_VENDOR_TARGET_MSP430:
	        if (jtag_check_init(0)<0) {
//                  printf("MSP430 JTAG init ERROR\n");
                  return -1;
                }
	  	udev->ep0.next_pkt_fnc = usb_msp430_pkt_rdmsp;
		udev->ep0.user_data=addr;
	  	usb_send_control_data(udev, loader_buffer, len); break;

	  default: return -1;
        }
	return 1;
      }
      break;
  }


  return 0;
}
