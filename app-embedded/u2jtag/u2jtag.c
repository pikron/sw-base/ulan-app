#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <usb/usbdebug.h>
#include <usb/usb.h>
#include <usb/lpc.h>
#include <usb/usb_loader.h>
#include <usb/usb_devdes.h>
#include "usb/usb_defs.h"

#include <JTAGfunc.h>
#include <LowLevelFunc.h>

#define WITH_USB

#ifdef WITH_USB
usb_device_t usb_device;
usb_ep_t eps[NUM_ENDPOINTS]; 
int usb_msp430_loader(usb_device_t *udev);
#endif /*WITH_USB*/

int main()
{

  InitController(); /*JTAG master initialization*/

 #ifdef WITH_USB
  memset( &usb_device, 0, sizeof( usb_device));
  usb_device.id = 1;
  usb_device.devdes_table = &usb_devdes_table;
  usb_device.init = usb_lpc_init;
  usb_debug_set_level(DEBUG_LEVEL_NONE);
  usb_device.cntep = NUM_ENDPOINTS;
  usb_device.ep = eps;

  usb_device.vendor_fnc=usb_msp430_loader;

  usb_init(&usb_device);
  usb_connect(&usb_device);
 #endif /*WITH_USB*/

  while(1){

   #ifdef WITH_USB
    usb_check_events(&usb_device);
    usb_control_response(&usb_device);
   #endif /*WITH_USB*/

  }
}

char jtag_init_ok=0;

int jtag_check_init(int force)
{
  if(!jtag_init_ok || force) {
    InitTarget();       // Initialize Target Board 
    if (!GetDevice())   // Set global DEVICE variable, stop
       return -1;       // here if invalid JTAG ID or 
    jtag_init_ok = 1;
  }
  return 0;
}

