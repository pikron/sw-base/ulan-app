/*
    HA LIGHT SWITCH
*/

#include <stdio.h>
#include <system_def.h>
#include <cpu_def.h>
#include <ul_lib/ulan.h>
#include <keyval_id.h>
#include <uloi_base.h>
#include <lt_timer.h>
#include <uldy_base.h>
#include <keyval_loc.h>
#include <lpciap_kvpb.h>
#include <hal_machperiph.h>
#include "light_switch.h"
#include "ul_idstr.h"

LT_TIMER_DEC(lt_1msec)
LT_TIMER_IMP(lt_1msec)
LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)
LT_TIMER_DEC(lt_1sec)
LT_TIMER_IMP(lt_1sec)

/***********************************/
// global variables
uloi_con_ulan_t uloi_con_ulan_global;
kvpb_block_t kvpb_block_global;
ul_dyac_t ul_dyac_global;
int tled_off;

kvpb_block_t *kvpb_block=&kvpb_block_global;
uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
ul_dyac_t *ul_dyac=&ul_dyac_global;
ul_fd_t ul_fd;
ul_fd_t ul_fd1;
ul_msginfo msginfo;

/* ulan variables */
unsigned char ubuff[30];
unsigned int  DATA uaddr;
unsigned long DATA usn;
uchar ustatus;

//object interface variables
unsigned int oi_led_state;

/****************************************************************************/
int uloi_led_state_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  return 1;
}

/***********************************/
char ul_save_sn(uint32_t usn)
{
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  return 0;
}

/***********************************/
char ul_save_adr(uint8_t uaddr)
{
  unsigned int v=uaddr;
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&v);
  return 0;
}

void sys_err()
{
  while(1);
}

int main(void) 
{
  int i=0,kl_wait=0,duty=0;

  /***********************************/
  // timer
  lt_1msec_init();
  lt_100msec_init();
  lt_1sec_init();

  /***********************************/
  //init values
  tled_off=0;

  /***********************************/
  // kvpb init
  kvpb_block->base=(uint8_t CODE*)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DEFAULT_FLAGS;
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=kvpb_erase;
  kvpb_block->copy=kvpb_copy;
  kvpb_block->flush=kvpb_flush;
  if (kvpb_check(kvpb_block,1)<0) sys_err();

  // read data from kvpb
  uaddr=62;
  usn=0L;
  oi_led_state=0;
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);

  //********************
  // uLan init
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) sys_err();    
  ul_fd1=ul_open(NULL,NULL);
  if (ul_fd1==UL_FD_INVALID) sys_err();    
  ul_setidstr(ul_fd,ul_idstr);
  ul_setmyadr(ul_fd,uaddr);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(ul_fd,&msginfo);
  ul_stroke(ul_fd);

  /***********************************/
  // uLan object interface init
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);

  /***********************************/
  // uLan dyac init
  uldy_init(ul_dyac,ul_fd,ul_save_sn,ul_save_adr,(char*)ul_idstr,usn);

  //********************
  // start
  while (1) {

    /* processing of ulan messages */
    if ((ul_inepoll(ul_fd)>0) && (ul_acceptmsg(ul_fd, &msginfo)>=0)) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo (uloi_objdes_array_t*)&uloi_objdes_main, &msginfo)<0) {
          if (uldy_process_msg(ULDY_ARG_ul_dyac &msginfo)<0) {
            ul_freemsg(ul_fd);
          }
        }
      } else 
        ul_freemsg(ul_fd);
    }

    /* 1ms timer */
    if (lt_1msec_expired(1)) {
      i++;
      if (i>20) i=0;

      CLR_OUT_PIN(LED_PORT,LED2_BIT);
      if ((i>(duty&0x3))) 
        CLR_OUT_PIN(LED_PORT,LED1_BIT);
      else
        SET_OUT_PIN(LED_PORT,LED1_BIT); 
    }

    /* 100ms timer */
    if (lt_100msec_expired(100)) {

      if ((GET_IN_PIN(IN_PORT,(SW1_BIT|SW3_BIT))==0) && (kl_wait==0)) {
        ubuff[0]=UL_CMD_OISV+1;
        ubuff[1]=0x41;
        ubuff[2]=0;
        ubuff[3]=200; /*200*/
        ubuff[4]=0;
        ubuff[5]=1;
        ubuff[6]=0;
        msginfo.cmd=UL_CMD_OISV;
        msginfo.flg=UL_BFL_SND | UL_BFL_ARQ;
        msginfo.dadr=50; /*50*/
        ul_newmsg(ul_fd,&msginfo);
        ul_write(ul_fd,ubuff,7);
        ul_freemsg(ul_fd);
        SET_OUT_PIN(LED_PORT,LED4_BIT);
        tled_off=10;
        kl_wait=5;
      }

      if (kl_wait) {
        if (GET_IN_PIN(IN_PORT,(SW1_BIT|SW3_BIT))==0) kl_wait=5;
        else kl_wait--;
      }

      if (tled_off) {
        tled_off--;
        if (!tled_off) {
          CLR_OUT_PIN(LED_PORT,LED4_BIT);
        }
      }


      /* watchdog */
     #ifdef WATCHDOG_ENABLED
      watchdog_feed();
     #endif /* WATCHDOG_ENABLED */
    
      /* ulan stroke */
     #if defined(SDCC) || defined(__SDCC) 
      ul_stroke(ul_fd);
     #endif
    }

    /* 1s timer */
    if (lt_1sec_expired(1000)) { 

      duty++;
    }

  }
  return 0;
}
