#ifndef _LIGHT_SWITCH_H
#define _LIGHT_SWITCH_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

extern const uloi_objdes_array_t uloi_objdes_main;

extern unsigned int oi_led_state;

int uloi_led_state_wrfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _LIGHT_SWITCH_H */

