/*
    BLINDER
*/
#include <stdio.h>
#include <system_def.h>
#include <cpu_def.h>
#include <ul_lib/ulan.h>
#include <keyval_id.h>
#include <keyval_id_his.h>
#include <uloi_base.h>
#include <lt_timer.h>
#include <uldy_base.h>
#include <keyval_loc.h>
#include <lpciap_kvpb.h>
#include <hal_machperiph.h>
#include "blinder.h"
#include "ul_idstr.h"


LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)

kvpb_block_t kvpb_block_global;
uloi_con_ulan_t uloi_con_ulan_global;
ul_dyac_t ul_dyac_global;

kvpb_block_t *kvpb_block=&kvpb_block_global;
uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
ul_dyac_t *ul_dyac=&ul_dyac_global;
ul_fd_t ul_fd;
ul_fd_t ul_fd1;
ul_msginfo msginfo;

//ulan variables
unsigned int uaddr;
unsigned long usn;
//timers
uint8_t tnew_cmd;
int tblinder_off,tblinder_off_start,tblinder_off_extra;
//blinder variables
uint16_t new_bl_pos;

//object interface variables
unsigned int oi_lighting;
unsigned int oi_bl_pos;
unsigned int oi_open_time;
unsigned int oi_bl_stop;
unsigned int oi_blp_enabled;
unsigned int oi_blp_up;
unsigned int oi_blp_down;
unsigned int oi_calibration;
unsigned int oi_ac_in;

int cal_timeout;
cal_fnc *cal_fnc_act=NULL;
int cal_wait;
int cal_time;

int oi_ac_in_rdfnc(ULOI_PARAM_coninfo void *context)
{
  oi_ac_in=0;
  if (GET_IN_PIN(IN_PORT,AC_IN_BIT)) 
    oi_ac_in=1;
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &oi_ac_in);
}


void cal_fnc_timeout(void)
{
  CLR_OUT_PIN(OUT_PORT,RE1_BIT); //stop
  CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
  CLR_OUT_PIN(LED_PORT,LED_GP); 
  cal_fnc_act=NULL;
  cal_timeout=0;
  cal_wait=0;
}

/* calibration - the blinder is openned - save messaured time */
int cal_fnc_state5(void)
{
  if (GET_IN_PIN(IN_PORT,AC_IN_BIT)) {
    cal_fnc_timeout();
    /* save meassured time */
    kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_OPENTIME,sizeof(unsigned int),&cal_time);
    cal_time=0;
    /* save blinder possition */
    oi_bl_pos=100;
    kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,sizeof(unsigned int),&oi_bl_pos);
  }
  return 0;
}

/* calibration - wait till engine starts */
int cal_fnc_state4(void)
{
  if (!GET_IN_PIN(IN_PORT,AC_IN_BIT)) {
    cal_fnc_act=cal_fnc_state5;
    cal_wait=500;
  }
  return 0;
}

/* calibration - open blinder and start meassure blinder openning time */
int cal_fnc_state3(void)
{
  /* start meassure time to close blinder */
  cal_time=0;
  SET_OUT_PIN(OUT_PORT,RE1_BIT); //close
  CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
  cal_fnc_act=cal_fnc_state4;
  cal_wait=500;
  return 0;
}

/* calibration - wait till blinder is closed */
int cal_fnc_state2(void)
{
  if (GET_IN_PIN(IN_PORT,AC_IN_BIT)) {
    /* the bliner is in the opened possition */
    CLR_OUT_PIN(OUT_PORT,RE1_BIT); //stop
    CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
    /* wait 500ms to go into state 2 */
    cal_fnc_act=cal_fnc_state3;
    cal_wait=500;
  }
  return 0;
}

/* calibration - close blinder */
int cal_fnc_state1(void)
{
  CLR_OUT_PIN(OUT_PORT,RE1_BIT); //open
  SET_OUT_PIN(OUT_PORT,RE2_BIT); 
  SET_OUT_PIN(LED_PORT,LED_GP); 
  /* wait 500ms to go into state 2 */
  cal_fnc_act=cal_fnc_state2;
  cal_wait=500;
  return 0;
}

void bl_set_pos(uint8_t pos)
{
  new_bl_pos=pos;
  if (new_bl_pos>100) new_bl_pos=100;
  if (tnew_cmd==0) {
    CLR_OUT_PIN(OUT_PORT,RE1_BIT); //stop
    CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
    CLR_OUT_PIN(LED_PORT,LED_GP); 
    tnew_cmd=10;
    if (tblinder_off)   //set new actual possition when blinder was running
      oi_bl_pos+=(100*(tblinder_off_start-tblinder_off))/oi_open_time;
    tblinder_off_extra=0;
  }
}

int oi_bl_pos_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uint32_t npos;
  uloi_uint_wrfnc(ULOI_ARG_coninfo &npos);
  bl_set_pos(npos);
  return 1;
}

int oi_open_time_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_OPENTIME,sizeof(unsigned int),context);
  return 1;
}

int oi_blp_up_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_BLP_UP,sizeof(unsigned int),context);
  return 1;
}

int oi_blp_down_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_BLP_DOWN,sizeof(unsigned int),context);
  return 1;
}

int oi_calibration_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  if ((oi_calibration==1) && (!cal_fnc_act)) {
    cal_fnc_act=cal_fnc_state1;
    cal_timeout=1000*60;
  }
  oi_calibration=0;
  return 1;
}


/***********************************/
char ul_save_sn(uint32_t usn)
{
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  return 0;
}

/***********************************/
char ul_save_adr(uint8_t uaddr)
{
  unsigned int v=uaddr;
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&v);
  return 0;
}

void sys_err()
{
  while(1);
}


int main(void) 
{
  uint16_t blp_last;

  SET_OUT_PIN(LED_PORT,LED5_BIT); 
  SET_OUT_PIN(LED_PORT,LED4_BIT); 
  SET_OUT_PIN(LED_PORT,LED3_BIT); 
  SET_OUT_PIN(LED_PORT,LED2_BIT); 

  /***********************************/
  // timers 
  lt_100msec_init();

  /***********************************/
  //init values
  tblinder_off=0;
  tblinder_off_extra=0;
  tnew_cmd=0;
  uaddr=25;
  usn=0L;
  oi_bl_pos=0;
  oi_open_time=160;
  oi_bl_stop=0;
  oi_blp_enabled=1;
  oi_blp_up=0;
  oi_blp_down=0;
  blp_last=0xffff; //impossible value

  //********************
  // kvpb init
  kvpb_block->base=(uint8_t CODE*)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DEFAULT_FLAGS;
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=kvpb_erase;
  kvpb_block->copy=kvpb_copy;
  kvpb_block->flush=kvpb_flush;
  if (kvpb_check(kvpb_block,1)<0) sys_err();

  // read data from kvpb
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,sizeof(unsigned int),&oi_bl_pos);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLINDER_OPENTIME,sizeof(unsigned int),&oi_open_time);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLP_UP,sizeof(unsigned int),&oi_blp_up);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLP_DOWN,sizeof(unsigned int),&oi_blp_down);

  //********************
  // uLan init
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) sys_err();    
  ul_fd1=ul_open(NULL,NULL);
  if (ul_fd1==UL_FD_INVALID) sys_err();    
  ul_setidstr(ul_fd,ul_idstr);
  ul_setmyadr(ul_fd,uaddr);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(ul_fd,&msginfo);
  ul_stroke(ul_fd);

  /***********************************/
  // uLan object interface init
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);

  /***********************************/
  // uLan dyac init
  uldy_init(ul_dyac,ul_fd,ul_save_sn,ul_save_adr,(char*)ul_idstr,usn);
  while (1) {
    
    /* processing of ulan messages */
    if ((ul_inepoll(ul_fd)>0) && (ul_acceptmsg(ul_fd, &msginfo)>=0)) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo (uloi_objdes_array_t*)&uloi_objdes_main, &msginfo)<0) {
          if (uldy_process_msg(ULDY_ARG_ul_dyac &msginfo)<0) {
            ul_freemsg(ul_fd);
          }
        }
      } else 
        ul_freemsg(ul_fd);
    }

    /* calibration */
    if (cal_fnc_act && !cal_wait) {
      while (cal_fnc_act()<0);
    }
    
    /* 100ms timer */
    if (lt_100msec_expired(100)) {

      /* ulan stroke */
      ul_stroke(ul_fd);

      /* watchdog */
     #ifdef WATCHDOG_ENABLED
      watchdog_feed();
     #endif /* WATCHDOG_ENABLED */

      /* calibration */
      if (cal_timeout) {
        cal_timeout-=100;
        if (cal_timeout<=0) {
          cal_fnc_timeout();
          cal_timeout=0;
        }
      }

      if (cal_wait) {
        cal_wait-=100;
        if (cal_timeout<=0)
          cal_wait=0;
      }

      if (cal_fnc_act)
        cal_time+=100;

      /* timers */
      if (tblinder_off!=0) {
        if (tblinder_off>0) tblinder_off--;
        else tblinder_off++;
        if (tblinder_off==0) {
          oi_bl_pos=new_bl_pos;
          kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,sizeof(unsigned int),&oi_bl_pos);
          if (tblinder_off_extra==0) {
              CLR_OUT_PIN(OUT_PORT,RE1_BIT); //stop
              CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
              CLR_OUT_PIN(LED_PORT,LED_GP); 
          }
        }
      } else {
        if (tblinder_off_extra) {
          if (tblinder_off_extra>0) tblinder_off_extra--;
          else tblinder_off_extra++;
          if (tblinder_off_extra==0) {
              CLR_OUT_PIN(OUT_PORT,RE1_BIT); //stop
              CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
              CLR_OUT_PIN(LED_PORT,LED_GP); 
          }
        }
      }

      /* stop command */
      if (oi_bl_stop) {
        oi_bl_stop=0;
        if (tblinder_off) {   //set new actual possition when blinder was running
          oi_bl_pos+=(100*(tblinder_off_start-tblinder_off))/oi_open_time;
          tblinder_off=0;
          kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,sizeof(unsigned int),&oi_bl_pos);
        }
        tblinder_off_extra=0;
          CLR_OUT_PIN(OUT_PORT,RE1_BIT); //stop
          CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
          CLR_OUT_PIN(LED_PORT,LED_GP); 
      }

      /* new possition */
      if (tnew_cmd) {
        tnew_cmd--;
        if (!tnew_cmd) {
          tblinder_off=((int16_t)(new_bl_pos-oi_bl_pos)*(int16_t)oi_open_time)/100;
          tblinder_off_start=tblinder_off;
          if (new_bl_pos==100)
            tblinder_off_extra=30;
          if (new_bl_pos==0)
            tblinder_off_extra=-30;
          /* set outputs */
          if ((tblinder_off>0) || (tblinder_off_extra>0)) {
            SET_OUT_PIN(OUT_PORT,RE1_BIT); //close
            CLR_OUT_PIN(OUT_PORT,RE2_BIT); 
          }
          if ((tblinder_off<0) || (tblinder_off_extra<0)) {
            CLR_OUT_PIN(OUT_PORT,RE1_BIT); //open
            SET_OUT_PIN(OUT_PORT,RE2_BIT); 
          }
          if ((tblinder_off) || (tblinder_off_extra)) {
            SET_OUT_PIN(LED_PORT,LED_GP); 
          }
        }
      }

      /* blps */
      if ((!tblinder_off_extra) && (!tblinder_off) && 
          (!tnew_cmd) && (oi_lighting) && (oi_blp_enabled)) {
        uint8_t blp;
        uint8_t l;

        if (oi_blp_up) {
          l=oi_blp_up>>8;
          blp=oi_blp_up;
          if (oi_lighting>l) {
            if (blp!=blp_last) { 
              blp_last=blp;
              bl_set_pos(blp);
            }
          }
        }

        if (oi_blp_down) {
          l=oi_blp_down>>8;
          blp=oi_blp_down;
          if (oi_lighting<l) {
            if (blp!=blp_last) { 
              blp_last=blp;
              bl_set_pos(blp);
            }
          }
        }
      }
    }
  }
  return 0;
}
