/*
    OBJECT INTERFACE FOR BLINDER
*/

#include <stdint.h>
#include <uloi_base.h>
#include "blinder.h"

#define I_LIGHTING 230
#define I_BLINDER_POSSITION 240
#define I_BLINDER_STOP 241
#define I_OPEN_TIME 242
#define I_BLP_ENABLED 245       /* BLINDER LIGHTING POSSITION */
#define I_BLP_UP 246		
#define I_BLP_DOWN 247
#define I_CALIBRATION 250

#define I_AC_IN 251

uint16_t status_val;

int uloi_int_rdfnc1(int *context);

int status_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &status_val);
}

int errclr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  status_val=0;
  return 1;
}

/* description of input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOII =
ULOI_GENOBJDES_RAW(ULOI_DOII,NULL,NULL_CODE,NULL,uloi_doii_fnc,(void*)&uloi_objdes_main)
/* description of output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOIO =
ULOI_GENOBJDES_RAW(ULOI_DOIO,NULL,NULL_CODE,NULL,uloi_doio_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOII =
ULOI_GENOBJDES_RAW(ULOI_QOII,NULL,NULL_CODE,NULL,uloi_qoii_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOIO =
ULOI_GENOBJDES_RAW(ULOI_QOIO,NULL,NULL_CODE,NULL,uloi_qoio_fnc,(void*)&uloi_objdes_main)
/* object values read request */
const ULOI_CODE uloi_objdes_t uloid_objdes_RDRQ =
ULOI_GENOBJDES_RAW(ULOI_RDRQ,NULL,NULL_CODE,NULL,uloi_rdrq_fnc,(void*)&uloi_objdes_main)

ULOI_GENOBJDES(STATUS,ULOI_STATUS,"u2",status_rdfnc,&status_val,NULL_CODE,NULL)
ULOI_GENOBJDES(ERRCLR,ULOI_ERRCLR,"e",NULL_CODE,NULL,errclr_wrfnc,(void*)&status_val)
ULOI_GENOBJDES(LIGHTING,I_LIGHTING,"u2",NULL_CODE,NULL,uloi_uint_wrfnc,&oi_lighting)
ULOI_GENOBJDES(BLINDER_POSSITION,I_BLINDER_POSSITION,"u2",uloi_uint_rdfnc,&oi_bl_pos,oi_bl_pos_wrfnc,&oi_bl_pos)
ULOI_GENOBJDES(BLINDER_STOP,I_BLINDER_STOP,"u2",NULL_CODE,NULL,uloi_uint_wrfnc,&oi_bl_stop)
ULOI_GENOBJDES(OPEN_TIME,I_OPEN_TIME,"u2",uloi_uint_rdfnc,&oi_open_time,oi_open_time_wrfnc,&oi_open_time)
ULOI_GENOBJDES(BLP_ENABLED,I_BLP_ENABLED,"u2",uloi_uint_rdfnc,&oi_blp_enabled,uloi_uint_wrfnc,&oi_blp_enabled)
ULOI_GENOBJDES(BLP_UP,I_BLP_UP,"u2",uloi_uint_rdfnc,&oi_blp_up,oi_blp_up_wrfnc,&oi_blp_up)
ULOI_GENOBJDES(BLP_DOWN,I_BLP_DOWN,"u2",uloi_uint_rdfnc,&oi_blp_down,oi_blp_down_wrfnc,&oi_blp_down)
ULOI_GENOBJDES(CALIBRATION,I_CALIBRATION,"u2",NULL,NULL,oi_calibration_wrfnc,&oi_calibration)
ULOI_GENOBJDES(AC_IN,I_AC_IN,"u2",oi_ac_in_rdfnc,&oi_ac_in,NULL_CODE,NULL)

const uloi_objdes_t * ULOI_CODE uloi_objdes_main_items[]={
  &uloid_objdes_DOII,
  &uloid_objdes_DOIO,
  &uloid_objdes_QOII,
  &uloid_objdes_QOIO,
  &uloid_objdes_RDRQ,

  &uloid_objdes_STATUS,
  &uloid_objdes_ERRCLR,
  &uloid_objdes_LIGHTING,
  &uloid_objdes_BLINDER_POSSITION,
  &uloid_objdes_BLINDER_STOP,
  &uloid_objdes_OPEN_TIME,
  &uloid_objdes_BLP_ENABLED,
  &uloid_objdes_BLP_UP,
  &uloid_objdes_BLP_DOWN,
  &uloid_objdes_CALIBRATION,
  &uloid_objdes_AC_IN
};

const ULOI_CODE uloi_objdes_array_t uloi_objdes_main={
  {
    uloi_objdes_main_items,
    sizeof(uloi_objdes_main_items)/sizeof(uloi_objdes_main_items[0]),
    -1
  }
};


