#include <local_config.h>

#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */

#ifdef CONFIG_ULAN_DY
  #define UL_DYC " .dy"
#else
  #define UL_DYC
#endif

#ifdef CONFIG_ULOI_LT
  #define UL_OIC " .oi"
#else
  #define UL_OIC
#endif

#define NAME ".mt " \
	     STRINGIFY(ULAN_ID) \
	     " .uP " \
	     STRINGIFY(MACH) \
	     UL_DYC \
	     UL_OIC \
	     " .co " \
              __DATE__ " " __TIME__

const char *ul_idstr = NAME;
