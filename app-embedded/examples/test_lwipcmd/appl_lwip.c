#include <system_def.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <lwip/tcp.h>
#include <lwip/stats.h>
#include <lwip/init.h>
#if LWIP_VERSION_MAJOR >= 2
#include <lwip/timeouts.h>
#else /*LWIP_VERSION_MAJOR*/
#include <lwip/timers.h>
#endif /*LWIP_VERSION_MAJOR*/
#include <arch/lwip-lpc.h>
#include <cmd_proc.h>
#include "cmd_netcon.h"
#include "cmd_instances.h"
#include "cmdio_netcon_lwip.h"

#include "appl_defs.h"

#ifdef CONFIG_LWIP_LWIP_DHCP
#include <lwip/dhcp.h>
#endif

#ifdef CONFIG_LWIP_SVC_MDNS
#include <lwip/igmp.h>
#include <mdns_responder.h>
#endif

#include <ul_log.h>
extern UL_LOG_CUST(ulogd_netcon)

#define NETCON_SERVER_MAX_CONNECTIONS 4

#define USE_SYS_CHECK_TIMEOUTS  1

#ifndef __STRINGIFY
#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#endif /*__STRINGIFY*/
#ifndef STRINGIFY
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */
#endif /*STRINGIFY*/

#ifdef CONFIG_LWIP_SVC_MDNS
static const struct mdns_service services[] = {
    {
        .name = "\x05_telnet\x04_tcp\x05local",
        .port = 23,
    },
};

static const char *txt_records[] = {
    "product=" "testlwipcmd",
    "version=" STRINGIFY(APP_VER_MAJOR) "."
               STRINGIFY(APP_VER_MINOR) "."
               STRINGIFY(APP_VER_PATCH),
    NULL
};
#endif /* CONFIG_LWIP_SVC_MDNS */

netcon_server_t netcon_server;

inline uint32_t sys_now(void)
{
    lt_mstime_update();
    return actual_msec;
}

static struct netif lpc_netif;
static ip_addr_t ipaddr, netmask, gw;

int init_lpc_netif(void)
{
  static struct netif *netif = &lpc_netif;
  unsigned char hwaddr[6] = {0xde,0xad,0xde,0xaf,0xba,0xbe};
  IP4_ADDR(&gw, 192,168,3,1);
  IP4_ADDR(&ipaddr, 192,168,3,34);
  IP4_ADDR(&netmask, 255,255,255,0);

  if (netif_add(netif, &ipaddr, &netmask, &gw, hwaddr,
                lpcnetif_init, ethernet_input) == NULL) {
    return -1;
  }
  netif_set_default(netif);

 #ifdef CONFIG_LWIP_LWIP_NETIF_HOSTNAME
  netif_set_hostname(netif, "testlwipcmd");
 #endif

 #ifdef CONFIG_LWIP_SVC_MDNS
  {
    err_t ret;
    ret = igmp_start(netif);
    //netif->flags |= NETIF_FLAG_IGMP;
    printf("igmp_start %d\n", (int)ret);
    ret = mdns_responder_init(netif, services, sizeof(services)/sizeof(*services),
                      txt_records);
    printf("mdns_responder_init %d\n", (int)ret);
  }
 #endif

  netif_set_up(netif);

 #ifdef CONFIG_LWIP_LWIP_DHCP
  dhcp_start(netif);
 #endif

  return 0;
}

int lwip_app_init(void)
{
  int res;

  lwip_init();
  printf("LwIP initialization\n");
  res = init_lpc_netif();
  if (res) {
    printf("init_lpc_netif failed %d\n", res);
    return -1;
  }

  printf("TCP TCP_WND %d\n", TCP_WND);

  return cmd_io_lwip_server_init(&netcon_server, 23,
             NETCON_SERVER_MAX_CONNECTIONS, NULL);
}

int lwip_app_poll(void)
{
 #ifndef USE_SYS_CHECK_TIMEOUTS
  static uint32_t last_arp_time = 0;
 #endif
  static uint32_t last_tcp_time = 0;
  netcon_server_t *ncserver = &netcon_server;


  {
    static int linkfail=-10;
    int lrc = lpcnetif_checklink(&lpc_netif);
    if (linkfail!=lrc) {
      printf(" ~~~ ETH new state = %d\n", lrc);
      if (lrc==LPCNETIF_LINK_OK) { /* newly connected and initialized ... try to get IP from DHCP */
        netif_set_link_up(&lpc_netif);
      } else if (lrc==LPCNETIF_LINK_DOWN) {
        netif_set_link_down(&lpc_netif);
      }
    }
    linkfail = lrc;
  }


  lpcnetif_input(&lpc_netif);

  cmd_io_lwip_poll(ncserver);

 #ifdef USE_SYS_CHECK_TIMEOUTS
  if (sys_now() != last_tcp_time) {
    sys_check_timeouts();
    last_tcp_time = sys_now();
  }
 #else /*USE_SYS_CHECK_TIMEOUTS*/
  if ((sys_now() - last_arp_time) >= ARP_TMR_INTERVAL) {
    etharp_tmr();
    last_arp_time = sys_now();
  }
  if((sys_now() - last_tcp_time) >= TCP_TMR_INTERVAL) {
    tcp_tmr();
    last_tcp_time = sys_now();
  }
 #endif /*USE_SYS_CHECK_TIMEOUTS*/

  return 0;
}

int cmd_io_putc_lwip_send_push(struct cmd_io *cmd_io, netcon_con_data_t *ncdata)
{
  lpcnetif_input(&lpc_netif);
 #ifdef USE_SYS_CHECK_TIMEOUTS
  sys_check_timeouts();
 #else /*USE_SYS_CHECK_TIMEOUTS*/
  tcp_tmr();
 #endif /*USE_SYS_CHECK_TIMEOUTS*/

  return 0;
}

int cmd_do_lwip_stats(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  stats_display();
  return 0;
}

int cmd_do_lwip_netipaddr(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res;
  int opchar;
  char *p = param[3];
  char str[20];
  static struct netif *netif = &lpc_netif;

  if ((opchar = cmd_opchar_check(cmd_io, des, param)) < 0)
    return opchar;

  if (opchar != '?')
    return -CMDERR_OPCHAR;

  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  ip4addr_ntoa_r(&netif->ip_addr, str, sizeof(str));
  cmd_io_write(cmd_io, str, strlen(str));

  return 0;
}

cmd_des_t const cmd_des_lwip_stats={0, 0,"lwipstats","print LwIP statistics",
			cmd_do_lwip_stats,
			{0,0}};

cmd_des_t const cmd_des_lwip_netipaddr = {0, CDESM_OPCHR | CDESM_RD,
                        "IFCONFIGIP","read IP address",
			cmd_do_lwip_netipaddr,
			{0,0}};

cmd_des_t const *const cmd_appl_lwip[]={
  &cmd_des_lwip_stats,
  &cmd_des_lwip_netipaddr,
  NULL
};
