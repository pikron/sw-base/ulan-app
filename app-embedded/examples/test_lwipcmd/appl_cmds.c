/*******************************************************************
  Motion and Robotic System (MARS) aplication components

  appl_cmds.c - application specific commands

  Copyright (C) 2001-2015 by Pavel Pisa - originator
                          pisa@cmp.felk.cvut.cz
            (C) 2001-2015 by PiKRON Ltd. - originator
                    http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#include <cpu_def.h>
#include <system_def.h>
#include <stdlib.h>
#include <string.h>
#include <cmd_proc.h>
#include <utils.h>

#include "appl_defs.h"
#include "appl_version.h"

#ifndef __STRINGIFY
#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#endif
#ifndef STRINGIFY
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */
#endif

int cmd_do_ioecho(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int val;
  int opchar;

  if((opchar=cmd_opchar_check(cmd_io, des, param))<0) return opchar;
  if(opchar == ':') {
    val = *param[3];
    if (val == '0')
      cmd_io->priv.ed_line.in->flg &= ~FL_ELB_ECHO;
    else if(val == '1')
      cmd_io->priv.ed_line.in->flg |= FL_ELB_ECHO;
    else return -CMDERR_BADPAR;
  }else{
    cmd_io_write(cmd_io, param[0], param[2]-param[0]);
    cmd_io_putc(cmd_io,'=');
    cmd_io_putc(cmd_io,cmd_io->priv.ed_line.in->flg & FL_ELB_ECHO?'1':'0');
  }
  return 0;
}

const char software_ver[]=APP_VER_ID " v"STRINGIFY(SW_VER_MAJOR)"."STRINGIFY(SW_VER_MINOR)
           " build Pi "__DATE__" "__TIME__;

int cmd_do_ver(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  if(*param[2]!='?') return -CMDERR_OPCHAR;
  cmd_io_write(cmd_io,param[0],param[2]-param[0]);
  cmd_io_putc(cmd_io,'=');
  cmd_io_write(cmd_io,software_ver,strlen(software_ver));
  return 0;
}

cmd_des_t const cmd_des_stamp={0, CDESM_OPCHR,
			"STAMP","host communication stamp",
			cmd_do_stamp,{}};
cmd_des_t const cmd_des_echo={0, CDESM_OPCHR|CDESM_RW,
			"ECHO","enable echoing of received character",cmd_do_ioecho,{}};
cmd_des_t const cmd_des_ver={0, CDESM_OPCHR|CDESM_RD,
			"VER","software version",cmd_do_ver,{}};

cmd_des_t const *const cmd_appl_specific[]={
  &cmd_des_stamp,
  &cmd_des_echo,
  &cmd_des_ver,
  NULL
};
