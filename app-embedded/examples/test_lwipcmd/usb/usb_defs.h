
#ifndef USB_DEFS_MODULE
#define USB_DEFS_MODULE

#include <usb/usb_spec.h>
#include <usb/usb_cdc.h>
#include <usb/lpcusb.h>
#include <endian.h>
#include <cpu_def.h>

#include <endian.h>
#if __BYTE_ORDER == __BIG_ENDIAN
#include <byteswap.h>
#define SWAP(x) ((((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
#else /*__LITTLE_ENDIAN*/
#define SWAP(x) (x)
#endif

#ifndef CODE
#define CODE
#endif

/*****************************************************/
/*** Static data structures for device descriptors ***/
/*****************************************************/
#define USB_VENDOR_ID      0x1669  /* PIKRON company bought vendor ID */
#define USB_PRODUCT_ID     0x1022  /* Education examples */
#define USB_RELEASE_VER    0x0100

/*** Class codes for device description ***/
#define USB_CLASS_CODE      USB_DEVICE_CLASS_COMMUNICATIONS
#define USB_SUBCLASS_CODE   0x00
#define USB_PROTOCOL_CODE   0x00

#define NUM_ENDPOINTS       3
#define CDC0_EP_NOTIFY      0x81
#define CDC0_EP_RXD         0x02
#define CDC0_EP_TXD         0x82
#define CDC0_MAX_PACKET     64

#define CONFIG_DESCRIPTOR_LENGTH \
  sizeof(USB_CONFIGURATION_DESCRIPTOR) \
   + sizeof(USB_INTERFACE_DESCRIPTOR) \
   + sizeof(USBCDC_HEADER_FCN_DESCRIPTOR) \
   + sizeof(USBCDC_CALLMGMT_FCN_DESCRIPTOR) \
   + sizeof(USBCDC_ACM_FCN_DESCRIPTOR) \
   + sizeof(USBCDC_UNION_FCN_DESCRIPTOR) \
   + sizeof(USB_INTERFACE_DESCRIPTOR) \
   + (NUM_ENDPOINTS*sizeof(USB_ENDPOINT_DESCRIPTOR))

/*** Device descriptor ***/
CODE const USB_DEVICE_DESCRIPTOR DeviceDescription =
{
  sizeof(USB_DEVICE_DESCRIPTOR),
  USB_DESCRIPTOR_TYPE_DEVICE,
  SWAP(0x0120),
  USB_CLASS_CODE,
  USB_SUBCLASS_CODE,
  USB_PROTOCOL_CODE,
  USB_MAX_PACKET0,
  SWAP(USB_VENDOR_ID),
  SWAP(USB_PRODUCT_ID),
  SWAP(USB_RELEASE_VER),
  1, /* manufacturer string ID */
  2, /* product string ID */
  3, /* serial number string ID */
  1  /* bNumConfigs */
};

/*** All In Configuration 0 ***/
CODE const struct
{
  USB_CONFIGURATION_DESCRIPTOR configuration;
  USB_INTERFACE_DESCRIPTOR        iface_comm;
  USBCDC_HEADER_FCN_DESCRIPTOR    cdcheader;
  USBCDC_CALLMGMT_FCN_DESCRIPTOR  cdccallmgt;
  USBCDC_ACM_FCN_DESCRIPTOR       cdcacm;
  USBCDC_UNION_FCN_DESCRIPTOR     cdcunion;
  USB_ENDPOINT_DESCRIPTOR         ep_notification;
  USB_INTERFACE_DESCRIPTOR        iface_data;
  USB_ENDPOINT_DESCRIPTOR         ep_rxd;
  USB_ENDPOINT_DESCRIPTOR         ep_txd;
} PACKED ConfigDescription =
{
  {
    /*** Configuration descriptor ***/
    sizeof(USB_CONFIGURATION_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_CONFIGURATION,
    SWAP(CONFIG_DESCRIPTOR_LENGTH),
    2, /* cnt of interfaces */
    1, /* this configuration ID */
    4, /* config.name string ID */
    USB_CONFIG_BUS_POWERED, /* CbmAttributes (bus powered) */
    250    /* device power current from host 500mA / 2 */
  },
  {
    /*** Interface Descriptor ***/
    sizeof(USB_INTERFACE_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_INTERFACE,
    0,    /* index of this interface for SetInterface request */
    0,    /* ID alternate interface */
    1,    /* number of endpoints in interface */
    USB_DEVICE_CLASS_COMMUNICATIONS,
    USBCDC_COM_IFACE_SUBCLS_ACM,
    0,    /* protocol */
    5
  },
  {
    /*** CDC Header Descriptor ***/
    sizeof(USBCDC_HEADER_FCN_DESCRIPTOR),
    USBCDC_COM_FCN_TYPE_CS_INTERFACE,
    USBCDC_COM_FCN_SUBTYPE_HEADER,    /* bDescriptorSubtype */
    SWAP(0x0120),                     /* bcdCDC (spec. release 1.2) */
  },
  {
    /*** CDC CALL MANAGEMENT Descriptor ***/
    sizeof(USBCDC_CALLMGMT_FCN_DESCRIPTOR),
    USBCDC_COM_FCN_TYPE_CS_INTERFACE,
    USBCDC_COM_FCN_SUBTYPE_CALLMGMT,
    0,
    1,
  },
  {
    /*** CDC ABSTRACT CONTROL MANAGEMENT Descriptor ***/
    sizeof(USBCDC_ACM_FCN_DESCRIPTOR),
    USBCDC_COM_FCN_TYPE_CS_INTERFACE,
    USBCDC_COM_FCN_SUBTYPE_ACMGMT,
    USBCDC_ACM_FCN_CAP_SUPPORT_LINECTRL,
  },
  {
    /*** CDC UNION Descriptor ***/
    sizeof(USBCDC_UNION_FCN_DESCRIPTOR),
    USBCDC_COM_FCN_TYPE_CS_INTERFACE,
    USBCDC_COM_FCN_SUBTYPE_UNION,
    0,
    1,
  },
  {
    /*** Endpoint 1 IN, type interrupt ***/
    sizeof(USB_ENDPOINT_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_ENDPOINT,
    CDC0_EP_NOTIFY,                   /* bEndpointAddress */
    USB_ENDPOINT_TYPE_INTERRUPT,
    SWAP(USB_MAX_PACKET),
    0x80,                             /* bInterval (polling interval: 50ms) */
  },
  {
    /*** Interface Descriptor ***/
    sizeof(USB_INTERFACE_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_INTERFACE,
    1,                                /* bInterfaceNumber */
    0,                                /* bAlternateSetting */
    2,                                /* number of EPs */
    USB_DEVICE_CLASS_CDC_DATA,        /* bInterfaceClass */
    0,                                /* bInterfaceSubclass */
    0,                                /* bDeviceProtocol */
    0,                                /* iInterface (string, 0=none) */
  },
  {
    /*** Endpoint 2 OUT, type bulk ***/
    sizeof(USB_ENDPOINT_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_ENDPOINT,
    CDC0_EP_RXD,                      /* bEndpointAddress */
    USB_ENDPOINT_TYPE_BULK,
    SWAP(CDC0_MAX_PACKET),
    0,                                /* bInterval (polling interval: 50ms) */
  },
  {
    /*** Endpoint 2 IN, type bulk ***/
    sizeof(USB_ENDPOINT_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_ENDPOINT,
    CDC0_EP_TXD,                      /* bEndpointAddress */
    USB_ENDPOINT_TYPE_BULK,
    SWAP(CDC0_MAX_PACKET),
    0,                                /* bInterval (polling interval: 50ms) */
  }
};

/*** Strings - in unicode ***/
CODE const char Str0Desc[] =    /* supported languages of strings */
{
  4, 0x03,  /* 2+2*N , N is count of supported languages */
  0x09, 0x04 /* english 0x0409 */
};

CODE const char Str1Desc[] =    /* 1 = manufacturer */
{
  24, 0x03,
  'P', 0,
  'i', 0,
  'K', 0,
  'R', 0,
  'O', 0,
  'N', 0,
  ' ', 0,
  'L', 0,
  't', 0,
  'd', 0,
  '.', 0,
};

CODE const char Str2Desc[] =    /* 2 = product */
{
  16, 0x03,
  'L', 0,
  'W', 0,
  'I', 0,
  'P', 0,
  'C', 0,
  'M', 0,
  'D', 0,
};

#define usb_devdes_serial_number Str3Desc
/* Serial number is stored in data area to be set at device start-up */
char Str3Desc[(1 + 8) * 2] = /* 3 = serial number */
{
  18, 0x03,
  'X', 0,
  'X', 0,
  'X', 0,
  'X', 0,
  'X', 0,
  'X', 0,
  'X', 0,
  'X', 0,
};

CODE const char Str4Desc[] =    /* 4 = configuration */
{
  34, 0x03,
  'C', 0,
  'o', 0,
  'n', 0,
  'f', 0,
  'i', 0,
  'g', 0,
  'u', 0,
  'r', 0,
  'a', 0,
  't', 0,
  'i', 0,
  'o', 0,
  'n', 0,
  ' ', 0,
  '#', 0,
  '1', 0
};
CODE const char Str5Desc[] =    /* 5 = interface */
{
  26, 0x03,
  'I', 0,
  'n', 0,
  't', 0,
  'e', 0,
  'r', 0,
  'f', 0,
  'a', 0,
  'c', 0,
  'e', 0,
  ' ', 0,
  '#', 0,
  '0', 0
};

/* all strings in pointers array */
CODE const PUSB_STRING_DESCRIPTOR StringDescriptors[] =
{
  (PUSB_STRING_DESCRIPTOR) Str0Desc,
  (PUSB_STRING_DESCRIPTOR) Str1Desc,
  (PUSB_STRING_DESCRIPTOR) Str2Desc,
  (PUSB_STRING_DESCRIPTOR) Str3Desc,
  (PUSB_STRING_DESCRIPTOR) Str4Desc,
  (PUSB_STRING_DESCRIPTOR) Str5Desc
};

#define CNT_STRINGS (sizeof(StringDescriptors)/sizeof(StringDescriptors[0]))

CODE const USB_DEVICE_CONFIGURATION_ENTRY usb_devdes_configurations[] =
{
  {
    .pConfigDescription = &ConfigDescription.configuration,
    .iConfigTotalLength = sizeof(ConfigDescription)
  }
};

CODE const USB_INTERFACE_DESCRIPTOR *usb_devdes_interfaces[] =
{
  &ConfigDescription.iface_comm,
  &ConfigDescription.iface_data
};

CODE const USB_DEVICE_DESCRIPTORS_TABLE usb_devdes_table =
{
  .pDeviceDescription = &DeviceDescription,
  .pConfigurations = usb_devdes_configurations,
  .pInterfaceDescriptors = usb_devdes_interfaces,
  .pStrings = StringDescriptors,
  .iNumStrings = CNT_STRINGS,
  .bNumEndpoints = NUM_ENDPOINTS,
  .bNumConfigurations = 1,
  .bNumInterfaces = 2
};

#endif /* USB_DEFS_MODULE */
