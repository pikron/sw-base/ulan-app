#include "appl_config.h"
#include "appl_version.h"

#ifndef __STRINGIFY
#define __STRINGIFY(x)     #x              /* stringify without expanding x */
#endif /*__STRINGIFY*/
#ifndef STRINGIFY
#define STRINGIFY(x)    __STRINGIFY(x)        /* expand x, then stringify */
#endif /*STRINGIFY*/

#ifdef CONFIG_ULAN_DY
  #define UL_DYC " .dy"
#else
  #define UL_DYC
#endif

#ifdef CONFIG_ULOI_LT
  #define UL_OIC " .oi"
#else
  #define UL_OIC
#endif

#ifdef APP_VER_MAJOR
#ifndef APP_VER_MINOR
#define APP_VER_MINOR 0
#endif
#define APP_UL_VER_STR " v" STRINGIFY(APP_VER_MAJOR) "." STRINGIFY(APP_VER_MINOR)
#else /*APP_VER_MAJOR*/
#define APP_UL_VER_STR
#endif /*APP_VER_MAJOR*/


#define NAME ".mt " \
	     STRINGIFY(ULAN_ID) \
             APP_UL_VER_STR \
	     " .uP " \
	     STRINGIFY(MACH) \
	     UL_DYC \
	     UL_OIC \
	     " .co " \
              __DATE__ " " __TIME__

const char *ul_idstr = NAME;
