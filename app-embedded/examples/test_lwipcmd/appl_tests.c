#include <system_def.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <inttypes.h>
#include <stddef.h>
#include <utils.h>
#include <cmd_proc.h>
#include <hal_gpio.h>
#include <hal_machperiph.h>
#include <spi_drv.h>
#include <lt_timer.h>

#include <ul_log.h>
#include <ul_logreg.h>

#include "appl_defs.h"

#ifdef APPL_WITH_FPGA
#include "appl_fpga.h"
#endif /*APPL_WITH_FPGA*/

FILE *cmd_io_as_direct_file(cmd_io_t *cmd_io, const char *mode)
{
  if (cmd_io->priv.ed_line.io_stack)
    cmd_io = cmd_io->priv.ed_line.io_stack;

  return cmd_io_as_file(cmd_io, mode);
}

int cmd_do_test_memusage(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  void *maxaddr;
  char str[40];

  maxaddr = sbrk(0);

  snprintf(str,sizeof(str),"memusage maxaddr 0x%08lx\n",(unsigned long)maxaddr);
  cmd_io_write(cmd_io,str,strlen(str));

  return 0;
}

#ifdef APPL_WITH_DISTORE_EEPROM_USER
int cmd_do_test_distore(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_set_check4change();
  return 0;
}

int cmd_do_test_diload(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  appl_distore_user_restore();
  return 0;
}
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

int cmd_do_test_loglevel_cb(ul_log_domain_t *domain, void *context)
{
  char s[30];
  cmd_io_t *cmd_io = (cmd_io_t *)context;

  s[sizeof(s)-1]=0;
  snprintf(s,sizeof(s)-1,"%s (%d)\r\n",domain->name, domain->level);
  cmd_io_puts(cmd_io, s);
  return 0;
}

int cmd_do_test_loglevel(cmd_io_t *cmd_io, const struct cmd_des *des, char *param[])
{
  int res=0;
  char *line;
  line = param[1];

  if(!line||(si_skspace(&line),!*line)) {
    ul_logreg_for_each_domain(cmd_do_test_loglevel_cb, cmd_io);
  } else {
    res=ul_log_domain_arg2levels(line);
  }

  return res>=0?0:CMDERR_BADPAR;
}


cmd_des_t const cmd_des_test_memusage={0, 0,
                        "memusage","report memory usage",cmd_do_test_memusage,
                        {0,
                         0}};

#ifdef APPL_WITH_DISTORE_EEPROM_USER
cmd_des_t const cmd_des_test_distore={0, 0,
                        "testdistore","test DINFO store",cmd_do_test_distore,
                        {0,
                         0}};

cmd_des_t const cmd_des_test_diload={0, 0,
                        "testdiload","test DINFO load",cmd_do_test_diload,
                        {0,
                         0}};
#endif /*APPL_WITH_DISTORE_EEPROM_USER*/

cmd_des_t const cmd_des_test_loglevel={0, 0,
                        "loglevel","select logging level",
                        cmd_do_test_loglevel,{}};

cmd_des_t const *const cmd_appl_tests[]={
  &cmd_des_test_memusage,
 #ifdef APPL_WITH_DISTORE_EEPROM_USER
  &cmd_des_test_distore,
  &cmd_des_test_diload,
 #endif /*APPL_WITH_DISTORE_EEPROM_USER*/
  &cmd_des_test_loglevel,
  NULL
};

