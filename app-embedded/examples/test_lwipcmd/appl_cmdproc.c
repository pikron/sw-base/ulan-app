#include <system_def.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <cmd_proc.h>
#include <ul_list.h>

#include "appl_defs.h"
#include "cmd_instances.h"

typedef struct cmdproc_instance_state_t {
  ul_list_node_t cmdproc_peers;
  cmd_io_t* cmd_io;
} cmdproc_instance_state_t;

typedef struct cmdproc_instances_t {
  ul_list_head_t instances;
  cmd_des_t const **commands;
} cmdproc_instances_t;

UL_LIST_CUST_DEC(cmdproc_instance, cmdproc_instances_t, cmdproc_instance_state_t,
                 instances, cmdproc_peers)

cmdproc_instances_t cmdproc_instances;

#ifndef APPL_WITH_SIM_POSIX
extern cmd_io_t cmd_io_usbcon;
extern cmd_io_t cmd_io_uartcon;

cmdproc_instance_state_t cmdproc_instance_uartcon = {
  .cmd_io = &cmd_io_uartcon,
};

cmdproc_instance_state_t cmdproc_instance_usbcon = {
  .cmd_io = &cmd_io_usbcon,
};

#else /*APPL_WITH_SIM_POSIX*/
extern cmd_io_t cmd_io_std_line;

cmdproc_instance_state_t cmdproc_instance_std_line = {
  .cmd_io = &cmd_io_std_line,
};

#endif /*APPL_WITH_SIM_POSIX*/

#ifdef CONFIG_PXMC
extern cmd_des_t const *cmd_pxmc_base[];
extern cmd_des_t const *cmd_pxmc_ptable[];
extern cmd_des_t const *cmd_pxmc_coordmv[];
extern cmd_des_t const *cmd_appl_pxmc[];
#endif
extern cmd_des_t const *cmd_appl_tests[];
extern cmd_des_t const *cmd_appl_specific[];
extern cmd_des_t const *cmd_appl_lwip[];

extern cmd_des_t const cmd_des_dprint;

cmd_des_t const **cmd_list;

cmd_des_t const cmd_des_help =
{
  0, 0,
  "help", "prints help for commands",
  cmd_do_help,
  {
    (char *) &cmd_list
  }
};

cmdproc_instance_state_t *cmdproc_instance_state4cmd_io(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;

  ul_list_for_each(cmdproc_instance, &cmdproc_instances, cmdinst) {
    if (cmdinst->cmd_io == cmd_io)
      return cmdinst;
  }
  return NULL;
}

int cmdproc_instances_insert_cmdio(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;

  cmdinst = (typeof(cmdinst))malloc(sizeof(*cmdinst));
  if (cmdinst == NULL)
    return -1;
  memset(cmdinst, 0, sizeof(*cmdinst));
  cmdinst->cmd_io = cmd_io;
  cmdproc_instance_insert(&cmdproc_instances, cmdinst);

  return 0;
}

int cmdproc_instances_remove_cmdio(cmd_io_t *cmd_io)
{
  cmdproc_instance_state_t *cmdinst;

  cmdinst = cmdproc_instance_state4cmd_io(cmd_io);
  if (cmdinst == NULL)
    return -1;

  cmdproc_instance_delete(&cmdproc_instances, cmdinst);
  memset(cmdinst, 0, sizeof(*cmdinst));
  free (cmdinst);
  return 0;
}

cmd_des_t const *cmd_list_main[] =
{
  &cmd_des_help,
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_tests),
#ifdef CONFIG_PXMC
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_base),
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_ptable),
  CMD_DES_INCLUDE_SUBLIST(cmd_pxmc_coordmv),
#endif
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_specific),
#ifdef APPL_WITH_LWIP
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_lwip),
#endif /*APPL_WITH_LWIP*/
#ifndef APPL_WITH_SIM_POSIX
#ifdef CONFIG_PXMC
  CMD_DES_INCLUDE_SUBLIST(cmd_appl_pxmc),
#endif
#endif /*APPL_WITH_SIM_POSIX*/
  NULL
};

cmdproc_instances_t cmdproc_instances = {
  .commands = cmd_list_main,
};

cmd_des_t const **cmd_list = cmd_list_main;


int cmdproc_init(void)
{
  cmdproc_instance_init_head(&cmdproc_instances);
 #ifndef APPL_WITH_SIM_POSIX
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_uartcon);
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_usbcon);
 #else /*APPL_WITH_SIM_POSIX*/
  cmdproc_instance_insert(&cmdproc_instances, &cmdproc_instance_std_line);
 #endif /*APPL_WITH_SIM_POSIX*/
  return 0;
}

int cmdproc_poll(void)
{
  cmdproc_instance_state_t * cmdinst;
  static typeof(actual_msec) old_check_time;
  typeof(actual_msec) new_check_time;
  int ret = 0;

  lt_mstime_update();

  new_check_time = actual_msec;
  if (new_check_time != old_check_time) {
    old_check_time = new_check_time;
  }

  ul_list_for_each(cmdproc_instance, &cmdproc_instances, cmdinst) {
    ret |= cmd_processor_run(cmdinst->cmd_io, cmdproc_instances.commands);
  }

  return ret;
}
