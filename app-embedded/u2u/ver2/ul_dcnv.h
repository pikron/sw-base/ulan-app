#ifndef _UL_DCNV_H
#define _UL_DCNV_H

#include "ul_lib/ulan.h"

#ifdef __cplusplus
/*extern "C" {*/
#endif

#ifndef XDATA
  #define XDATA
#endif

#ifndef UL_WITHOUT_HANDLE 

#define PS2UL_INPROC_MAX 16

typedef struct ps2ul_stamp {
  int ul;
  unsigned char ps;
} ps2ul_stamp_t;

typedef struct ps2ul_stamp_queue {
  ps2ul_stamp_t *head;
  ps2ul_stamp_t *tail;
  ps2ul_stamp_t stamps[PS2UL_INPROC_MAX];
} ps2ul_stamp_queue_t;

static inline void ps2ul_stamp_idx_inc(ps2ul_stamp_queue_t *queue, ps2ul_stamp_t **ptr)
{
  ps2ul_stamp_t *p=(*ptr);
  p++;
  if(p>=queue->stamps+PS2UL_INPROC_MAX)
    p=queue->stamps;
  *ptr=p;
}

#endif /*UL_WITHOUT_HANDLE*/

typedef struct ul_dcnv_state_t {
  int tx_2send;
  int tx_error;
  int rx_2rec;
  int rx_error;
  int rx_wait4host;
#ifndef UL_WITHOUT_HANDLE
  char *idstr;
  char *dev_name;
  int subdevidx;
  ul_fd_t tx_fd;
  ul_fd_t rx_fd;
  int tx_tail;
  int rx_tail;
  int rx_tail_stamp;
  ps2ul_stamp_queue_t stamp_queue;
  unsigned char tx_psstamp;
#endif /*UL_WITHOUT_HANDLE*/
#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
  uchar ext_buff[11]; /* ExtHead ESADR[4] EDADR[4] hops cmd */
  uchar ext_pos;
  uchar ext_len;
#endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
} ul_dcnv_state_t;

int ul_dcnv_init_by_name(ul_dcnv_state_t XDATA *cnvst, char *ul_dev_name, int subdevidx);
int ul_dcnv_init_state(ul_dcnv_state_t XDATA *cnvst,ul_fd_t tx_fd,ul_fd_t rx_fd);
int ul_dcnv_open(ul_dcnv_state_t XDATA *cnvst,int my_adr, int baudrate, const char *idstr);
int ul_dcnv_close(ul_dcnv_state_t XDATA *cnvst);
int ul_dcnv_send(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size);
int ul_dcnv_rec(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size);
int ul_dcnv_rec_start(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size);

static inline int ul_dcnv_is_open(ul_dcnv_state_t XDATA *cnvst)
{
  return (cnvst->tx_fd!=UL_FD_INVALID)?1:0;
}

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _UL_UTIL_H */
