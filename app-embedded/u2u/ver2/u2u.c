#include <stdio.h>
#include <string.h>
#include <cpu_def.h> 
#include <system_def.h>
#include <hal_machperiph.h>
#include <lt_timer.h>
/* ulan */
#include <ul_lib/ulan.h>
/* USB support */
#include <usb/usbdebug.h>
#include <usb/lpc.h>
#include "ul_dcnv.h"
#include "u2u_vend.h"
#include <usb/usb_devdes.h>
#include "usb/usb_defs.h"
#include <local_config.h>
#ifdef CONFIG_APP_U2U_V2_WITH_KEYVAL
#include <keyvalpb.h>
#include <keyval_loc.h>
#include <lpciap_kvpb.h>
#endif /* CONFIG_APP_U2U_V2_WITH_KEYVAL */

#define MASK_EP1RX  0x01
#define MASK_EP1TX  0x02

LT_TIMER_DEC(lt_10msec)
LT_TIMER_IMP(lt_10msec)

usb_device_t usb_device;
usb_ep_t eps[2]; 
ul_msginfo msginfo;
ul_dcnv_state_t ep1_dcnv_state;
unsigned char ep1_rx_buff[USB_MAX_PACKET];
unsigned char ep1_tx_buff[USB_MAX_PACKET];
uint8_t timer_str,timer_rx_off,timer_tx_off,timer_configured; 
#ifdef CONFIG_APP_U2U_V2_WITH_KEYVAL
kvpb_block_t kvpb_block_global;
kvpb_block_t *kvpb_block=&kvpb_block_global;
#endif /* CONFIG_APP_U2U_V2_WITH_KEYVAL */

void timer_10ms(void) 
{
  if (timer_tx_off!=0) timer_tx_off--;
  else SET_OUT_PIN(LED_PORT,LED1_BIT);
  if (timer_rx_off!=0) timer_rx_off--;
  else SET_OUT_PIN(LED_PORT,LED2_BIT);
  if (!timer_str) {
    timer_str=5;
    if (ul_dcnv_is_open(&ep1_dcnv_state))
      ul_stroke(ep1_dcnv_state.rx_fd);
  } else timer_str--;
  if (timer_configured!=0) timer_configured--;
  else {
    timer_configured=20;
    if (!ul_dcnv_is_open(&ep1_dcnv_state)) {
      CLR_OUT_PIN(LED_PORT,LED1_BIT);
      CLR_OUT_PIN(LED_PORT,LED2_BIT);
      timer_rx_off=timer_tx_off=5;
    }
  }
}

int usb_u2u_vendor(usb_device_t *udev)
{
  return usb_u2u_vendor4dcnv(&ep1_dcnv_state, udev);
}

int main() {  
  ul_fd_t ul_fd;

  /***********************************/
  // timers 
  lt_10msec_init();

  //********************
  //uLan init
  ul_fd=ul_open(NULL, NULL);
  ul_setmyadr(ul_fd,0);
  /*
  if(ul_fd!=UL_FD_INVALID) {
    ul_msginfo msginfo
    memset(&msginfo,0,sizeof(msginfo));
    ul_addfilt(ul_fd, &msginfo);
    msginfo.dadr=0;
    msginfo.cmd=55;
    ul_newmsg(ul_fd, &msginfo);
    ul_freemsg(ul_fd);
  }*/
  ul_dcnv_init_by_name(&ep1_dcnv_state,NULL,0);

  //********************
  // USB init
  memset( &usb_device, 0, sizeof( usb_device));
  usb_device.id = 1;
  usb_device.devdes_table = &usb_devdes_table;
  usb_device.init = usb_lpc_init;
  usb_debug_set_level(DEBUG_LEVEL_NONE);
  usb_device.cntep = 2;
  usb_device.ep = eps;

  eps[0].max_packet_size = USB_MAX_PACKET;
  eps[1].max_packet_size = USB_MAX_PACKET;
  eps[0].epnum = 0x01;
  eps[1].epnum = 0x81;
  eps[0].event_mask = 0x04;
  eps[1].event_mask = 0x08;
  eps[0].udev = &usb_device;
  eps[1].udev = &usb_device;

  usb_device.vendor_fnc=usb_u2u_vendor;

  usb_init(&usb_device);
  usb_connect(&usb_device);


 #ifdef CONFIG_APP_U2U_V2_WITH_KEYVAL
  /***********************************/
  // kvpb init
  kvpb_block->base=(uint8_t*)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DESC_DOUBLE|KVPB_DESC_CHUNKWO;
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=lpcisp_kvpb_erase;
  kvpb_block->copy=lpcisp_kvpb_copy;
  kvpb_block->flush=lpcisp_kvpb_flush;
  if (kvpb_check(kvpb_block,1)<0) { /* todo */};
 #endif /* CONFIG_APP_U2U_V2_WITH_KEYVAL */
 
  /********************/
  //start
  timer_rx_off=timer_tx_off=timer_str=timer_configured=0;

  while(1){

    usb_check_events(&usb_device);
    usb_control_response(&usb_device);

    /* 10ms timer */
    if (lt_10msec_expired(10)) {
      timer_10ms();

      #ifdef WATCHDOG_ENABLED
        watchdog_feed();
      #endif /* WATCHDOG_ENABLED */
    }

    /* local dummy (for now) fd */
    if(ul_fd!=UL_FD_INVALID) {
      if(ul_inepoll(ul_fd)>0) {
        ul_msginfo msginfo;
        ul_acceptmsg(ul_fd, &msginfo);
        ul_freemsg(ul_fd);
      }
    }

    /* serve ulan communication */
    if (!ul_dcnv_is_open(&ep1_dcnv_state))
      continue;

    if (usb_device.ep_events & MASK_EP1RX) {  //EP1RX
      int size;
      size=usb_udev_read_endpoint(&eps[0],ep1_rx_buff,USB_MAX_PACKET);
      ul_dcnv_send(&ep1_dcnv_state,ep1_rx_buff,size);
      usb_device.ep_events &= ~MASK_EP1RX;
      timer_tx_off=5; 		//rozsviceni diod pri vysilani 
      CLR_OUT_PIN(LED_PORT,LED1_BIT);
    }

    if(usb_device.ep_events & MASK_EP1TX){
      ep1_dcnv_state.rx_wait4host=0;
      usb_device.ep_events &= ~MASK_EP1TX;
    }

    if (!ep1_dcnv_state.rx_wait4host) { // EP1TX
      int size=ul_dcnv_rec(&ep1_dcnv_state, ep1_tx_buff, USB_MAX_PACKET);
      if (size){
        usb_udev_write_endpoint(&eps[1],ep1_tx_buff,size);
        ep1_dcnv_state.rx_wait4host=1;
      } else {
        if (ul_dcnv_rec_start(&ep1_dcnv_state, ep1_tx_buff,8)==8) { //HEADER
          usb_udev_write_endpoint(&eps[1],ep1_tx_buff,8);
          timer_rx_off=5;        //rosviceni diody pri prijmu 
          CLR_OUT_PIN(LED_PORT,LED2_BIT);
          ep1_dcnv_state.rx_wait4host=1;
        }
      }
    }
  }
}
