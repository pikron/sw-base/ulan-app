#ifndef USB_VENDOR
#define USB_VENDOR

#include <usb/usb.h>

#define VENDOR_START_ULAN       0     /* Inform converter about uLan driver ready */
#define VENDOR_STOP_ULAN        1     /* Inform about uLan driver stopping  */
#define VENDOR_IS_RUNNING_ULAN  2
#define VENDOR_SETPROMODE_ULAN  3     /* Set promiscuous monitoring mode */

struct ul_dcnv_state_t;

int usb_u2u_vendor4dcnv(struct ul_dcnv_state_t *cvnst, usb_device_t *udev);

#endif /* USB_VENDOR */
