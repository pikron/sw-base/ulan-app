#include <stdio.h>
#include <system_def.h>
#include <usb/usb_loader.h>
#include <ul_lib/ulan.h>
//#include <hal_intr.h>
#include <hal_machperiph.h>
#include <local_config.h>
#ifdef CONFIG_APP_U2U_V2_WITH_KEYVAL
#include <keyvalpb.h>
#include <keyval_id.h>
#include <keyval_loc.h>
#endif /* CONFIG_APP_U2U_V2_WITH_KEYVAL */
#include "ul_dcnv.h"
#include "u2u_vend.h"
#include "ul_idstr.h"


uchar ulan_configured; /* have to be global variable - usb_send_control_data sends data async !!! */

#ifdef CONFIG_APP_U2U_V2_WITH_KEYVAL
extern kvpb_block_t *kvpb_block;
#endif /* CONFIG_APP_U2U_V2_WITH_KEYVAL */

int usb_u2u_vendor4dcnv(struct ul_dcnv_state_t *cvnst, usb_device_t *udev)
{
  int my_adr,baudrate;
  my_adr=(udev->request.wIndex & 0x3F);
  switch ( udev->request.bRequest) {
    case VENDOR_START_ULAN:
      baudrate=udev->request.wValue;
      ulan_configured=0;
      switch (baudrate) {
        case 9600: ulan_configured=1;break;
        case 19200:ulan_configured=1;break;
        case 38400:ulan_configured=1;break;
        case 56800:ulan_configured=1;break;
      }
      if (ulan_configured) {
       #ifdef CONFIG_APP_U2U_V2_WITH_KEYVAL
        {
          unsigned int keyval_my_adr;
          kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&keyval_my_adr);
          if (keyval_my_adr!=my_adr)
            kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&my_adr);
        }
       #endif /* CONFIG_APP_U2U_V2_WITH_KEYVAL */
        ul_dcnv_open(cvnst,my_adr,baudrate,cvnst->idstr?cvnst->idstr:ul_idstr);
      } else {
        ul_dcnv_close(cvnst);
      }
      usb_send_control_data(udev,&ulan_configured,1);
      return 1;
    case VENDOR_STOP_ULAN:
      if (ul_dcnv_is_open(cvnst)) {
        ul_setpromode(cvnst->rx_fd,0);
        ul_setmyadr(cvnst->rx_fd,0);
      }
      ul_dcnv_close(cvnst);
      usb_send_control_data(udev,NULL,0);
      return 1;
    case VENDOR_IS_RUNNING_ULAN:
      usb_send_control_data(udev,&ulan_configured,1);
      return 1;
    case VENDOR_SETPROMODE_ULAN:
      if (ul_dcnv_is_open(cvnst))
        ul_setpromode(cvnst->rx_fd,udev->request.wValue?1:0);
      usb_send_control_data(udev,NULL,0);
      return 1;
    case  USB_VENDOR_RESET_DEVICE:
      usb_send_control_data(udev,NULL,0);
      /* after 10ms invoke hardware reset by watchdog */
      lpc_watchdog_init(1, 10);
      watchdog_feed();
      return 1;
  }

  return 0;
}
