
#ifndef USB_DEFS_MODULE
  #define USB_DEFS_MODULE
  
  #include <usb/usb_spec.h>
//  #include <usb/pdiusb.h>
  #include <usb/lpcusb.h>
  #include <endian.h>
  #include <cpu_def.h>

  #include <endian.h>
  #if __BYTE_ORDER == __BIG_ENDIAN
    #include <byteswap.h>
    #define SWAP(x) ((((x) & 0xFF) << 8) | (((x) >> 8) & 0xFF))
  #else /*__LITTLE_ENDIAN*/
    #define SWAP(x) (x)
  #endif

  #ifndef CODE
    #define CODE
  #endif
    
/*****************************************************/
/*** Static data structures for device descriptors ***/
/*****************************************************/
#ifndef USB_VENDOR_ID
  #define USB_VENDOR_ID      0xDEAD  /* vymyslene cislo ( snad ho nikdo nema ... ;-) */
#endif
#ifndef USB_PRODUCT_ID
  #define USB_PRODUCT_ID     0x1001  /* test code for ULAD21 */
#endif
  #define USB_RELEASE_VER    0x0100

/*** Class codes for device description ***/
  #define USB_CLASS_CODE      0xFF
  #define USB_SUBCLASS_CODE   0x00
  #define USB_PROTOCOL_CODE   0x00
  
  
  #define NUM_ENDPOINTS  2
  #define CONFIG_DESCRIPTOR_LENGTH sizeof( USB_CONFIGURATION_DESCRIPTOR) \
                                     + sizeof( USB_INTERFACE_DESCRIPTOR) \
                                     + ( NUM_ENDPOINTS * sizeof( USB_ENDPOINT_DESCRIPTOR))

/*** Device descriptor ***/
  CODE const USB_DEVICE_DESCRIPTOR DeviceDescription = {
    sizeof( USB_DEVICE_DESCRIPTOR),
    USB_DESCRIPTOR_TYPE_DEVICE,
    SWAP( 0x0100),
    USB_CLASS_CODE,
    USB_SUBCLASS_CODE,
    USB_PROTOCOL_CODE,
    USB_MAX_PACKET0,
    SWAP( USB_VENDOR_ID),
    SWAP( USB_PRODUCT_ID),  
    SWAP( USB_RELEASE_VER),
    1, /* manufacturer string ID */
    2, /* product string ID */
    3, /* serial number string ID */
    1
  };
  
/*** All In Configuration 0 ***/
  CODE const struct {
    USB_CONFIGURATION_DESCRIPTOR configuration;
    USB_INTERFACE_DESCRIPTOR interface;
    USB_ENDPOINT_DESCRIPTOR endpoint_tx;
    USB_ENDPOINT_DESCRIPTOR endpoint_rx;
  } ConfigDescription = {
    /*** Configuration descriptor ***/
    {
      sizeof( USB_CONFIGURATION_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_CONFIGURATION,
      SWAP( CONFIG_DESCRIPTOR_LENGTH),
      1, /* cnt of interfaces */
      1, /* this configuration ID */
      4, /* config.name string ID*/
      0x80, /* cfg, in spec is, taha bit 7 must be set to one -> 0xe0 , orig 0x60*/ 
      0x32    /* device power current from host 100mA */
    },
    /*** Interface Descriptor ***/
    {
      sizeof( USB_INTERFACE_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_INTERFACE,
      0,    /* index of this interface for SetInterface request */
      0,    /* ID alternate interface */
      NUM_ENDPOINTS,
      USB_CLASS_CODE,
      USB_SUBCLASS_CODE,
      USB_PROTOCOL_CODE,
      5
    },
    /*** Endpoint 1 - Tx,Bulk ***/
    {
      sizeof( USB_ENDPOINT_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_ENDPOINT,
      0x01,
      USB_ENDPOINT_TYPE_BULK,
      SWAP( USB_MAX_PACKET),
      0
    },
    /*** Endpoint 1 - Rx,Bulk ***/
    {
      sizeof( USB_ENDPOINT_DESCRIPTOR),
      USB_DESCRIPTOR_TYPE_ENDPOINT,
      0x81,
      USB_ENDPOINT_TYPE_BULK,
      SWAP( USB_MAX_PACKET),
      0
    }    
  };
  /*** Strings - in unicode ***/
  CODE const char Str0Desc[] = {  /* supported languages of strings */
    4, 0x03,  /* 2+2*N , N is count of supported languages */
    0x09,0x04 /* english 0x0409 */
  };
  
  CODE const char Str1Desc[] = {  /* 1 = manufacturer */  
    48,0x03,
    'S',0,
    'm',0,
    'o',0,
    'l',0,
    'i',0,
    'k',0,
    ',',0,
    'B',0,
    'a',0,
    'r',0,
    't',0,
    'o',0,
    's',0,
    'i',0,
    'n',0,
    's',0,
    'k',0,
    'i',0,
    ',',0,
    'P',0,
    'i',0,
    's',0,
    'a',0
  };

  CODE const char Str2Desc[] = {  /* 2 = product */
    38, 0x03,                                               
    'u',0,
    'L',0,
    'a',0,
    'n',0,
    '2',0,
    'u',0,
    's',0,
    'b',0,
    ' ',0,
    'c',0,
    'o',0,
    'n',0,
    'v',0,
    'e',0,
    'r',0,
    't',0,
    'o',0,
    'r',0
  };


  CODE const char Str3Desc[] = {  /* 3 = version */
    26, 0x03,                                               
    '2',0,
    '3',0,
    '.',0,
    '0',0,
    '7',0,
    '.',0,
    '0',0,
    '4',0,
    '-',0,
    '1',0,
    '.',0,
    '1',0
  };
  CODE const char Str4Desc[] = {  /* 4 = configuration */
    34, 0x03,                                               
    'C',0,
    'o',0,
    'n',0,
    'f',0,
    'i',0,
    'g',0,
    'u',0,
    'r',0,
    'a',0,
    't',0,
    'i',0,
    'o',0,
    'n',0,
    ' ',0,
    '#',0,
    '1',0
  };
  CODE const char Str5Desc[] = {  /* 5 = interface */
    26,0x03,                                               
    'I',0,
    'n',0,
    't',0,
    'e',0,
    'r',0,
    'f',0,
    'a',0,
    'c',0,
    'e',0,
    ' ',0,
    '#',0,
    '0',0
  };

  CODE const char Str6Desc[] = {  /* EP1 OUT descriptor */
    136,0x03,  
    'E',0,
    'P',0,
    '1',0,
    'O',0,
    'U',0,
    'T',0,
    '-',0,
    's',0,
    'e',0,
    'n',0,
    'd',0,
    ' ',0,
    'm',0,
    'e',0,
    's',0,
    's',0,
    'a',0,
    'g',0,
    'e',0,
    '-',0,
    'd',0,
    'a',0,
    'd',0,
    'r',0,
    ',',0,
    's',0,
    'a',0,
    'd',0,
    'r',0,
    ',',0,
    'c',0,
    'm',0,
    'd',0,
    ',',0,
    'f',0,
    'l',0,
    'g',0,
    ',',0,
    's',0,
    't',0,
    'a',0,
    'm',0,
    'p',0,
    ',',0,
    'f',0,
    'r',0,
    'e',0,
    'e',0,
    ',',0,
    'l',0,
    'e',0,
    'n',0,
    '-',0,
    'l',0,
    'o',0,
    ',',0,
    'l',0,
    'e',0,
    'n',0,
    '-',0,
    'h',0,
    'i',0,
    ',',0,
    'd',0,
    'a',0,
    't',0,
    'a',0
  };

  CODE const char Str7Desc[] = {  /* EP2 IN descriptor */
    140,0x03,
    'E',0,
    'P',0,
    '1',0,
    'I',0,
    'N',0,
    '-',0,
    'r',0,
    'e',0,
    'c',0,
    'e',0,
    'i',0,
    'v',0,
    'e',0,
    ' ',0,
    'm',0,
    'e',0,
    's',0,
    's',0,
    'a',0,
    'g',0,
    'e',0,
    '-',0,
    'd',0,
    'a',0,
    'd',0,
    'r',0,
    ',',0,
    's',0,
    'a',0,
    'd',0,
    'r',0,
    ',',0,
    'c',0,
    'm',0,
    'd',0,
    ',',0,
    'f',0,
    'l',0,
    'g',0,
    ',',0,
    's',0,
    't',0,
    'a',0,
    'm',0,
    'p',0,
    ',',0,
    'f',0,
    'r',0,
    'e',0,
    'e',0,
    ',',0,
    'l',0,
    'e',0,
    'n',0,
    '-',0,
    'l',0,
    'o',0,
    ',',0,
    'l',0,
    'e',0,
    'n',0,
    '-',0,
    'h',0,
    'i',0,
    ',',0,
    'd',0,
    'a',0,
    't',0,
    'a',0
  };

  /* all strings in pointers array */
  CODE const PUSB_STRING_DESCRIPTOR StringDescriptors[] = {
    (PUSB_STRING_DESCRIPTOR) Str0Desc,
    (PUSB_STRING_DESCRIPTOR) Str1Desc,
    (PUSB_STRING_DESCRIPTOR) Str2Desc,
    (PUSB_STRING_DESCRIPTOR) Str3Desc,
    (PUSB_STRING_DESCRIPTOR) Str4Desc,
    (PUSB_STRING_DESCRIPTOR) Str5Desc,
    (PUSB_STRING_DESCRIPTOR) Str6Desc,
    (PUSB_STRING_DESCRIPTOR) Str7Desc
  };

  #define CNT_STRINGS (sizeof(StringDescriptors)/sizeof(StringDescriptors[0]))

  CODE const USB_DEVICE_CONFIGURATION_ENTRY usb_devdes_configurations[] = {
    {
      .pConfigDescription = &ConfigDescription.configuration,
      .iConfigTotalLength = CONFIG_DESCRIPTOR_LENGTH
    }
  };

  CODE const USB_INTERFACE_DESCRIPTOR *usb_devdes_interfaces[] = {
    &ConfigDescription.interface
  };

  CODE const USB_DEVICE_DESCRIPTORS_TABLE usb_devdes_table = {
    .pDeviceDescription = &DeviceDescription,
    .pConfigurations = usb_devdes_configurations,
    .pInterfaceDescriptors = usb_devdes_interfaces,
    .pStrings = StringDescriptors,
    .iNumStrings = CNT_STRINGS,
    .bNumEndpoints = NUM_ENDPOINTS,
    .bNumConfigurations = 1,
    .bNumInterfaces = 1
  };

#endif /* USB_DEFS_MODULE */
