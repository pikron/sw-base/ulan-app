#include <system_def.h>
#include <cpu_def.h>
#include <stdio.h>
#include "ul_dcnv.h"
#include "local_config.h"

#ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
#include <string.h>
#include <ul_msg_adr.h>
#endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/

//#define U2UBFL_LNMM     0x080         /* Length of received frame must match expected len */ 
#define U2UBFL_NORE     0x040         /* Do not try to repeat if error occurs */ 
#define U2UBFL_TAIL     0x020         /* Message has tail frame */
#define U2UBFL_REC      0x010         /* Request receiption of block */
#define U2UBFL_FAIL     0x008         /* Message cannot be send - error */
#define U2UBFL_PROC	0x004         /* Message succesfull send */
#define U2UBFL_AAP	0x003         /* Request imediate proccessing of frame by receiver station with acknowledge */
#define U2UBFL_PRQ	0x002         /* Request imediate proccessing of frame by receiver station */
#define U2UBFL_ARQ	0x001         /* Request imediate acknowledge by receiving station */

unsigned char 
cbfl_ul2ps(unsigned int bfl,int proc)
{
  unsigned char ret=0;
  if(bfl&UL_BFL_ARQ) ret|=U2UBFL_ARQ;
  if(bfl&UL_BFL_PRQ) ret|=U2UBFL_PRQ;
  if(proc) ret|=U2UBFL_PROC;
  if(bfl&UL_BFL_FAIL) ret|=U2UBFL_FAIL;
  if(bfl&UL_BFL_REC) ret|=U2UBFL_REC;
  if(bfl&UL_BFL_TAIL) ret|=U2UBFL_TAIL;
  if(bfl&UL_BFL_NORE) ret|=U2UBFL_NORE;
  return ret;
}

unsigned int 
cbfl_ps2ul(unsigned char bfl)
{
  unsigned int ret=0;
  if(bfl&U2UBFL_ARQ) ret|=UL_BFL_ARQ;
  if(bfl&U2UBFL_PRQ) ret|=UL_BFL_PRQ;
  if(bfl&U2UBFL_REC) ret|=UL_BFL_REC;
  else ret|=UL_BFL_SND;
  if(bfl&U2UBFL_TAIL) ret|=UL_BFL_TAIL;
  if(bfl&U2UBFL_NORE) ret|=UL_BFL_NORE;
  return ret;
}

int 
ul_dcnv_init_state(ul_dcnv_state_t XDATA *cnvst,ul_fd_t tx_fd,ul_fd_t rx_fd) 
{
  cnvst->tx_2send=-1;
  cnvst->rx_2rec=-1;		//wait for request for receiving
  cnvst->tx_error=cnvst->rx_error=0;
  cnvst->rx_wait4host=0;
#ifndef UL_WITHOUT_HANDLE 
  cnvst->tx_fd=tx_fd;
  cnvst->rx_fd=rx_fd;
  cnvst->tx_tail=0;
  cnvst->rx_tail=0;
  cnvst->stamp_queue.head=cnvst->stamp_queue.stamps;
  cnvst->stamp_queue.tail=cnvst->stamp_queue.head;
#endif

  return 0;
}

int ul_dcnv_open(ul_dcnv_state_t XDATA *cnvst,int my_adr, int baudrate, const char *idstr)
{
#ifndef UL_WITHOUT_HANDLE 
  ul_msginfo msginfo;
#endif
  ul_fd_t rx_fd,tx_fd;
  if (!ul_dcnv_is_open(cnvst)) {
    rx_fd=ul_open(cnvst->dev_name, NULL);
    tx_fd=ul_open(cnvst->dev_name, NULL);
   #if CONFIG_OC_UL_DRV_WITH_MULTI_DEV
    ul_setsubdev(rx_fd, cnvst->subdevidx);
    ul_setsubdev(tx_fd, cnvst->subdevidx);
   #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/
  } else {
    rx_fd=cnvst->rx_fd;
    tx_fd=cnvst->tx_fd;
  }
  ul_setidstr(rx_fd,idstr);
  ul_setmyadr(rx_fd,my_adr);
  if (baudrate)
    ul_setbaudrate(rx_fd,baudrate);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(rx_fd,&msginfo);
  ul_stroke(rx_fd);
  ul_dcnv_init_state(cnvst,tx_fd,rx_fd);
  return 1;
}

int ul_dcnv_init_by_name(ul_dcnv_state_t XDATA *cnvst, char *ul_dev_name, int subdevidx)
{
  cnvst->dev_name=ul_dev_name;
  cnvst->subdevidx=subdevidx;
  return ul_dcnv_init_state(cnvst,UL_FD_INVALID,UL_FD_INVALID);
}


int ul_dcnv_close(ul_dcnv_state_t XDATA *cnvst)
{
  if (!ul_dcnv_is_open(cnvst))
    return 0;
  ul_close(cnvst->rx_fd);
  ul_close(cnvst->tx_fd);
  cnvst->rx_fd=UL_FD_INVALID;
  cnvst->tx_fd=UL_FD_INVALID;
  return 1;
}

int 
ul_dcnv_send(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size)
{
 #ifndef UL_WITHOUT_HANDLE
  ul_msginfo msginfo;
 #endif /*UL_WITHOUT_HANDLE*/
  int ret=0,stamp;
  unsigned char XDATA *buff_save=buff;

  if (cnvst->tx_2send==-1) {
    if(size<8) return 0;
    cnvst->tx_2send =buff[6]+buff[7]*0x100;
    msginfo.dadr=buff[0]&0x7F;  // dadr
    msginfo.sadr=buff[1]&0x7F;  // sadr
    msginfo.cmd =buff[2];       // cmd
    msginfo.flg =cbfl_ps2ul(buff[3]); // flg zpravy
    msginfo.flg |= UL_BFL_M2IN;
    cnvst->tx_psstamp=buff[4];      // stamp
    if(cnvst->tx_tail) {
      msginfo.len=cnvst->tx_2send;
      ret = ul_tailmsg(cnvst->tx_fd, &msginfo);
    } else
      ret = ul_newmsg(cnvst->tx_fd, &msginfo);
    if(ret<0) {
      cnvst->tx_error++;
      return -1;
    }
    cnvst->tx_tail=msginfo.flg&UL_BFL_TAIL?1:0;
    buff+=8;
    size-=8;
  }
  if ((cnvst->tx_2send>0) && (size>0)) {
    if(size>cnvst->tx_2send)
      size=cnvst->tx_2send;
    if(ul_write(cnvst->tx_fd, buff, size) != size){
      cnvst->tx_error++;
      ret=-1;
    }
    cnvst->tx_2send-=size;
    buff+=size;
  }
  if (cnvst->tx_2send<=0) {
    cnvst->tx_2send=-1;
    if(!cnvst->tx_tail){
      stamp=ul_freemsg(cnvst->tx_fd);
      if(stamp>=0){
        cnvst->stamp_queue.head->ps=cnvst->tx_psstamp;
        cnvst->stamp_queue.head->ul=stamp;
        ps2ul_stamp_idx_inc(&cnvst->stamp_queue, &cnvst->stamp_queue.head);
      }
    }
  }
  return ret>=0?buff-buff_save:ret;
}

int 
ul_dcnv_rec(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size)
{
  int ret;
  int ext_placed=0;
  if (cnvst->rx_2rec == -1) {
    return 0;
  } else {
   #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
    if(cnvst->ext_pos<cnvst->ext_len) {
      ext_placed=cnvst->ext_len-cnvst->ext_pos;
      if (ext_placed>size)
        ext_placed=size;
      memcpy((char*)buff, (char*)cnvst->ext_buff, ext_placed);
      cnvst->ext_pos+=ext_placed;
      cnvst->rx_2rec-=ext_placed;
      if (ext_placed==size)
        return ext_placed;
      buff+=ext_placed;
      size-=ext_placed;
    }
   #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
    if(size>cnvst->rx_2rec) size=cnvst->rx_2rec;
    ret=ul_read(cnvst->rx_fd, buff, size);
    if(ret!=size){
      cnvst->rx_error++;
      ret=-2;
      cnvst->rx_2rec=-1;
      ul_abortmsg(cnvst->rx_fd);
    } else {
      cnvst->rx_2rec-=size;
      if(!cnvst->rx_2rec) {
        cnvst->rx_2rec=-1;
        if(!cnvst->rx_tail) {
          ul_freemsg(cnvst->rx_fd);
        }
      }
      ret+=ext_placed;
    }
    return ret;
  }
}

int 
ul_dcnv_rec_start(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size)
{
  ul_msginfo msginfo;
  int proc, psstamp, ret;
  ps2ul_stamp_t *stamp_p;

  
  ul_inepoll(cnvst->tx_fd);

  /* I can't call ul_acceptmsg during processing of a tx message (tail) - tx message will be freed */
  if ((cnvst->tx_2send==-1) && (cnvst->tx_tail==0) &&
      (ul_acceptmsg(cnvst->tx_fd, &msginfo)>=0)) { 
    ul_freemsg(cnvst->tx_fd);
  }

  if (cnvst->rx_2rec==-1){
    proc=0;
    psstamp=0;
    if (cnvst->rx_tail) {
      ret=ul_actailmsg(cnvst->rx_fd, &msginfo);
      psstamp=cnvst->rx_tail_stamp;
      proc=1;
    } else
      ret=ul_acceptmsg(cnvst->rx_fd, &msginfo);
    if (ret<0) return 0;
    cnvst->rx_2rec=msginfo.len;
    stamp_p=cnvst->stamp_queue.tail;
    while(stamp_p!=cnvst->stamp_queue.head){
      if(stamp_p->ul==msginfo.stamp){
        psstamp=stamp_p->ps;
        cnvst->rx_tail_stamp=psstamp;
        if(!(msginfo.flg&UL_BFL_FAIL))
          proc=1;
        ps2ul_stamp_idx_inc(&cnvst->stamp_queue, &stamp_p);
        cnvst->stamp_queue.tail=stamp_p;
	break;
      }
      ps2ul_stamp_idx_inc(&cnvst->stamp_queue, &stamp_p);
    }
   #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_NET
    {
      uchar ext_header;
      uchar hops=0;
      int dadr_lg2l=ul_msg_adr_log2len(msginfo.dadr);
      int sadr_lg2l=ul_msg_adr_log2len(msginfo.sadr);
      cnvst->ext_len=0;
      cnvst->ext_pos=0;
      ext_header=ul_msg_adr_log2len_to_ext_header(dadr_lg2l, sadr_lg2l);
      if (ext_header) {
        uchar *ptr = cnvst->ext_buff;
        *(ptr++)=ext_header;
        if (dadr_lg2l) {
          ptr=ul_msg_adr2buff(ptr, msginfo.dadr, dadr_lg2l);
          msginfo.dadr=1;
        }
        if (sadr_lg2l) {
          ptr=ul_msg_adr2buff(ptr, msginfo.sadr, sadr_lg2l);
          msginfo.sadr=1;
        }
        *(ptr++)=hops;
        *(ptr++)=msginfo.cmd;
        msginfo.cmd=UL_CMD_EADR;
        cnvst->ext_len=ptr-cnvst->ext_buff;
        cnvst->rx_2rec+=cnvst->ext_len;
      }
    }
   #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_NET*/
    if ((proc && !(msginfo.flg&UL_BFL_REC)) || (msginfo.flg&UL_BFL_FAIL))
      cnvst->rx_2rec=0;
    buff[0]=msginfo.dadr;
    buff[1]=msginfo.sadr;
    buff[2]=msginfo.cmd;
    buff[3]=cbfl_ul2ps(msginfo.flg,proc);
    buff[4]=psstamp;
    buff[5]=0;
    buff[6]=cnvst->rx_2rec%0x100;
    buff[7]=cnvst->rx_2rec/0x100;
    if (!(msginfo.flg&UL_BFL_TAIL) || (msginfo.flg&UL_BFL_FAIL)) {
      cnvst->rx_tail=0;
      if (cnvst->rx_2rec<=0) {
        ul_freemsg(cnvst->rx_fd);
        cnvst->rx_2rec=-1;
      }
    } else
      cnvst->rx_tail=1;
    buff+=8; size-=8;
    return 8;
  } else 
    return 0;
}
