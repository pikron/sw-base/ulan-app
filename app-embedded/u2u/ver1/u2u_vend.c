#include <stdio.h>
#include <system_def.h>
#include <ul_lib/ulan.h>
#include "u2u_vend.h"

extern char ulan_configured;

int usb_u2u_vendor(usb_device_t *udev) __reentrant
{
  switch ( udev->request.bRequest) {
    case VENDOR_START_ULAN:
      ulan_configured=0;
      ES_U=0;                                            //disable int. from serial
      switch (udev->request.wValue) {
        case 9600:
          ulan_configured=1;
          ul_drv_set_adr(udev->request.wIndex & 0x3F);
          ul_drv_set_bdiv(BAUD2BAUDDIV(9600));
          ES_U=1;
          break;
        case 19200:
          ulan_configured=1;
          ul_drv_set_adr(udev->request.wIndex & 0x3F);
          ul_drv_set_bdiv(BAUD2BAUDDIV(19200)); 
          ES_U=1;
          break;
        case 38400:
          ulan_configured=1;
          ul_drv_set_adr(udev->request.wIndex & 0x3F);
          ul_drv_set_bdiv(BAUD2BAUDDIV(38400));
          ES_U=1;
          break;
        case 56800:
          ulan_configured=1;
          ul_drv_set_adr(udev->request.wIndex & 0x3F);
          ul_drv_set_bdiv(BAUD2BAUDDIV(56800));
          ES_U=1;
          break;
      }
      usb_send_control_data(udev,&ulan_configured,1);
      return 1;
    case VENDOR_STOP_ULAN:
      ulan_configured=0;
      ES_U=0;                                            //disable int. from serial
      return 1;
    case VENDOR_IS_RUNNING_ULAN:
      return ulan_configured;
  }

  return 0;
}
