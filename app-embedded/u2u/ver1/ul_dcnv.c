#include <system_def.h>
#include <cpu_def.h>
#include <stdio.h>
#include <usb/usb.h>
#include "ul_dcnv.h"

int 
ul_dcnv_init(ul_dcnv_state_t XDATA *cnvst,ul_fd_t tx_fd,ul_fd_t rx_fd) 
{
  cnvst->tx_2send=-1;
  cnvst->rx_2rec=-1;		//wait for request for receiving
  cnvst->tx_error=cnvst->rx_error=0;
  cnvst->rx_wait4host=0;
#ifndef UL_WITHOUT_HANDLE 
  cnvst->tx_fd=tx_fd;
  cnvst->rx_fd=rx_fd;
#endif
  return 0;
}

int 
ul_dcnv_send(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size)
{
 #ifndef UL_WITHOUT_HANDLE
  ul_msginfo msginfo;
 #endif /*UL_WITHOUT_HANDLE*/
  int ret=0;

  if (cnvst->tx_2send==-1) {
    if(size<8) return 0;
    cnvst->tx_2send =buff[6]+buff[7]*0x100;
    msginfo.dadr=buff[0]&0x7F;  // dadr
    msginfo.sadr=buff[1]&0x7F;  // sadr
    msginfo.cmd =buff[2];       // cmd
    msginfo.flg =buff[3];       // status zpravy
    msginfo.stamp=buff[4];      // stamp
    if(ul_newmsg(cnvst->tx_fd, &msginfo)<0) {
      cnvst->tx_error++;
      return -1;
    }
    buff+=8;
  } else {
    if (cnvst->tx_2send>0) {
      if(ul_write(cnvst->tx_fd, buff, size) != size){
        cnvst->tx_error++;
        ret=-1;
      }
      cnvst->tx_2send-=size;
    }
  }
  if (cnvst->tx_2send<=0) {
    ul_o_close(cnvst->tx_fd);
    cnvst->tx_2send=-1;
  }
  return ret;
}

int 
ul_dcnv_rec(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size)
{
  int ret;
  if (cnvst->rx_2rec == -1) {
    return 0;
  } else {
    if(size>cnvst->rx_2rec) size=cnvst->rx_2rec;
    ret=ul_read(cnvst->rx_fd, buff, size);
    if(ret!=size){
      cnvst->rx_error++;
      ret=-2;
      cnvst->rx_2rec=-1;
      //ul_abortmsg(cnvst->rx_fd);
    } else {
      cnvst->rx_2rec-=size;
      if(!cnvst->rx_2rec) {
        ul_i_close(cnvst->rx_fd);
        cnvst->rx_2rec=-1;
      }
    }
    return ret;
  }
}

int 
ul_dcnv_rec_start(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size)
{
 #ifndef UL_WITHOUT_HANDLE
  ul_msginfo msginfo;
 #endif /*UL_WITHOUT_HANDLE*/

  if ((cnvst->rx_2rec==-1) && (ul_acceptmsg(cnvst->rx_fd, &msginfo)>=0) && 1) {
    cnvst->rx_2rec=msginfo.len;
    buff[0]=msginfo.dadr;      buff[1]=msginfo.sadr;
    buff[2]=msginfo.cmd;       buff[3]=msginfo.flg;
    buff[4]=msginfo.stamp;     buff[5]=0;
    buff[6]=cnvst->rx_2rec%0x100;  buff[7]=cnvst->rx_2rec/0x100;
    if (cnvst->rx_2rec<=0) {
      ul_i_close(cnvst->rx_fd);
      cnvst->rx_2rec=-1;
    }
    buff+=8; size-=8;
    return 8;
  } else {
    return 0;
  }
}
