/*****************************************************/
/***   Module : USB module                         ***/
/***   Author : Roman Bartosinski (C) 28.04.2002   ***/
/***   Modify : 08.08.2002, 16.04.2003             ***/
/*****************************************************/

#include <string.h>

#include <system_def.h>
#include <usb/usb.h>
#include <usb/usb_spec.h>
#include <usb/pdiusb.h>
#include <usb/usbdebug.h>
#include <usb/usb_srq.h>
#include <usb/usb_defs.h>
#include "ul_dcnv.h"

extern ul_dcnv_state_t XDATA ep1_dcnv_state;

  // ****************************
  int usb_stdreq_get_status( usb_device_t *udev)
  {
    unsigned char c,buf[2] = { 0, 0};
    unsigned char epid = (unsigned char) udev->request.wIndex;

    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetStatus\n"));
    switch( udev->request.bmRequestType & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:
        if ( udev->flags & USB_FLAG_REMOTE_WAKE) //.remote_wake_up == 1)
          buf[0] = USB_GETSTATUS_REMOTE_WAKEUP_ENABLED | USB_GETSTATUS_SELF_POWERED;
        else
          buf[0] = USB_GETSTATUS_SELF_POWERED;
        break;
      case USB_RECIPIENT_INTERFACE:
        break;
      case USB_RECIPIENT_ENDPOINT:
        if ( epid & USB_ENDPOINT_DIRECTION_MASK)
          c = pdiSelectEp(pdiEp2Idx(epid)); // endpoint in
        else
          c = pdiSelectEp(pdiEp2Idx(epid));     // endpoint Out
          #ifdef PDIUSBD12
          buf[0] = (( c & PDI_SELEP_STALL) == PDI_SELEP_STALL);
          #else
          buf[0] = 0;
          #endif
        break;
      default:
        return USB_COMPLETE_FAIL;
    }
    pdiWriteEndpoint( PDI_EP0_TX, 2, buf);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_clear_feature( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    unsigned char epid = (unsigned char) dreq->wIndex;

    usb_debug_print( DEBUG_LEVEL_HIGH, ("ClearFeature\n"));
    switch( dreq->bmRequestType & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:
        if ( dreq->wValue == USB_FEATURE_REMOTE_WAKEUP) {
          udev->flags &= ~USB_FLAG_REMOTE_WAKE; //.remote_wake_up = 0;
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
      case USB_RECIPIENT_ENDPOINT:
        if ( dreq->wValue == USB_FEATURE_ENDPOINT_STALL) {
          if ( epid & USB_ENDPOINT_DIRECTION_MASK)
            pdiSetEpStatus(pdiEp2Idx(epid), 0); // clear TX stall for IN on EPn
          else
            pdiSetEpStatus(pdiEp2Idx(epid), 0); // clear RX stall for OUT on EPn
          usb_udev_ack_setup( udev);
	  //initialize state of ulan convertor
	  switch (epid&(USB_ENDPOINT_DIRECTION_MASK|0x03)) {
	    case 0x01:                          //rx USB -> tx ulan
	      ul_o_close(ep1_dcnv_state.tx_fd);
              ep1_dcnv_state.tx_2send=-1;
	      break;
	    case 0x81:				//tx USB -> rx ulan
	      ul_o_close(ep1_dcnv_state.rx_fd);
              ep1_dcnv_state.rx_2rec=-1;
              ep1_dcnv_state.rx_wait4host=0;
	      break;
	  }
          return USB_COMPLETE_OK;
        }
        break;
    }
    return USB_COMPLETE_FAIL;
  }

  int usb_stdreq_set_feature( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    unsigned char epid = (unsigned char) dreq->wIndex;

    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetFeature\n"));
    switch( dreq->bmRequestType & USB_RECIPIENT) {
      case USB_RECIPIENT_DEVICE:
        if ( dreq->wValue == USB_FEATURE_REMOTE_WAKEUP) {
          udev->flags |= USB_FLAG_REMOTE_WAKE; //.remote_wake_up = 1;
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
      case USB_RECIPIENT_ENDPOINT:
        if ( dreq->wValue == USB_FEATURE_ENDPOINT_STALL) {
          if ( epid & USB_ENDPOINT_DIRECTION_MASK)
            pdiSetEpStatus( pdiEp2Idx(epid), 1); // set TX stall for IN on EPn
          else
            pdiSetEpStatus( pdiEp2Idx(epid), 1);     // set RX stall for OUT on EPn
          usb_udev_ack_setup( udev);
          return USB_COMPLETE_OK;
        }
        break;
    }
    return USB_COMPLETE_FAIL;
  }

  int usb_stdreq_set_address( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetAddr\n"));
    usb_udev_ack_setup( udev);
    pdiSetAddressEnable( (dreq->wValue & DEVICE_ADDRESS_MASK) + PDI_ENAD_ENABLE);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_get_configuration( usb_device_t *udev)
  {
    unsigned char buf = udev->configuration; //usb_flags.configured;
    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetConfig\n"));
    pdiWriteEndpoint( PDI_EP0_TX, 1, &buf);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_set_configuration( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    unsigned char iCfg = dreq->wValue & 0xff;
    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetConfig\n"));
    if ( iCfg < 2) {       // put device in unconfigured state or set configuration 1 ( no else)
      usb_udev_ack_setup( udev);
      pdiSetEndpointEnable( 0); // USBInitUnconfig();
      if ( iCfg) {
        pdiSetEndpointEnable( PDI_EPEN_ENABLE); //USBInitConfig();
        udev->flags |= USB_FLAG_CONFIGURED;
      } else {
        udev->flags &= ~USB_FLAG_CONFIGURED;
      }
      udev->configuration = iCfg;  //usb_flags.configured = iCfg;
      return USB_COMPLETE_OK;
    } else
      return USB_COMPLETE_FAIL;
  }

  int usb_stdreq_get_interface( usb_device_t *udev)
  {
    unsigned char buf = 0; /// udev->interface
    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetIface\n"));
    pdiWriteEndpoint( PDI_EP0_TX, 1, &buf);
    return USB_COMPLETE_OK;
  }

  int usb_stdreq_set_interface( usb_device_t *udev)
  {
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    
    usb_debug_print( DEBUG_LEVEL_HIGH, ("SetIface\n"));
    if (( dreq->wValue == 0) && ( dreq->wIndex == 0)) {
      usb_udev_ack_setup( udev);
      return USB_COMPLETE_OK;
    } else {
      return USB_COMPLETE_FAIL;
    }
  }

  int usb_stdreq_get_descriptor( usb_device_t *udev)
  {
    unsigned char *pDesc;
    unsigned short Len = 0;
    USB_DEVICE_REQUEST *dreq = &(udev->request);
    int i;

    i = (dreq->wValue >> 8) & 0xff; /* MSB part of wValue */
    usb_debug_print( DEBUG_LEVEL_HIGH, ("GetDesc\n"));
    usb_debug_print( DEBUG_LEVEL_VERBOSE, ( " - %s desc.\n", /*(unsigned int)*/ usb_debug_get_std_descriptor(i)));

    switch (i) {
      case USB_DESCRIPTOR_TYPE_DEVICE:
        pDesc = (unsigned char *)&DeviceDescription;
        Len = sizeof( USB_DEVICE_DESCRIPTOR);
        break;
      case USB_DESCRIPTOR_TYPE_CONFIGURATION:
        pDesc = (unsigned char *)&ConfigDescription;
        Len = CONFIG_DESCRIPTOR_LENGTH;
        break;
      case USB_DESCRIPTOR_TYPE_INTERFACE:
        pDesc = (unsigned char *)&ConfigDescription.interface;
        Len = sizeof( USB_INTERFACE_DESCRIPTOR);
        break;
      case USB_DESCRIPTOR_TYPE_STRING:
        i = dreq->wValue & 0xff; /* LSB part of wValue */
        /*printf("Get descriptor indx=0x%02x\n", i);*/
        if ( i < CNT_STRINGS) {
          pDesc = (unsigned char *) StringDescriptors[ i];
          Len = *pDesc;
          /*usb_debug_print(0,("indx=0x%02x ptr=%p len=%d : '%c'\n", i, pDesc, Len, pDesc[2]));*/
        } else {
          return USB_COMPLETE_FAIL;
        }
        break;
      default:
        return USB_COMPLETE_FAIL;
    }
    if ( dreq->wLength < Len) Len = dreq->wLength;
    usb_send_control_data( udev, pDesc, Len);
    return USB_COMPLETE_OK;
  }


/*  
  void usb_init_stdreq_fnc( usb_device_t *udev)
  {
    // memset( udev->stdreq, 0, sizeof(udev->stdreq));
    
    udev->stdreq[USB_REQUEST_GET_STATUS] = usb_stdreq_get_status;
    udev->stdreq[USB_REQUEST_CLEAR_FEATURE] = usb_stdreq_clear_feature;
    udev->stdreq[USB_REQUEST_SET_FEATURE] = usb_stdreq_set_feature;
    udev->stdreq[USB_REQUEST_SET_ADDRESS] = usb_stdreq_set_address;
    udev->stdreq[USB_REQUEST_GET_DESCRIPTOR] = usb_stdreq_get_descriptor;
    udev->stdreq[USB_REQUEST_GET_CONFIGURATION] = usb_stdreq_get_configuration;
    udev->stdreq[USB_REQUEST_SET_CONFIGURATION] = usb_stdreq_set_configuration;
    udev->stdreq[USB_REQUEST_GET_INTERFACE] = usb_stdreq_get_interface;
    udev->stdreq[USB_REQUEST_SET_INTERFACE] = usb_stdreq_set_interface;
  }
*/
