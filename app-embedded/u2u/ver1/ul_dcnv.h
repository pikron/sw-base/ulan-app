#ifndef _UL_DCNV_H
#define _UL_DCNV_H

#include "ul_lib/ulan.h"

#ifdef __cplusplus
/*extern "C" {*/
#endif

#ifndef XDATA
  #define XDATA
#endif

typedef struct ul_dcnv_state_t {
  int tx_2send;
  int tx_error;
  int rx_2rec;
  int rx_error;
  int rx_wait4host;
#ifndef UL_WITHOUT_HANDLE 
  ul_fd_t tx_fd;
  ul_fd_t rx_fd;
#endif /*UL_WITHOUT_HANDLE*/
} ul_dcnv_state_t;

int ul_dcnv_init(ul_dcnv_state_t XDATA *cnvst,ul_fd_t tx_fd,ul_fd_t rx_fd);
int ul_dcnv_send(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size);
int ul_dcnv_rec(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size);
int ul_dcnv_rec_start(ul_dcnv_state_t XDATA *cnvst, unsigned char XDATA *buff, int size);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _UL_UTIL_H */
