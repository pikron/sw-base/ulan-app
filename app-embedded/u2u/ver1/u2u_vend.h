#ifndef USB_VENDOR
#define USB_VENDOR

#include <usb/usb.h>

#define VENDOR_START_ULAN       0     /* Inform converter about uLan driver ready */
#define VENDOR_STOP_ULAN        1     /* Inform about uLan driver stopping  */
#define VENDOR_IS_RUNNING_ULAN  2
#define VENDOR_ARC_ON_ULAN      3     /* Set promiscuous monitoring mode */
#define VENDOR_ARC_OFF_ULAN     4     /* Return to std. adress recognition mode */
  
int usb_u2u_vendor(usb_device_t *udev) __reentrant;
  
#endif /* USB_VENDOR */
