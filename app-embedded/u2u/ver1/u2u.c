#include <stdio.h>
#include <string.h>
/* USB support */
#include <usb/usb.h>
#include <usb/usbdebug.h>
#include <usb/pdiusb.h> /* pdiGetFrameNumber */
#include <system_def.h>
#include <cpu_def.h> 
#include <ul_lib/ulan.h>
#include <lt_timer.h>
#include "ul_dcnv.h"
#include "u2u_vend.h"
#ifdef HAVE_VECT_H
#include "vect.h"
#endif

#define MASK_EP1RX  0x01
#define MASK_EP1TX  0x02

LT_TIMER_DEC(lt_10msec)
LT_TIMER_IMP(lt_10msec)

usb_device_t usb_pdi_device;
usb_ep_t eps[2]; 
#ifndef UL_WITHOUT_HANDLE
ul_fd_t ul_fd;
#endif
ul_dcnv_state_t XDATA ep1_dcnv_state;
uint8_t ulan_configured;
unsigned char XDATA ep1_rx_buff[PDI_EP1_PACKET_SIZE];
unsigned char XDATA ep1_tx_buff[PDI_EP1_PACKET_SIZE];
uint8_t timer_str,timer_rx_off,timer_tx_off,timer_configured; 

void timer_10ms(void) 
{
  if (timer_rx_off!=0) timer_rx_off--;
  else LEDRX=1;
  if (timer_tx_off!=0) timer_tx_off--;
  else LEDTX=1;
  if (!timer_str) {
    timer_str=5;
    ul_stroke(ul_fd);
  } else timer_str--;
  if (timer_configured!=0) timer_configured--;
  else {
    timer_configured=20;
    if (!ulan_configured) {
      LEDRX=LEDTX=0;
      timer_rx_off=timer_tx_off=5;
    }
  }
}
    
int main() {  
#ifndef HAVE_VECT_H
  setup_board();
#endif

  /***********************************/
  // timers 
  lt_10msec_init();

  //********************
  //uLan init
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_init();
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
 #endif
  ul_fd=ul_open(NULL, NULL);
  ul_setmyadr(ul_fd,62);
  //unconfigure ->
  ES_U=0;ulan_configured=0;

  //********************
  // USB init
  memset( &usb_pdi_device, 0, sizeof( usb_pdi_device));
  usb_pdi_device.id = 1;
  #ifndef USB_PDI_DIRECT_FNC
  usb_pdi_device.init = usb_pdi_init;
  #endif /*USB_PDI_DIRECT_FNC*/
  usb_debug_set_level(DEBUG_LEVEL_VERBOSE);
  usb_pdi_device.cntep = 2;
  usb_pdi_device.ep = eps;

  eps[0].max_packet_size = PDI_EP1_PACKET_SIZE;
  eps[1].max_packet_size = PDI_EP1_PACKET_SIZE;
  eps[0].epnum = PDI_EP1_RX;
  eps[1].epnum = PDI_EP1_TX;
  eps[0].event_mask = PDI_INT_EP1_OUT;
  eps[1].event_mask = PDI_INT_EP1_IN;

  usb_pdi_device.vendor_fnc=usb_u2u_vendor;

  usb_init(&usb_pdi_device);
  usb_connect(&usb_pdi_device);

  //********************
  //start
  timer_rx_off=timer_tx_off=timer_str=timer_configured=0;
  ul_dcnv_init(&ep1_dcnv_state,ul_fd,ul_fd);

  EA=1;   // Enable interrupts

  while(1){
  
    if( !(IPDI)) 
      usb_check_events( &usb_pdi_device);
    usb_control_response( &usb_pdi_device);
        
    if (usb_pdi_device.ep_events & MASK_EP1RX) {  //EP1RX
      int size=usb_udev_read_endpoint(&eps[0],ep1_rx_buff,PDI_EP1_PACKET_SIZE);
      ul_dcnv_send(&ep1_dcnv_state,ep1_rx_buff,size);
      usb_pdi_device.ep_events &= ~MASK_EP1RX;
      timer_tx_off=5;LEDTX=0;         //rozsviceni diod pri vysilani 
    }

    if(usb_pdi_device.ep_events & MASK_EP1TX){
      ep1_dcnv_state.rx_wait4host=0;
      usb_pdi_device.ep_events &= ~MASK_EP1TX;
    }

    if (!ep1_dcnv_state.rx_wait4host) { // EP1TX
      int size=ul_dcnv_rec(&ep1_dcnv_state, ep1_tx_buff, PDI_EP1_PACKET_SIZE);
      if (size){
        usb_udev_write_endpoint(&eps[1],ep1_tx_buff,size);
        ep1_dcnv_state.rx_wait4host=1;
      } else {
        if (ul_dcnv_rec_start(&ep1_dcnv_state, ep1_tx_buff,8)==8) { //HEADER
          usb_udev_write_endpoint(&eps[1],ep1_tx_buff,8);
          timer_rx_off=5;LEDRX=0;        //rosviceni diody pri prijmu 
          ep1_dcnv_state.rx_wait4host=1;
        }
      }
    }
    
    /* 10ms timer */
    if (lt_10msec_expired(10)) 
      timer_10ms();
  }
}
