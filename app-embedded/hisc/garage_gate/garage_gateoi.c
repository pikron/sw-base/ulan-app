/*
    HOMEBELL KEYPAD
*/

#include <stdint.h>
#include <uloi_base.h>
#include "garage_gate.h"

#define I_GATE_PUSH	200
#define I_GATE_CLOSE    201
#define I_GATE_OPEN	202
#define I_STATE_CLOSE   240
#define I_STATE_OPEN	241

#define I_HOMEBELL_ADDR 250

unsigned status_val;

int status_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &status_val);
}

int errclr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  status_val=0;
  return 1;
}

/* description of input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOII =
ULOI_GENOBJDES_RAW(ULOI_DOII,NULL,NULL_CODE,NULL,uloi_doii_fnc,(void*)&uloi_objdes_main)
/* description of output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOIO =
ULOI_GENOBJDES_RAW(ULOI_DOIO,NULL,NULL_CODE,NULL,uloi_doio_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOII =
ULOI_GENOBJDES_RAW(ULOI_QOII,NULL,NULL_CODE,NULL,uloi_qoii_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOIO =
ULOI_GENOBJDES_RAW(ULOI_QOIO,NULL,NULL_CODE,NULL,uloi_qoio_fnc,(void*)&uloi_objdes_main)
/* object values read request */
const ULOI_CODE uloi_objdes_t uloid_objdes_RDRQ =
ULOI_GENOBJDES_RAW(ULOI_RDRQ,NULL,NULL_CODE,NULL,uloi_rdrq_fnc,(void*)&uloi_objdes_main)

ULOI_GENOBJDES(STATUS,ULOI_STATUS,"u2",status_rdfnc,&status_val,NULL_CODE,NULL)
ULOI_GENOBJDES(ERRCLR,ULOI_ERRCLR,"e",NULL_CODE,NULL,errclr_wrfnc,&status_val)
ULOI_GENOBJDES(GATE_PUSH,I_GATE_PUSH,"u2",NULL_CODE,NULL,uloi_gate_push_wrfnc,&oi_gate_push)
ULOI_GENOBJDES(GATE_CLOSE,I_GATE_CLOSE,"u2",NULL_CODE,NULL,uloi_gate_close_wrfnc,&oi_gate_close)
ULOI_GENOBJDES(GATE_OPEN,I_GATE_OPEN,"u2",NULL_CODE,NULL,uloi_gate_open_wrfnc,&oi_gate_open)
ULOI_GENOBJDES(STATE_CLOSE,I_STATE_CLOSE,"u2",uloi_uint_rdfnc,&oi_state_close,NULL_CODE,NULL)
ULOI_GENOBJDES(STATE_OPEN,I_STATE_OPEN,"u2",uloi_uint_rdfnc,&oi_state_open,NULL_CODE,NULL)
ULOI_GENOBJDES(HOMEBELL_ADDR,I_HOMEBELL_ADDR,"u2",uloi_uint_rdfnc,&oi_homebell_addr,uloi_homebell_addr_wrfnc,&oi_homebell_addr)

const uloi_objdes_t * ULOI_CODE uloi_objdes_main_items[]={
  &uloid_objdes_DOII,
  &uloid_objdes_DOIO,
  &uloid_objdes_QOII,
  &uloid_objdes_QOIO,
  &uloid_objdes_RDRQ,

  &uloid_objdes_STATUS,
  &uloid_objdes_ERRCLR,
  &uloid_objdes_GATE_PUSH,
  &uloid_objdes_GATE_CLOSE,
  &uloid_objdes_GATE_OPEN,
  &uloid_objdes_STATE_CLOSE,
  &uloid_objdes_STATE_OPEN,
  &uloid_objdes_HOMEBELL_ADDR,
};

const ULOI_CODE uloi_objdes_array_t uloi_objdes_main={
  {
    (uloi_objdes_t **)uloi_objdes_main_items,
    sizeof(uloi_objdes_main_items)/sizeof(uloi_objdes_main_items[0]),
    -1
  }
};
