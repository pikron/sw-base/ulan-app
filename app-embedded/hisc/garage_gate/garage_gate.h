#ifndef _KEYPAD_H
#define _KEYPAD_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

extern const uloi_objdes_array_t uloi_objdes_main;

extern unsigned int oi_gate_push;
extern unsigned int oi_gate_close;
extern unsigned int oi_gate_open;
extern unsigned int oi_state_close;
extern unsigned int oi_state_open;
extern unsigned int oi_homebell_addr;

int uloi_gate_push_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_gate_close_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_gate_open_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_homebell_addr_wrfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _KEYPAD_H */

