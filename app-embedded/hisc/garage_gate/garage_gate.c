/*
    GARAGE GATE
*/

#include <stdio.h>
#include <system_def.h>
#include <cpu_def.h>
#include <ul_lib/ulan.h>
#include <keyval_id.h>
#include <uloi_base.h>
#include <lt_timer.h>
#include <uldy_base.h>
#include <keyval_id_his.h>
#if !defined(SDCC) && !defined(__SDCC)
  #include <keyval_loc.h>
  #include <lpciap_kvpb.h>
  #include <hal_machperiph.h>
#endif
#include "garage_gate.h"
#include "ul_idstr.h"

LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)

/***********************************/
// global variables
kvpb_block_t kvpb_block_global;
uloi_con_ulan_t uloi_con_ulan_global;
ul_dyac_t ul_dyac_global;
int tgate_off;
int tgate_close;

#ifndef UL_WITHOUT_HANDLE
kvpb_block_t *kvpb_block=&kvpb_block_global;
uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
ul_dyac_t *ul_dyac=&ul_dyac_global;
ul_fd_t ul_fd;
ul_fd_t ul_fd1;
ul_msginfo msginfo;
#endif /* UL_WITHOUT_HANDLE */

/* ulan variables */
unsigned char ubuff[30];
unsigned int  DATA uaddr;
unsigned long DATA usn;
uchar ustatus;

//object interface variables
unsigned int oi_gate_push;
unsigned int oi_gate_close;
unsigned int oi_gate_open;
unsigned int oi_state_close;
unsigned int oi_state_open;
unsigned int oi_homebell_addr;

/****************************************************************************/
int uloi_gate_push_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  if (oi_gate_push==1) {
    CLR_OUT_PIN(OUT_PORT,P1_26_GATE);
    tgate_off=5;
  }
  return 1;
}

/****************************************************************************/
int uloi_gate_close_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  if ((oi_gate_close==1) && (oi_state_close==0)) {
    CLR_OUT_PIN(OUT_PORT,P1_26_GATE);
    tgate_off=5;
  }
  return 1;
}

/****************************************************************************/
int uloi_gate_open_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  if ((oi_gate_open==1) && (oi_state_close!=0)) {
    CLR_OUT_PIN(OUT_PORT,P1_26_GATE);
    tgate_off=5;
  }
  return 1;
}

/****************************************************************************/
int uloi_homebell_addr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_HOMEBELL_ADDR,sizeof(unsigned int),&oi_homebell_addr);
  return 1;
}

/***********************************/
char ul_save_sn(uint32_t usn)
{
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  return 0;
}

/***********************************/
char ul_save_adr(uint8_t uaddr)
{
  unsigned int v=uaddr;
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&v);
  return 0;
}

void sys_err()
{
  while(1);
}

int main(void) 
{

  /***********************************/
  // timer
  lt_100msec_init();

  /***********************************/
  //init values
  tgate_off=0;
  tgate_close=0;

  /***********************************/
  // kvpb init
  kvpb_block->base=(uint8_t CODE*)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DEFAULT_FLAGS;
 #ifndef  KVPB_MINIMALIZED
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=kvpb_erase;
  kvpb_block->copy=kvpb_copy;
  kvpb_block->flush=kvpb_flush;
 #endif
  if (kvpb_check(kvpb_block,1)<0) sys_err();

  // read data from kvpb
  uaddr=62;
  usn=0L;
  oi_gate_push=0;
  oi_gate_close=0;
  oi_gate_open=0;
  oi_homebell_addr=0;
  kvpb_get_key(kvpb_block,KVPB_KEYID_HOMEBELL_ADDR,sizeof(unsigned int),&oi_homebell_addr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);

  //********************
  // uLan init
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_init();
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
 #endif
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) sys_err();    
 #ifndef UL_WITHOUT_HANDLE
  ul_fd1=ul_open(NULL,NULL);
  if (ul_fd1==UL_FD_INVALID) sys_err();    
 #endif
  ul_setidstr(ul_fd,ul_idstr);
  ul_setmyadr(ul_fd,uaddr);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(ul_fd,&msginfo);
  ul_stroke(ul_fd);

  /***********************************/
  // uLan object interface init
 #ifndef UL_WITHOUT_HANDLE
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #else
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, UL_FD_INVALID);
 #endif /*UL_WITH_HANDLE*/

  /***********************************/
  // uLan dyac init
  uldy_init(ul_dyac,ul_fd,ul_save_sn,ul_save_adr,(char*)ul_idstr,usn);

  //********************
  // start
 #if defined(SDCC) || defined(__SDCC) 
  EA=1;       // Enable interrupts
 #endif

  while (1) {

    /* processing of ulan messages */
    if ((ul_inepoll(ul_fd)>0) && (ul_acceptmsg(ul_fd, &msginfo)>=0)) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo (uloi_objdes_array_t*)&uloi_objdes_main, &msginfo)<0) {
          if (uldy_process_msg(ULDY_ARG_ul_dyac &msginfo)<0) {
            ul_freemsg(ul_fd);
          }
        }
      } else 
        ul_freemsg(ul_fd);
    }
    
    if (GET_IN_PIN(IO1,P1_30_DOOR_CLOSE))
      oi_state_close=0;
    else
      oi_state_close=1;


    if (GET_IN_PIN(IO1,P1_31_DOOR_OPEN))
      oi_state_open=0;
    else
      oi_state_open=1;

    /* 100ms timer */
    if (lt_100msec_expired(100)) {
    
     if (tgate_off) {
       tgate_off--;    
       if (!tgate_off) {
         SET_OUT_PIN(OUT_PORT,P1_26_GATE);
       }
     }

     if (oi_state_close==0) {
       tgate_close++;
       if (tgate_close>=(60*60*10)) {
         tgate_close=0;
         if (oi_homebell_addr) {
           ubuff[0]=UL_CMD_OISV+1;
           ubuff[1]=0x41;
           ubuff[2]=0;
           ubuff[3]=220; //oid=220
           ubuff[4]=0;
           ubuff[5]=1;   //value=1
           ubuff[6]=0;
           msginfo.cmd=UL_CMD_OISV;
           msginfo.flg=UL_BFL_SND | UL_BFL_ARQ;
           msginfo.dadr=oi_homebell_addr;
           ul_newmsg(ul_fd,&msginfo);
           ul_write(ul_fd,ubuff,7);
           ul_freemsg(ul_fd);
         }
       }
     } else {
       tgate_close=0;
     }

      /* watchdog */
     #ifdef WATCHDOG_ENABLED
      watchdog_feed();
     #endif /* WATCHDOG_ENABLED */
    
      /* ulan stroke */
     #if defined(SDCC) || defined(__SDCC) 
      ul_stroke(ul_fd);
     #endif
    }
  }
  return 0;
}
