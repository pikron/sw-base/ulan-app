/*
    WEATHER_STATION
*/

#include <stdint.h>
#include <uloi_base.h>
#include "weather_station.h"

unsigned status_val;

int status_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &status_val);
}

int errclr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  status_val=0;
  return 1;
}

/* description of input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOII =
ULOI_GENOBJDES_RAW(ULOI_DOII,NULL,NULL_CODE,NULL,uloi_doii_fnc,(void*)&uloi_objdes_main)
/* description of output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOIO =
ULOI_GENOBJDES_RAW(ULOI_DOIO,NULL,NULL_CODE,NULL,uloi_doio_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOII =
ULOI_GENOBJDES_RAW(ULOI_QOII,NULL,NULL_CODE,NULL,uloi_qoii_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOIO =
ULOI_GENOBJDES_RAW(ULOI_QOIO,NULL,NULL_CODE,NULL,uloi_qoio_fnc,(void*)&uloi_objdes_main)
/* object values read request */
const ULOI_CODE uloi_objdes_t uloid_objdes_RDRQ =
ULOI_GENOBJDES_RAW(ULOI_RDRQ,NULL,NULL_CODE,NULL,uloi_rdrq_fnc,(void*)&uloi_objdes_main)

ULOI_GENOBJDES(STATUS,ULOI_STATUS,"u2",status_rdfnc,&status_val,NULL_CODE,NULL)
ULOI_GENOBJDES(ERRCLR,ULOI_ERRCLR,"e",NULL_CODE,NULL,errclr_wrfnc,&status_val)
ULOI_GENOBJDES(LIGHTING,I_LIGHTING,"u2",uloi_uint_rdfnc,&oi_lighting,NULL_CODE,NULL)
ULOI_GENOBJDES(TEMP_CPU,I_TEMP_CPU,"u2",uloi_int_rdfnc,&oi_temp_cpu,NULL_CODE,NULL)
ULOI_GENOBJDES(TEMP,I_TEMP,"u2",uloi_int_rdfnc,&oi_temp,NULL_CODE,NULL)
ULOI_GENOBJDES(PRESS,I_PRESS,"u2",uloi_int_rdfnc,&oi_press,NULL_CODE,NULL)
ULOI_GENOBJDES(WIND_SPEED_MIN,I_WIND_SPEED_MIN,"u2",uloi_int_rdfnc,&oi_wind_speed_min,NULL_CODE,NULL)
ULOI_GENOBJDES(WIND_SPEED_HOUR,I_WIND_SPEED_HOUR,"u2",uloi_int_rdfnc,&oi_wind_speed_hour,NULL_CODE,NULL)

const uloi_objdes_t * ULOI_CODE uloi_objdes_main_items[]={
  &uloid_objdes_DOII,
  &uloid_objdes_DOIO,
  &uloid_objdes_QOII,
  &uloid_objdes_QOIO,
  &uloid_objdes_RDRQ,

  &uloid_objdes_STATUS,
  &uloid_objdes_ERRCLR,
  &uloid_objdes_LIGHTING,
  &uloid_objdes_TEMP_CPU,
  &uloid_objdes_TEMP,
  &uloid_objdes_PRESS,
  &uloid_objdes_WIND_SPEED_MIN,
  &uloid_objdes_WIND_SPEED_HOUR,
};

const ULOI_CODE uloi_objdes_array_t uloi_objdes_main={
  {
    uloi_objdes_main_items,
    sizeof(uloi_objdes_main_items)/sizeof(uloi_objdes_main_items[0]),
    -1
  }
};
