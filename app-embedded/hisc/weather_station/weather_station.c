#include <system_def.h>
#include <string.h>
#include <uloi_base.h>
#include <msc_adc.h>
#include <vect.h>
#include "kty.h"
//#include <keyval_id.h>
//#include <keyval_id_his.h>
#include <lt_timer.h>
#include "weather_station.h"

LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)

uloi_con_ulan_t uloi_con_ulan_global;
//kvpb_block_t kvpb_block_global;

uchar ubuff[7];

//ulan variables
//uint8_t DATA uaddr;

//object interface variables
uint16_t oi_lighting;
int16_t oi_temp_cpu;
int16_t oi_temp;
uint16_t oi_press;
uint16_t oi_wind_speed_min;
uint16_t oi_wind_speed_hour;

uint16_t wind_measure_timer_min;
uint16_t wind_measure_cnt;
uint16_t wind_measure_max;
uint8_t  wind_measure_timer_hour;
uint16_t wind_speed_hour_max;

void wind_int(void) interrupt 
{
  uint8_t exif_old;
  exif_old=EXIF;
  EXIF=0;
  if (exif_old&0x80) {
    wind_measure_cnt++;
  }
} 

int main()
{
 int tmin=0;
 #ifndef UL_WITHOUT_HANDLE
  uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
//  kvpb_block_t *kvpb_block=&kvpb_block_global;
  ul_msginfo msginfo;
 #endif  /*UL_WITHOUT_HANDLE*/
  msc_adc_input_t msc_adc_input_temp_cpu={0xFF,0x41,0,30,0,5};
  msc_adc_input_t msc_adc_input_lighting={0x08,0x41,0,30,0,5};
  msc_adc_input_t msc_adc_input_temp={0x18,0x41,0,30,0,5};
  msc_adc_input_t msc_adc_input_press={0x28,0x41,0,30,0,5};
  msc_adc_input_t *msc_adc_input_defaults[]={
    &msc_adc_input_temp_cpu,
    &msc_adc_input_lighting,
    &msc_adc_input_temp,
    &msc_adc_input_press
  };
  msc_adc_input_list_t msc_adc_input_list={
    sizeof(msc_adc_input_defaults)/sizeof(msc_adc_input_defaults[0]),
    -1,
    msc_adc_input_defaults,
    0
  };

  /***********************************/
  // variables
  wind_measure_timer_min=0;
  wind_measure_timer_hour=0;
  wind_measure_cnt=0;
  wind_speed_hour_max=0;

  /***********************************/
  // timers 
  lt_100msec_init();

  //********************
  // kvpb init
/*  kvpb_block->base=(CODE uint8_t*)KVPB_BASE;
  kvpb_block->size=KVPB_SIZE;
  kvpb_block->flags=KVPB_DESC_DOUBLE;
  if(kvpb_check(kvpb_block,1)<0) 
    while(1);

  // read data from kvpb
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,1,&uaddr);
*/

  //********************
  // uLan init
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_init();
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
 #endif
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) while(1);    
  ul_setmyadr(ul_fd,10);

  /***********************************/
  // uLan object interface init
 #ifndef UL_WITHOUT_HANDLE
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #else
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, UL_FD_INVALID);
 #endif /*UL_WITH_HANDLE*/

  //********************
  // start
 #if defined(SDCC) || defined(__SDCC) 
  EA=1;            // Enable interrupts
 #endif
 
  msc_adc_set_input(0xff,0x41); 

  vec_set(wind_int,0x5B);  //INT5
  EX5=1;

  while(1){
  
    /* processing of ulan messages */
    if (ul_acceptmsg(ul_fd, &msginfo)>=0) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo &uloi_objdes_main, &msginfo)<0) {
          ul_freemsg(ul_fd);       
        }
      } else 
        ul_freemsg(ul_fd);
    }

    if (msc_adc_ready()) {
      msc_adc_input_t *msc_adc_input;
      unsigned long lvalue;
      float value;
      
      msc_adc_input=msc_adc_input_list.inputs[msc_adc_input_list.input_position];
      lvalue=msc_adc_unipolar();
      if (msc_adc_input->adcon1&0x40) 
        value=(float)lvalue*MSC_ADC_LSB_UNIPOLAR;    
      else
        value=(float)lvalue*MSC_ADC_LSB_BIPOLAR;    
      msc_adc_input->samples_cnt++;
      if(msc_adc_input->samples_cnt>msc_adc_input->calibration) {
        msc_adc_input->avg_value=(value+msc_adc_input->avg_value)/2;
      }
      //new stored value
      if (msc_adc_input->samples_cnt>msc_adc_input->samples) {
        msc_adc_input->samples_cnt=0;
        //update values for oi
        switch (msc_adc_input_list.input_position) {
  	  case 0:
            oi_temp_cpu=MSC_ADC_V2T_INTERNAL(msc_adc_input->avg_value);
	    break;
          case 1:
            oi_lighting=(msc_adc_input->avg_value/2.5)*100.0;
	    break;
          case 2:{
            uint16_t r;
            r=4690*(msc_adc_input->avg_value/(2.5-msc_adc_input->avg_value));
            oi_temp=kty81_2_r2t(r);
	    };break;
          case 3:{
            float error;
            /* Vout = VS x (0.009 x P - 0.095) ± (Pressure Error x Temp. Factor x 0.009 x VS) */
	    /* VS = 5.0 ± 0.25 Vdc -> for me 2.48V */
            error=0*1*0.009*4.97;
            oi_press=(((msc_adc_input->avg_value*2.016/4.97+error)+0.095)/0.009) * 10;
            };break;
	  default:break;
        }
        msc_adc_input_list.input_position++;
        if (msc_adc_input_list.input_position==msc_adc_input_list.count) {
          msc_adc_input_list.input_position=0;
        }
        msc_adc_input=msc_adc_input_list.inputs[msc_adc_input_list.input_position];
        msc_adc_set_input(msc_adc_input->ain,msc_adc_input->adcon1); 
      }      
    }
    
    /* 100ms timer */
    if (lt_100msec_expired(100)) {

      wind_measure_timer_min++;
      if (wind_measure_timer_min>=600) {
        wind_measure_timer_min=0;
        EA=0;
        oi_wind_speed_min=wind_measure_cnt/100;
        wind_measure_cnt=0;
        EA=1;
        if (oi_wind_speed_min>wind_speed_hour_max)
          wind_speed_hour_max=oi_wind_speed_min;

        wind_measure_timer_hour++;
        if (wind_measure_timer_hour>=60) {
          oi_wind_speed_hour=wind_speed_hour_max;
          wind_measure_timer_hour=0;
          wind_speed_hour_max=0;
        }

      }

      /* 1min timer */
      if (++tmin==600) {
        tmin=0;
        ubuff[0]=UL_CMD_OISV+1;
        ubuff[1]=0x41;
        ubuff[2]=0;
        ubuff[3]=0xe6;
        ubuff[4]=0;
        ubuff[5]=oi_lighting;
        ubuff[6]=oi_lighting>>8;
        msginfo.cmd=UL_CMD_OISV;
        msginfo.flg=UL_BFL_SND;
        msginfo.dadr=0;
        ul_newmsg(ul_fd,&msginfo);
        ul_write(ul_fd,ubuff,7);
        ul_freemsg(ul_fd);
      }

      /* ulan stroke */
      ul_stroke(ul_fd);
    }
  }
}
