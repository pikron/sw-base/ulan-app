#ifndef _WEATHER_STATION_H
#define _WEATHER_STATION_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

#define I_LIGHTING 230
#define I_TEMP_CPU 231
#define I_TEMP 232
#define I_PRESS 233
#define I_WIND_SPEED_MIN 234
#define I_WIND_SPEED_HOUR 235

extern const ULOI_CODE uloi_objdes_array_t uloi_objdes_main;

//object interface variables
extern uint16_t oi_lighting;
extern int16_t oi_temp_cpu;
extern int16_t oi_temp;
extern uint16_t oi_press;
extern uint16_t oi_wind_speed_min;
extern uint16_t oi_wind_speed_hour;

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _WEATHER_STATION_H */

