#ifndef _KTY_H
#define _KTY_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

typedef struct kty {
  int8_t temp_base;
  uint16_t r_base;
  } kty_t;

int8_t kty81_2_r2t(uint16_t r);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _WEATHER_STATION_H */

