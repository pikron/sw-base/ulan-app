#include <cpu_def.h>
#include "kty.h"

CODE kty_t kty81_2[]={
    {-40,1135},
    {-30,1247},
    {-20,1367},
    {-10,1495},
    {1,1630},
    {10,1772},
    {20,1922},
    {30,2080},
    {40,2245},
    {50,2417},
    {60,2597},
    {70,2785}
  };

int8_t kty81_2_r2t(uint16_t r) 
{
  int8_t i;
  i=sizeof(kty81_2)/sizeof(kty_t)-2;
  while(i!=0) {
    if (kty81_2[i].r_base<=r) {
      uint8_t k,td;
      td=kty81_2[i+1].temp_base-kty81_2[i].temp_base;
      k=(uint16_t)(kty81_2[i+1].r_base-kty81_2[i].r_base)*10/td;
      i=kty81_2[i].temp_base+(uint16_t)((r-kty81_2[i].r_base)*10/k);
      return i;
    }
    i--;
  }
  return 0;
}
