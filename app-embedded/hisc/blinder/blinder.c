/*
    BLINDER
*/

#include <system_def.h>
#include <ul_lib/ulan.h>
#include <vect.h>
#include <keyval_id.h>
#include <keyval_id_his.h>
#include <uloi_base.h>
#include <lt_timer.h>
#include "blinder.h"

LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)

kvpb_block_t kvpb_block_global;
uloi_con_ulan_t uloi_con_ulan_global;

#ifndef UL_WITHOUT_HANDLE
kvpb_block_t *kvpb_block=&kvpb_block_global;
uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
ul_msginfo msginfo;
#endif  /*UL_WITHOUT_HANDLE*/

//ulan variables
unsigned int DATA uaddr;
unsigned long DATA usn;
//timers
uint8_t DATA tnew_cmd;
int DATA tblinder_off,tblinder_off_start,tblinder_off_extra;
//blinder variables
uint16_t DATA new_bl_pos;

//object interface variables
unsigned int DATA oi_lighting;
unsigned int DATA oi_bl_pos;
unsigned int DATA oi_open_time;
unsigned int DATA oi_bl_stop;
unsigned int DATA oi_blp_enabled;
unsigned int DATA oi_blp_up;
unsigned int DATA oi_blp_down;

void bl_set_pos(uint8_t pos)
{
  new_bl_pos=pos;
  if (new_bl_pos>100) new_bl_pos=100;
  if (tnew_cmd==0) {
    OUTB(OUT_PORT,0xff); //stop
    tnew_cmd=10;
    if (tblinder_off)   //set new actual possition when blinder was runnig
      oi_bl_pos+=(100*(tblinder_off_start-tblinder_off))/oi_open_time;
    tblinder_off_extra=0;
  }
}

int oi_bl_pos_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uint16_t npos;
  uloi_uint_wrfnc(ULOI_ARG_coninfo &npos);
  bl_set_pos(npos);
  return 1;
}

int oi_open_time_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_OPENTIME,sizeof(unsigned int),context);
  return 1;
}

int oi_blp_up_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_BLP_UP,sizeof(unsigned int),context);
  return 1;
}

int oi_blp_down_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_BLP_DOWN,sizeof(unsigned int),context);
  return 1;
}

void main(void) 
{
  uint16_t DATA blp_last;

  /***********************************/
  // timers 
  lt_100msec_init();

  /***********************************/
  //init values
  tblinder_off=0;
  tblinder_off_extra=0;
  tnew_cmd=0;
  uaddr=25;
  usn=0L;
  oi_bl_pos=0;
  oi_open_time=160;
  oi_bl_stop=0;
  oi_blp_enabled=1;
  oi_blp_up=0;
  oi_blp_down=0;
  blp_last=0xffff; //impossible value

  //********************
  // kvpb init
  kvpb_block->base=(CODE uint8_t*)KVPB_BASE;
  kvpb_block->size=KVPB_SIZE;
  kvpb_block->flags=KVPB_DESC_DOUBLE;
  if(kvpb_check(kvpb_block,1)<0) 
    while(1);

  // read data from kvpb
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,sizeof(unsigned int),&oi_bl_pos);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLINDER_OPENTIME,sizeof(unsigned int),&oi_open_time);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLP_UP,sizeof(unsigned int),&oi_blp_up);
  kvpb_get_key(kvpb_block,KVPB_KEYID_BLP_DOWN,sizeof(unsigned int),&oi_blp_down);

  //********************
  // uLan init
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_init();
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
 #endif
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) while(1);    
  ul_setmyadr(ul_fd,uaddr);

  /***********************************/
  // uLan object interface init
 #ifndef UL_WITHOUT_HANDLE
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #else
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, UL_FD_INVALID);
 #endif /*UL_WITH_HANDLE*/

  //********************
  // start
 #if defined(SDCC) || defined(__SDCC)
  EA=1;         // Enable interrupt
 #endif
  while (1) {
    
    /* processing of ulan messages */
    if (ul_acceptmsg(ul_fd, &msginfo)>=0) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo &uloi_objdes_main, &msginfo)<0) {
          ul_freemsg(ul_fd);
        } 
      } else {
        ul_freemsg(ul_fd);
      }
    }
    
    /* 100ms timer */
    if (lt_100msec_expired(100)) {

      /* ulan stroke */
      ul_stroke(ul_fd);

      /* timers */
      if (tblinder_off!=0) {
        if (tblinder_off>0) tblinder_off--;
        else tblinder_off++;
        if (tblinder_off==0) {
          oi_bl_pos=new_bl_pos;
          kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,2,&oi_bl_pos);
          if (tblinder_off_extra==0) {
            OUTB(OUT_PORT,0xff); //stop
            LED_GP=1;
          }
        }
      } else {
        if (tblinder_off_extra) {
          if (tblinder_off_extra>0) tblinder_off_extra--;
          else tblinder_off_extra++;
          if (tblinder_off_extra==0) {
            OUTB(OUT_PORT,0xff); //stop
            LED_GP=1;
          }
        }
      }

      /* stop command */
      if (oi_bl_stop) {
        oi_bl_stop=0;
        if (tblinder_off) {   //set new actual possition when blinder was running
          oi_bl_pos+=(100*(tblinder_off_start-tblinder_off))/oi_open_time;
          tblinder_off=0;
          kvpb_set_key(kvpb_block,KVPB_KEYID_BLINDER_POSSITION,2,&oi_bl_pos);
        }
        tblinder_off_extra=0;
        OUTB(OUT_PORT,0xff); //stop
        LED_GP=1;
      }

      /* new possition */
      if (tnew_cmd) {
        tnew_cmd--;
        if (!tnew_cmd) {
          tblinder_off=((int16_t)(new_bl_pos-oi_bl_pos)*(int16_t)oi_open_time)/100;
          tblinder_off_start=tblinder_off;
          if (new_bl_pos==100)
            tblinder_off_extra=30;
          if (new_bl_pos==0)
            tblinder_off_extra=-30;
          /* set outputs */
          if ((tblinder_off>0) || (tblinder_off_extra>0)) 
            OUTB(OUT_PORT,0x7f);     //close
          if ((tblinder_off<0) || (tblinder_off_extra<0))
            OUTB(OUT_PORT,0xfe);     //open
          if ((tblinder_off) || (tblinder_off_extra))
            LED_GP=0;
        }
      }

      /* blps */
      if ((!tblinder_off_extra) && (!tblinder_off) && 
          (!tnew_cmd) && (oi_lighting) && (oi_blp_enabled)) {
        uint8_t DATA blp;
        uint8_t DATA l;

        if (oi_blp_up) {
          l=oi_blp_up>>8;
          blp=oi_blp_up;
          if (oi_lighting>l) {
            if (blp!=blp_last) { 
              blp_last=blp;
              bl_set_pos(blp);
            }
          }
        }

        if (oi_blp_down) {
          l=oi_blp_down>>8;
          blp=oi_blp_down;
          if (oi_lighting<l) {
            if (blp!=blp_last) { 
              blp_last=blp;
              bl_set_pos(blp);
            }
          }
        }
      }
    }
  }
}
