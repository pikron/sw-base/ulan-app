#ifndef _BLINDER_H
#define _BLINDER_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>
#include <cpu_def.h>

extern const ULOI_CODE uloi_objdes_array_t uloi_objdes_main;
extern unsigned int DATA oi_lighting;
extern unsigned int DATA oi_bl_pos;
extern unsigned int DATA oi_open_time;
extern unsigned int DATA oi_bl_stop;
extern unsigned int DATA oi_blp_enabled;
extern unsigned int DATA oi_blp_up;
extern unsigned int DATA oi_blp_down;

int oi_bl_pos_wrfnc(ULOI_PARAM_coninfo void *context);
int oi_open_time_wrfnc(ULOI_PARAM_coninfo void *context);
int oi_blp_up_wrfnc(ULOI_PARAM_coninfo void *context);
int oi_blp_down_wrfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _BLINDER_H */

