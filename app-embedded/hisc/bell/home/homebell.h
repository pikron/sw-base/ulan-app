#ifndef _HOMEBELL_H
#define _HOMEBELL_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>
#include <cpu_def.h>

extern const ULOI_CODE uloi_objdes_array_t uloi_objdes_main;

extern unsigned int DATA oi_song_play;

int oi_song_play_wrfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _HOMEBELL_H */

