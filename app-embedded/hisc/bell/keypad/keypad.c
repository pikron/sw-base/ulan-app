/*
    HOMEBELL KEYPAD
*/

#include <stdio.h>
#include <system_def.h>
#include <cpu_def.h>
#include <ul_lib/ulan.h>
#include <keyval_id.h>
#include <uloi_base.h>
#include <lt_timer.h>
#include <uldy_base.h>
#if !defined(SDCC) && !defined(__SDCC)
  #include <keyval_loc.h>
  #include <lpciap_kvpb.h>
  #include <hal_machperiph.h>
  #include <kbd.h>
#endif
#include "keypad.h"
#include "ul_idstr.h"

LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)
LT_TIMER_DEC(lt_1msec)
LT_TIMER_IMP(lt_1msec)

/***********************************/
// global variables
kvpb_block_t kvpb_block_global;
uloi_con_ulan_t uloi_con_ulan_global;
ul_dyac_t ul_dyac_global;
unsigned char kbuff[30];
int kbuff_ptr;

#ifndef UL_WITHOUT_HANDLE
kvpb_block_t *kvpb_block=&kvpb_block_global;
uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
ul_dyac_t *ul_dyac=&ul_dyac_global;
ul_fd_t ul_fd;
ul_fd_t ul_fd1;
ul_msginfo msginfo;
#endif /* UL_WITHOUT_HANDLE */

/* ulan variables */
unsigned char ubuff[30];
unsigned int  DATA uaddr;
unsigned long DATA usn;
uchar ustatus;

//object interface variables
unsigned int oi_access_code;
unsigned int oi_ring_sign;

/****************************************************************************/
int uloi_access_code_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo context);
  kvpb_set_key(kvpb_block,KVPB_KEYID_HIS_ACCESS_CODE,sizeof(unsigned int),context);
  return 1;
}

/****************************************************************************/
int uloi_ring_sign_rdfnc(ULOI_PARAM_coninfo void *context)
{
  int r=uloi_uint_rdfnc(ULOI_ARG_coninfo context);
  oi_ring_sign=0;
  return r;
}


/***********************************/
char ul_save_sn(uint32_t usn)
{
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  return 0;
}

/***********************************/
char ul_save_adr(uint8_t uaddr)
{
  unsigned int v=uaddr;
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&v);
  return 0;
}

void sys_err()
{
  while(1);
}

int main(void) 
{
  kbd_key_t buf;
  kbd_keymod_t modifiers;
  kbd_scan_code_t scancode;
  int r,ring_key,sound_div,tsound,taccess;

  /***********************************/
  // timer
  lt_100msec_init();
  lt_1msec_init();

  /***********************************/
  //init values

  /***********************************/
  // kvpb init
  kvpb_block->base=(uint8_t CODE*)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DEFAULT_FLAGS;
 #ifndef  KVPB_MINIMALIZED
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=kvpb_erase;
  kvpb_block->copy=kvpb_copy;
  kvpb_block->flush=kvpb_flush;
 #endif
  if (kvpb_check(kvpb_block,1)<0) sys_err();

  // read data from kvpb
  uaddr=62;
  usn=0L;
  oi_access_code=641;
  oi_ring_sign=0;
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  kvpb_get_key(kvpb_block,KVPB_KEYID_HIS_ACCESS_CODE,sizeof(unsigned int),&oi_access_code);

  //********************
  // uLan init
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_init();
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
 #endif
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) sys_err();    
 #ifndef UL_WITHOUT_HANDLE
  ul_fd1=ul_open(NULL,NULL);
  if (ul_fd1==UL_FD_INVALID) sys_err();    
 #endif
  ul_setidstr(ul_fd,ul_idstr);
  ul_setmyadr(ul_fd,uaddr);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(ul_fd,&msginfo);
  ul_stroke(ul_fd);

  /***********************************/
  // uLan object interface init
 #ifndef UL_WITHOUT_HANDLE
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #else
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, UL_FD_INVALID);
 #endif /*UL_WITH_HANDLE*/

  /***********************************/
  // uLan dyac init
  uldy_init(ul_dyac,ul_fd,ul_save_sn,ul_save_adr,(char*)ul_idstr,usn);

  //********************
  // start
 #if defined(SDCC) || defined(__SDCC) 
  EA=1;       // Enable interrupts
 #endif

  kbd_Open(0);
  ring_key=0;

  sound_div=0;tsound=0;
  kbuff_ptr=0;
  taccess=0;
  while (1) {

    /* processing of ulan messages */
    if ((ul_inepoll(ul_fd)>0) && (ul_acceptmsg(ul_fd, &msginfo)>=0)) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo (uloi_objdes_array_t*)&uloi_objdes_main, &msginfo)<0) {
          if (uldy_process_msg(ULDY_ARG_ul_dyac &msginfo)<0) {
            ul_freemsg(ul_fd);
          }
        }
      } else 
        ul_freemsg(ul_fd);
    }

    /* keyboard */
    r=kbd_Read(&buf, &modifiers, &scancode);
    /* bell */
    if (((r==1) && (!ring_key)) && ((buf=='A') || (buf=='B') || (buf=='C'))) {
      oi_ring_sign=1;
      tsound=50;
      CLR_OUT_PIN(LED_PORT,P0_30_LED_R);
      ring_key=1;
      ubuff[0]=UL_CMD_OISV+1;
      ubuff[1]=0x41;
      ubuff[2]=0;
      ubuff[3]=210;
      ubuff[4]=0;
      ubuff[5]=1;
      ubuff[6]=0;
      msginfo.cmd=UL_CMD_OISV;
      msginfo.flg=UL_BFL_SND | UL_BFL_ARQ;
      msginfo.dadr=40;
      ul_newmsg(ul_fd,&msginfo);
      ul_write(ul_fd,ubuff,7);
      ul_freemsg(ul_fd);
    }
    if (((r==2) && (ring_key)) && ((buf=='A') || (buf=='B') || (buf=='C'))) {
      ring_key=0;
    }
    /* all keys */
    if ((r==1) && (!ring_key)) {
      tsound=50;
      CLR_OUT_PIN(LED_PORT,P0_30_LED_R);
      kbuff[kbuff_ptr]=buf;
      if (kbuff_ptr<sizeof(kbuff)) kbuff_ptr++;
      if (buf==13) {
        int unit,oid,val,ret=0;
        kbuff[kbuff_ptr]=0;
	/* test for access command */
        ret=sscanf(kbuff,"1%d\n",&val);
        if ((ret==1) && (val==oi_access_code)) {
          SET_OUT_PIN(OUT_PORT,P1_16_RELE);
	  taccess=5000;
          CLR_OUT_PIN(LED_PORT,P0_31_LED_G);
        }
	/* test for oid value setting command */
        ret=sscanf(kbuff,"9%d*%d*%d\n",&unit,&oid,&val);
	if (ret==3) {
          ubuff[0]=UL_CMD_OISV+1;
          ubuff[1]=0x41;
          ubuff[2]=0;
          ubuff[3]=oid;
          ubuff[4]=oid>>8;
          ubuff[5]=val;
          ubuff[6]=val>>8;
          msginfo.cmd=UL_CMD_OISV;
          msginfo.flg=UL_BFL_SND | UL_BFL_ARQ;
          msginfo.dadr=unit;
          ul_newmsg(ul_fd,&msginfo);
          ul_write(ul_fd,ubuff,7);
          ul_freemsg(ul_fd);
        }
        kbuff_ptr=0;
      }
    }

    /* 1ms timer */
    if (lt_1msec_expired(1)) {
      if (tsound) {
        tsound--;
        if (!tsound) {
          CLR_OUT_PIN(IO0,P0_29_SOUND);
          SET_OUT_PIN(LED_PORT,P0_30_LED_R);
        } else {
          sound_div++;
          if (sound_div&1) {
            SET_OUT_PIN(IO0,P0_29_SOUND);
          } else {
            CLR_OUT_PIN(IO0,P0_29_SOUND);
          }
        }
      }
      if (taccess) {
        taccess--;
        if (!taccess) {
          CLR_OUT_PIN(OUT_PORT,P1_16_RELE);
          SET_OUT_PIN(LED_PORT,P0_31_LED_G);
        }
      }
    }

    /* 100ms timer */
    if (lt_100msec_expired(100)) {

      /* watchdog */
     #ifdef WATCHDOG_ENABLED
      watchdog_feed();
     #endif /* WATCHDOG_ENABLED */
    
      /* ulan stroke */
     #if defined(SDCC) || defined(__SDCC) 
      ul_stroke(ul_fd);
     #endif
    }
  }
  return 0;
}
