/*
    HOMEBELL KEYPAD
*/

#include <stdint.h>
#include <uloi_base.h>
#include "keypad.h"

#define I_ACCESS_CODE	200
#define I_RING_SIGN	210

unsigned status_val;

int status_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &status_val);
}

int errclr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  status_val=0;
  return 1;
}

/* description of input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOII =
ULOI_GENOBJDES_RAW(ULOI_DOII,NULL,NULL_CODE,NULL,uloi_doii_fnc,(void*)&uloi_objdes_main)
/* description of output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOIO =
ULOI_GENOBJDES_RAW(ULOI_DOIO,NULL,NULL_CODE,NULL,uloi_doio_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOII =
ULOI_GENOBJDES_RAW(ULOI_QOII,NULL,NULL_CODE,NULL,uloi_qoii_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOIO =
ULOI_GENOBJDES_RAW(ULOI_QOIO,NULL,NULL_CODE,NULL,uloi_qoio_fnc,(void*)&uloi_objdes_main)
/* object values read request */
const ULOI_CODE uloi_objdes_t uloid_objdes_RDRQ =
ULOI_GENOBJDES_RAW(ULOI_RDRQ,NULL,NULL_CODE,NULL,uloi_rdrq_fnc,(void*)&uloi_objdes_main)

ULOI_GENOBJDES(STATUS,ULOI_STATUS,"u2",status_rdfnc,&status_val,NULL_CODE,NULL)
ULOI_GENOBJDES(ERRCLR,ULOI_ERRCLR,"e",NULL_CODE,NULL,errclr_wrfnc,&status_val)
ULOI_GENOBJDES(ACCESS_CODE,I_ACCESS_CODE,"u2",uloi_uint_rdfnc,&oi_access_code,uloi_access_code_wrfnc,&oi_access_code)
ULOI_GENOBJDES(RING_SIGN,I_RING_SIGN,"u2",uloi_ring_sign_rdfnc,&oi_ring_sign,NULL_CODE,NULL)

const uloi_objdes_t * ULOI_CODE uloi_objdes_main_items[]={
  &uloid_objdes_DOII,
  &uloid_objdes_DOIO,
  &uloid_objdes_QOII,
  &uloid_objdes_QOIO,
  &uloid_objdes_RDRQ,

  &uloid_objdes_STATUS,
  &uloid_objdes_ERRCLR,
  &uloid_objdes_ACCESS_CODE,
  &uloid_objdes_RING_SIGN
};

const ULOI_CODE uloi_objdes_array_t uloi_objdes_main={
  {
    (uloi_objdes_t **)uloi_objdes_main_items,
    sizeof(uloi_objdes_main_items)/sizeof(uloi_objdes_main_items[0]),
    -1
  }
};
