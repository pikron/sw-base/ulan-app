#ifndef _KEYPAD_H
#define _KEYPAD_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

extern const uloi_objdes_array_t uloi_objdes_main;

extern unsigned int oi_access_code;
extern unsigned int oi_ring_sign;

int uloi_access_code_wrfnc(ULOI_PARAM_coninfo void *context);
int uloi_ring_sign_rdfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _KEYPAD_H */

