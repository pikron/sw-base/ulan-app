/*
    KETTLE SWITCH TIMER
*/

#include <system_def.h>
#include <cpu_def.h>
#include <ul_lib/ulan.h>
#include <keyval_id.h>
#include <uloi_base.h>
#include <lt_timer.h>
#if !defined(SDCC) && !defined(__SDCC)
  #include <keyval_loc.h>
  #include <lpciap_kvpb.h>
#endif
#include "kswtimer.h"
#include "ul_idstr.h"

LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)

/***********************************/
// global variables
kvpb_block_t kvpb_block_global;
uloi_con_ulan_t uloi_con_ulan_global;

#ifndef UL_WITHOUT_HANDLE
kvpb_block_t *kvpb_block=&kvpb_block_global;
uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
ul_fd_t ul_fd;
ul_fd_t ul_fd1;
ul_msginfo msginfo;
#endif /* UL_WITHOUT_HANDLE */

/* ulan variables */
unsigned int  DATA uaddr;
unsigned long DATA usn;
uchar ustatus;

//object interface variables
unsigned int oi_cp_val;

int oi_cp_val_wrfnc(ULOI_PARAM_coninfo void *context)
{
  uloi_uint_wrfnc(ULOI_ARG_coninfo &oi_cp_val);
  if (oi_cp_val) { 
    CLR_OUT_PIN(OUT_PORT,OUT_CYCLE_PUMP);
    CLR_OUT_PIN(LED_PORT,LED_GP);
  } else {
    SET_OUT_PIN(OUT_PORT,OUT_CYCLE_PUMP);
    SET_OUT_PIN(LED_PORT,LED_GP);
  }
  return 1;
}


void sys_err()
{
  while(1);
}

void main(void) 
{

  /***********************************/
  // timer
  lt_100msec_init();

  /***********************************/
  //init values
  oi_cp_val=0;

  /***********************************/
  // kvpb init
  kvpb_block->base=(uint8_t CODE*)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DEFAULT_FLAGS;
 #ifndef  KVPB_MINIMALIZED
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=kvpb_erase;
  kvpb_block->copy=kvpb_copy;
  kvpb_block->flush=kvpb_flush;
 #endif
  if (kvpb_check(kvpb_block,1)<0) sys_err();

  // read data from kvpb
  uaddr=62;
  usn=0L;
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);

  //********************
  // uLan init
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_init();
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
 #endif
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) sys_err();    
 #ifdef UL_WITH_HANDLE
  ul_fd1=ul_open(NULL,NULL);
  if (ul_fd1==UL_FD_INVALID) sys_err();    
 #endif
  ul_setidstr(ul_fd,ul_idstr);
  ul_setmyadr(ul_fd,uaddr);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(ul_fd,&msginfo);
  ul_stroke(ul_fd);

  /***********************************/
  // uLan object interface init
 #ifndef UL_WITHOUT_HANDLE
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #else
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, UL_FD_INVALID);
 #endif /*UL_WITH_HANDLE*/

  //********************
  // start
 #if defined(SDCC) || defined(__SDCC) 
  EA=1;       // Enable interrupts
 #endif
  while (1) {
    
    /* processing of ulan messages */
    if (ul_acceptmsg(ul_fd, &msginfo)>=0) {
      if (!(msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))) {
        if (uloi_process_msg(ULOI_ARG_coninfo (uloi_objdes_array_t*)&uloi_objdes_main, &msginfo)<0) {
          ul_freemsg(ul_fd);       
        }
      } else 
        ul_freemsg(ul_fd);
    }

    /* 100ms timer */
    if (lt_100msec_expired(100)) {

      /* ulan stroke */
     #if defined(SDCC) || defined(__SDCC) 
      ul_stroke(ul_fd);
     #endif
    }
  }
}
