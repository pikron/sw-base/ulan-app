#ifndef _KSWTIMER_H
#define _KSWTIMER_H

#ifdef __cplusplus
/*extern "C" {*/
#endif

#include <stdint.h>

extern const ULOI_CODE uloi_objdes_array_t uloi_objdes_main;
extern unsigned int oi_cp_val;

int oi_cp_val_wrfnc(ULOI_PARAM_coninfo void *context);

#ifdef __cplusplus
/*}*/ /* extern "C"*/
#endif

#endif /* _KSWTIMER_H */

