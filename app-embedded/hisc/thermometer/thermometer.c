#include <string.h>
#include <uloi_base.h>
#ifdef UL_WITHOUT_HANDLE
#include <system_def.h>
#endif  /*UL_WITHOUT_HANDLE*/

extern const ULOI_CODE uloi_objdes_array_t uloi_objdes_main;

#define I_TEST 230

unsigned status_val=0x1234;
int test_val;

int status_rdfnc(ULOI_PARAM_coninfo void *context)
{
  return uloi_uint_rdfnc(ULOI_ARG_coninfo &status_val);
}

int errclr_wrfnc(ULOI_PARAM_coninfo void *context)
{
  if(status_val<0)
    status_val=0;
  else
    status_val++;
  return 1;
}


/* description of input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOII =
ULOI_GENOBJDES_RAW(ULOI_DOII,NULL,NULL_CODE,NULL,uloi_doii_fnc,(void*)&uloi_objdes_main)
/* description of output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_DOIO =
ULOI_GENOBJDES_RAW(ULOI_DOIO,NULL,NULL_CODE,NULL,uloi_doio_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized input objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOII =
ULOI_GENOBJDES_RAW(ULOI_QOII,NULL,NULL_CODE,NULL,uloi_qoii_fnc,(void*)&uloi_objdes_main)
/* ID numbers of recognized output objects */
const ULOI_CODE uloi_objdes_t uloid_objdes_QOIO =
ULOI_GENOBJDES_RAW(ULOI_QOIO,NULL,NULL_CODE,NULL,uloi_qoio_fnc,(void*)&uloi_objdes_main)
/* object values read request */
const ULOI_CODE uloi_objdes_t uloid_objdes_RDRQ =
ULOI_GENOBJDES_RAW(ULOI_RDRQ,NULL,NULL_CODE,NULL,uloi_rdrq_fnc,(void*)&uloi_objdes_main)

ULOI_GENOBJDES(STATUS,ULOI_STATUS,"u2",status_rdfnc,&status_val,NULL_CODE,NULL)
ULOI_GENOBJDES(ERRCLR,ULOI_ERRCLR,"e",NULL_CODE,NULL,errclr_wrfnc,&status_val)
ULOI_GENOBJDES(TEST,I_TEST,"s2",uloi_uint_rdfnc,&test_val,uloi_uint_wrfnc,&test_val)

const uloi_objdes_t * ULOI_CODE uloi_objdes_main_items[]={
  &uloid_objdes_DOII,
  &uloid_objdes_DOIO,
  &uloid_objdes_QOII,
  &uloid_objdes_QOIO,
  &uloid_objdes_RDRQ,

  &uloid_objdes_STATUS,
  &uloid_objdes_ERRCLR,
  &uloid_objdes_TEST
};

const ULOI_CODE uloi_objdes_array_t uloi_objdes_main={
  {
    uloi_objdes_main_items,
    sizeof(uloi_objdes_main_items)/sizeof(uloi_objdes_main_items[0]),
    -1
  }
};


uloi_con_ulan_t uloi_con_ulan_global;

int main()
{
 #ifndef UL_WITHOUT_HANDLE
  uloi_coninfo_t *coninfo=&uloi_con_ulan_global.con;
  ul_msginfo msginfo;
 #endif  /*UL_WITHOUT_HANDLE*/

 #ifdef UL_WITHOUT_HANDLE
  ul_drv_set_adr(3);
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
  ul_drv_init();
  ul_fd=ul_open(0,0);
  EA=1;   // Enable interrupts
 #else  /*UL_WITHOUT_HANDLE*/
  coninfo->ul_fd=ul_open(NULL,NULL);
  if(uloi_con_ulan_rdfd(coninfo)==UL_FD_INVALID)
    return 1;
  coninfo->ul_fd1=ul_open(NULL,NULL);
  if(uloi_con_ulan_wrfd(coninfo)==UL_FD_INVALID){
    ul_close(uloi_con_ulan_wrfd(coninfo));
    return 1;
  }
  memset(&msginfo, 0, sizeof(msginfo));
  msginfo.cmd=UL_CMD_OISV;
  ul_addfilt(uloi_con_ulan_rdfd(coninfo), &msginfo);
 #endif  /*UL_WITHOUT_HANDLE*/

 #ifndef UL_WITHOUT_HANDLE
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, ul_fd1);
 #else
  uloi_con_ulan_set_cmd_fd(coninfo, UL_CMD_OISV, ul_fd, UL_FD_INVALID);
 #endif /*UL_WITH_HANDLE*/

  while(1){
  
    if((ul_inepoll(uloi_con_ulan_rdfd(coninfo))>0)||(ul_fd_wait(uloi_con_ulan_rdfd(coninfo),100)>0)){
      uloi_process_msg(ULOI_ARG_coninfo &uloi_objdes_main, NULL);
    }
  }
}
