#include "reg1210.h"
#include "ul_histb.h"

static
bit msc_adc_ready(void)
{
   return AIE&0x20; 
}

static
long msc_adc_unipolar(void)
{
  #pragma asm
	mov     r4,#0
	mov     r5,ADRESH
	mov     r6,ADRESM
	mov     r7,ADRESL
  #pragma endasm
}

static
long msc_adc_bipolar(void)
{
  #pragma asm
        mov     a,ADRESH
        mov     r5,a
	mov     c,acc.7
	subb    a,acc
        mov     r4,a
        mov     r6,ADRESM
        mov     r7,ADRESL
  #pragma endasm
}

long msc_adc_summer(void)
{
  #pragma asm
        mov     r4,SUMR3
        mov     r5,SUMR2
        mov     r6,SUMR1
        mov     r7,SUMR0
  #pragma endasm
}


#include <stdio.h>

int msc_adc_init(void)
{
  PDCON &= 0x0f7;	//turn on adc
  			// ACLK min 32
  ACLK = 15;		// ACLK = 18.432MHz/(1+1)= 0.9216MHz
  
  // max 2047		// Data Rate = (ACLK + 1)/64/DECIMATION
  DECIMATION = 1800;	// Data Rate = 18.432/2/64/14400 = 10.00Hz

  //         7   6    5      4     3    2    1    0
  // ADCON0  -   BOD  EVREF  VREFH EBUF PGA2 PGA1 PGA0
  ADCON0 = 0x20;	// Vref on 1.25V, Buff on, BOD off, PGA 1

  //       7    6    5    4    3    2    1    0
  // ADMUX INP3 INP2 INP1 INP0 INN3 INN2 INN1 INN0
  // 0-7 inputs, 8 AINCOM, 0xFF Temperature
  ADMUX = 0x01;		// differencial AIN0-AIN1

  //         7  6   5   4   3   2    1    0
  // ADCON1  -  POL SM1 SM0 -   CAL2 CAL1 CAL0
  ADCON1 = 0x01;	// bipolar, auto, self calibration, offset

  return 0;
}

long msc_adc_test(void)
{
  long result;
  while(!msc_adc_ready());
  result=msc_adc_bipolar();
  //printf("ADC: %08lx\n",result);
  printf("%08ld\n",result);
  return result;
}
