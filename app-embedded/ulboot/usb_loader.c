#include <stdint.h>
#include <cpu_def.h>
#include <system_def.h>
#include <string.h>
#include <stdio.h>
#include <endian.h>
#include <usb/lpc.h>
#include <usb/usb.h>
#include <usb/usb_loader.h>
#include <usb/usbdebug.h>

#include "usb/usb_defs.h"

#include <local_config.h>

#ifdef CONFIG_KEYVAL
#include <keyvalpb.h>
#include <lpciap.h>
#include <lpciap_kvpb.h>
#include <hal_machperiph.h>
#endif /*CONFIG_KEYVAL*/


unsigned char loader_buffer[USB_MAX_PACKET0];
extern unsigned char XDATA watchdog_feed_disabled;

int usb_loadder_active = 0;

typedef void (*FNC)(); //function ptr

static int usb_flash_pkt_wr(struct usb_ep_t *ep, int len, int code)
{
  unsigned char *ptr=ep->ptr-len;

 #ifdef CONFIG_KEYVAL
  lpcisp_kvpb_copy(NULL, (void*)ep->user_data, ptr, len);
 #endif /*CONFIG_KEYVAL*/

  ep->user_data += len;
  ep->ptr = loader_buffer;
  return USB_COMPLETE_OK;
}

static int usb_flash_erase(unsigned addr, unsigned len)
{
 #ifdef CONFIG_KEYVAL
  lpcisp_erase((void *)addr, len);
 #endif /*CONFIG_KEYVAL*/
  return 0;
}


static void usb_goto(unsigned address)
{
  ((FNC)address)();
}

int usb_common_loader(usb_device_t *udev)

{
  unsigned long addr;
  unsigned len;
  USB_DEVICE_REQUEST *dreq;

  usb_loadder_active = 1;

  dreq=&udev->request;

 #ifdef CONFIG_KEYVAL
  if(dreq->bRequest != (USB_VENDOR_GET_SET_MEMORY |
                        USB_DATA_DIR_FROM_HOST | USB_VENDOR_TARGET_FLASH))
    lpcisp_kvpb_flush(NULL);
 #endif /*CONFIG_KEYVAL*/

  switch ( dreq->bRequest & USB_VENDOR_MASK) {

    case USB_VENDOR_GET_CAPABILITIES:
//      printf("GET_CAPABILITIES\n");
      loader_buffer[0] = 0xAA; // test
      usb_send_control_data(udev, loader_buffer, 1);
      return 1;

    case USB_VENDOR_RESET_DEVICE:
//      printf("RESET_DEVICE\n");
      usb_send_control_data(udev,NULL,0);
     #ifdef CONFIG_KEYVAL
      /* after 10ms invoke hardware reset by watchdog */
      lpc_watchdog_init(1, 10);
      watchdog_feed();
      watchdog_feed_disabled=1;
     #endif /*CONFIG_KEYVAL*/
      return 1;

    case USB_VENDOR_GOTO:
//      printf( "GOTO 0x%X\n", dreq->wValue);
      usb_send_control_data(udev,NULL,0);
      addr = (dreq->wValue & 0xffff) | (((unsigned long)dreq->wIndex&0xffff)<<16);
      usb_app_stop();
      usb_goto(addr);
      return 1;

    case USB_VENDOR_ERASE_MEMORY:
//      printf( "ERASE 0x%X 0x%X\n", dreq->wValue, dreq->wIndex);
      usb_send_control_data(udev,NULL,0);
      usb_flash_erase(dreq->wValue, dreq->wIndex);
      return 1;

    case USB_VENDOR_ERASE_1KB_MEMORY:  /* erase memory for 1 KB */
      usb_send_control_data(udev,NULL,0);
      usb_flash_erase((uint32_t)dreq->wValue<<10, dreq->wIndex<<10);
      return 1;

    case USB_VENDOR_GET_SET_MEMORY:
      addr = (dreq->wValue & 0xffff) | (((unsigned long)dreq->wIndex&0xffff)<<16);
      len = dreq->wLength;
//      printf("GET_SET_MEMORY, addr=0x%lx, len=%d\n", (long)addr, len);
      if (( dreq->bmRequestType & USB_DATA_DIR_MASK) == USB_DATA_DIR_FROM_HOST) {
        // read from HOST ???
        switch( dreq->bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_RAM:
	  	udev->ep0.ptr = (void*)addr; break;

          case USB_VENDOR_TARGET_FLASH:
	  	udev->ep0.next_pkt_fnc = usb_flash_pkt_wr;
		udev->ep0.user_data=addr;
	  	udev->ep0.ptr = loader_buffer; break;

	  default: return -1;
        }
        if ( len) usb_set_control_endfnc(udev, usb_ack_setup);
        else usb_send_control_data(udev,NULL,0);
	return 1;
      } else {
        switch( dreq->bRequest & USB_VENDOR_TARGET_MASK) {
          case USB_VENDOR_TARGET_RAM:
	  	usb_send_control_data(udev, (void*)addr, len); break;

	  default: return -1;
        }
	return 1;
      }
      break;
  }


  return 0;
}

usb_device_t usb_device;
usb_ep_t eps[NUM_ENDPOINTS]; 

int usb_app_init(void)
{
  memset( &usb_device, 0, sizeof( usb_device));
  usb_device.id = 1;
  usb_device.devdes_table = &usb_devdes_table;
  usb_device.init = usb_lpc_init;
  usb_debug_set_level(DEBUG_LEVEL_NONE);
  usb_device.cntep = NUM_ENDPOINTS;
  usb_device.ep = eps;

  usb_device.vendor_fnc=usb_common_loader;

  usb_init(&usb_device);
  usb_connect(&usb_device);
  return 0;
}

int usb_app_pool(void)
{
  int active = usb_loadder_active;
  usb_loadder_active = 0;

  usb_check_events(&usb_device);
  usb_control_response(&usb_device);

  return active;
}

int usb_app_stop(void)
{
  usb_disconnect(&usb_device);
  return 0;
}
