#include <local_config.h>
#include <system_def.h>
#include <keyval_id.h>
#ifdef CONFIG_ULBOOT_WITH_ULAN
  #include <ul_lib/ulan.h>
  #include <uldy_base.h>
#endif
#include <lt_timer.h>
#if !defined(SDCC) && !defined(__SDCC)
  #include <mem_loc.h>
  #include <hal_machperiph.h>
  #include <keyval_loc.h>
  #include <lpciap_kvpb.h>
  #include <ul_idstr.h>
#else
  #include <vect.h>
  void timer(void);
#endif /* SDCC */

#ifdef CONFIG_ULBOOT_WITH_USB
int usb_app_init(void);
int usb_app_pool(void);
int usb_app_stop(void);
#endif /* CONFIG_ULBOOT_WITH_USB */

LT_TIMER_DEC(lt_100msec)
LT_TIMER_IMP(lt_100msec)
LT_TIMER_DEC(lt_2sec)
LT_TIMER_IMP(lt_2sec)

#ifndef ULBOOT_APPSTART_DELAY_MS
  #define ULBOOT_APPSTART_DELAY_MS() 2000
#endif /*ULBOOT_APPSTART_DELAY_MS*/

/***********************************/
// global variables
KVPB_BLOCK_LOC kvpb_block_t kvpb_block_global;
#ifdef CONFIG_ULBOOT_WITH_ULAN
UL_DYAC_VAR_LOC ul_dyac_t ul_dyac_global;
#endif
unsigned char XDATA watchdog_feed_disabled;

#ifndef UL_WITHOUT_HANDLE
kvpb_block_t *kvpb_block=&kvpb_block_global;
#ifdef CONFIG_ULBOOT_WITH_ULAN
ul_dyac_t *ul_dyac=&ul_dyac_global;
ul_fd_t ul_fd;
ul_msginfo msginfo;
#endif
#endif /* UL_WITHOUT_HANDLE */

/* ulan variables */
unsigned int XDATA uaddr;
unsigned long XDATA usn;
//uint16_t ustatus;

typedef void (*FNC)(); //function ptr

/***********************************/
int sys_err() 
{
  unsigned char i=0;

  while(1) {
   #if defined(SDCC) || defined(__SDCC)
    /* serve interrupt rutine for timer */
    timer();      
   #endif

    if (lt_100msec_expired(100)) {
      i++;
      if (i&1) {
        SET_OUT_PIN(LED_PORT,LED_ERR);
      } else {
        CLR_OUT_PIN(LED_PORT,LED_ERR);
      }
     #ifdef WATCHDOG_ENABLED
      watchdog_feed();
     #endif /* WATCHDOG_ENABLED */
    }
  }
  return 0;
}

/***********************************/
char ul_save_sn(uint32_t usn)
{
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);
  return 0;
}

/***********************************/
char ul_save_adr(uint8_t uaddr_new)
{
  uaddr=uaddr_new;
  kvpb_set_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  return 0;
}

/***********************************/
int main(void) 
{
  int boot_activated = 0;
  watchdog_feed_disabled=0;

  /***********************************/
  // timers 
  lt_100msec_init();
  lt_2sec_init();

//  ustatus=0;
  CLR_OUT_PIN(LED_PORT,LED_GP);

  /***********************************/
  // kvpb init
  kvpb_block->base=(CODE uint8_t *)KEYVAL_START;
  kvpb_block->size=KEYVAL_PAGE_LEN;
  kvpb_block->flags=KVPB_DEFAULT_FLAGS;
 #ifndef KVPB_MINIMALIZED
  kvpb_block->chunk_size=KVPB_CHUNK_SIZE;
  kvpb_block->erase=kvpb_erase;
  kvpb_block->copy=kvpb_copy;
  kvpb_block->flush=kvpb_flush;
 #endif
  if (kvpb_check(kvpb_block,1)<0) sys_err();

  /***********************************/
  // set configuration for device
  uaddr=62;
  usn=0;
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_ADDR,sizeof(unsigned int),&uaddr);
  kvpb_get_key(kvpb_block,KVPB_KEYID_ULAN_SN,sizeof(unsigned long),&usn);

  /***********************************/
  // ulan init
#ifdef CONFIG_ULBOOT_WITH_ULAN
 #ifdef UL_WITHOUT_HANDLE
  ul_drv_set_bdiv(BAUD2BAUDDIV(19200));
  ul_drv_init();
 #endif
  ul_fd=ul_open(NULL,NULL);
  if (ul_fd==UL_FD_INVALID) sys_err();    
  ul_setidstr(ul_fd,ul_idstr);
  ul_setmyadr(ul_fd,uaddr);
  msginfo.sadr=0;
  msginfo.dadr=0;
  msginfo.cmd=0;
  ul_addfilt(ul_fd,&msginfo);
  ul_stroke(ul_fd);
  /***********************************/
  // uLan dyac init
  uldy_init(ULDY_ARG_ul_dyac ULDY_ARG_ul_fd ul_save_sn,ul_save_adr,(char*)ul_idstr,usn);
#endif	/* CONFIG_ULBOOT_WITH_ULAN */

 #ifdef CONFIG_ULBOOT_WITH_USB
   usb_app_init();
 #endif /* CONFIG_ULBOOT_WITH_USB */

  /********************/
  // start
  while (1) {
   #ifdef CONFIG_ULBOOT_WITH_ULAN
    if(ul_dyac->boot_activated)
      boot_activated = 1;
   #endif

    if(!boot_activated) {
      if(*(int CODE*)MEM_APP_START != (int) ~0)
        if(lt_2sec_expired(ULBOOT_APPSTART_DELAY_MS()))
          break;
    }

   #if defined(SDCC) || defined(__SDCC)
    /* serve interrupt rutine for uLan */
    ul_int();

    /* serve interrupt rutine for timer */
    timer();
   #endif

   #ifdef CONFIG_ULBOOT_WITH_ULAN
    /* test for ulan message */
    if(ul_inepoll(ul_fd)>0){
      uldy_process_msg(ULDY_ARG_ul_dyac NULL);
    }

    /* test request for address */
    if (uldy_rqa(ULDY_ARG1_ul_dyac)) 
      uldy_addr_rq(ULDY_ARG1_ul_dyac);
   #endif	/* CONFIG_ULBOOT_WITH_ULAN */

   #ifdef CONFIG_ULBOOT_WITH_USB
    if(usb_app_pool() > 0)
      boot_activated = 1;
   #endif /* CONFIG_ULBOOT_WITH_USB */

   #ifdef WATCHDOG_ENABLED
    if (!watchdog_feed_disabled)
      watchdog_feed();
   #endif /* WATCHDOG_ENABLED */

   #ifdef CONFIG_ULBOOT_WITH_ULAN
    /* serve 100ms timer */
    if (lt_100msec_expired(100)) { 
      /* ulan stroke */
      ul_stroke(ul_fd);
    }
   #endif
  }
 #ifdef CONFIG_ULBOOT_WITH_USB
  usb_app_stop();
 #endif /* CONFIG_ULBOOT_WITH_USB */
  SET_OUT_PIN(LED_PORT,LED_GP);
  //((FNC)MEM_APP_START)();
  /* unreachable code */
 #if defined(SDCC) || defined(__SDCC)
  vec_jmp(0x0);  /* need to call a function from misc to correct linking */
 #endif
  return 0;
}
