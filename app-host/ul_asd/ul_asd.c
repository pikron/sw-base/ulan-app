/*******************************************************************
  uLan Communication - addresss server 

  ul_asd.c	- 

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ul_netbase.h>
#include <orbit/orbit.h>
#include "ul_asd.h"
#include <signal.h>

#define GSA_OFFSET(_type,_member) \
		((int)&(((_type*)0)->_member))

int daemon_flg=0;
int short_flg	= 0;
int log_all_flg = 0;
int terminate_flg = 0;

ul_asd_context_t ul_cont[1];

void sig_usr(int signo) {
  if ((signo==SIGTERM) || (signo==SIGINT)) {
    terminate_flg=1;
  }
}

static int daemon_init(void) {
#ifndef _WIN32
  pid_t pid;

  if ((pid = fork()) < 0) {
    return -1;
  } else
    if (pid != 0) {
      exit(0);	/* parent vanishes */
    }
  /* child process */
  setsid();
  umask(0);
  close(0);
  close(1);
  close(2);
#endif
  return 0;
}


static void
usage()
{
  printf("Usage: ul_asd <parameters>\n");
  printf("  -d, --uldev <name>     name of uLan device [%s]\n", ul_cont->ul_dev_name);
  printf("  -n, --ns <name>        addr of name server\n");
  printf("        [%s]\n", ul_cont->orbit_ns_addr);
  printf("  -i, --ip <name>        IP address for communication with client(s)\n");
  printf("        [%s]\n", ul_cont->orbit_orb_iiop_ipname);
  printf("  -c, --conf <name>      name of configuration file [%s]\n", ul_cont->conf_filename);
#ifndef _WIN32
  printf("  -D, --daemon           starting like deamon\n");
#endif  /* _WIN32 */
  printf("  -V, --version          show version\n");
  printf("  -h, --help             this usage screen\n");
}

static void
main_options()
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "conf", 1, 0, 'c' },
    { "ns", 1, 0, 'n' },
    { "ip", 1, 0, 'i' },
#ifndef _WIN32
    { "daemon",  0, 0, 'D' },
#endif /* _WIN32 */
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int  opt;

 #ifndef HAS_GETOPT_LONG
    while ((opt = getopt(ul_cont->argc, ul_cont->argv, "d:c:n:i:DVh")) != EOF)
 #else
    while ((opt = getopt_long(ul_cont->argc, ul_cont->argv, "d:c:n:i:DVh",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_cont->ul_dev_name=optarg;
      break;
    case 'c':
      strcpy(ul_cont->conf_filename,optarg);
      break;
    case 'n':
      ul_cont->orbit_ns_addr=optarg;
      break;
    case 'i':
      ul_cont->orbit_orb_iiop_ipname=optarg;
      break;
    #ifndef _WIN32
    case 'D':
      daemon_flg=1;
      break;  
    #endif  /* _WIN32 */
    case 'V':
      printf("uLan address server - version 0.1\n");
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }
}

int main(int argc, char *argv[])
{
  GError  *err    = NULL;
  char basename[128];
  char *filename="ul_asd.xml";

  /**********************************************************/
  /* Initizalization */
  ul_cont->argc = argc;
  ul_cont->argv = argv;
  ul_cont->ul_dev_name=UL_DEV_NAME;
  ul_cont->orbit_ns_addr=ORBIT_NS_ADDR_DEFAULT;
  ul_cont->orbit_orb_iiop_ipname=ORBIT_ORBIIOPIPName_DEFAULT;

#ifdef _WIN32
  GetWindowsDirectory(basename,sizeof(basename));
#else
  strcpy(basename,"/etc/ulan/");
#endif
  sprintf(ul_cont->conf_filename,"%s%s",basename,filename);

  main_options();

  //daemonize
  if (daemon_flg) 
    daemon_init();

  if (ul_asd_server_init(ul_cont)<0) 
    exit(1);

  if (ul_asd_net_init(ul_cont)<0) 
    exit(1);
 
//  g_thread_init (NULL);
  ul_cont->mutex = g_mutex_new (); 

  ul_asd_conf_load(NULL,ul_cont); 

  /**********************************************************/
  /* Starting */
  ul_cont->ul_server_thread = g_thread_create ((GThreadFunc) ul_asd_server_run, 
  			     ul_cont, 
			     TRUE,   //joinable 
			     &err);
  ul_cont->ul_net_thread =   g_thread_create ((GThreadFunc) ul_asd_net_run, 
     		             ul_cont, 
			     TRUE,   //joinable 
			     &err);

  /**********************************************************/
  /* init signal handling */
  signal(SIGINT,  sig_usr);
  signal(SIGHUP,  sig_usr);

  while(!terminate_flg) {
    usleep(100*1000);
  }
  
  //stop & cleanup server thread
  ul_asd_server_stop(ul_cont);
  g_thread_join (ul_cont->ul_server_thread);
  ul_asd_server_cleanup(ul_cont);
  
  //stop & cleanup net thread
  ul_asd_net_stop(ul_cont);
  g_thread_join (ul_cont->ul_net_thread);
  ul_asd_net_cleanup(ul_cont);

  g_mutex_free (ul_cont->mutex);

  return 0;
}
