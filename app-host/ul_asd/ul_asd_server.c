/*
 * echo-server program. Hacked from Echo test suite by
 * <birney@sanger.ac.uk>, ORBit2 udpate by Frank Rehberger
 * <F.Rehberger@xtradyne.de>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <orbit/orbit.h>
#include "ul_asd.h"

#include "ulci_as-skelimpl.c"
#include "ul_corba_toolkit.h" /* provides etk_abort_if_exception */ 

	
/* Inits ORB @orb using @argv arguments for configuration. For each
 * ORBit options consumed from vector @argv the counter of @argc_ptr
 * will be decremented. Signal handler is set to call
 * echo_server_shutdown function in case of SIGINT and SIGTERM
 * signals.  If error occures @ev points to exception object on
 * return.
 */static 
void 
server_init (int                 *argc_ptr, 
	     char                *argv[],
	     CORBA_ORB           *orb,
	     PortableServer_POA  *poa,
	     CORBA_Environment   *ev)
{
	PortableServer_POAManager  poa_manager = CORBA_OBJECT_NIL; 

	CORBA_Environment  local_ev[1];
	CORBA_exception_init(local_ev);
	
	/* create Object Request Broker (ORB) */
	
        (*orb) = CORBA_ORB_init(argc_ptr, argv, "orbit-local-mt-orb", ev);
	if (etk_raised_exception(ev)) 
		goto failed_orb;

        (*poa) = (PortableServer_POA) 
		CORBA_ORB_resolve_initial_references(*orb, "RootPOA", ev);
	if (etk_raised_exception(ev)) 
		goto failed_poa;

        poa_manager = PortableServer_POA__get_the_POAManager(*poa, ev);
	if (etk_raised_exception(ev)) 
		goto failed_poamanager;

	PortableServer_POAManager_activate(poa_manager, ev);
	if (etk_raised_exception(ev)) 
		goto failed_activation;

        CORBA_Object_release ((CORBA_Object) poa_manager, ev);
	return;

 failed_activation:
 failed_poamanager:
        CORBA_Object_release ((CORBA_Object) poa_manager, local_ev);
 failed_poa:
	CORBA_ORB_destroy(*orb, local_ev);		
 failed_orb:
	return;
}


int 
ul_asd_server_init(ul_asd_context_t *ctx)
{
        gchar *id_stat[] = {"ulci", "statistics", NULL};
        gchar *id_set[] = {"ulci", "settings", NULL};
        char *argv[7];
        int argc=6;

        //init corba services
        ctx->orb=CORBA_OBJECT_NIL;
        ctx->ns=CORBA_OBJECT_NIL;

        argv[0] = "dummy";
        argv[1] = ctx->orbit_ns_addr;
        argv[2] = "-ORBIIOPIPv4=1";
        argv[3] = "-ORBIIOPUNIX=0";
        argv[4] = "-ORBCorbaloc=1";
        argv[5] = ctx->orbit_orb_iiop_ipname;
        argv[6] = NULL;

	CORBA_exception_init(ctx->ev);
	
	server_init (&argc, argv, &ctx->orb, &ctx->root_poa, ctx->ev);
	etk_abort_if_exception(ctx->ev, "failed ORB init");

	g_print ("Binding to NS: %s\n",ctx->orbit_ns_addr);

	ctx->ns = etk_get_name_service (ctx->orb, ctx->ev);
	etk_abort_if_exception(ctx->ev, "failed resolving name-service");

        ctx->serv_stat = impl_ulci_statistics__create(ctx->root_poa, ctx->ev, ctx);
	etk_abort_if_exception(ctx->ev, "failed activating service");

        ctx->serv_set = impl_ulci_settings__create(ctx->root_poa, ctx->ev, ctx);
	etk_abort_if_exception(ctx->ev, "failed activating service");

	g_print ("Binding service reference at name service against id: %s/%s\n", id_stat[0], id_stat[1]);
	
	etk_name_service_bind (ctx->ns, ctx->serv_stat, id_stat, ctx->ev);
	etk_abort_if_exception(ctx->ev, "failed binding of service");

	g_print ("Binding service reference at name service against id: %s/%s\n", id_set[0], id_set[1]);
	
	etk_name_service_bind (ctx->ns, ctx->serv_set, id_set, ctx->ev);
	etk_abort_if_exception(ctx->ev, "failed binding of service");
	
	g_print ("Binded\n");
	return 0;
}
	
/* Entering main loop @orb handles incoming request and delegates to
 * servants. If error occures @ev points to exception object on
 * return.
 */
void 
ul_asd_server_run(ul_asd_context_t *ctx) 
{
	
        CORBA_ORB_run(ctx->orb, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return;
}


int 
ul_asd_server_stop(ul_asd_context_t *ctx) 
{
	CORBA_Environment  local_ev[1];
	CORBA_exception_init(local_ev);
	
        if (ctx->orb != CORBA_OBJECT_NIL)
        {
                CORBA_ORB_shutdown (ctx->orb, FALSE, local_ev);
                etk_abort_if_exception (local_ev, "caught exception");
        }
	
	return 0;
}

/* Releases @servant object and finally destroys @orb. If error
 * occures @ev points to exception object on return.
 */
int 
ul_asd_server_cleanup(ul_asd_context_t *ctx) 
{
	PortableServer_ObjectId   *objid       = NULL;

	/* deaktive service statistics */
	objid = PortableServer_POA_reference_to_id (ctx->root_poa, ctx->serv_stat, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;
	/* Servant: deactivatoin - will invoke  __fini destructor */
	PortableServer_POA_deactivate_object (ctx->root_poa, objid, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;

	/* deaktive service settings */
	objid = PortableServer_POA_reference_to_id (ctx->root_poa, ctx->serv_set, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;
	/* Servant: deactivatoin - will invoke  __fini destructor */
	PortableServer_POA_deactivate_object (ctx->root_poa, objid, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;

	PortableServer_POA_destroy (ctx->root_poa, TRUE, FALSE, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;

	CORBA_free (objid);

        CORBA_Object_release ((CORBA_Object) ctx->root_poa, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;
	
        CORBA_Object_release (ctx->serv_stat, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;

        CORBA_Object_release (ctx->serv_set, ctx->ev);
	if (etk_raised_exception(ctx->ev)) return -1;

        /* ORB: tear down the ORB */
        if (ctx->orb != CORBA_OBJECT_NIL)
        {
                /* going to destroy orb.. */
                CORBA_ORB_destroy(ctx->orb, ctx->ev);
		if (etk_raised_exception(ctx->ev)) return -1;
        }
	
	return 0;
}
