/*******************************************************************
  uLan Communication - addresss server 

  ul_asd.c	- 

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include "ul_asd.h"

#define GSA_OFFSET(_type,_member) \
		((int)&(((_type*)0)->_member))

#define WITHOUT_SYS_SELECT
#define UL_DRV_IN_LIB

#ifndef WITHOUT_SYS_SELECT
#include <errno.h>
#endif /*WITHOUT_SYS_SELECT*/

int
ul_asd_net_init(ul_asd_context_t *ul_cont)
{
  ul_root_htimer_init(0, NULL);
  /* FIXME: the check that ul_sn_array has been there in past */
  if(ul_net_new(&ul_cont->ul_net,ul_cont->ul_dev_name)<0) {
    perror("ul_net_new failed");
    return -1;  
  }
  if(ul_net_dy_init(ul_cont->ul_net)<0) {
    perror("ul_net_dy_init failed");
    return -1;  
  }
  ul_dy_cycle_subfn=0;
  ul_cont->ul_net_terminate=0;
  return 0;
}


void
ul_asd_net_run(ul_asd_context_t *ul_cont)
{
  ul_msdiff_t ms_sleep;

  while(!ul_cont->ul_net_terminate){
    ul_htimer_queue_t *root_htimer;
    ul_htim_time_t actual_time;
    ul_htim_time_t next_expire;

    g_mutex_lock(ul_cont->mutex);
    ul_root_htimer_current_time(0, &actual_time);
    root_htimer = ul_root_htimer_get(0, NULL);
    ul_htimer_run_expired(root_htimer,&actual_time);
    if(!ul_htimer_next_expire(root_htimer,&next_expire))
      next_expire=actual_time+0x10000000;
    ul_root_htimer_put(root_htimer);
    g_mutex_unlock(ul_cont->mutex);

    ul_root_htimer_current_time(0, &actual_time);

    ul_htime_sub2ms(&ms_sleep, &next_expire, &actual_time);
    if(ms_sleep<0) ms_sleep=0;
    /* we do not want to overflow usec field of timeout.tv_usec */
    if(ms_sleep>(~(ul_mstime_t)0/2001)) ms_sleep=~(ul_mstime_t)0/2001;

    if(ul_inepoll(ul_cont->ul_net->ul_fd)>0){
	int loopcnt=100;
	do {
          g_mutex_lock(ul_cont->mutex);
          ul_net_do_rec_msg(ul_cont->ul_net);
          g_mutex_unlock(ul_cont->mutex);
	} while((ul_inepoll(ul_cont->ul_net->ul_fd)>0)&&loopcnt--);
    }else {
    #ifdef WITHOUT_SYS_SELECT
     #ifdef _WIN32
      if(ms_sleep>100) ms_sleep=100;
      Sleep(ms_sleep);
     #elif defined(UL_DRV_IN_LIB)
      if(ms_sleep>100) ms_sleep=100;
      usleep(ms_sleep*1000);
     #endif /* UL_DRV_IN_LIB */
    #else /* !WITHOUT_SYS_SELECT */
      {
	int ret;
	struct timeval timeout;
	fd_set set;

	FD_ZERO (&set);
	FD_SET (ul_fd2sys_fd(ul_cont->ul_net->ul_fd), &set);
	FD_SET (0, &set);
	timeout.tv_sec = 0;
	timeout.tv_usec = ms_sleep*1000;
	while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
        	&&errno==-EINTR);
	/* printf("select %d ",ret); */
        if(FD_ISSET(0,&set))
        { char ch;
          read(0,&ch,1);
          switch(ch) {
            case 'q' : 
              printf("\nUser requested quit\n");
              return;
          }
        }
      }
    #endif /* !WITHOUT_SYS_SELECT */
    }
  }
}

int
ul_asd_net_stop(ul_asd_context_t *ul_cont)
{
  ul_cont->ul_net_terminate=1;
  return 0;
}

int
ul_asd_net_cleanup(ul_asd_context_t *ul_cont)
{
  return 0;
}
