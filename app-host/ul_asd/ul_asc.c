/*
 * account-server program. Hacked from Frank Rehberger
 * <F.Rehberger@xtradyne.de>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <signal.h>
#include <getopt.h>
#include <orbit/orbit.h>
#include "ul_corba_toolkit.h"
#include <ul_netbase.h>
#include "ul_asd.h"

#include "ulci_as.h"

CORBA_Environment ev[1];
CORBA_ORB orb = CORBA_OBJECT_NIL;
CosNaming_NamingContext ns = CORBA_OBJECT_NIL;
ulci_statistics serv_stat = CORBA_OBJECT_NIL;
ulci_settings serv_set = CORBA_OBJECT_NIL;
char *orbit_ns_addr=ORBIT_NS_ADDR_DEFAULT;

int statistics_flg=0;
int settings_flg=0;
int units_flg=0;
int add_flg=0;
int del_flg=0;
int save_flg=0;
int status_period_flg=0;
int cycle_start_flg=0;
int first_dy_adr_flg=0;

int module=0;
unsigned long sn_add=0;
unsigned long sn_remove=0;
int status_period=0;
int cycle_start=0;
int first_dy_adr=0;

int
corba_client_init()
{
  gchar *id_stat[] = {"ulci", "statistics", NULL};
  gchar *id_set[] = {"ulci", "settings", NULL};
  char *argv[3];
  int argc=2;

  argv[0] = "dummy";
  argv[1] = orbit_ns_addr;
  argv[2] = NULL;

  CORBA_exception_init(ev);
  orb=CORBA_ORB_init(&argc, argv, "orbit-local-orb", ev);
  etk_abort_if_exception(ev, "init failed");

  ns=etk_get_name_service (orb, ev);
  etk_abort_if_exception(ev,"failed resolving name-service");

  serv_stat=(ulci_statistics) 
	etk_name_service_resolve (ns, id_stat, ev);
  etk_abort_if_exception(ev,"failed resolving service at name-service");

  serv_set=(ulci_settings) 
	etk_name_service_resolve (ns, id_set, ev);
  etk_abort_if_exception(ev,"failed resolving service at name-service");

  return 0;
}

int
corba_client_cleanup()
{
  /* releasing managed object */
  CORBA_Object_release(serv_stat, ev);
  if (etk_raised_exception(ev)) return -1;
  CORBA_Object_release(serv_set, ev);
  if (etk_raised_exception(ev)) return -1;
 
  /* tear down the ORB */
  if (orb != CORBA_OBJECT_NIL) {
    /* going to destroy orb.. */
    CORBA_ORB_destroy(orb, ev);
    if (etk_raised_exception(ev)) return -1;
  }
  return 0;
}
         	
void
print_statistics()
{
  CORBA_long         i;
  CORBA_string       s;

  g_print ("statistics:\n");

  if (etk_raised_exception (ev)) return;
  s =  ulci_statistics__get_start_time (serv_stat, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  start_time     : %s\n", s);
  CORBA_free (s);         
  
  i =  ulci_statistics__get_dy_adr_asg_cnt (serv_stat, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  dy_adr_asg_cnt : %d\n", i);
  
  s =  ulci_statistics__get_dy_adr_asg_last (serv_stat, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  dy_adr_asg_last: %s\n", s);
  CORBA_free (s);         

  i =  ulci_statistics__get_st_adr_req_cnt (serv_stat, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  st_adr_req_cnt : %d\n", i);

  s =  ulci_statistics__get_st_adr_reg_last (serv_stat, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  st_adr_reg_last: %s\n", s);
  CORBA_free (s);         
}

void
print_settings()
{
  CORBA_long         i;

  g_print ("settings:\n");
  
  i=ulci_settings__get_status_period (serv_set, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  status_period: %d\n", i);

  i=ulci_settings__get_cycle_start (serv_set, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  cycle_start  : %d\n", i);

  i=ulci_settings__get_first_dy_adr (serv_set, ev);
  if (etk_raised_exception (ev)) return;
  g_print ("  first_dy_adr : %d\n", i);
}

void
print_units()
{
  ulci_units_t    *units;
  ulci_unit_t     *unit;
  CORBA_long         i;
         	
  g_print ("units:\n");

  units=ulci_settings_get_units(serv_set,ev);
  if (etk_raised_exception(ev)) return;

  g_print ("  unit(s) for static address assigment:\n");
  for (i = 0; i < units->_length; ++i) {
    unit=&ORBit_sequence_index (units, i);
    if (unit->flags&ULMI_STATIC) 
      printf("    adr:%2d, sn:0x%08x\n",unit->adr,unit->sn);
    }
  
  g_print ("  active unit(s):\n");
  for (i = 0; i < units->_length; ++i) {
    unit=&ORBit_sequence_index (units, i);
    if (unit->flags&ULMI_PRESENT) 	    
      printf("    adr:%2d, sn:0x%08x, des:%s\n",unit->adr,unit->sn,unit->des);
//      CORBA_free (unit->des);         
    }
    
  CORBA_free (units);         
}

static void
usage()
{
  printf("Usage: ul_asc <parameters>\n");
  printf("  -n, --ns <name>        addr of name server\n");
  printf("      [%s]\n", orbit_ns_addr);
  printf("  -t, --stat             print statistics of ul_asd\n");
  printf("  -e, --set              print settings of ul_asd\n");
  printf("  -p, --print            print units\n");
  printf("  -m, --module           module\n");
  printf("  -a, --add <sn>         add static module spec. by -m and sn\n");
  printf("  -d, --del <sn>         remove static module\n");
  printf("  -P, --period <value>   status period in seconds\n");
  printf("  -c, --cycle <value>    cycle_start\n");
  printf("  -f, --first <value>    first dynamic address\n");
  printf("  -S, --save 		   save settings in ul_asd\n");
  printf("  -V, --version          show version\n");
  printf("  -h, --help             this usage screen\n");
}

int 
main(int argc, char *argv[])
{
  static struct option long_opts[] = {
    { "ns", 1, 0, 'n' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int  opt;

 #ifndef HAS_GETOPT_LONG
    while ((opt = getopt(argc, argv, "n:m:P:c:f:a:d:tepdSVh")) != EOF)
 #else
    while ((opt = getopt_long(argc, argv, "n:m:P:c:f:a:d:tepSVh",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'n':
      orbit_ns_addr=optarg;
      break;
    case 't':
      statistics_flg=1;
      break;
    case 'e':
      settings_flg=1;
      break;
    case 'P':
      status_period=strtol(optarg,NULL,0);;
      status_period_flg=1;
      break;
    case 'c':
      cycle_start=strtol(optarg,NULL,0);;
      cycle_start_flg=1;
      break;
    case 'f':
      first_dy_adr=strtol(optarg,NULL,0);;
      first_dy_adr_flg=1;
      break;
    case 'p':
      units_flg=1;
      break;
    case 'm':
      module=strtol(optarg,NULL,0);
      break;
    case 'a':
      sn_add=strtol(optarg,NULL,0);
      add_flg=1;
      break;
    case 'd':
      sn_remove=strtol(optarg,NULL,0);
      del_flg=1;
      break;
    case 'S':
      save_flg=1;
      break;
    case 'V':
      printf("uLan address server, client - version 0.1\n");
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }
  
  printf("Binding to NS: %s\n",orbit_ns_addr);
  corba_client_init();
  printf("Binded\n");

  if (add_flg) 
    ulci_settings_add_static_unit(serv_set,module,sn_add,ev);

  if (del_flg)
    ulci_settings_remove_static_unit(serv_set,sn_remove,ev);

  if (first_dy_adr_flg)
    ulci_settings__set_first_dy_adr(serv_set,first_dy_adr,ev);

  if (cycle_start_flg)
    ulci_settings__set_cycle_start(serv_set,cycle_start,ev);

  if (status_period_flg)
    ulci_settings__set_status_period(serv_set,status_period,ev);

  if (statistics_flg)
    print_statistics();
    
  if (settings_flg)
    print_settings();
    
  if (units_flg)
    print_units();

  if (save_flg)
    ulci_settings_save(serv_set,ev);
    
  corba_client_cleanup();

  return 0;
}
