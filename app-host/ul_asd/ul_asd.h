#ifndef _UL_ASD_MAIN_H
#define _UL_ASD_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include <orbit/orbit.h>
#include <ORBitservices/CosNaming.h>
#include <ul_netbase.h>


#define ORBIT_NS_ADDR_DEFAULT "-ORBNamingIOR=corbaloc:iiop:1.2@localhost:4444/NameService"
#define ORBIT_ORBIIOPIPName_DEFAULT "-ORBIIOPIPName=localhost"
/* 
 * background task 
 */
typedef struct {
  int argc;
  char **argv;
  GMutex *mutex;
  char *orbit_ns_addr;
  char *orbit_orb_iiop_ipname;
  char conf_filename[128];

  //uLan thread
  char *ul_dev_name;
  ul_net_info_t *ul_net;
  GThread *ul_net_thread;
  int ul_net_terminate;
	
  //CORBA server thread
  CORBA_Environment ev[1];
  CORBA_ORB orb;
  PortableServer_POA root_poa; 
  CosNaming_NamingContext ns;
  CORBA_Object serv_set;
  CORBA_Object serv_stat;
  GThread *ul_server_thread;
} ul_asd_context_t;

int ul_asd_conf_load(const char *filename,ul_asd_context_t *ctx); 
int ul_asd_conf_save(const char *filename,ul_asd_context_t *ctx); 

int ul_asd_net_init(ul_asd_context_t *ctx);
void ul_asd_net_run(ul_asd_context_t *ctx);
int ul_asd_net_stop(ul_asd_context_t *ctx);
int ul_asd_net_cleanup(ul_asd_context_t *ctx);

int ul_asd_server_init(ul_asd_context_t *ctx);
void ul_asd_server_run(ul_asd_context_t *ctx);
int ul_asd_server_stop(ul_asd_context_t *ctx);
int ul_asd_server_cleanup(ul_asd_context_t *ctx);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_ASD_MAIN_H */
