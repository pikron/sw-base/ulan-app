#include <string.h>
#include <stdlib.h>
#include <libxml/parser.h>
#include <ul_netbase.h>
#include "ul_asd.h"


/*******************************************************************/
int 
ul_asd_conf_load(const char *filename, ul_asd_context_t *ul_cont) 
{
    xmlDocPtr doc;
    xmlNodePtr cur,cur1;

    if (!filename) 
      filename=ul_cont->conf_filename;
    
    xmlKeepBlanksDefault(1);
    doc = xmlParseFile(filename);
    if (doc == NULL) return(-1);
    cur = xmlDocGetRootElement(doc);
    if (cur == NULL) {
        fprintf(stderr,"empty document\n");
	xmlFreeDoc(doc);
	return(-1);
    }
    cur = cur->children;
    while(cur){
      if (!xmlStrcmp(cur->name, (const xmlChar *)"body")) break;
      cur = cur->next;
    }
    if (!cur) {
        fprintf(stderr,"no body section!\n");
	xmlFreeDoc(doc);
	return(-1);
    }
    cur = cur->children;
    while (cur) {
      //configuration section
      if (!xmlStrcmp(cur->name,(const xmlChar *)"configuration")) {
        cur1 = cur->children;
	while(cur1) {
	  xmlChar *value = xmlGetProp(cur1, (const xmlChar *)"value");
          if (!xmlStrcmp(cur1->name,(const xmlChar *)"status_period")) {
	    ul_cont->ul_net->dy_state->period=strtol((char*)value,NULL,0);
	  } else if (!xmlStrcmp(cur1->name,(const xmlChar *)"first_dynamic_address")) {
            ul_cont->ul_net->dy_state->first_dy_adr=strtol((char*)value,NULL,0);
	  } else if (!xmlStrcmp(cur1->name,(const xmlChar *)"cycle_start")) {
	    ul_dy_cycle_subfn=strtol((char*)value,NULL,0);
	  }
	  free(value);
          cur1 = cur1->next;
	}
      }
      //static_addresses section
      if (!xmlStrcmp(cur->name,(const xmlChar *)"static_addresses")) {
        cur1 = cur->children;
	while(cur1) {
          if (!xmlStrcmp(cur1->name,(const xmlChar *)"module")) {
  	    xmlChar *adr,*sn;
	    adr= xmlGetProp(cur1, (const xmlChar *)"address");
	    sn= xmlGetProp(cur1, (const xmlChar *)"sn");
	    if (ul_mod_static_add(ul_cont->ul_net,
		strtol((char*)adr,NULL,0),
		strtol((char*)sn,NULL,0))<0) {
   	      printf("fail adding static module:%s,%s\n",adr,sn);
	    }	    
  	    free(adr);
  	    free(sn);
	  }
          cur1 = cur1->next;
	}
        
      }
      cur = cur->next;
    }
    xmlFreeDoc(doc);
    return 0;
}

/*******************************************************************/
int 
ul_asd_conf_save(const char *filename, ul_asd_context_t *ul_cont) 
{
  xmlNodePtr root, node_body, node_conf, node_statica, node;
  xmlDocPtr doc;
  ul_mod_info_t *mod;
  xmlChar value[20];
  int mindx;
  

  if (!filename) 
    filename=ul_cont->conf_filename;
    
  doc = xmlNewDoc ((const xmlChar *)"1.0");
  root = xmlNewDocNode (doc, NULL, (const xmlChar *)"xml", NULL);
  xmlDocSetRootElement (doc, root);
			
  node_body = xmlNewChild (root, NULL, (const xmlChar *)"body", NULL);
  node_conf = xmlNewChild (node_body, NULL, (const xmlChar *)"configuration", NULL);
  node_statica = xmlNewChild (node_body, NULL, (const xmlChar *)"static_addresses", NULL);
  
  //configuration
  node = xmlNewChild (node_conf, NULL, (const xmlChar *)"status_period", NULL);
  sprintf(value,(const xmlChar *)"%d",ul_cont->ul_net->dy_state->period);
  xmlSetProp (node, (const xmlChar *)"value", value);

  node = xmlNewChild (node_conf, NULL, (const xmlChar *)"first_dynamic_address", NULL);
  sprintf(value,(const xmlChar *)"%d",ul_cont->ul_net->dy_state->first_dy_adr);
  xmlSetProp (node, (const xmlChar *)"value", value);

  node = xmlNewChild (node_conf, NULL, (const xmlChar *)"cycle_start", NULL);
  sprintf(value,(const xmlChar *)"%d",ul_dy_cycle_subfn);
  xmlSetProp (node, (const xmlChar *)"value", value);

  //static addresses
  for(mindx=0;mindx<ul_cont->ul_net->modules.count;mindx++){
    mod=ul_cont->ul_net->modules.items[mindx];
    if (mod->flg&ULMI_STATIC) {
      node = xmlNewChild (node_statica, NULL, (const xmlChar *)"module", NULL);
      sprintf(value,(const xmlChar *)"%d",mod->mod_adr);
      xmlSetProp (node, (const xmlChar *)"address", value);
      sprintf(value,(const xmlChar *)"0x%lx",mod->mod_sn);
      xmlSetProp (node, (const xmlChar *)"sn", value);
    }
  }
  
  xmlKeepBlanksDefault(0);
  xmlSaveFormatFile (filename, doc, 1);
  xmlFreeDoc (doc);
  return 0;
}

