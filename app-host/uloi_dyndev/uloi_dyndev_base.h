#ifndef _ULOI_DYNDEV_BASE_H
#define _ULOI_DYNDEV_BASE_H

#include <uloi_base.h>
#include <uloi_doitab.h>
#include <uloi_array.h>
#include <uloi_pdoproc.h>
#include <uloi_pdomap.h>
#include <uloi_pdoev.h>
#include <uloi_dynoids.h>
#include <uloi_dynpdo.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct uloi_dyndev_base_t {
  uloi_objdes_array_t objdes_main;
  uloi_doitabdes_t doitabdes;
  uloi_pdoproc_state_t pdostate_main;
  uloi_ciddes_array_t ciddes_main_pico;
  uloi_ciddes_array_t ciddes_main_poco;
  uloi_arraydes_t piom_des;
  uloi_pev2cdes_array_t pev2cdes_main;
  uloi_evarr_t evarr;
  char destroy_evarr;
} uloi_dyndev_base_t;

int
uloi_dyndev_base_init(uloi_dyndev_base_t *dyndev);

void
uloi_dyndev_base_done(uloi_dyndev_base_t *dyndev);

int
uloi_dyndev_base_std_setup(uloi_dyndev_base_t *dyndev, int a_options,
     unsigned pico_items, unsigned poco_items, unsigned piom_items, unsigned pev2c_items);

int
uloi_dyndev_base_evarr_setup(uloi_dyndev_base_t *dyndev, unsigned evnum_max);

void
uloi_dyndev_base_set_ev(uloi_dyndev_base_t *dyndev, int evnum);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_ULOI_DYNDEV_BASE_H*/
