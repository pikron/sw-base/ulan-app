#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <uloi_base.h>
#include <uloi_doitab.h>
#include <uloi_array.h>
#include <uloi_pdoproc.h>
#include <uloi_pdomap.h>
#include <uloi_pdoev.h>
#include <uloi_dynoids.h>
#include <uloi_dynpdo.h>
#include "appl_config.h"
#include "uloi_dyndev_base.h"

int
uloi_dyndev_base_init(uloi_dyndev_base_t *dyndev)
{
  uloi_objdes_array_init_array_field(&dyndev->objdes_main);
  return 0;
}

void
uloi_dyndev_base_done(uloi_dyndev_base_t *dyndev)
{
  if(dyndev==NULL)
    return;

  uloi_ciddes_array_resize(dyndev->pdostate_main.pico_ciddes_array, 0);
  uloi_ciddes_array_resize(dyndev->pdostate_main.poco_ciddes_array, 0);
  uloi_pev2cdes_array_resize(dyndev->pdostate_main.pev2cdes_array, 0);

  if(dyndev->pdostate_main.meta_base!=NULL)
    free(dyndev->pdostate_main.meta_base);
  dyndev->pdostate_main.meta_base=NULL;
  dyndev->pdostate_main.meta_len=0;

  {
    uloi_objdes_t *objdes;
    while((objdes=uloi_objdes_array_cut_last(&dyndev->objdes_main))!=NULL) {
      if(objdes->aoid)
        free((void*)objdes->aoid);
      free(objdes);
    }

    uloi_objdes_array_delete_all(&dyndev->objdes_main);
  }

  if(dyndev->destroy_evarr){
    if(dyndev->evarr.bits!=NULL)
      free(dyndev->evarr.bits);
    dyndev->evarr.evnum_max=0;

    if(dyndev->pdostate_main.pev2cdes_array->pev2c_map!=NULL)
      free(dyndev->pdostate_main.pev2cdes_array->pev2c_map);
    dyndev->pdostate_main.pev2cdes_array->pev2c_map=NULL;
  }

  if(dyndev->pdostate_main.buff!=NULL)
    free(dyndev->pdostate_main.buff);
  dyndev->pdostate_main.buff=NULL;
  dyndev->pdostate_main.buff_len=0;
}

#if 0
ULOI_GENOBJDES(PMAP_CLEAR,ULOI_PMAP_CLEAR,"e",NULL_CODE,NULL,&uloi_pmap_clear_wrfnc,(void*)&uloi_pdostate_main)
ULOI_GENOBJDES(PMAP_SAVE,ULOI_PMAP_SAVE,"e",NULL_CODE,NULL,&uloi_pmap_save_wrfnc,(void*)&uloi_pdostate_main)
#ifdef CONFIG_ULOI_WITH_GMASK
ULOI_GENOBJDES(PICO_GMASK,ULOI_PICO_GMASK,ULOI_GEN_ARR_SIZE(ULOI_PICO_ITEMS) ULOI_GMASK_TYPE_DES,uloi_array_rdfnc,(void*)&uloi_pico_gmask_des,uloi_array_wrfnc,(void*)&uloi_pico_gmask_des)
ULOI_GENOBJDES(PEV2C_GMASK,ULOI_PEV2C_GMASK,ULOI_GEN_ARR_SIZE(ULOI_PEV2C_ITEMS) ULOI_GMASK_TYPE_DES,uloi_array_rdfnc,(void*)&uloi_pev2c_gmask_des,uloi_array_wrfnc,(void*)&uloi_pev2c_gmask_des)
#endif /* CONFIG_ULOI_WITH_GMASK */
#endif

const struct {uloi_oid_t oid;
  int (*wrfnc)(ULOI_PARAM_coninfo void *context);}
uloi_dyndev_base_oids4enum[]={
  {ULOI_DOII,uloi_doii_fnc},
  {ULOI_DOIO,uloi_doio_fnc},
  {ULOI_QOII,uloi_qoii_fnc},
  {ULOI_QOIO,uloi_qoio_fnc},
  {ULOI_RDRQ,uloi_rdrq_fnc}
};

int
uloi_dyndev_base_std_setup(uloi_dyndev_base_t *dyndev, int a_options,
     unsigned pico_items, unsigned poco_items, unsigned piom_items, unsigned pev2c_items)
{
  int i;
  char s[21];
  uloi_objdes_t *objdes;

  do {
    for(i=0;i<sizeof(uloi_dyndev_base_oids4enum)/sizeof(*uloi_dyndev_base_oids4enum);i++) {
      objdes=uloi_objdes_new_and_setup(uloi_dyndev_base_oids4enum[i].oid,NULL,NULL,
               NULL_CODE,NULL, uloi_dyndev_base_oids4enum[i].wrfnc,(void*)&dyndev->objdes_main);
      if(objdes==NULL)
        break;
      if(uloi_objdes_array_insert(&dyndev->objdes_main,objdes)<0) {
        free(objdes);
        objdes=NULL;
        break;
      }
    }
    if(objdes==NULL)
      break;

    dyndev->doitabdes.objdes_array=&dyndev->objdes_main;
    objdes=uloi_objdes_new_and_setup(ULOI_DOITAB,NULL,NULL,
               uloi_doitab_rdfnc,&dyndev->doitabdes,
               uloi_doitab_fnc,&dyndev->doitabdes);
    if(objdes==NULL)
      break;
    if(uloi_objdes_array_insert(&dyndev->objdes_main,objdes)<0) {
      free(objdes);
      break;
    }

    dyndev->pdostate_main.objdes_array = &dyndev->objdes_main;

    if(dyndev->pdostate_main.buff==NULL) {
      if(!dyndev->pdostate_main.buff_len) {
        dyndev->pdostate_main.buff_len=127;
      }
      dyndev->pdostate_main.buff=malloc(dyndev->pdostate_main.buff_len);
      if(dyndev->pdostate_main.buff==NULL)
        break;
    }

    dyndev->pdostate_main.meta_base = malloc(sizeof(uchar)*piom_items);
    if(dyndev->pdostate_main.meta_base==NULL)
      break;
    memset(dyndev->pdostate_main.meta_base,0,sizeof(uchar)*piom_items);
    dyndev->pdostate_main.meta_len = piom_items;

    dyndev->piom_des.array_size = dyndev->pdostate_main.meta_len;
    dyndev->piom_des.item_size = sizeof(uchar);
    dyndev->piom_des.array_fl = 0;
    dyndev->piom_des.item_rdfnc = uloi_uchar_rdfnc;
    dyndev->piom_des.item_rdcontext = dyndev->pdostate_main.meta_base;
    dyndev->piom_des.item_wrfnc = uloi_uchar_wrfnc;
    dyndev->piom_des.item_wrcontext = dyndev->pdostate_main.meta_base;

    dyndev->pdostate_main.pico_ciddes_array = &dyndev->ciddes_main_pico;
    dyndev->pdostate_main.poco_ciddes_array = &dyndev->ciddes_main_poco;

    dyndev->pdostate_main.pev2cdes_array = &dyndev->pev2cdes_main;
    dyndev->pdostate_main.pev2cdes_array->evarr = &dyndev->evarr;

    if(uloi_ciddes_array_resize(dyndev->pdostate_main.pico_ciddes_array, pico_items)<0)
      break;

    if(uloi_ciddes_array_resize(dyndev->pdostate_main.poco_ciddes_array, poco_items)<0)
      break;

    if(uloi_pev2cdes_array_resize(dyndev->pdostate_main.pev2cdes_array, pev2c_items)<0)
      break;

    snprintf(s,sizeof(s)-1,"[%u]pico",pico_items);
    objdes=uloi_objdes_new_and_setup(ULOI_PICO,"PICO",s,
               uloi_pico_array_rdfnc, (void*)&dyndev->pdostate_main,
               uloi_pico_array_wrfnc, (void*)&dyndev->pdostate_main);
    if(objdes==NULL)
      break;
    if(uloi_objdes_array_insert(&dyndev->objdes_main,objdes)<0) {
      free(objdes);
      objdes=NULL;
      break;
    }

    snprintf(s,sizeof(s)-1,"[%u]poco",poco_items);
    objdes=uloi_objdes_new_and_setup(ULOI_POCO,"POCO",s,
               uloi_poco_array_rdfnc, (void*)&dyndev->pdostate_main,
               uloi_poco_array_wrfnc, (void*)&dyndev->pdostate_main);
    if(objdes==NULL)
      break;
    if(uloi_objdes_array_insert(&dyndev->objdes_main,objdes)<0) {
      free(objdes);
      objdes=NULL;
      break;
    }

    snprintf(s,sizeof(s)-1,"[%u]u1",piom_items);
    objdes=uloi_objdes_new_and_setup(ULOI_PIOM,"PIOM",s,
               uloi_array_rdfnc, (void*)&dyndev->piom_des,
               uloi_array_wrfnc, (void*)&dyndev->piom_des);
    if(objdes==NULL)
      break;
    if(uloi_objdes_array_insert(&dyndev->objdes_main,objdes)<0) {
      free(objdes);
      objdes=NULL;
      break;
    }

    snprintf(s,sizeof(s)-1,"[%u]pev2c",pev2c_items);
    objdes=uloi_objdes_new_and_setup(ULOI_PEV2C,"PEV2C",s,
               uloi_pev2c_array_rdfnc, (void*)&dyndev->pdostate_main,
               uloi_pev2c_array_wrfnc, (void*)&dyndev->pdostate_main);
    if(objdes==NULL)
      break;
    if(uloi_objdes_array_insert(&dyndev->objdes_main,objdes)<0) {
      free(objdes);
      objdes=NULL;
      break;
    }

    return 0;
  } while(0);

  return -1;
}

int
uloi_dyndev_base_evarr_setup(uloi_dyndev_base_t *dyndev, unsigned evnum_max)
{
  void *evarr_bits;
  uloi_pev2cdes_t **pev2c_map;
  size_t evarr_bits_size;

  evarr_bits_size=((evnum_max+ULOI_EVARR_BASETYPE_BIT)/ULOI_EVARR_BASETYPE_BIT)*
                    sizeof(*dyndev->evarr.bits);

  evarr_bits=malloc(evarr_bits_size);
  if(evarr_bits==NULL)
    return -1;
  memset(evarr_bits,0,evarr_bits_size);

  pev2c_map=malloc((evnum_max+1)*sizeof(*pev2c_map));
  if(pev2c_map==NULL){
    free(evarr_bits);
    return -1;
  }
  memset(pev2c_map,0,(evnum_max+1)*sizeof(*pev2c_map));

  dyndev->evarr.bits=evarr_bits;
  dyndev->evarr.evnum_max=evnum_max;

  dyndev->pdostate_main.pev2cdes_array->evarr = &dyndev->evarr;
  dyndev->pdostate_main.pev2cdes_array->pev2c_map=pev2c_map;

  dyndev->destroy_evarr=1;

  return 0;
}

void
uloi_dyndev_base_set_ev(uloi_dyndev_base_t *dyndev, int evnum)
{
  if(dyndev->pdostate_main.pev2cdes_array==NULL)
    return;
  if(dyndev->pdostate_main.pev2cdes_array->evarr==NULL)
    return;
  uloi_evarr_set_ev(dyndev->pdostate_main.pev2cdes_array->evarr,evnum);
}
