#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <uloi_base.h>
#include <uloi_doitab.h>
#include <uloi_array.h>
#include <uloi_pdoproc.h>
#include <uloi_pdomap.h>
#include <uloi_pdoev.h>
#include <uloi_dynoids.h>
#include <uloi_dynpdo.h>
#include <ul_evpoll.h>
#include <ul_evcbase.h>
#include "appl_config.h"
#include "uloi_dyndev_base.h"

struct uloi_dyndev_dyac_state_t;

typedef struct uloi_dyndevice_t {
  uloi_dyndev_base_t base;
  evc_rx_hub_t ev_rx_hub;
  ul_fd_t ul_rdfd;
  ul_fd_t ul_wrfd;
  ul_evptrig_t ul_rdfd_evptrig;
  ul_evptrig_t ul_wrfd_evptrig;
  uloi_con_ulan_t con_ulan;
  uloi_coninfo_t *coninfo;
  uloi_con_ulan_t con_ulan_pdo_send;
  uloi_coninfo_t *coninfo_pdo_send;
  struct uloi_dyndev_dyac_state_t *dyac_state;
} uloi_dyndevice_t;

#ifdef CONFIG_ULAN_DY
#include <uldy_base.h>
typedef struct uloi_dyndev_dyac_state_t {
  ul_dyac_t dyac;
} uloi_dyndev_dyac_state_t;

char uloi_dyndevice_save_sn(uint32_t usn)
{
  return 0;
}
char uloi_dyndevice_save_adr(uint8_t uaddr)
{
  return 0;
}
#endif /*CONFIG_ULAN_DY*/

void uloi_dyndevice_ev_rx_fnc(uloi_dyndevice_t *dyndev, long evnum);

uloi_dyndevice_t *
uloi_dyndevice_new(void)
{
  uloi_dyndevice_t *dyndev;
  dyndev=malloc(sizeof(*dyndev));
  if(dyndev==NULL)
    return NULL;
  memset(dyndev,0,sizeof(*dyndev));
  uloi_dyndev_base_init(&dyndev->base);

  evc_rx_hub_init(&dyndev->ev_rx_hub, (evc_rx_fnc_t*)uloi_dyndevice_ev_rx_fnc, dyndev);


  return dyndev;
}

void
uloi_dyndevice_done(uloi_dyndevice_t *dyndev)
{
  if(dyndev==NULL)
    return;

 #ifdef CONFIG_ULAN_DY
  if(dyndev->dyac_state != NULL) {
    if(dyndev->dyac_state->dyac.ul_idstr != NULL)
      free(dyndev->dyac_state->dyac.ul_idstr);
    free(dyndev->dyac_state);
    dyndev->dyac_state = NULL;
  }
 #endif /*CONFIG_ULAN_DY*/

  if(!ul_evptrig_is_detached(&dyndev->ul_rdfd_evptrig))
    ul_evptrig_done(&dyndev->ul_rdfd_evptrig);

  if(!ul_evptrig_is_detached(&dyndev->ul_wrfd_evptrig))
    ul_evptrig_done(&dyndev->ul_wrfd_evptrig);

  evc_rx_hub_done(&dyndev->ev_rx_hub);

  uloi_dyndev_base_done(&dyndev->base);
}

void
uloi_dyndevice_destroy(uloi_dyndevice_t *dyndev)
{
  uloi_dyndevice_done(dyndev);
  free(dyndev);
}

int
uloi_dyndevice_std_setup(uloi_dyndevice_t *dyndev, int a_options,
     unsigned pico_items, unsigned poco_items, unsigned piom_items, unsigned pev2c_items)
{
  return uloi_dyndev_base_std_setup(&dyndev->base, a_options,
              pico_items, poco_items, piom_items, pev2c_items);
}

int
uloi_dyndevice_evarr_setup(uloi_dyndevice_t *dyndev, unsigned evnum_max)
{
  return uloi_dyndev_base_evarr_setup(&dyndev->base, evnum_max);
}

void uloi_dyndevice_ev_rx_fnc(uloi_dyndevice_t *dyndev, long evnum)
{
  uloi_dyndev_base_set_ev(&dyndev->base, evnum);

  /* FIXME: deffer to main loop later */
  uloi_pev2c_procev(dyndev->base.pdostate_main.pev2cdes_array);
  uloi_pev2c_proclocal(&dyndev->base.pdostate_main);
  uloi_pev2c_procsend(dyndev->coninfo_pdo_send, &dyndev->base.pdostate_main, 300);
}


void uloi_dyndevice_std_ul_rdfd_cb(ul_evptrig_t *evptrig, int what)
{
  int ret;
  uloi_dyndevice_t *dyndev = UL_CONTAINEROF(evptrig, uloi_dyndevice_t, ul_rdfd_evptrig);

  if(what & UL_EVP_DONE) {
    /* Destroy dyndevice */
    return;
  }

  if(what & UL_EVP_TIMEOUT) {
    return;
  }

  if(what & UL_EVP_READ) {
    if((ul_inepoll(uloi_con_ulan_rdfd(dyndev->coninfo))>0)){
     #if 0
      uloi_process_msg(dyndev->coninfo, &dyndev->objdes_main, NULL);
     #else
      int free_fl = 0;
      do {
        ul_msginfo msginfo;
        if (ul_acceptmsg(uloi_con_ulan_rdfd(dyndev->coninfo), &msginfo)<0)
          break;        /* No mesage reported - break */
        free_fl = 1;
        if (msginfo.flg&(UL_BFL_PROC|UL_BFL_FAIL))
          break;        /* Reported message informs about fault or carried out processing */
        if(msginfo.cmd==uloi_con_ulan_cmd(dyndev->coninfo)){
          if (uloi_process_msg(dyndev->coninfo, &dyndev->base.objdes_main, &msginfo)>=0)
            free_fl = 0;
          break;
        }

        if(msginfo.cmd==UL_CMD_PDO){
          if (uloi_pdoproc_msg(dyndev->coninfo, &dyndev->base.pdostate_main, &msginfo)>=0)
            free_fl = 0;
          break;
        }

       #ifdef CONFIG_ULAN_DY
        if ((msginfo.cmd==UL_CMD_NCS) && (dyndev->dyac_state != NULL)){
          dyndev->dyac_state->dyac.ul_fd = uloi_con_ulan_rdfd(dyndev->coninfo);
          if (uldy_process_msg(&dyndev->dyac_state->dyac, &msginfo)>=0)
            free_fl = 0;
          break;
        }
       #endif /*CONFIG_ULAN_DY*/
      } while(0);

      if (free_fl)
        ul_freemsg(uloi_con_ulan_rdfd(dyndev->coninfo));

      uloi_pev2c_procev(dyndev->base.pdostate_main.pev2cdes_array);
      uloi_pev2c_proclocal(&dyndev->base.pdostate_main);
      uloi_pev2c_procsend(dyndev->coninfo_pdo_send, &dyndev->base.pdostate_main, 300);
     #endif
    }
  }
}

void uloi_dyndevice_std_ul_wrfd_cb(ul_evptrig_t *evptrig, int what)
{
  int ret;
  uloi_dyndevice_t *dyndev = UL_CONTAINEROF(evptrig, uloi_dyndevice_t, ul_wrfd_evptrig);

  if(what & UL_EVP_DONE) {
    /* Destroy dyndevice */
    return;
  }

  if(what & UL_EVP_TIMEOUT) {
    return;
  }

  if(what & UL_EVP_READ) {
    ul_msginfo msginfo;
    if((ul_inepoll(uloi_con_ulan_wrfd(dyndev->coninfo))>0)){
      ul_acceptmsg(uloi_con_ulan_wrfd(dyndev->coninfo), &msginfo);
      ul_freemsg(uloi_con_ulan_wrfd(dyndev->coninfo));
    }
  }
}

int
uloi_dyndevice_connect(uloi_dyndevice_t *dyndev, ul_evpbase_t *evpbase,
         const char *dev_name, int subdev, int module, const char *idstr)
{
  int res;
  ul_msginfo msginfo;

  if(ul_evptrig_init(evpbase,&dyndev->ul_rdfd_evptrig)<0)
    return -1;
  if(ul_evptrig_init(evpbase,&dyndev->ul_wrfd_evptrig)<0)
    return -1;

  dyndev->ul_rdfd=ul_open(dev_name,NULL);
  if(dyndev->ul_rdfd==UL_FD_INVALID) {
    fprintf(stderr, "ul_open \"%s\" failed: %s\n", dev_name?dev_name:"", strerror(errno));
    return -1;
  }
  dyndev->ul_wrfd=ul_open(dev_name,NULL);
  if(dyndev->ul_wrfd==UL_FD_INVALID){
    ul_close(dyndev->ul_rdfd);
    fprintf(stderr, "ul_open \"%s\" failed: %s\n", dev_name?dev_name:"", strerror(errno));
    return -1;
  }
 #ifdef CONFIG_OC_UL_DRV_WITH_MULTI_DEV
  if(subdev>=0) {
    if(ul_setsubdev(dyndev->ul_rdfd, subdev)<0) {
      fprintf(stderr, "ul_setsubdev %d failed: %s\n", subdev, strerror(errno));
      return -1;
    }
    if(ul_setsubdev(dyndev->ul_wrfd, subdev)<0) {
      fprintf(stderr, "ul_setsubdev %d failed: %s\n", subdev, strerror(errno));
      return -1;
    }
  }
  if(module>0) {
    if(ul_setmyadr(dyndev->ul_rdfd, module)<0) {
      fprintf(stderr, "ul_setmyadr %d failed: %s\n", module, strerror(errno));
      return -1;
    }
  }
  if(idstr!=NULL) {
    if((res=ul_setidstr(dyndev->ul_rdfd,idstr))<0) {
      fprintf(stderr, "ul_setidstr \"%s\" failed: %s\n", idstr, strerror(errno));
      return -1;
    }
  }
 #endif /*CONFIG_OC_UL_DRV_WITH_MULTI_DEV*/

  dyndev->coninfo=&dyndev->con_ulan.con;
  dyndev->coninfo_pdo_send=&dyndev->con_ulan_pdo_send.con;

  uloi_con_ulan_set_cmd_fd(dyndev->coninfo, UL_CMD_OISV, dyndev->ul_rdfd, dyndev->ul_wrfd);
  uloi_con_ulan_set_cmd_fd(dyndev->coninfo_pdo_send, 0, dyndev->ul_rdfd, dyndev->ul_wrfd);

  memset(&msginfo, 0, sizeof(msginfo));
  msginfo.cmd=uloi_con_ulan_cmd(dyndev->coninfo);
  ul_addfilt(uloi_con_ulan_rdfd(dyndev->coninfo), &msginfo);

  msginfo.cmd=UL_CMD_PDO;
  ul_addfilt(uloi_con_ulan_rdfd(dyndev->coninfo), &msginfo);

 #ifdef CONFIG_ULAN_DY
  dyndev->dyac_state = malloc(sizeof(*dyndev->dyac_state));
  if (dyndev->dyac_state == NULL) {
    fprintf(stderr, "can not allocate memory for dyac state\n");
    return -1;
  }
  res = uldy_init(&dyndev->dyac_state->dyac, dyndev->ul_rdfd, uloi_dyndevice_save_sn,
            uloi_dyndevice_save_adr, strdup(idstr), /*sn*/0xff001234);
  if(res < 0) {
    fprintf(stderr, "uldy_init failed\n");
    return -1;
  }
  msginfo.cmd=UL_CMD_NCS;
  ul_addfilt(uloi_con_ulan_rdfd(dyndev->coninfo), &msginfo);
 #endif /*CONFIG_ULAN_DY*/

/*
  uloi_pdoproc_pico_ciddes_table_remap(&dyndev->pdostate_main);
  uloi_pdoproc_poco_ciddes_table_remap(&dyndev->pdostate_main);
  uloi_pev2c_table_remap(dyndev->pdostate_main.pev2cdes_array);
  uloi_evarr_clear_all(dyndev->pdostate_main.pev2cdes_array->evarr);
 */

  if(ul_evptrig_set_and_arm_fd(&dyndev->ul_rdfd_evptrig, uloi_dyndevice_std_ul_rdfd_cb,
        ul_fd2sys_fd(dyndev->ul_rdfd), UL_EVP_READ, NULL, 0)<0)
    return -1;
  if(ul_evptrig_set_and_arm_fd(&dyndev->ul_wrfd_evptrig, uloi_dyndevice_std_ul_wrfd_cb,
        ul_fd2sys_fd(dyndev->ul_wrfd), UL_EVP_READ, NULL, 0)<0)
    return -1;

  return 0;
}

#include <unistd.h>

typedef struct console_fd_t {
  ul_evptrig_t evptrig;
  int fd;
  int count;
  evc_tx_hub_t ev_tx_hub;
} console_fd_t;

void console_fd_cb(ul_evptrig_t *evptrig, int what)
{
  int ret;
  console_fd_t *cf = UL_CONTAINEROF(evptrig, console_fd_t, evptrig);

  if(what & UL_EVP_DONE) {
    ul_evptrig_done(&cf->evptrig);
    evc_tx_hub_done(&cf->ev_tx_hub);
    free(cf);
    return;
  }

  cf->count++;

  if(what & UL_EVP_READ) {
    char ch;
    long val;

    ret = read(cf->fd, &ch, 1);
    if(ret != 1)
      return;
    switch(ch) {
      case 'q':
        ul_evpoll_set_option(ul_evptrig_get_base(evptrig),UL_EVP_OPTION_QUIT,1);
        break;
      case '0' ... '9':
        val=ch-'0';
        evc_tx_hub_emit(&cf->ev_tx_hub,val);
        break;
    }
  }
}

console_fd_t *add_console_fd(ul_evpbase_t *evpbase, int fd)
{
  console_fd_t *cf;
  cf = malloc(sizeof(console_fd_t));
  if(!cf) {
    return NULL;
  }

  cf->fd = fd;
  cf->count = 0;

  if(ul_evptrig_init(evpbase, &cf->evptrig)<0) {
    free(cf);
    return NULL;
  }

  evc_tx_hub_init(&cf->ev_tx_hub);

  ul_evptrig_set_callback(&cf->evptrig, console_fd_cb);

  ul_evptrig_set_fd(&cf->evptrig, cf->fd, UL_EVP_READ);

  if(ul_evptrig_arm(&cf->evptrig)<0) {
    ul_evptrig_done(&cf->evptrig);
    free(cf);
    return NULL;
  }

  return cf;
}

int main(int argc, char *argv[])
{
  int res;
  uloi_dyndevice_t *dyndev;
  ul_evpbase_t *evpbase;
  const char *dev_name="/dev/ulan";
  int subdev=1;
  int module=4;
  const char *idstr=".mt dyndev";
  console_fd_t *cf;

  evpbase = ul_evpoll_chose_implicit_base();

  dyndev=uloi_dyndevice_new();
  if(dyndev==NULL)
    return 1;

  res=uloi_dyndevice_std_setup(dyndev,0, 100, 100, 100, 100);
  if(res<0){
    fprintf(stderr, "uloi_dyndevice_std_setup %d\n",res);
    return 1;
  }

  res=uloi_dyndevice_evarr_setup(dyndev, 200);
  if(res<0){
    fprintf(stderr, "uloi_dyndevice_evarr_setup %d\n",res);
    return 1;
  }

  res=uloi_dyndevice_connect(dyndev, evpbase, dev_name, subdev, module, idstr);
  if(res<0){
    fprintf(stderr, "uloi_dyndevice_connect %d\n",res);
    return 1;
  }

  cf=add_console_fd(evpbase, 0);
  if(cf==NULL){
    fprintf(stderr, "add_console_fd setup failed\n");
    return 1;
  }
  {
    evc_link_t *link=evc_link_new();
    res=evc_link_connect(link, &cf->ev_tx_hub, &dyndev->ev_rx_hub,
                   evc_link_propagate_i_p_l);
    evc_link_dec_refcnt(link);
  }
  if(res<0){
    fprintf(stderr, "evc_link_connect failed\n");
    return 1;
  }

  while(1) {
    res = ul_evpoll_dispatch(evpbase, NULL);
    if(res == UL_EVP_DISPRET_QUIT)
      break;
  }

  uloi_dyndevice_destroy(dyndev);

  ul_evpoll_destroy(evpbase);

  return 0;
}
