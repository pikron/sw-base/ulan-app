unit prop_ciddes_flg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ButtonPanel, ExtCtrls;

type

  { Tfprop_ciddes_flg }

  Tfprop_ciddes_flg = class(TForm)
    ButtonPanel1: TButtonPanel;
    Properties: TCheckGroup;
    Location: TRadioGroup;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fprop_ciddes_flg: Tfprop_ciddes_flg;

implementation

{ Tfprop_ciddes_flg }


initialization
  {$I prop_ciddes_flg.lrs}

end.

