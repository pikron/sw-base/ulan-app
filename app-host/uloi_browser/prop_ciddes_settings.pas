unit prop_ciddes_settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ButtonPanel, Grids, LCLType, ComCtrls, Buttons, uloi_pdo_types,
  prop_ciddes_flg;

type

  { Tfprop_ciddes_settings }

  Tfprop_ciddes_settings = class(TForm)
    ButtonPanel1: TButtonPanel;
    GroupBox1: TGroupBox;
    SGProp: TStringGrid;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ToolBar1: TToolBar;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure SGPropCreate(cnt:integer; list:TList);
    procedure CIDDesCreate(list:TList; sl:TStringList);
  end;

var
  fprop_ciddes_settings: Tfprop_ciddes_settings;

implementation

{ Tfprop_ciddes_settings }

procedure Tfprop_ciddes_settings.SpeedButton1Click(Sender: TObject);
var i:integer;
begin
  for i:=1 to SGProp.RowCount-1 do begin
    SGProp.Cells[0,i]:='0';
    SGProp.Cells[1,i]:='0';
    SGProp.Cells[2,i]:='0';
    SGProp.Cells[3,i]:='0';
  end;
end;

procedure Tfprop_ciddes_settings.SpeedButton2Click(Sender: TObject);
var flg,loc:integer;
begin
  if SGProp.Row<1 then
    exit;
  flg:=strtointdef(SGProp.Cells[1,SGProp.Row],0);
  loc:=flg and $0f;
  if loc>4 then exit;
  fprop_ciddes_flg.Location.ItemIndex:=loc;
  if (flg and $10)<>0 then
    fprop_ciddes_flg.Properties.Checked[0]:=true
  else
    fprop_ciddes_flg.Properties.Checked[0]:=false;
  if (flg and $20)<>0 then
    fprop_ciddes_flg.Properties.Checked[1]:=true
  else
    fprop_ciddes_flg.Properties.Checked[1]:=false;
  if (flg and $40)<>0 then
    fprop_ciddes_flg.Properties.Checked[2]:=true
  else
    fprop_ciddes_flg.Properties.Checked[2]:=false;
  if (flg and $80)<>0 then
    fprop_ciddes_flg.Properties.Checked[3]:=true
  else
    fprop_ciddes_flg.Properties.Checked[3]:=false;
  if (flg and $100)<>0 then
    fprop_ciddes_flg.Properties.Checked[4]:=true
  else
    fprop_ciddes_flg.Properties.Checked[4]:=false;
  if fprop_ciddes_flg.ShowModal=mrOK then begin
    flg:=fprop_ciddes_flg.Location.ItemIndex;
    if fprop_ciddes_flg.Properties.Checked[0] then flg:=flg+$10;
    if fprop_ciddes_flg.Properties.Checked[1] then flg:=flg+$20;
    if fprop_ciddes_flg.Properties.Checked[2] then flg:=flg+$40;
    if fprop_ciddes_flg.Properties.Checked[3] then flg:=flg+$80;
    if fprop_ciddes_flg.Properties.Checked[4] then flg:=flg+$100;
    SGProp.Cells[1,SGProp.Row]:=inttostr(flg);
  end;
end;

procedure Tfprop_ciddes_settings.SGPropCreate(cnt:integer; list:TList);
var i:integer;
    uoi_ciddes:puoi_ciddes;
begin
  if list=nil then
    exit;
  SGProp.RowCount:=cnt+1;
  for i:=1 to cnt do begin
    if i<=list.Count then begin
      uoi_ciddes:=list.Items[i-1];
      SGProp.Cells[0,i]:=inttostr(uoi_ciddes^.cid);
      SGProp.Cells[1,i]:=inttostr(uoi_ciddes^.flg);
      SGProp.Cells[2,i]:=inttostr(uoi_ciddes^.meta);
      SGProp.Cells[3,i]:=inttostr(uoi_ciddes^.meta_len);
    end else begin
      SGProp.Cells[0,i]:='0';
      SGProp.Cells[1,i]:='0';
      SGProp.Cells[2,i]:='0';
      SGProp.Cells[3,i]:='0';
    end;
  end;
end;

procedure Tfprop_ciddes_settings.CIDDesCreate(list:TList; sl:TStringList);
var i:integer;
    uoi_ciddes:puoi_ciddes;
    cid,flg,meta,meta_len:dword;
begin
  if list=nil then
    exit;
  sl.Clear;
  for i:=1 to SGProp.RowCount-1 do begin
    cid:=strtointdef(SGProp.Cells[0,i],0);
    flg:=strtointdef(SGProp.Cells[1,i],0);
    meta:=strtointdef(SGProp.Cells[2,i],0);
    meta_len:=strtointdef(SGProp.Cells[3,i],0);
    if (cid<>0) then begin
{      uoi_ciddes:=new(puoi_ciddes);
      uoi_ciddes^.cid:=cid;
      uoi_ciddes^.flg:=flg;
      uoi_ciddes^.meta:=meta;
      uoi_ciddes^.meta_len:=meta_len;
      list.Add(uoi_ciddes);}
      sl.Add(inttostr(cid)+','+inttostr(flg)+','+inttostr(meta)+','+inttostr(meta_len));
    end else begin
      break;
    end;
  end;
end;

initialization
  {$I prop_ciddes_settings.lrs}

end.

