unit mainform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, Menus, ComCtrls, Buttons, StdCtrls, uLan, Grids,
  SortList, {$IFDEF Win32}Windows, {$ENDIF} xmlcfg, uloi_pdo_types;

type

  tul_msgbuff = record
    data: array [0..4096] of byte;
    idx: integer;
    size:integer;
  end;
  pul_msgbuff = ^tul_msgbuff;

  tuoi_array_range = record
    start: integer;
    count: integer;
  end;
  puoi_array_range = ^tuoi_array_range;

  tuoi_prop = record
    number:integer;
    fread:boolean;
    fwrite:boolean;
    farray:boolean;
    farray_ranges:TList;
    name:string;
    typ:string;
    typ_full:string;
    items:integer;
    value:string;
    value_table:TList;
    value_len:integer;
  end;
  puoi_prop = ^tuoi_prop;

  tumod = record
    address:integer;
    sn:integer;
    idname:string;
    iddes:string;
    mt:string;
    uoi_prop_tree:TSortList;
  end;
  ptumod = ^tumod;

  rdvalue_fnc = procedure (msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
  wrvalue_fnc = procedure (msgbuff:pul_msgbuff; vallen: integer; s:string);

  { Tfmainform }

  Tfmainform = class(TForm)
    iddes: TLabel;
    idname: TLabel;
    ImageList2: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    modaddr: TLabel;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    SBReset: TSpeedButton;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    SBSetNewAddress: TSpeedButton;
    Shape6: TShape;
    Shape7: TShape;
    SpeedButton10: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    Splitter1: TSplitter;
    SGProp: TStringGrid;
    StatusBar: TStatusBar;
    ToolBar1: TToolBar;
    ToolBar2: TToolBar;
    ToolBar3: TToolBar;
    tvunits: TTreeView;
    uLan1: TuLan;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    function  oi_var_oiddes_add(tree:TSortList;oid:integer;despack:pbyte):puoi_prop;
    procedure oi_var_oidval_get(coninfo:puloi_coninfo_t;uoi_prop:puoi_prop);
    procedure oi_var_oidval_set(msgbuff: pul_msgbuff;ranges_arr:TList;uoi_prop:puoi_prop;value_to_set:TStringList);
    procedure SBResetClick(Sender: TObject);
    procedure SGPropRefresh(Sender: TObject);
    procedure SGPropSetOID(Sender: TObject);
    procedure SBSetNewAddressClick(Sender: TObject);
    procedure PreferenciesClick(Sender: TObject);
    procedure tvunitsCompare(Sender: TObject; Node1, Node2: TTreeNode;
      var Compare: Integer);
    procedure tvunitsChange(Sender: TObject; Node: TTreeNode);
    procedure UnitOIPropertiesAdd(tree:TSortList;u:integer);
    procedure Autodetection(Sender: TObject);
    procedure SGPropCreate(tree:TSortList);
    function mt2ImageIndex(mt:String):Integer;
    procedure CIDDesDispose(list:TList);
    procedure PEV2CDesDispose(list:TList);
    procedure RangesDispose(list:TList);
    procedure UnitOIPropertiesDispose(pumod:ptumod);
    procedure tvunitsDisposeData;
  private
    { private declarations }
    uerror:boolean;
    ustart,ustop:integer;
    ustartup_autodetection:boolean;
    ushow_oi_dictionary_by_identificator:boolean;
    startup:boolean;
    XMLConfig: TXMLConfig;
  public
    { public declarations }
  end; 

var
  fmainform: Tfmainform;

implementation

uses ul_drvdef, sd_info_process, prop_settings, sd_error, prop_set_new_address,
     fAbout, sd_confirm, preferences, prop_ciddes_settings, prop_pev2cdes_settings;

//*********************************************
function sort_uoi_prop_number(i1,i2: Pointer): integer;
var d1,d2: puoi_prop;
begin
  d1:=i1; d2:=i2;
  if d1^.number < d2^.number then Result:=-1
  else
    if d1^.number > d2^.number then Result:=1
    else
      Result:=0;
end;

procedure split(var arr:TStringList; const str: string);
var i,j:integer;
  strline, strfield: string;
begin
  strline:=str;
  repeat
    i:=1;
    while((i<=length(strline)) and (pos(strline[i],'[]')<>0)) do inc(i);
    if (i>length(strline)) then break;
    j:=i;
    while((j<=length(strline)) and (pos(strline[j],'[],')=0)) do inc(j);
    if (j>length(strline)) then begin
      strfield:=strline;
      strline:='';
    end else begin
      strfield:= Copy(strline, i, j-i);
      delete(strline,1,j);
    end;
    arr.Add(strfield);
    //check for last empty item
    if (length(strline)>0) and (strline=']') then
      arr.Add('');
  until false;
end;

function get_array_ranges(destype:string; ranges:TList):integer;
var p1,p2,val:integer;
    vs:string;
    range:puoi_array_range;
begin
  Result:=0;
  while true do begin
    p1:=Pos('[', destype);
    p2:=Pos(']', destype);
    if (p1>0) and (p2>0) and (p2>p1) then begin
      vs:=COPY(destype, p1+1, p2-p1-1);
      val:=strtointdef(vs,-1);
      delete(destype,p1,p2-p1+1);
      range:=new(puoi_array_range);
      range^.start:=0;
      range^.count:=val;
      ranges.Add(range);
    end else
      break;
  end;
end;

function get_array_primitive_type(s:string): string;
var p:integer;
begin
  while true do begin
    p:=Pos(']', s);
    if p=0 then begin
      Result:=s;
      exit;
    end;
    delete(s,1,p);
  end;
end;

procedure array_value_exec_gen(level:integer; ranges:TList; var s:string);
var i:integer;
    range:puoi_array_range;
begin
  if level=0 then begin
    s:=s+',';
    exit;
  end;

  range:=ranges[level-1];
  s:=s+'[';
  for i:=1 to range^.count-1 do begin
    array_value_exec_gen(level-1,ranges,s);
  end;
  s:=s+']';
end;

function uloi_array_rdrange_buff(msgbuff:pul_msgbuff; var from_idx: integer; var item_cnt: integer):integer;
var {$IFNDEF CPU64}
    v_u:dword;
    {$ELSE CPU64}
    v_u:qword;
    {$ENDIF CPU64}
begin
  Result:=-1;

  if ((msgbuff^.size - msgbuff^.idx -1) < 2) then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u, @(msgbuff^.data[msgbuff^.idx]), 2) < 0 then
    exit;
  msgbuff^.idx := msgbuff^.idx + 2;

  if (v_u and ULOI_ARRAY_USE_ITEM_CNT)=0 then begin
    from_idx := v_u;
    item_cnt := 1;
    Result := 0;
    exit;
  end;
  v_u := v_u and not ULOI_ARRAY_USE_ITEM_CNT;
  if (v_u and ULOI_ARRAY_USE_COMMAND)=0 then begin
    item_cnt := v_u;
    if ((msgbuff^.size - msgbuff^.idx -1) < 2) then
      exit;
    if uloi_tb_buff_rd_unsigned(@v_u, @(msgbuff^.data[msgbuff^.idx]), 2) < 0 then
      exit;
    msgbuff^.idx := msgbuff^.idx + 2;
    from_idx := v_u;
    Result := 0;
    exit;
  end;
  v_u := v_u and not ULOI_ARRAY_USE_COMMAND;
  Result:= v_u;
end;

function uloi_array_wrrange_buff(msgbuff:pul_msgbuff; from_idx: integer; item_cnt: integer):integer;
var {$IFNDEF CPU64}
    v_u:dword;
    {$ELSE CPU64}
    v_u:qword;
    {$ENDIF CPU64}
begin
  Result:=-1;

  if ((item_cnt<>1) or (from_idx>=ULOI_ARRAY_USE_ITEM_CNT)) then begin
    if ((msgbuff^.size - msgbuff^.idx -1) < 2) then
      exit;
    v_u:=item_cnt or ULOI_ARRAY_USE_ITEM_CNT;
    msgbuff^.data[msgbuff^.idx]:=v_u mod 256;
    msgbuff^.data[msgbuff^.idx+1]:=v_u div 256;
    msgbuff^.idx:=msgbuff^.idx+2;
  end;
  if ((msgbuff^.size - msgbuff^.idx -1) < 2) then
    exit;
  msgbuff^.data[msgbuff^.idx]:=from_idx mod 256;
  msgbuff^.data[msgbuff^.idx+1]:=from_idx div 256;
  msgbuff^.idx:=msgbuff^.idx+2;
  Result:=0;
end;

procedure uloi_tb_buff_wr_array_ranges_gen(level,dim,cnt:integer; ranges:TList; msgbuff:pul_msgbuff);
var i:integer;
    range:puoi_array_range;
begin
  if (level=dim) then
    exit;

  for i:=1 to cnt do begin
    range:=ranges[level];
    if (range^.count=1) then begin
      msgbuff^.data[msgbuff^.idx]:=range^.start mod 256;
      msgbuff^.data[msgbuff^.idx+1]:=range^.start div 256;
      msgbuff^.idx:=msgbuff^.idx+2;
    end else begin
      msgbuff^.data[msgbuff^.idx]:=range^.count mod 256;
      msgbuff^.data[msgbuff^.idx+1]:=(range^.count div 256) + $80;
      msgbuff^.data[msgbuff^.idx+2]:=range^.start mod 256;
      msgbuff^.data[msgbuff^.idx+3]:=range^.start div 256;
      msgbuff^.idx:=msgbuff^.idx+4;
    end;
    uloi_tb_buff_wr_array_ranges_gen(level+1, dim, range^.count, ranges, msgbuff);
    if (level=0) then
      break;
  end;
end;


procedure oi_var_rdvalue_float(msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
var v_f:single;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;
  if uloi_tb_buff_rd_float(@v_f, @(msgbuff^.data[msgbuff^.idx]), vallen) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx+vallen;
  s:=s+FloatToStr(v_f);
end;

procedure oi_var_wrvalue_float(msgbuff:pul_msgbuff; vallen: integer; s:string);
var v_f:single;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  v_f:=StrToFloat(s);
  if uloi_tb_buff_wr_float(@(msgbuff^.data[msgbuff^.idx]), vallen, v_f) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx+vallen;
end;

procedure oi_var_rdvalue_signed(msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
var {$IFNDEF CPU64}
    v_s:longint;
    {$ELSE CPU64}
    v_s:int64;
    {$ENDIF CPU64}
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;
  if uloi_tb_buff_rd_signed(@v_s, @(msgbuff^.data[msgbuff^.idx]), vallen) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx+vallen;
  s:=s+IntToStr(v_s);
end;

procedure oi_var_wrvalue_signed(msgbuff:pul_msgbuff; vallen: integer; s:string);
var {$IFNDEF CPU64}
    v_s:longint;
    {$ELSE CPU64}
    v_s:int64;
    {$ENDIF CPU64}
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  v_s:=StrToInt(s);
  if uloi_tb_buff_wr_signed(@(msgbuff^.data[msgbuff^.idx]), vallen, v_s) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx+vallen;
end;

procedure oi_var_rdvalue_unsigned(msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
var {$IFNDEF CPU64}
    v_u:dword;
    {$ELSE CPU64}
    v_u:qword;
    {$ENDIF CPU64}
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u, @(msgbuff^.data[msgbuff^.idx]), vallen) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx+vallen;
  s:=s+IntToStr(v_u);
end;

procedure oi_var_wrvalue_unsigned(msgbuff:pul_msgbuff; vallen: integer; s:string);
var {$IFNDEF CPU64}
    v_u:dword;
    {$ELSE CPU64}
    v_u:qword;
    {$ENDIF CPU64}
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  v_u:=StrToInt(s);
  if uloi_tb_buff_wr_unsigned(@(msgbuff^.data[msgbuff^.idx]), vallen, v_u) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx+vallen;
end;


procedure oi_var_rdvalue_string(msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
var sp:string;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  SetString(sp,@msgbuff^.data[msgbuff^.idx+1],integer(msgbuff^.data[msgbuff^.idx]));
  msgbuff^.idx:=msgbuff^.idx + integer(msgbuff^.data[msgbuff^.idx]) + 1;

  s:=s+sp;
end;

procedure oi_var_wrvalue_string(msgbuff:pul_msgbuff; vallen: integer; s:string);
var len:integer;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  len:=length(s);
  msgbuff^.data[msgbuff^.idx]:=len;
  if len>0 then
    StrPCopy(@msgbuff^.data[msgbuff^.idx+1],s);

  msgbuff^.idx:=msgbuff^.idx + len + 1;
end;


procedure oi_var_rdvalue_uoi_ciddes(msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
var {$IFNDEF CPU64}
    v_u1,v_u2,v_u3,v_u4:dword;
    {$ELSE CPU64}
    v_u1,v_u2,v_u3,v_u4:qword;
    {$ENDIF CPU64}
    uoi_ciddes:^tuoi_ciddes;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  if uloi_tb_buff_rd_unsigned(@v_u1, @(msgbuff^.data[msgbuff^.idx]), 2) < 0 then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u2, @(msgbuff^.data[msgbuff^.idx+2]), 2) < 0 then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u3, @(msgbuff^.data[msgbuff^.idx+4]), 2) < 0 then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u4, @(msgbuff^.data[msgbuff^.idx+6]), 2) < 0 then
    exit;

  msgbuff^.idx:=msgbuff^.idx+8;
  s:=s+'{'+IntToStr(v_u1)+','+IntToStr(v_u2)+','+IntToStr(v_u3)+','+IntToStr(v_u4)+'}';

  if lval<>nil then begin
    uoi_ciddes:=new(puoi_ciddes);
    uoi_ciddes^.cid:=v_u1;
    uoi_ciddes^.flg:=v_u2;
    uoi_ciddes^.meta:=v_u3;
    uoi_ciddes^.meta_len:=v_u4;
    lval.Add(uoi_ciddes);
  end;
end;

procedure oi_var_wrvalue_u2x4(msgbuff:pul_msgbuff; vallen: integer; s:string);
var {$IFNDEF CPU64}
    v_u1,v_u2,v_u3,v_u4:dword;
    {$ELSE CPU64}
    v_u1,v_u2,v_u3,v_u4:qword;
    {$ENDIF CPU64}
    len:integer;
    arr:TStringList;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  arr:=TStringList.Create;
  split(arr,s);
  if arr.Count<>4 then begin
    arr.Free;
    exit;
  end;
  v_u1:=strtointdef(arr.Strings[0],0);
  v_u2:=strtointdef(arr.Strings[1],0);
  v_u3:=strtointdef(arr.Strings[2],0);
  v_u4:=strtointdef(arr.Strings[3],0);
  arr.Free;
  if uloi_tb_buff_wr_unsigned(@(msgbuff^.data[msgbuff^.idx]), 2, v_u1) < 0 then
    exit;
  if uloi_tb_buff_wr_unsigned(@(msgbuff^.data[msgbuff^.idx+2]), 2, v_u2) < 0 then
    exit;
  if uloi_tb_buff_wr_unsigned(@(msgbuff^.data[msgbuff^.idx+4]), 2, v_u3) < 0 then
    exit;
  if uloi_tb_buff_wr_unsigned(@(msgbuff^.data[msgbuff^.idx+6]), 2, v_u4) < 0 then
    exit;
  msgbuff^.idx:=msgbuff^.idx + vallen;
end;

procedure oi_var_rdvalue_uio_ev2cdes(msgbuff:pul_msgbuff; vallen: integer; var s:string; lval: tlist);
var {$IFNDEF CPU64}
    v_u1,v_u2,v_u3,v_u4:dword;
    {$ELSE CPU64}
    v_u1,v_u2,v_u3,v_u4:qword;
    {$ENDIF CPU64}
    uoi_pev2cdes:^tuoi_pev2cdes;
begin
  if ((msgbuff^.size - msgbuff^.idx -1) < vallen) then
    exit;

  if uloi_tb_buff_rd_unsigned(@v_u1, @(msgbuff^.data[msgbuff^.idx]), 2) < 0 then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u2, @(msgbuff^.data[msgbuff^.idx+2]), 2) < 0 then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u3, @(msgbuff^.data[msgbuff^.idx+4]), 2) < 0 then
    exit;
  if uloi_tb_buff_rd_unsigned(@v_u4, @(msgbuff^.data[msgbuff^.idx+6]), 2) < 0 then
    exit;

  msgbuff^.idx:=msgbuff^.idx+8;
  s:=s+'{'+IntToStr(v_u1)+','+IntToStr(v_u2)+','+IntToStr(v_u3)+','+IntToStr(v_u4)+'}';

  if lval<>nil then begin
    uoi_pev2cdes:=new(puoi_pev2cdes);
    uoi_pev2cdes^.evnum:=v_u1;
    uoi_pev2cdes^.cid:=v_u2;
    uoi_pev2cdes^.dadr:=v_u3;
    uoi_pev2cdes^.flg:=v_u4;
    lval.Add(uoi_pev2cdes);
  end;
end;

procedure oi_var_wrvalue_exec(msgbuff:pul_msgbuff; vallen: integer; s:string);
begin
end;


function oi_var_rdarray(dim: integer; msgbuff:pul_msgbuff; vallen: integer; rd_fnc: rdvalue_fnc; var s:string; lval: tlist):integer;
var idx,cnt: integer;
begin
  Result:=uloi_array_rdrange_buff(msgbuff, idx, cnt);
  if(Result < 0) then
    exit;

  s:=s+'[';
  while (cnt<>0) do begin
    dec(cnt);
    if (dim<=1) then begin
      if rd_fnc<>nil then
        rd_fnc(msgbuff,vallen,s,lval);
      if (cnt<>0) then
        s:=s+',';
    end else begin
      oi_var_rdarray(dim-1,msgbuff,vallen,rd_fnc,s,lval);
    end;
  end;
  s:=s+']';
end;

function oi_var_wrarray(ranges_arr:TList; range_idx:integer; msgbuff: pul_msgbuff; data:TStringList;var data_idx:integer;
                        vallen: integer; wr_fnc: wrvalue_fnc):integer;
var idx,cnt: integer;
  range:puoi_array_range;
begin
  range:=ranges_arr[range_idx];
  idx:=range^.start;
  cnt:=range^.count;
  if (cnt=-1) then begin
    Result:=-1;
    exit;
  end;
  Result:=uloi_array_wrrange_buff(msgbuff, idx, cnt);
  if(Result < 0) then
    exit;

  while(cnt<>0) do begin
    dec(cnt);
    if (range_idx>=ranges_arr.Count-1) then begin
      if wr_fnc<>nil then begin
        wr_fnc(msgbuff,vallen,data.Strings[data_idx]);
        inc(data_idx);
      end;
    end else begin
      oi_var_wrarray(ranges_arr,range_idx+1,msgbuff,data,data_idx,vallen,wr_fnc);
    end;
  end;
end;

{ Tfmainform }

procedure Tfmainform.oi_var_oidval_get(coninfo:puloi_coninfo_t;uoi_prop:puoi_prop);
var rd_fnc: rdvalue_fnc;
  msgbuff: tul_msgbuff;
  metabuff: tul_msgbuff;
  ret: integer;
begin
  if (uoi_prop^.value_len<0) then
    exit;

  rd_fnc:=nil;
  if (length(uoi_prop^.typ)>=2) and (uoi_prop^.typ[2]>='0') and
     (uoi_prop^.typ[2]<='9') then begin
    case uoi_prop^.typ[1] of
      'u' : begin { u1, u2, u3, u4 types }
         rd_fnc:=@oi_var_rdvalue_unsigned;
        end;
      's' : begin { s1, s2, s3, s4 types }
         rd_fnc:=@oi_var_rdvalue_signed;
        end;
      'f' : begin { f2, f4 types }
         rd_fnc:=@oi_var_rdvalue_float;
        end;
    end;
  end else if (length(uoi_prop^.typ)>=2) and (uoi_prop^.typ[1]='v') and (uoi_prop^.typ[2]='s') then begin
    rd_fnc:=@oi_var_rdvalue_string;
  end else if (uoi_prop^.typ='pico') or (uoi_prop^.typ='poco') then begin
    rd_fnc:=@oi_var_rdvalue_uoi_ciddes;
  end else if uoi_prop^.typ='pev2c'  then begin
    rd_fnc:=@oi_var_rdvalue_uio_ev2cdes;
  end;

  if rd_fnc=nil then
    exit;

  msgbuff.size:=length(msgbuff.data);
  msgbuff.idx:=0;
  metabuff.size:=length(msgbuff.data);
  metabuff.idx:=0;

  if (uoi_prop^.farray_ranges.Count>0) then
     uloi_tb_buff_wr_array_ranges_gen(0,uoi_prop^.farray_ranges.Count,1,uoi_prop^.farray_ranges,@metabuff);

  ret:=uloi_get_var(coninfo,uoi_prop^.number,@metabuff.data,metabuff.idx,@msgbuff.data,msgbuff.size);
  if ret<0 then exit;

  uoi_prop^.value:='';
  if (uoi_prop^.farray_ranges.Count=0) then begin
    rd_fnc(@msgbuff,uoi_prop^.value_len,uoi_prop^.value,uoi_prop^.value_table);
  end else begin
    oi_var_rdarray(uoi_prop^.farray_ranges.Count,@msgbuff,uoi_prop^.value_len,rd_fnc,uoi_prop^.value,uoi_prop^.value_table);
  end;
end;

procedure Tfmainform.oi_var_oidval_set(msgbuff: pul_msgbuff;ranges_arr:TList;uoi_prop:puoi_prop;value_to_set:TStringList);
var wr_fnc: wrvalue_fnc;
  ret,value_idx: integer;
  s: string;
  range:puoi_array_range;
  items,i:integer;
begin
  if (uoi_prop^.value_len<0) then
    exit;

  items:=1;
  for i:=0 to ranges_arr.Count-1 do begin
    range:=ranges_arr[i];
    if range^.count=-1 then begin
      items:=-1;
      break;
    end else begin
      items:=items * range^.count;
    end;
  end;
  if items=-1 then
    exit;
  if (items<>value_to_set.Count) then
    exit;

  wr_fnc:=nil;
  if (length(uoi_prop^.typ)>=2) and (uoi_prop^.typ[2]>='0') and
     (uoi_prop^.typ[2]<='9') then begin
    case uoi_prop^.typ[1] of
      'u' : begin { u1, u2, u3, u4 types }
         wr_fnc:=@oi_var_wrvalue_unsigned;
        end;
      's' : begin { s1, s2, s3, s4 types }
         wr_fnc:=@oi_var_wrvalue_signed;
        end;
      'f' : begin { f2, f4 types }
         wr_fnc:=@oi_var_wrvalue_float;
        end;
    end;
  end else if (length(uoi_prop^.typ)>=2) and (uoi_prop^.typ[1]='v') and (uoi_prop^.typ[2]='s') then begin
    wr_fnc:=@oi_var_wrvalue_string;
  end else if (uoi_prop^.typ='pico') or (uoi_prop^.typ='poco') then begin
    wr_fnc:=@oi_var_wrvalue_u2x4;
  end else if uoi_prop^.typ='pev2c'  then begin
    wr_fnc:=@oi_var_wrvalue_u2x4;
  end else if uoi_prop^.typ='e'  then begin
    wr_fnc:=@oi_var_wrvalue_exec;
  end;

  if wr_fnc=nil then
    exit;

  if (ranges_arr.Count=0) then begin
    s:=value_to_set.Strings[0];
    wr_fnc(msgbuff,uoi_prop^.value_len,s);
  end else begin
    value_idx:=0;
    oi_var_wrarray(ranges_arr,0,msgbuff,value_to_set,value_idx,uoi_prop^.value_len,wr_fnc);
  end;
end;

function Tfmainform.oi_var_oiddes_add(tree:TSortList;oid:integer;despack:pbyte):puoi_prop;
var
  desname:pchar;
  destype:pchar;
  uoi_prop:puoi_prop;
  suoi_prop:tuoi_prop;
  i:integer;
  range:puoi_array_range;
  v:string;
begin
  Result:=nil;
  desname:=uloi_oiddespack_strdup(despack,0);
  destype:=uloi_oiddespack_strdup(despack,1);
  if (desname<>nil) then begin
    suoi_prop.number:=oid;
    i:=tree.Search(@suoi_prop);
    if i=-1 then begin
      v:='';
      uoi_prop:=new(puoi_prop);
      uoi_prop^.number:=oid;
      uoi_prop^.name:=desname;
      uoi_prop^.farray:=false;
      uoi_prop^.farray_ranges:=TList.Create;
      get_array_ranges(destype,uoi_prop^.farray_ranges);
      uoi_prop^.items:=1;
      if uoi_prop^.farray_ranges.Count>0 then begin
        uoi_prop^.farray:=true;
        uoi_prop^.typ:=get_array_primitive_type(destype);
        uoi_prop^.typ_full:=destype;
        for i:=0 to uoi_prop^.farray_ranges.Count-1 do begin
          range:=uoi_prop^.farray_ranges[i];
          if range^.count=-1 then begin
            uoi_prop^.items:=-1;
            break;
          end else begin
            uoi_prop^.items:=uoi_prop^.items * range^.count;
          end;
        end;
        if (uoi_prop^.typ='e') and (uoi_prop^.items<>-1) then
          array_value_exec_gen(uoi_prop^.farray_ranges.Count,uoi_prop^.farray_ranges,v);
      end else begin
        uoi_prop^.typ:=destype;
        uoi_prop^.typ_full:=destype;
      end;
      uoi_prop^.value_len:=uloi_tb_type2size(pchar(uoi_prop^.typ), length(uoi_prop^.typ));
      uoi_prop^.fread:=false;
      uoi_prop^.fwrite:=false;
      uoi_prop^.value:=v;
      uoi_prop^.value_table:=TList.Create;
      tree.AddSort(uoi_prop);
    end else begin
      uoi_prop:=tree.Items[i];
    end;
    Result:=uoi_prop;
  end;
  uloi_cfree(desname);
  uloi_cfree(destype);
end;

procedure Tfmainform.SBResetClick(Sender: TObject);
var
  pumod:ptumod;
  r:ttreenode;
  buf_out:array [0..10] of byte;
  ret:integer;
begin
  r:=tvunits.Selected;
  if r=nil then exit;
  while true do begin
    if (r.Parent<>nil) then r:=r.Parent
    else break;
  end;
  pumod:=r.Data;
  if pumod=nil then exit;
  fsd_confirm.LText.Caption:='Do you want to make module '+inttostr(pumod^.address)+' reset?';
  if fsd_confirm.ShowModal=mrOk then begin
    buf_out[0]:=ULRES_CPU;
    buf_out[1]:=$55;
    buf_out[2]:=$aa;
    ret:=uLan1.CommandSendWait(pumod^.address,UL_CMD_RES,UL_BFL_ARQ or UL_BFL_PRQ,@buf_out,3);
    if ret<0 then
      MessageDlg('Module '+inttostr(pumod^.address)+' reset ERROR',mtError,[mbOk],-1)
    else
      MessageDlg('Module '+inttostr(pumod^.address)+' reset OK',mtInformation,[mbOk],-1);
  end;
end;

procedure Tfmainform.UnitOIPropertiesAdd(tree:TSortList;u:integer);
var coninfo:puloi_coninfo_t;
    i,ret:integer;
    oids:plongint;
    despack:pbyte;
    uoi_prop:puoi_prop;
begin
  coninfo:=uloi_open(pchar(string(uLan1.OSDeviceName)),u,$10,0,10);
  if coninfo=nil then begin
    writeln('error open coninfo');
    exit;
  end;
  { set vars }
  ret:=uloi_get_oids(coninfo,ULOI_QOII,@oids);
  if(ret<0) then exit;
  for i:=0 to ret-1 do begin
    despack:=nil;
    if(uloi_get_oiddes(coninfo,ULOI_DOII,oids[i],@despack)>=0) then begin
      uoi_prop:=oi_var_oiddes_add(tree,oids[i],despack);
      if uoi_prop<>nil then begin
        uoi_prop^.fwrite:=true;
      end;
      uloi_cfree(despack);
    end;
  end;
  uloi_cfree(oids);
  { get vars }
  ret:=uloi_get_oids(coninfo,ULOI_QOIO,@oids);
  if(ret<0) then exit;
  for i:=0 to ret-1 do begin
    despack:=nil;
    if(uloi_get_oiddes(coninfo,ULOI_DOIO,oids[i],@despack)>=0) then begin
      uoi_prop:=oi_var_oiddes_add(tree,oids[i],despack);
      if uoi_prop<>nil then begin
         oi_var_oidval_get(coninfo,uoi_prop);
         uoi_prop^.fread:=true;
      end;
      uloi_cfree(despack);
    end;
  end;
  uloi_cfree(oids);
  uloi_close(coninfo);
end;

function Tfmainform.mt2ImageIndex(mt:String):Integer;
begin
  Result:=0;
  if mt='rja2000m30' then Result:=1;
  if mt='blinder-hisc' then Result:=2;
end;

procedure Tfmainform.CIDDesDispose(list:TList);
var uoi_ciddes:^tuoi_ciddes;
begin
  if list = nil then
    exit;
  while true do begin
    uoi_ciddes:=list.First;
    if uoi_ciddes=nil then break;
    list.Delete(0);
    Dispose(uoi_ciddes);
  end;
  list.Free;
end;

procedure Tfmainform.PEV2CDesDispose(list:TList);
var uoi_pev2cdes:^tuoi_pev2cdes;
begin
  if list = nil then
    exit;
  while true do begin
    uoi_pev2cdes:=list.First;
    if uoi_pev2cdes=nil then break;
    list.Delete(0);
    Dispose(uoi_pev2cdes);
  end;
  list.Free;
end;

procedure Tfmainform.RangesDispose(list:TList);
var range:puoi_array_range;
begin
  if list = nil then
    exit;
  while true do begin
    range:=list.First;
    if range=nil then break;
    list.Delete(0);
    Dispose(range);
  end;
  list.free;
end;

procedure Tfmainform.UnitOIPropertiesDispose(pumod:ptumod);
var
  uoi_prop_tree:TSortList;
  uoi_prop:^tuoi_prop;
  value_pico_table:TList;
begin
  if pumod=nil then
    exit;
  {oi property}
  uoi_prop_tree:=pumod^.uoi_prop_tree;
  while true do begin
    uoi_prop:=uoi_prop_tree.First;
    if uoi_prop=nil then break;
    if uoi_prop^.value_table <> nil then begin
      if (uoi_prop^.typ='pico') or (uoi_prop^.typ='poco') then begin
        CIDDesDispose(uoi_prop^.value_table);
        uoi_prop^.value_table:=nil;
      end else if(uoi_prop^.typ='pev2c') then begin
        PEV2CDesDispose(uoi_prop^.value_table);
        uoi_prop^.value_table:=nil;
      end else begin
        { unfred memory}
        FreeAndNil(uoi_prop^.value_table);
      end
    end;
    { free descriptor ranges }
    RangesDispose(uoi_prop^.farray_ranges);
    uoi_prop^.farray_ranges:=nil;
    uoi_prop_tree.Delete(0);
    Dispose(uoi_prop);
  end;
  uoi_prop_tree.Free;
end;


procedure Tfmainform.tvunitsDisposeData;
var
  i: integer;
  Item: TTreeNode;
  pumod:ptumod;
begin
  if tvunits.Items.Count = 0 then exit;
  for i:=0 to tvunits.Items.Count-1 do begin
    Item:=tvunits.Items.Item[i];
    pumod:=Item.Data;
    if pumod<>nil then begin
      UnitOIPropertiesDispose(pumod);
      Dispose(pumod);
      Item.Data:=nil;
    end;
  end;
end;

procedure Tfmainform.Autodetection(Sender: TObject);
var ur,cr:ttreenode;
    ii,i,buf_len,pb,pe,po:integer;
    buf:array[0..256] of char;
    pbuf:pchar;
    mt:string;
    pumod:ptumod;
    ifrm:Tfsd_info_process;
begin
  pbuf:=@buf;
  SGProp.RowCount:=1;
  ifrm:=Tfsd_info_process.Create(self);
  ifrm.uSetText('Autodetection');
  application.ProcessMessages;
  tvunitsDisposeData;
  tvunits.Items.Clear;
  modaddr.Caption:='';
  idname.Caption:='';
  iddes.Caption:='';
  ifrm.uShow(ustart,ustop);
  try
    for i := ustart to ustop do begin
      buf_len:=sizeof(buf);
      FillChar(buf, SizeOf(buf), 0);                                                     //max input buffer size
      if( uLan1.QuerySendWait(i, UL_CMD_SID, UL_BFL_NORE or UL_BFL_PRQ, nil, 0, pbuf, buf_len) > 0 ) then begin
        pb:=pos('.mt ',pbuf);
        mt:='';
        if pb<>0 then begin
          pb:=pb+4;
          pe:=pos(' ',pbuf+pb);
          if (pe<>0) then begin
            mt:=copy(pbuf,pb,pe);
          end;
        end;
        po:=pos('.oi',pbuf);
        {add only units have object interface}
        if (po<>0) or (not ushow_oi_dictionary_by_identificator) then begin
          ii:=mt2ImageIndex(mt);
          ur:=tvunits.Items.AddChild(nil,inttostr(i)+', '+mt);
          ur.ImageIndex:=ii;
          ur.SelectedIndex:=ii;
          pumod:=New(ptumod);
          pumod^.address:=i;
          pumod^.idname:=buf;
          pumod^.mt:=mt;
          pumod^.iddes:='';
          pumod^.sn:=0;
          pumod^.uoi_prop_tree:=TSortList.Create;
          pumod^.uoi_prop_tree.Compare:=@sort_uoi_prop_number;
          ur.Data:=pumod;
          cr:=tvunits.Items.AddChild(ur,buf);
          cr.ImageIndex:=-1;
          cr.SelectedIndex:=-1;
          UnitOIPropertiesAdd(pumod^.uoi_prop_tree,i);
        end;
      end;
      ifrm.uSetPosition(i);
      application.ProcessMessages;
    end;
  finally
    ifrm.uHide;
    ifrm.Free;
  end;
end;

procedure Tfmainform.SGPropSetOID(Sender: TObject);
var uoi_prop:puoi_prop;
  pumod:ptumod;
  r:ttreenode;
  coninfo:puloi_coninfo_t;
  despack:pbyte;
  range:puoi_array_range;
  i,items:integer;
  value_to_set:TStringList;
  range_arr:TList;
  range_item:puoi_array_range;
  msgbuff: tul_msgbuff;
  oid_num_used:boolean;
label end_err;
begin
  if SGProp.Row<1 then exit;
  r:=tvunits.Selected;
  if r=nil then exit;
  while true do begin
    if (r.Parent<>nil) then r:=r.Parent
    else break;
  end;
  pumod:=r.Data;
  if pumod=nil then exit;
  uoi_prop:=pumod^.uoi_prop_tree[SGProp.Row-1];
  if not uoi_prop^.fwrite then begin
    fsd_error.LText.Caption:='The property '''+uoi_prop^.name+''' hasn''t write access!';
    fsd_error.ShowModal;
    exit;
  end;
  coninfo:=uloi_open(pchar(string(uLan1.OSDeviceName)),pumod^.address,$10,0,10);
  if coninfo=nil then begin
    writeln('error open coninfo');
    exit;
  end;
  range_arr:=nil;
  msgbuff.size:=length(msgbuff.data);
  msgbuff.idx:=0;
  value_to_set:=nil;
  if (uoi_prop^.typ='pico') or (uoi_prop^.typ='poco') then begin
    fprop_ciddes_settings.SGPropCreate(uoi_prop^.items,uoi_prop^.value_table);
    if fprop_ciddes_settings.ShowModal<>mrOK then
      goto end_err;
    CIDDesDispose(uoi_prop^.value_table);
    uoi_prop^.value_table:=TList.Create;
    value_to_set:=TStringList.Create;
    fprop_ciddes_settings.CIDDesCreate(uoi_prop^.value_table,value_to_set);
    range_arr:=TList.Create;
    range_item:=new(puoi_array_range);
    range_item^.start:=0;
    range_item^.count:=value_to_set.Count;
    range_arr.Add(range_item);
    oi_var_oidval_set(@msgbuff,range_arr,uoi_prop,value_to_set);
    RangesDispose(range_arr);
  end else if (uoi_prop^.typ='pev2c') then begin
    fprop_pev2cdes_settings.SGPropCreate(uoi_prop^.items,uoi_prop^.value_table);
    if fprop_pev2cdes_settings.ShowModal<>mrOk then
      goto end_err;
    PEV2CDesDispose(uoi_prop^.value_table);
    uoi_prop^.value_table:=TList.Create;
    value_to_set:=TStringList.Create;
    fprop_pev2cdes_settings.PEV2CDesCreate(uoi_prop^.value_table,value_to_set);
    range_arr:=TList.Create;
    range_item:=new(puoi_array_range);
    range_item^.start:=0;
    range_item^.count:=value_to_set.Count;
    range_arr.Add(range_item);
    oi_var_oidval_set(@msgbuff,range_arr,uoi_prop,value_to_set);
    RangesDispose(range_arr);
  end else begin
    fprop_settings.LNumber.Caption:=inttostr(uoi_prop^.number);
    fprop_settings.LName.Caption:=uoi_prop^.name;
    fprop_settings.EValue.Text:=uoi_prop^.value;
    if fprop_settings.ShowModal<>mrOK then
      goto end_err;
    value_to_set:=TStringList.Create;
    split(value_to_set, fprop_settings.EValue.Text);
    if uoi_prop^.items<>-1 then begin
      if value_to_set.Count=uoi_prop^.items then begin
        if (uoi_prop^.typ='e') and (uoi_prop^.farray_ranges.Count>0) then begin
          if uoi_prop^.farray_ranges.Count<>1 then begin
            fsd_error.LText.Caption:='Doesn''t supported write access to multidimension execute variable.';
            fsd_error.ShowModal;
            goto end_err;
          end;
          oid_num_used:=true;
          for i:=0 to value_to_set.Count-1 do begin
            if value_to_set.Strings[i]<>'' then begin
              if not oid_num_used then begin
                if uloi_tb_buff_wr_unsigned(@(msgbuff.data[msgbuff.idx]), 2, uoi_prop^.number) < 0 then
                  goto end_err;
                msgbuff.idx:=msgbuff.idx + 2;
              end;
              if uloi_tb_buff_wr_unsigned(@(msgbuff.data[msgbuff.idx]), 2, i) < 0 then
                goto end_err;
              msgbuff.idx:=msgbuff.idx + 2;
              oid_num_used:=false;
            end;
          end;
          if msgbuff.idx=0 then
            goto end_err;
        end else begin
          oi_var_oidval_set(@msgbuff,uoi_prop^.farray_ranges,uoi_prop,value_to_set);
        end;
      end else begin
        fsd_error.LText.Caption:='OID needs '+inttostr(uoi_prop^.items)+'item(s), but was written '+inttostr(value_to_set.Count)+'.';
        fsd_error.ShowModal;
        goto end_err;
      end;
    end else begin
      fsd_error.LText.Caption:='Doesn''t supported write access to undefined array dimension variable.';
      fsd_error.ShowModal;
      goto end_err;
    end;
  end;
  uloi_set_var(coninfo,uoi_prop^.number,@msgbuff.data,msgbuff.idx);
  if uoi_prop^.typ='e' then
    uoi_prop^.value:=fprop_settings.EValue.Text;
    SGProp.Cells[4,SGProp.Row]:=uoi_prop^.value;
  if uoi_prop^.fread then begin
    oi_var_oidval_get(coninfo,uoi_prop);
    SGProp.Cells[4,SGProp.Row]:=uoi_prop^.value;
  end;
end_err:
  if value_to_set<>nil then
    FreeAndNil(value_to_set);
  uloi_close(coninfo);
end;

procedure Tfmainform.SGPropRefresh(Sender: TObject);
var
  pumod:ptumod;
  r:ttreenode;
begin
  if SGProp.Row<1 then exit;
  r:=tvunits.Selected;
  if r=nil then exit;
  while true do begin
    if (r.Parent<>nil) then r:=r.Parent
    else break;
  end;
  pumod:=r.Data;
  if pumod=nil then exit;
  SGProp.RowCount:=1;
  Application.ProcessMessages;
  UnitOIPropertiesDispose(pumod);
  pumod^.uoi_prop_tree:=TSortList.Create;
  pumod^.uoi_prop_tree.Compare:=@sort_uoi_prop_number;
  UnitOIPropertiesAdd(pumod^.uoi_prop_tree,pumod^.address);
  SGPropCreate(pumod^.uoi_prop_tree);
end;

procedure Tfmainform.SBSetNewAddressClick(Sender: TObject);
var
  pumod:ptumod;
  r:ttreenode;
  buf_out:array [0..10] of byte;
  module_sn,new_addr:integer;
begin
  r:=tvunits.Selected;
  if r=nil then exit;
  while true do begin
    if (r.Parent<>nil) then r:=r.Parent
    else break;
  end;
  pumod:=r.Data;
  if pumod=nil then exit;
  fprop_set_new_address.LAddress.Caption:=inttostr(pumod^.address);
  fprop_set_new_address.ESN.Text:=inttostr(pumod^.sn);
  fprop_set_new_address.ENAddress.Text:='';
  if fprop_set_new_address.ShowModal=mrOK then begin
    module_sn:=strtointdef(fprop_set_new_address.ESN.Text,0);
    new_addr:=strtointdef(fprop_set_new_address.ENAddress.Text,0);
    if (new_addr<1) or (new_addr>63) then exit;
    buf_out[0]:=ULNCS_SET_ADDR;	{ SN0 SN1 SN2 SN3 NEW_ADR }
    buf_out[1]:=module_sn>>0;
    buf_out[2]:=module_sn>>8;
    buf_out[3]:=module_sn>>16;
    buf_out[4]:=module_sn>>24;
    buf_out[5]:=new_addr;
    uLan1.CommandSend(pumod^.address,UL_CMD_NCS,UL_BFL_ARQ,@buf_out,6);
    if fprop_set_new_address.CBSave.Checked then begin
      buf_out[0]:=ULNCS_ADDR_NVSV;
      buf_out[1]:=module_sn>>0;
      buf_out[2]:=module_sn>>8;
      buf_out[3]:=module_sn>>16;
      buf_out[4]:=module_sn>>24;
      uLan1.CommandSend(new_addr,UL_CMD_NCS,UL_BFL_ARQ,@buf_out,5);
    end;
    pumod^.address:=new_addr;
    r.Text:=inttostr(new_addr)+', '+pumod^.mt;
    tvunits.SortType:=stNone;
    tvunits.SortType:=stData;
    r.Expanded:=True;
    r.Expanded:=False;
  end;
end;

procedure Tfmainform.PreferenciesClick(Sender: TObject);
begin
  fpreferences.EOSDriverName.Text:=ulan1.OSDeviceName;
  fpreferences.EFirstUnit.Text:=inttostr(ustart);
  fpreferences.ELastUnit.Text:=inttostr(ustop);
  fpreferences.CBAutodetection.Checked:=ustartup_autodetection;
  fpreferences.CBOIDictionary.Checked:=ushow_oi_dictionary_by_identificator;
  if fpreferences.ShowModal=mrOK then begin
{$IFDEF Win32}
    XMLConfig.SetValue('settings/OSDeviceNameWindows',fpreferences.EOSDriverName.Text);
{$ENDIF}
{$IFDEF UNIX}
    XMLConfig.SetValue('settings/OSDeviceNameLinux',fpreferences.EOSDriverName.Text);
{$ENDIF}
    ustart:=strtointdef(fpreferences.EFirstUnit.Text,1);
    XMLConfig.SetValue('settings/first_unit',ustart);
    ustop:=strtointdef(fpreferences.ELastUnit.Text,63);
    XMLConfig.SetValue('settings/last_unit',ustop);
    XMLConfig.SetValue('settings/startup_autodetection',fpreferences.CBAutodetection.Checked);
    ushow_oi_dictionary_by_identificator:=fpreferences.CBOIDictionary.Checked;
    XMLConfig.SetValue('settings/show_oi_dictionary_by_identificator',fpreferences.CBOIDictionary.Checked);
    XMLConfig.Flush;
  end
end;

procedure Tfmainform.tvunitsCompare(Sender: TObject; Node1, Node2: TTreeNode;
  var Compare: Integer);
var pumod1,pumod2:ptumod;
begin
  Compare:=0;
  pumod1:=ptumod(Node1.Data);
  pumod2:=ptumod(Node2.Data);
  if (pumod1=nil) or (pumod2=nil) then exit;
  if pumod1^.address < pumod2^.address then Compare:=-1
    else
      if pumod1^.address > pumod2^.address then Compare:=1
      else
        Compare:=0;
end;


procedure Tfmainform.SGPropCreate(tree:TSortList);
var i:integer;
 uoi_prop:puoi_prop;
 s:string;
begin
  SGProp.RowCount:=1;
  for i:=0 to tree.Count-1 do begin
    uoi_prop:=tree.Items[i];
    SGProp.RowCount:=SGProp.RowCount+1;
    s:='';
    if uoi_prop^.fread then s:='R';
    if uoi_prop^.fwrite then s:=s+'W';
    SGProp.Cells[0,SGProp.RowCount-1]:=inttostr(uoi_prop^.number);
    SGProp.Cells[1,SGProp.RowCount-1]:=uoi_prop^.typ_full;
    SGProp.Cells[2,SGProp.RowCount-1]:=s;
    SGProp.Cells[3,SGProp.RowCount-1]:=uoi_prop^.name;
    SGProp.Cells[4,SGProp.RowCount-1]:=uoi_prop^.value;
  end;
end;

procedure Tfmainform.tvunitsChange(Sender: TObject; Node: TTreeNode);
var pumod:ptumod;
begin
  if node=nil then exit;
  while true do begin
    if (node.Parent<>nil) then node:=node.Parent
    else break;
  end;
  pumod:=node.Data;
  if pumod=nil then exit;
  modaddr.caption:=inttostr(pumod^.address);
  idname.Caption:=pumod^.idname;
  iddes.Caption:=pumod^.iddes;
  SGPropCreate(pumod^.uoi_prop_tree);
end;


procedure Tfmainform.FormCreate(Sender: TObject);
var FData:string;
    SizeVer:Integer;
    dwHandle: DWord;
    FVer: Pointer;
{$IFDEF Win32}
    SBSize: UInt;
    FileInfo: PVSFixedFileInfo;
{$ENDIF}
begin
{$IFDEF Win32}
  { cti verzi }
  SizeVer := GetFileVersionInfoSize(PChar(Application.exename), dwHandle);
  if SizeVer > 0 then begin
    SetLength(FData, SizeVer);
    try
      if GetFileVersionInfo(PChar(Application.exename), dwHandle, SizeVer, PChar(FData)) then
        if VerQueryValue(PChar(FData), '\', FVer, SBSize) then begin
            FileInfo := PVSFixedFileInfo(FVer);
            StatusBar.Panels.Items[1].Text :=
                            IntToStr(HIWORD(FileInfo^.dwFileVersionMS)) + '.' +
                            IntToStr(LOWORD(FileInfo^.dwFileVersionMS)) + '.' +
                            IntToStr(HIWORD(FileInfo^.dwFileVersionLS)) + '.' +
                            IntToStr(LOWORD(FileInfo^.dwFileVersionLS));
                            IntToStr(LOWORD(FileInfo^.dwProductVersionLS));
        end;
    finally
    end;
  end;
{$ELSE}
  StatusBar.Panels.Items[1].Text :='0.1.2.701';
{$ENDIF}
  XMLConfig:=TXMLConfig.Create(Self);
  XMLConfig.Filename:=ChangeFileExt(Application.exename,'.xml');
  {defautl values}
  ulan1.OSDeviceName:=UL_DEV_NAME;
  ustart:=1;
  ustop:=63;
  ustartup_autodetection:=true;
  ushow_oi_dictionary_by_identificator:=true;
  {load config}
{$IFDEF Win32}
  ulan1.OSDeviceName:=XMLConfig.GetValue('settings/OSDeviceNameWindows',ulan1.OSDeviceName);
{$ENDIF}
{$IFDEF UNIX}
  ulan1.OSDeviceName:=XMLConfig.GetValue('settings/OSDeviceNameLinux',ulan1.OSDeviceName);
{$ENDIF}
  ustart:=XMLConfig.GetValue('settings/first_unit',ustart);
  ustop:=XMLConfig.GetValue('settings/last_unit',ustop);
  ustartup_autodetection:=XMLConfig.GetValue('settings/startup_autodetection',ustartup_autodetection);
  ushow_oi_dictionary_by_identificator:=XMLConfig.GetValue('settings/show_oi_dictionary_by_identificator',ushow_oi_dictionary_by_identificator);
  ulan1.Active:=True;
  if not ulan1.GetActive then begin
    MessageDlg('Error during opening of ulan driver.',mtError,[mbOk],-1);
    uerror:=True;
  end;
  startup:=True;
end;

procedure Tfmainform.FormDestroy(Sender: TObject);
begin
  tvunitsDisposeData;
  XMLConfig.Free;
end;

procedure Tfmainform.MenuItem2Click(Sender: TObject);
begin
  close;
end;

procedure Tfmainform.MenuItem4Click(Sender: TObject);
begin
  ShowAboutBox(StatusBar.Panels.Items[1].Text);
end;

procedure Tfmainform.FormActivate(Sender: TObject);
begin
  if uerror then begin
    close;
    exit;
  end;
  if startup and ustartup_autodetection then begin
    startup:=False;
    Autodetection(Sender);
  end;
end;

initialization
  {$I mainform.lrs}

end.

