unit prop_settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ButtonPanel;

type

  { Tfprop_settings }

  Tfprop_settings = class(TForm)
    ButtonPanel1: TButtonPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    LNumber: TLabel;
    Label3: TLabel;
    LName: TLabel;
    Label5: TLabel;
    EValue: TMemo;
    procedure FormActivate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fprop_settings: Tfprop_settings;

implementation

{ Tfprop_settings }

procedure Tfprop_settings.FormActivate(Sender: TObject);
begin
  ActiveControl:=Evalue;
end;

initialization
  {$I prop_settings.lrs}
end.

