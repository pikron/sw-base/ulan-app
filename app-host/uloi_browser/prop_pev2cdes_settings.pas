unit prop_pev2cdes_settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, ButtonPanel, Grids, LCLType, ComCtrls, Buttons, uloi_pdo_types,
  prop_pev2cdes_flg;

type

  { Tfprop_pev2cdes_settings }

  Tfprop_pev2cdes_settings = class(TForm)
    ButtonPanel1: TButtonPanel;
    GroupBox1: TGroupBox;
    SGProp: TStringGrid;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ToolBar1: TToolBar;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure SGPropCreate(cnt:integer; list:TList);
    procedure PEV2CDesCreate(list:TList; sl:TStringList);
  end;

var
  fprop_pev2cdes_settings: Tfprop_pev2cdes_settings;

implementation

{ Tfprop_pev2cdes_settings }

procedure Tfprop_pev2cdes_settings.SpeedButton1Click(Sender: TObject);
var i:integer;
begin
  for i:=1 to SGProp.RowCount-1 do begin
    SGProp.Cells[0,i]:='0';
    SGProp.Cells[1,i]:='0';
    SGProp.Cells[2,i]:='0';
    SGProp.Cells[3,i]:='0';
  end;
end;

procedure Tfprop_pev2cdes_settings.SpeedButton2Click(Sender: TObject);
var flg:integer;
begin
  if SGProp.Row<1 then
    exit;
  flg:=strtointdef(SGProp.Cells[3,SGProp.Row],0);
  if (flg and $1)<>0 then
    fprop_pev2cdes_flg.Properties.Checked[0]:=true
  else
    fprop_pev2cdes_flg.Properties.Checked[0]:=false;
  if (flg and $2)<>0 then
    fprop_pev2cdes_flg.Properties.Checked[1]:=true
  else
    fprop_pev2cdes_flg.Properties.Checked[1]:=false;
  if (flg and $4)<>0 then
    fprop_pev2cdes_flg.Properties.Checked[2]:=true
  else
    fprop_pev2cdes_flg.Properties.Checked[2]:=false;
  if (flg and $8)<>0 then
    fprop_pev2cdes_flg.Properties.Checked[3]:=true
  else
    fprop_pev2cdes_flg.Properties.Checked[3]:=false;
  if fprop_pev2cdes_flg.ShowModal=mrOK then begin
    flg:=0;
    if fprop_pev2cdes_flg.Properties.Checked[0] then flg:=flg+$1;
    if fprop_pev2cdes_flg.Properties.Checked[1] then flg:=flg+$2;
    if fprop_pev2cdes_flg.Properties.Checked[2] then flg:=flg+$4;
    if fprop_pev2cdes_flg.Properties.Checked[3] then flg:=flg+$8;
    SGProp.Cells[3,SGProp.Row]:=inttostr(flg);
  end;
end;

procedure Tfprop_pev2cdes_settings.SGPropCreate(cnt:integer; list:TList);
var i:integer;
    uoi_pev2cdes:puoi_pev2cdes;
begin
  if list=nil then
    exit;
  SGProp.RowCount:=list.Count+1;
  SGProp.RowCount:=cnt+1;
  for i:=1 to cnt do begin
    if i<=list.Count then begin
      uoi_pev2cdes:=list.Items[i-1];
      SGProp.Cells[0,i]:=inttostr(uoi_pev2cdes^.evnum);
      SGProp.Cells[1,i]:=inttostr(uoi_pev2cdes^.cid);
      SGProp.Cells[2,i]:=inttostr(uoi_pev2cdes^.dadr);
      SGProp.Cells[3,i]:=inttostr(uoi_pev2cdes^.flg);
    end else begin
      SGProp.Cells[0,i]:='0';
      SGProp.Cells[1,i]:='0';
      SGProp.Cells[2,i]:='0';
      SGProp.Cells[3,i]:='0';
    end;
  end;
end;

procedure Tfprop_pev2cdes_settings.PEV2CDesCreate(list:TList; sl:TStringList);
var i:integer;
    uoi_pev2cdes:puoi_pev2cdes;
    evnum,cid,dadr,flg:dword;
begin
  if list=nil then
    exit;
  sl.Clear;
  for i:=1 to SGProp.RowCount-1 do begin
    evnum:=strtointdef(SGProp.Cells[0,i],0);
    cid:=strtointdef(SGProp.Cells[1,i],0);
    dadr:=strtointdef(SGProp.Cells[2,i],0);
    flg:=strtointdef(SGProp.Cells[3,i],0);
    if (evnum<>0) then begin
{      uoi_pev2cdes:=new(puoi_pev2cdes);
      uoi_pev2cdes^.evnum:=evnum;
      uoi_pev2cdes^.cid:=cid;
      uoi_pev2cdes^.dadr:=dadr;
      uoi_pev2cdes^.flg:=flg;
      list.Add(uoi_pev2cdes);}
      sl.Add(inttostr(evnum)+','+inttostr(cid)+','+inttostr(dadr)+','+inttostr(flg));
    end else begin
      break;
    end;
  end;
end;

initialization
  {$I prop_pev2cdes_settings.lrs}

end.

