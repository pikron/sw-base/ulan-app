unit prop_pev2cdes_flg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ButtonPanel, ExtCtrls;

type

  { Tfprop_pev2cdes_flg }

  Tfprop_pev2cdes_flg = class(TForm)
    ButtonPanel1: TButtonPanel;
    Properties: TCheckGroup;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fprop_pev2cdes_flg: Tfprop_pev2cdes_flg;

implementation

initialization
  {$I prop_pev2cdes_flg.lrs}

end.

