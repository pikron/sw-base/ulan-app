unit sd_info_process;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls;

type

  { Tfsd_info_process }

  Tfsd_info_process = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    ProgressBar1: TProgressBar;
  private
    { private declarations }
  public
    { public declarations }
    procedure uShow(min,max:integer);
    procedure uHide;
    procedure uSetPosition(value:integer);
    procedure uSetText(txt:string);
  end; 

var
  fsd_info_process: Tfsd_info_process;

implementation

procedure Tfsd_info_process.uShow(min,max:integer);
begin
  Show;
//  ProgressBar1.Min:=0;ProgressBar1.Max:=0;
  ProgressBar1.Max:=max;ProgressBar1.Min:=min;
  ProgressBar1.Position:=min;
  Application.ProcessMessages;
end;

procedure Tfsd_info_process.uHide;
begin
  Hide;
end;

procedure Tfsd_info_process.uSetPosition(value:integer);
begin
  ProgressBar1.Position:=value;
end;

procedure Tfsd_info_process.uSetText(txt:string);
begin
  Label1.Caption:=txt;
end;

initialization
  {$I sd_info_process.lrs}
end.

