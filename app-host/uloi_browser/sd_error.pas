unit sd_error;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ComCtrls,
  ExtCtrls, StdCtrls, Buttons;

type

  { Tfsd_error }

  Tfsd_error = class(TForm)
    BitBtn1: TBitBtn;
    Image1: TImage;
    Label1: TLabel;
    LText: TLabel;
    Panel1: TPanel;
    Shape1: TShape;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fsd_error: Tfsd_error;

implementation

initialization
  {$I sd_error.lrs}

end.

