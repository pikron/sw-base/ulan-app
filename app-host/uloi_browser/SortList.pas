{*******************************************************************************
* TSortList                                                                    *
* ---------                                                                    *
* Class for operation with the sorted list. Uses the base TList class.         *
* Allows to add items in the sorted out order and to make fast search of items.*
* Are added 1 property and 8 new methods:                                      *
*------------------------------------------------------------------------------*
* Property: Compare - name of the function of comparing of a type              *
* TListSortCompare. (Look the description of a method Sort in TList).          *
* The name of the function should be assigned before execution AddSort and     *
* Search. If it is not made, is generated Error.                               *
*------------------------------------------------------------------------------*
* Methods:                                                                     *
* AddSort -      allows to add elements in List the sorted order.              *
* Search -       carries out fast search of an element in the sorted list.     *
*                Item - the index on a required element. Can contain only the  *
*                "key" values used in function of comparison.                  *
*                Return - number of an element in the list, since 0.           *
*                If the element is not found, negative value (-1) comes back.  *
* SearchFirst -  it is similar Search, but returns the index on the first      *
*                element with set key.                                         *
* GetItem -      returns the index on the found element. If the element is     *
*                absent,comes back nil.                                        *
* GetFirstItem - returns the index on the first element satisfying set keys.   *
* GetItemCount - returns quantity of elements of the list which satisfy to set *
*                key.                                                          *
* GetItemList -  returns the list (TList) the elements found on a key.         *
* ClearAll -     clears memory of indexes and the memory distributed under     *
*                storage of elements Item.                                     *
*                                                                              *
* -----------------------------------------------------------------------------*
* Attention! In order to prevent of violation about sorting, do not use        *
* together with new AddSort by methods Add and Insert, stayed from TList.      *
*************************************** ****************************************
* Can be used without limitations.                                             *
*************************************** ****************************************
* Developer: Ivanov Yuri. E-mail: i7@mail.ru                                   *
* The information on other developments of the author can be looked on page    *
*  http: // i7.da.ru (Russian)                                                 *
*                                                                              *
* December, 2000 - July, 2006                                                  *
********************************************************************************
Example of use:
---------------
interface
...
type Tdat = record
     code: integer;
     txt: string[50];
     num: double;
end;
...
var
    ldat: TSortList;
    dat: ^Tdat;
...
implementation
//*********************************************
function Sort_dat(i1,i2: Pointer): integer;
var
d1,d2: ^Tdat;
begin
    d1:=i1; d2:=i2;
    if d1^.code < d2^.code then Result:=-1
    else
      if d1^.code > d2^.code then Result:=1
      else
        Result:=0;
end;
//*********************************************
procedure ....
var
 d: Tdat;
 pos: integer;
begin
...
   // Addition of a item
        New(dat);
        dat^.code:=codd;
        dat^.txt:=st;
        dat^.num:=dob;
        ldat.AddSort(dat);
    end;
...
  // Search of a item
        d.code:=8613;
        pos:=ldat.Search(@d);
        if pos < 0 then
          ShowMessage('The item '+ IntToStr(d.code) + ' is not found')
        else
        dat:=ldat.Items[pos];
...
  // Clearing of the list and Items memory
        ldat.ClearAll;
...
end;
...
initialization
  ldat:=TSortList.Create;
  ldat.Compare:=Sort_dat;
finalization
  ldat.Free;
end.
*********************************************************************}
unit SortList;

interface
uses Classes, SysUtils;

type
     TSortList = class(TList)
     private
       Ret: integer;
       ERR: byte;
       pcl,pcr: Pointer;
       FCompare: TListSortCompare;
       procedure SetCompare(Value: TListSortCompare);
       function SearchItem(Item: Pointer): integer;
       procedure QuickSearch(Item: Pointer; L,R: integer);
     public
       procedure AddSort(item: Pointer);
       function  Search(Item: Pointer):integer;
       function  SearchFirst(Item: Pointer):integer;
       procedure ClearAll;
       function GetItem(Item: Pointer): Pointer;
       function GetFirstItem(Item: Pointer): Pointer;
       function GetItemCount(Item: Pointer): integer;
       function GetItemList(Item: Pointer) : TList;
       property Compare: TListSortCompare read FCompare write SetCompare;
end;
implementation
//*******************************************
procedure TSortList.ClearAll;
var
i: integer;
Item: Pointer;
begin
  if Count <> 0 then
    for i:=0 to Count-1 do
    begin
      item:=Items[i];
      try
//        if Item <> nil then Dispose(Item);
        item:=nil;
      except end;
    end;
  Clear;
end;
//------------------------------------------------------
procedure TSortList.SetCompare(Value: TListSortCompare);
begin
   FCompare:=Value;
end;
//-----------------------------------------------------------
procedure TSortList.QuickSearch(Item: Pointer; L,R: integer);
var
  K: Integer;
  P: Pointer;
begin
   ERR:=0;
   Ret:=-1;
   pcl:=Items[L];
   if Compare(Item,pcl) < 0 then
   begin
     Ret:=L;
     ERR:=1;
     exit;
   end
   else
   if Compare(Item,pcl) = 0 then
   begin
     Ret:=L;
     exit;
   end;
   pcr:=Items[R];
   if Compare(Item,pcr) > 0 then
   begin
     Ret:=R;
     ERR:=2;
     exit;
   end
   else
   if Compare(Item,pcr) = 0 then
   begin
     Ret:=R;
     exit;
   end;
//-----------------
   if R-L > 1 then
   begin
     K:=(R-L) div 2;
     P:=items[L+K];
     if Compare(Item,P) < 0 then
	QuickSearch(Item,L,L+K)
     else
     begin
      if Compare(Item,P) > 0 then
	QuickSearch(Item,L+K,R)
      else
	if Compare(Item,P) = 0 then
	begin
	   Ret:=L+K;
	   exit;
	end;
     end;
   end
   else
   begin
     ERR:=1;
     ret:=R;
   end;
end;
//----------------------------------------------------
function TSortList.SearchItem(Item: Pointer): integer;
begin
    if Count > 0 then
    begin
      QuickSearch(Item, 0, Count - 1);
      Result:=Ret;
    end
    else
    begin
	Result:=0;
	ERR:=2;
    end;
end;
//------------------------------------------------
function TSortList.Search(Item: Pointer): integer;
begin
    if Addr(Compare)=nil then
    begin
      Error('������� ��������� �� ���������',-1);
      Result:=-1;
      exit;
    end;
    Result:=SearchItem(item);
    if ERR <> 0 then Result:=-1;
end;
//-----------------------------------------
procedure TSortList.AddSort(item: Pointer);
var
i: integer;
begin
    if Addr(Compare)=nil then
    begin
      Error('������� ��������� �� ���������',-1);
      exit;
    end;
    i:=SearchItem(item);
    if (ERR=0) or (ERR=1) then  Insert(i,item)
    else if ERR=2 then Add(item);
end;
//-------------------------------------------------
function TSortList.GetItem(Item: Pointer): Pointer;
var i: integer;
begin
  i:=Search(Item);
  if i = -1 then Result:=nil
  else Result:=Items[i];
end;
//-----------------------------------------------------
function TSortList.SearchFirst(Item: Pointer): integer;
var
i: integer;
begin
  i:=Search(Item);
  while i > 0 do
  begin
    if Compare(Item,Items[i-1])=0 then
    Dec(i)
    else break;
  end;
  Result:=i;
end;
//-----------------------------------------------------
function TSortList.GetFirstItem(Item: Pointer): Pointer;
var i: integer;
begin
  i:=SearchFirst(Item);
  if i >=0 then Result:=Items[i]
  else Result:=nil;
end;
//-----------------------------------------------------
function TSortList.GetItemCount(Item: Pointer): integer;
var
i:integer;
begin
  Result:=0;
  i:=SearchFirst(Item);
  if i >=0 then
  begin
    Result:=1;
    while (i+1 <= Count-1) and (Compare(Items[i],Items[i+1]) = 0) do
    begin
      Inc(Result);
      Inc(i);
    end;
  end;
end;
//-----------------------------------------------------
function TSortList.GetItemList(Item: Pointer): TList;
var
i:integer;
begin
  Result:=nil;
  i:=SearchFirst(Item);
  if i >=0 then
  begin
    Result:=TList.Create;
    Result.Add(Items[i]);
    while (i+1 <= Count-1) and (Compare(Items[i],Items[i+1]) = 0) do
    begin
      Inc(i);
      Result.Add(Items[i]);
    end;
  end;
end;
end.
