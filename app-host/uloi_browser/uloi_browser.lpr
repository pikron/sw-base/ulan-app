program uloi_browser;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms
  { add your units here }, mainform, uLanlaz, sd_info_process, prop_settings,
  sd_error, prop_set_new_address, preferences, fAbout, sd_confirm,
  prop_pev2cdes_settings, uloi_pdo_types, prop_ciddes_settings, prop_ciddes_flg,
  prop_pev2cdes_flg;


{$IFDEF WINDOWS} {$R uloi_browser.rc} {$ENDIF}

begin
  Application.Initialize;
  Application.CreateForm(Tfmainform, fmainform);
  Application.CreateForm(Tfprop_settings, fprop_settings);
  Application.CreateForm(Tfsd_error, fsd_error);
  Application.CreateForm(Tfprop_set_new_address, fprop_set_new_address);
  Application.CreateForm(Tfpreferences, fpreferences);
  Application.CreateForm(Tfsd_confirm, fsd_confirm);
  Application.CreateForm(Tfprop_pev2cdes_settings, fprop_pev2cdes_settings);
  Application.CreateForm(Tfprop_ciddes_settings, fprop_ciddes_settings);
  Application.CreateForm(Tfprop_ciddes_flg, fprop_ciddes_flg);
  Application.CreateForm(Tfprop_pev2cdes_flg, fprop_pev2cdes_flg);
  Application.Run;
end.

