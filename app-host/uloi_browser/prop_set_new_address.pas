unit prop_set_new_address;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons;

type

  { Tfprop_set_new_address }

  Tfprop_set_new_address = class(TForm)
    CBSave: TCheckBox;
    ENAddress: TEdit;
    ESN: TEdit;
    GB: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    LAddress: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fprop_set_new_address: Tfprop_set_new_address;

implementation

{ Tfprop_set_new_address }

procedure Tfprop_set_new_address.SpeedButton1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
  exit;
end;

procedure Tfprop_set_new_address.SpeedButton2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
  exit;
end;

initialization
  {$I prop_set_new_address.lrs}

end.

