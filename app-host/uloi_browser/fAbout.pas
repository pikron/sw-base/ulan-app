unit fAbout;

{$mode objfpc}{$H+}

interface

uses
  LResources,
  Graphics, Forms, Controls,  StdCtrls, ExtCtrls, ActnList,Buttons,
  SysUtils, Classes, lcltype;

type

  { TfrmAbout }

  TfrmAbout = class(TForm)
    LVersion: TLabel;
    lblVersion: TLabel;
    OKButton: TButton;
    Panel1: TPanel;
    imgLogo: TImage;
    memInfo: TMemo;
    lblTitle: TStaticText;
    procedure OKButtonClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure frmAboutShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


procedure ShowAboutBox(v:string);

implementation

const
  cAboutMsg =
    'This program is free software under GNU GPL 2 license'+#13+#10+#13+#10+
    'Authors: '+ #13 + #10 +
    '  Petr Smolik (petr.smolik@wo.cz) - author' + #13 + #10 +
    'Contributors:'+#13+ #10 +
    '  Pavel Pisa (pisa@cmp.felk.cvut.cz)' + #13 + #10 +
    '  Big thanks to Lazarus and FreePascal Team';

procedure ShowAboutBox(v:string);
begin
  with TfrmAbout.Create(Application) do
  try
    Lversion.Caption:=v;
    ShowModal;
  finally
    Free;
  end;
end;

procedure TfrmAbout.OKButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmAbout.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_Escape) then
   Close;
end;

procedure TfrmAbout.frmAboutShow(Sender: TObject);
begin
  memInfo.Lines.Text:=cAboutMsg;
end;

initialization
 {$I fAbout.lrs}
end.

