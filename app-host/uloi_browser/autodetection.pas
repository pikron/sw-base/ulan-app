unit autodetection;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls;

type

  { Tfautodetection }

  Tfautodetection = class(TForm)
    Label1: TLabel;
    ProgressBar1: TProgressBar;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fautodetection: Tfautodetection;

implementation

initialization
  {$I autodetection.lrs}

end.

