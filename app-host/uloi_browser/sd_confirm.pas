unit sd_confirm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ComCtrls,
  ExtCtrls, StdCtrls, Buttons;

type

  { Tfsd_error }

  { Tfsd_confirm }

  Tfsd_confirm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Image1: TImage;
    Label1: TLabel;
    LText: TLabel;
    Panel1: TPanel;
    Shape1: TShape;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fsd_confirm: Tfsd_confirm;

implementation

{ Tfsd_confirm }

initialization
 {$I sd_confirm.lrs}

end.

