unit uloi_pdo_types; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
   ULOI_ARRAY_USE_ITEM_CNT = $8000;
   ULOI_ARRAY_USE_COMMAND  = $4000;

type

  tuoi_ciddes = record
    cid:word;
    flg:word;
    meta:word;
    meta_len:word;
  end;
  puoi_ciddes = ^tuoi_ciddes;

  tuoi_pev2cdes = record
    evnum:word;
    cid:word;
    dadr:word;
    flg:word;
  end;
  puoi_pev2cdes = ^tuoi_pev2cdes;

implementation

end.

