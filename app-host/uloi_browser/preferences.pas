unit preferences;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ExtCtrls, ButtonPanel;

type

  { Tfpreferences }

  Tfpreferences = class(TForm)
    ButtonPanel1: TButtonPanel;
    CBAutodetection: TCheckBox;
    CBOIDictionary: TCheckBox;
    EOSDriverName: TEdit;
    EFirstUnit: TEdit;
    ELastUnit: TEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Shape1: TShape;
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  fpreferences: Tfpreferences;

implementation

{ Tfpreferences }


initialization
  {$I preferences.lrs}

end.

