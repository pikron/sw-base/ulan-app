#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/types.h>
#ifdef _WIN32
 #include <winsock.h>
 #include <fcntl.h>
 typedef int socklen_t;
#else
 #include <sys/socket.h>
 #include <netinet/in.h>
 #include <arpa/inet.h>
 #define closesocket(s)  close(s)
#endif /* _WIN32 */
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <ctype.h>

#include <ul_log.h>
#include <ul_logreg.h>
#include <ul_l_drv_eth.h>
#include "ul_ethd.h"
#include "log_domains.inc"

#define DEBUG_LEVEL "all.1"

UL_LOG_CUST(ulogd_ethd)

// tree of clients connected to the server
client_root_t client_root;
pthread_mutex_t client_mutex;
//global variables
int server_socket;
int server_terminated;
pthread_t server_threadid;

/*******************************************************************/
void client_process_request(client_context_t *client)
{
  char *command=client->msg_cmd;

  if (strcmp(command,"open")==0) {
    ul_eth_server_command_open(client);
    return;
  } 
  else if (strcmp(command,"close")==0)
  {
    ul_eth_server_command_close(client);
    return;
  }
  else if (strcmp(command,"drv_version")==0)
  {
    ul_eth_server_command_drv_version(client);
    return;
  }
  else if (strcmp(command,"fd_wait")==0)
  {
    ul_eth_server_command_fd_wait(client);
    return;
  }
  else if (strcmp(command,"fd_wait_recv")==0)
  {
    ul_eth_server_command_fd_wait_recv(client);
    return;
  }
  else if (strcmp(command,"addfilt")==0)
  {
    ul_eth_server_command_addfilt(client);
    return;
  }
  else if (strcmp(command,"send")==0)
  {
    ul_eth_server_command_send(client);
    return;
  }
  else if (strcmp(command,"recv")==0)
  {
    ul_eth_server_command_recv(client,"recv");
    return;
  }
}

/*******************************************************************/
void* client_run(void *vcontext)
{
  client_context_t *client=(client_context_t*)vcontext;
  ul_dbuff_t rcvbuff;
  int fd = client->sock,r;

  ul_dbuff_init(&rcvbuff, 0);
  ul_logdeb("Thread for client fd:%d is running\n",fd);

  while (1) {

    if (client->terminated) {
      r=0;
      break;
    }
        
    r=ul_eth_recv_msg(fd,&rcvbuff,0,&client->terminated);

    if(r<0) 
      break;

    ul_logdeb("client on file %p says '%s'\n", client->sock_wfile,(char*)rcvbuff.data);

    client->msg_header = strtok ((char*)rcvbuff.data," ");

    if (client->msg_header!=NULL) {
      client->msg_sn = strtok (NULL," ");
      if (client->msg_sn!=NULL) {
        client->msg_cmd = strtok (NULL," ");
        if (client->msg_cmd!=NULL) {
          client->msg_data = strtok (NULL," ");
          client->msg_data_len=0;
          if (client->msg_data!=NULL) 
            client->msg_data_len=rcvbuff.len-(client->msg_data-(char*)rcvbuff.data)-1;

          client_process_request(client);
        }
      }    
      if (strcmp(client->msg_header,"help")==0) {
        ul_eth_server_command_help(client);
      }
      if (strcmp(client->msg_header,"quit")==0) {
        r=-1; 
        break;
      }
    }
  }
  ul_logdeb("Thread for client fd:%d terminated by rcode:%d\n",fd,r);

  ul_dbuff_destroy(&rcvbuff);

  fflush(client->sock_wfile);
  fclose(client->sock_wfile);

  if (client->ul_fd!=UL_FD_INVALID)
    ul_close(client->ul_fd);
 
  if (r==-1) {
    ul_logdeb("Closing socket in thread\n");
    closesocket(client->sock);
    /* terminated by client side request */
    pthread_mutex_lock (&client_mutex);
    client_delete(&client_root,client);  
    pthread_mutex_unlock (&client_mutex);

    free(client);
  }

  ul_logdeb("Thread for client fd:%d was terminated\n",fd);
  pthread_exit(NULL);
  return NULL;
}


void destroy_all_clients() 
{
  client_context_t *client;

  pthread_mutex_lock (&client_mutex);
  /* terminate all clients */
  ul_list_for_each(client,&client_root,client) {
    client->terminated=1;
    closesocket(client->sock);
  }
  /* wait for terminated */
  ul_list_for_each_cut(client,&client_root,client) {
    pthread_t threadid=client->threadid;
    pthread_join(threadid,0);
    free(client);
  }
  pthread_mutex_unlock (&client_mutex);
}

char *ul_dev_name = UL_DEV_NAME;
int server_port=15000;
int server_running_as_thread;

int server_init(void)
{
  struct sockaddr_in address;
  int yes=1;
 #ifdef _WIN32
  WORD wVersionRequested;
  WSADATA wsaData;
  int sockopt = SO_SYNCHRONOUS_NONALERT;
 #endif

  ul_logdeb("Server init\n");

  client_init_head(&client_root);
  pthread_mutex_init(&client_mutex, NULL);
  server_running_as_thread=0;
  server_terminated=0;

 #ifdef _WIN32
  wVersionRequested = MAKEWORD(2, 0);
  WSAStartup(wVersionRequested, &wsaData);
  setsockopt(INVALID_SOCKET, SOL_SOCKET, SO_OPENTYPE, (char *)&sockopt, sizeof(sockopt));   
 #endif

  if ((server_socket = socket(AF_INET,SOCK_STREAM,0)) < 0) {
    ul_loginf("The socket created error\n");
    exit(1);
  }
  ul_loginf("The Socket was created\n");

  /* Make listening socket's port reusable - must appear before bind */
  if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes,sizeof(yes)) < 0) {
    ul_logfatal("setsockopt failure\n");
    exit(1);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(server_port);
  if (bind(server_socket,(struct sockaddr *)&address,sizeof(address)) < 0) {
    ul_loginf("Bind error\n");
    exit(1);    
  }
  ul_loginf("Binded on socket\n");

  /*  Create a connection queue and initialize readfds to handle input from server_socket. */
  if(listen(server_socket, 5) < 0) {
    perror("listen");
    exit(1);
  }

  return 0;
}

void* server_run(void *c)
{
  int csock;
  fd_set readfds, testfds;
  int result,f;
  socklen_t addrlen;
  struct sockaddr_in address;

  ul_logdeb("Server run\n");

  FD_ZERO(&readfds);
  FD_SET(server_socket, &readfds);

  while(!server_terminated) {
    struct timeval timeout = {.tv_sec = 1, .tv_usec = 0};

    testfds=readfds;
    result = select(FD_SETSIZE, &testfds, (fd_set *)0, (fd_set *)0, &timeout);
    
    if(result < 0) 
      break;
   
    if (result == 0)
      continue;

    if(FD_ISSET(server_socket, &testfds)) { /*  request for a new connection */
      addrlen = sizeof(struct sockaddr_in);
      csock = accept(server_socket,(struct sockaddr *)&address,&addrlen);
      if (csock > 0){
        client_context_t *client;
        ul_logdeb("The Client %s is connected...\n",inet_ntoa(address.sin_addr));

        client=(client_context_t *)malloc(sizeof(client_context_t));
        if (client==NULL) {
  	  ul_logfatal("Could not get memory\n");
          exit(1);
        }
        memset(client, 0, sizeof(client_context_t));
        client->sock=csock;
        client->ul_fd=UL_FD_INVALID;
        client->terminated=0;
        ul_msg_buf_init(&client->msg_buf);

       #ifndef _WIN32
        f=client->sock;
       #else
        f=_open_osfhandle(client->sock,_O_WRONLY);
        if (f<0) {
  	  ul_logfatal("Error _open_osfhandle\n");
          exit(1);
        }
       #endif /* _WIN32 */ 

        client->sock_wfile=fdopen(f,"w");
        if (!client->sock_wfile) {
  	  ul_logfatal("Error fdopen\n");
          exit(1);
        }
                
        pthread_mutex_lock (&client_mutex);
        client_insert(&client_root,client);
        pthread_mutex_unlock (&client_mutex);

        if( pthread_create(&client->threadid, NULL, client_run, (void*)client)!= 0){
  	  ul_logfatal("Could not create client thread\n");
          exit(1);
        }
      }      
    } else {
      // this should never happen if the program work well
      ul_logfatal("ERROR: read from unregistered fd\n");
      exit(1);
    }    
  }
  if (server_running_as_thread)
    pthread_exit(NULL);
  return NULL;
}

int server_start(void)
{
  ul_logdeb("Server start\n");
  server_terminated=0;
  server_running_as_thread=1;
  if (pthread_create(&server_threadid, NULL, server_run, NULL)!= 0){
    ul_logfatal("Could not create server thread\n");
    exit(1);
  }      
  return 0;
}

int server_stop(void)
{
  ul_logdeb("Server stop\n");
  server_terminated=1;
  closesocket(server_socket);  
  if (server_running_as_thread)
    pthread_join(server_threadid,0);
  destroy_all_clients();
  ul_logdeb("Server terminated\n");
  server_running_as_thread=0;
  return 0;
}

void
sigexit(int sig)
{
  server_stop();
  exit(0);
}

#ifndef _WIN32
/*******************************************************************/

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

static void daemonize(void)
{
  pid_t pid, sid;

  /* already a daemon */
  if ( getppid() == 1 ) return;

  /* Fork off the parent process */
  pid = fork();
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }
  /* If we got a good PID, then we can exit the parent process. */
  if (pid > 0) {
   exit(EXIT_SUCCESS);
  }

  /* At this point we are executing as the child process */

  /* Change the file mode mask */
  umask(0);

  /* Create a new SID for the child process */
  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Change the current working directory.  This prevents the current
     directory from being locked; hence not being able to remove it. */
  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  /* Redirect standard files to /dev/null */
  freopen( "/dev/null", "r", stdin);
  freopen( "/dev/null", "w", stdout);
  freopen( "/dev/null", "w", stderr);
}
#endif /* _WIN32 */

#ifdef _WIN32
//Windows service support
void serviceDispatchTable(void);  //forward declaration
void removeService(void);         //forward declaration
void installService(void);        //forward declaration
#endif

int server_daemonize=0;

static void
usage(void)
{
  printf("Usage: ul_ethd <parameters>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -p, --port <value>       server port [%d]\n",server_port);
  printf("  -D, --daemonize          run ul_ethd like daemon\n");
  printf("  -v, --verbosity <sec.lev:...> set verbosity for SECTION. LEVEL:..., [%s]\n",DEBUG_LEVEL);
#ifdef _WIN32
  printf("  -i, --install_service         install service into service manager on Windows\n");
  printf("  -r, --remove_service          remove service from service manager on Windows\n");
#endif
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "port", 1, 0, 'p' },
    { "daemonize", 0, 0, 'D' },
    { "verbosity", 1, 0, 'v' },
#ifdef _WIN32
    { "install_service",0,0, 'i' },
    { "remove_service",0,0, 'r' },
#endif
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int opt;

 ul_logreg_domain(&ulogd_ethd);
 ul_logreg_domain(&ulogd_commands);
 ul_logreg_domain(&ulogd_drv_eth);

 ul_log_domain_arg2levels(DEBUG_LEVEL);

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:p:v:DsirVh")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:p:v:DsirVh",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'D':
      server_daemonize=1;
      break;
    case 'p':
      server_port = strtol(optarg,NULL,0);
      break;
    case 'v':
      if (ul_log_domain_arg2levels(optarg)<0) {
        exit(1);
      }
      break;
   #ifdef _WIN32
    case 's':
      serviceDispatchTable();
      exit(0);
      break;
    case 'i':
      installService();
      exit(0);
    case 'r':
      removeService();
      exit(0);
      break;
   #endif
    case 'V':
      fputs("ul_ethd v.:0.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

 #ifndef _WIN32
  if (server_daemonize)
    daemonize();
 #endif

  server_init();
  signal(SIGINT,  sigexit);
  signal(SIGTERM,  sigexit);
  server_run(NULL);
  return 0;
}
