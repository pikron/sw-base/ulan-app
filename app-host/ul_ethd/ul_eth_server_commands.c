#include <stdio.h>
#ifdef _WIN32
 #include <winsock.h>
#else
 #include <sys/socket.h>
#endif /* _WIN32 */
#include <string.h>
#include <stdlib.h>

#include <ul_log.h>
#include <ul_l_drv_eth.h>
#include "ul_ethd.h"

UL_ATTR_WEAK UL_LOG_CUST(ulogd_commands)

void 
ul_eth_server_command_open(client_context_t *client)
{
  char *ul_dev_name = UL_DEV_NAME;
  
  if (client->msg_data!=NULL)
    ul_dev_name=client->msg_data;

  ul_loginf("ul_open dev_name='%s'\n", ul_dev_name);
  client->ul_fd=ul_open(ul_dev_name, NULL);
  fprintf(client->sock_wfile,"usr %s open %d\n",client->msg_sn,client->ul_fd!=UL_FD_INVALID?1:0);
  fflush(client->sock_wfile);
  ul_logdeb("server on file %p says 'usr %s open %d'\n", client->sock_wfile,client->msg_sn,client->ul_fd!=UL_FD_INVALID?1:0);
}

void 
ul_eth_server_command_close(client_context_t *client)
{
  int r;
  ul_loginf("ul_close\n");
  r=ul_close(client->ul_fd);
  ul_loginf("ul_close ret=0x%x\n",r);
  fprintf(client->sock_wfile,"usr %s close %d\n",client->msg_sn,r);
  fflush(client->sock_wfile);
  ul_logdeb("server on file %p says 'usr %s close %d'\n", client->sock_wfile,client->msg_sn,r);
  client->ul_fd=UL_FD_INVALID;
}

void 
ul_eth_server_command_drv_version(client_context_t *client)
{
  int r;
  ul_loginf("ul_drv_version\n");
  r=ul_drv_version(client->ul_fd);
  ul_loginf("ul_drv_version ret=0x%x\n",r);
  fprintf(client->sock_wfile,"usr %s drv_version %d\n",client->msg_sn,r);
  fflush(client->sock_wfile);
  ul_logdeb("server on file %p says 'usr %s drv_version %d'\n", client->sock_wfile,client->msg_sn,r);
}

void 
ul_eth_server_command_fd_wait(client_context_t *client)
{
  int r,s=1;

  if (client->msg_data!=NULL)
    s=strtol(client->msg_data,NULL,0);

  ul_loginf("ul_fd_wait %d\n",s);
  r=ul_fd_wait(client->ul_fd,s);
  ul_loginf("ul_fd_wait ret=0x%x\n",r);
  fprintf(client->sock_wfile,"usr %s fd_wait %d\n",client->msg_sn,r);
  fflush(client->sock_wfile);
  ul_logdeb("server on file %p says 'usr %s fd_wait %d'\n", client->sock_wfile,client->msg_sn,r);
}

void 
ul_eth_server_command_fd_wait_recv(client_context_t *client)
{
  int r,s=1;

  if (client->msg_data!=NULL)
    s=strtol(client->msg_data,NULL,0);

  ul_loginf("ul_fd_wait_recv %d\n",s);
  r=ul_fd_wait(client->ul_fd,s);
  ul_loginf("ul_fd_wait_recv ret=0x%x\n",r);
  if (r<0) {
    fprintf(client->sock_wfile,"usr %s fd_wait_recv %d\n",client->msg_sn,r);
    fflush(client->sock_wfile);
    ul_logdeb("server on file %p says 'usr %s fd_wait_recv %d'\n", client->sock_wfile,client->msg_sn,r);
  } else {
    ul_eth_server_command_recv(client,"fd_wait_recv");
  }
}

void 
ul_eth_server_command_addfilt(client_context_t *client)
{
  int r;
  ul_msginfo msginfo;

  r=sscanf(client->msg_data,"%02x%02x%02x",&msginfo.sadr,&msginfo.dadr,&msginfo.cmd);
  if (r!=3) {
    fprintf(client->sock_wfile,"usr %s addfilt -1\n",client->msg_sn);
    fflush(client->sock_wfile);
    return;
  }

  ul_loginf("ul_addfilt sadr:%d dadr:%d cmd:%d\n",msginfo.sadr,msginfo.dadr,msginfo.cmd);
  r=ul_addfilt(client->ul_fd,&msginfo);
  ul_loginf("ul_addfilt ret=0x%x\n",r);
  fprintf(client->sock_wfile,"usr %s addfilt %d\n",client->msg_sn,r);
  fflush(client->sock_wfile);
  ul_logdeb("server on file %p says 'usr %s addfilt %d'\n", client->sock_wfile,client->msg_sn,r);
}

void 
ul_eth_server_command_send(client_context_t *client)
{
  int r;

  ul_loginf("ul_send\n");
  if (ul_eth_demarshal_stream(&client->msg_buf,client->msg_data,client->msg_data_len)<0) {
    fprintf(client->sock_wfile,"usr %s send -1\n",client->msg_sn);
    fflush(client->sock_wfile);
    return;
  }
  r=ul_msg_buf_wr(&client->msg_buf,client->ul_fd);
  ul_loginf("ul_send stamp:%d\n",r);
  fprintf(client->sock_wfile,"usr %s send %d\n",client->msg_sn,r);
  fflush(client->sock_wfile);
  ul_logdeb("server on file %p says 'usr %s send %d'\n", client->sock_wfile,client->msg_sn,r);
}

void
ul_eth_server_command_recv(client_context_t *client,const char *service)
{
  ul_loginf("ul_%s\n",service);
  ul_msg_buf_destroy(&client->msg_buf);
  if( (ul_acceptmsg(client->ul_fd,&client->msg_buf.msginfo)<0) ||
      (ul_msg_buf_rd_rest(&client->msg_buf, client->ul_fd)<0) ) {
    fprintf(client->sock_wfile,"usr %s %s -1\n",client->msg_sn,service);
    fflush(client->sock_wfile);
    return;
  }
  fprintf(client->sock_wfile,"usr %s %s ",client->msg_sn,service);
  ul_logdeb("server on file %p says 'usr %s %s ", client->sock_wfile,client->msg_sn,service);
  ul_eth_send_msg(client->sock_wfile,&client->msg_buf);
  fprintf(client->sock_wfile,"\n");
  ul_log(&ulogd_commands,UL_LOGL_DEB|UL_LOGL_CONT,"'\n");
  fflush(client->sock_wfile);
}

void 
ul_eth_server_command_help(client_context_t *client)
{
  static char help_msg[] =
  "ul_ethd - server\n\n"
  "commnads\n"
  "  usc [SN] open [device_name] - open device\n"
  "  usc [SN] close - close device\n"
  "  usc [SN] drv_version - driver version\n"
  "  usc [SN] send [SSDDCCFFFFTTTTTTTTLLLL[DD..]..] - send data defined by HEX\n"
  "               SS   - source address\n"
  "               DD   - destination address\n"
  "               CC   - destination address\n"
  "               FFFF - flags\n"
  "               TTTTTTTT - stamp\n"
  "               LLLL - length\n"
  "               DD   - data\n"
  "            Next tail continue after previous tail or first message\n"
  "  usc [SN] recv - request for receive data\n"
  "  usc [SN] fd_wait [sec] - wait for fd makes active\n"
  "  usc [SN] fd_wait_recv [sec] - wait for fd makes active and receive data\n"
  "  usc [SN] addfilt [SSDDCC] - add message filter defined by SSDDCC\n"
  "responses\n"
  "  usr [SN] [command] [value] - returning value from the synchronic [command]\n"
  "  usr [SN] recv [SSDDCCFFFFTTTTTTTTLLLL[DD..]..] - send received data defined by HEX\n";

  ul_loginf("ul_help\n");
  fprintf(client->sock_wfile,help_msg);
  fflush(client->sock_wfile);
}

