#ifndef UL_ETHD
#define UL_ETHD

#include <pthread.h>
#include <ul_list.h>
#include <ul_netbase.h>
#include <ul_lib/ulan.h>

typedef struct client_context_t {
  ul_list_node_t         node;
  int 			 sock;
  FILE 			 *sock_wfile;
  pthread_t 		 threadid;
  int                    terminated;
  char 			 *msg_header;
  char 			 *msg_sn;
  char 			 *msg_cmd;
  char 			 *msg_data;
  int			 msg_data_len;
  ul_msg_buf_t		 msg_buf;
  ul_fd_t 		 ul_fd;
} client_context_t;

typedef struct client_root_t {
  ul_list_head_t         root;
} client_root_t; 


UL_LIST_CUST_DEC(client,
                 client_root_t,client_context_t,
                 root,node);

/* forward declarations */
void ul_eth_server_command_open(client_context_t *client);
void ul_eth_server_command_close(client_context_t *client);
void ul_eth_server_command_drv_version(client_context_t *client);
void ul_eth_server_command_fd_wait(client_context_t *client);
void ul_eth_server_command_fd_wait_recv(client_context_t *client);
void ul_eth_server_command_addfilt(client_context_t *client);
void ul_eth_server_command_send(client_context_t *client);
void ul_eth_server_command_recv(client_context_t *client,const char *service);
void ul_eth_server_command_help(client_context_t *client);


#endif /* UL_ETHD */

