/*
 * This is generated file, do not edit it directly.
 * Take it from standard output of "ul_log_domains"
 * script called in the top level project directory
 */

#define UL_LOGL_DEF 1

ul_log_domain_t ulogd_ethd	= {UL_LOGL_DEF, "ethd"};
ul_log_domain_t ulogd_commands  = {UL_LOGL_DEF, "commands"};
extern ul_log_domain_t ulogd_drv_eth; 

ul_log_domain_t *ul_log_domains_array[] = {
   &ulogd_ethd,
   &ulogd_commands,
   &ulogd_drv_eth,
};
