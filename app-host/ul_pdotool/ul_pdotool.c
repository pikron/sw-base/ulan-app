/*******************************************************************
  uLan Object Communication

  ul_pdotool.c	- process data connection management tool

  (C) Copyright 1996-2009 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2009 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2009 Petr Smolik
  (C) 2009 DCE FEE CTU Prague
        http://dce.fel.cvut.cz

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <string.h>
#include <stdlib.h>
#include <libxml/parser.h>
#include <uloi_pdoproc.h>
#include <uloi_pdoev.h>
#include <ul_gsa.h>
#include <ul_gsacust.h>
#include <getopt.h>
#include <uloi_array.h>

typedef struct pdotool_ciddes_array_t {
  gsa_array_field_t sorted;
} pdotool_ciddes_array_t;

typedef struct pdotool_pev2cdes_array_t {
  gsa_array_field_t sorted;
} pdotool_pev2cdes_array_t;

typedef struct ul_pdomap_t {
  pdotool_ciddes_array_t pico;
  pdotool_ciddes_array_t poco;
  void *meta_base;
  unsigned meta_len;
  pdotool_pev2cdes_array_t pev2c;
  uloi_gmask_t *pico_gmask;
  unsigned pico_gmask_len;
  uloi_gmask_t *pev2c_gmask;
  unsigned pev2c_gmask_len;
} ul_pdomap_t;

GSA_CUST_DEC(pdotool_ciddes_array, pdotool_ciddes_array_t, uloi_ciddes_t, uloi_cid_t,
        sorted, cid, uloi_cid_cmp_fnc)

GSA_CUST_DEC(pdotool_pev2cdes_array, pdotool_pev2cdes_array_t, uloi_pev2cdes_t, uloi_evnum_t,
        sorted, evnum, uloi_evnum_cmp_fnc)

static inline int
uloi_cid_cmp_fnc(const uloi_cid_t *a, const uloi_cid_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

static inline int
uloi_evnum_cmp_fnc(const uloi_evnum_t *a, const uloi_evnum_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GSA_CUST_IMP(pdotool_ciddes_array, pdotool_ciddes_array_t, uloi_ciddes_t, uloi_cid_t,
        sorted, cid, uloi_cid_cmp_fnc, GSA_FAFTER)

GSA_CUST_IMP(pdotool_pev2cdes_array, pdotool_pev2cdes_array_t, uloi_pev2cdes_t, uloi_evnum_t,
        sorted, evnum, uloi_evnum_cmp_fnc, GSA_FAFTER)


/*******************************************************************/
int pdomap_init_items(ul_pdomap_t *pdomap)
{
  pdotool_ciddes_array_init_array_field(&pdomap->pico);
  pdotool_ciddes_array_init_array_field(&pdomap->poco);
  pdomap->meta_len=0;
  pdotool_pev2cdes_array_init_array_field(&pdomap->pev2c);
  pdomap->pico_gmask=NULL;
  pdomap->pico_gmask_len=0;
  pdomap->pev2c_gmask=NULL;
  pdomap->pev2c_gmask_len=0;
  return 0;
}


/*******************************************************************/
int pdomap_free_items(ul_pdomap_t *pdomap)
{
  uloi_ciddes_t *ciddes;
  uloi_pev2cdes_t *pev2cdes;
  /* pico */
  gsa_cust_for_each_cut(pdotool_ciddes_array,&pdomap->pico,ciddes)
    free(ciddes);
  /* poco */
  gsa_cust_for_each_cut(pdotool_ciddes_array,&pdomap->poco,ciddes)
    free(ciddes);
  /* piom */
  if (pdomap->meta_base)
    free(pdomap->meta_base);
  pdomap->meta_len=0;
  /* pev2c */
  gsa_cust_for_each_cut(pdotool_pev2cdes_array,&pdomap->pev2c,pev2cdes)
    free(pev2cdes);
  /* pico gmask */
  free(pdomap->pico_gmask);
  pdomap->pico_gmask=NULL;
  pdomap->pico_gmask_len=0;
  /* pev2c gmask */
  free(pdomap->pev2c_gmask);
  pdomap->pev2c_gmask=NULL;
  pdomap->pev2c_gmask_len=0;
  return 0;
}

/*******************************************************************/

uloi_ciddes_t *
pdomap_load_parse_ciddes_item(xmlNodePtr ciddes_item)
{
  xmlChar *cid,*oid,*meta,*metadata,*flg,*len;
  char *p, *r;
  uloi_ciddes_t *ciddes;
  int ciddes_ok= 0;
  cid= xmlGetProp(ciddes_item, (const xmlChar *)"cid");
  oid= xmlGetProp(ciddes_item, (const xmlChar *)"oid");
  meta= xmlGetProp(ciddes_item, (const xmlChar *)"meta");
  metadata= xmlGetProp(ciddes_item, (const xmlChar *)"metadata");
  flg= xmlGetProp(ciddes_item, (const xmlChar *)"flg");
  len= xmlGetProp(ciddes_item, (const xmlChar *)"len");

  ciddes=malloc(sizeof(uloi_ciddes_t));
  do {
    if (!ciddes) {
      printf("malloc failed for item descriptor\n");
      break;
    }
    memset(ciddes, 0, sizeof(uloi_ciddes_t));

    if (!cid) {
      printf("cid not specified for item descriptor\n");
      break;
    }

    /*printf("PxCO item: cid:%s, oid: %s,flg: %s, oid:%s, len:%s\n",cid,oid,flg,oid,len);*/
    ciddes->cid=strtol((char*)cid,NULL,0);
    if (flg)
      ciddes->flg=strtol((char*)flg,NULL,0);
    if (oid) {
      if (meta || metadata) {
        printf("superfluous meta or metadata in oid based item descriptor (cid:%s)\n",cid);
      }
      ciddes->meta.oid=strtol((char*)oid,NULL,0);
      if (len)
        ciddes->meta_len=strtol((char*)len,NULL,0);
      else
        ciddes->meta_len=2;
    } else if (meta) {
      if (metadata) {
        printf("superfluous metadata in meta based item descriptor (cid:%s)\n",cid);
      }
      ciddes->meta.offs=strtol((char*)meta,NULL,0);
      if (len)
        ciddes->meta_len=strtol((char*)len,NULL,0);
      else
        printf("len specification missing for item descriptor (cid:%s)\n",cid);
      if (!(ciddes->flg & ULOI_CIDFLG_META_LOC))
        ciddes->flg|= ULOI_CIDFLG_META_OFFS;
    } else if (metadata) {
      p= (char*)metadata;
      int i = 0;
      int meta_len = 4;
      #define PDOMAP_METADATA_MAX 2
      unsigned int u[PDOMAP_METADATA_MAX];
      memset(u, 0, sizeof(*u) * PDOMAP_METADATA_MAX);
      if (len)
        meta_len=strtol((char*)len,NULL,0);
      do {
        u[i]=strtol(p,&r,0);
        if(p==r)
          break;
        i++;
        p=r;
        while((*p > 0) && (*p <= ' '))
          r++;
        if(*p != ',')
          break;
        p++;
      } while(i<PDOMAP_METADATA_MAX);
      ciddes->meta.oid=u[0];
      ciddes->meta_len=u[1];
      if (!(ciddes->flg & ULOI_CIDFLG_META_LOC)) {
        if (meta_len==3)
          ciddes->flg|= ULOI_CIDFLG_META_EM3B;
        if (meta_len==4)
          ciddes->flg|= ULOI_CIDFLG_META_EM4B;
      }
    } else {
      printf("no oid, meta or metadata informations in item descriptor (cid:%s)\n",cid);
      break;
    }
    ciddes_ok=1;
  } while(0);

  free(len);
  free(flg);
  free(meta);
  free(metadata);
  free(oid);
  free(cid);

  if (ciddes_ok)
    return ciddes;

  free(ciddes);
  return NULL;
}

int 
pdomap_load(char *filename, ul_pdomap_t *pdomap) 
{
  xmlDocPtr doc;
  xmlNodePtr cur,cur1;

  if (!filename) 
    filename="ul_pdomap.xml";
  xmlKeepBlanksDefault(1);
  doc = xmlParseFile(filename);
  if (doc == NULL) return(-1);
  cur = xmlDocGetRootElement(doc);
  if (cur == NULL) {
      fprintf(stderr,"empty document\n");
      xmlFreeDoc(doc);
      return(-1);
  }
  cur = cur->children;
  while(cur){
    if (!xmlStrcmp(cur->name, (const xmlChar *)"body")) break;
    cur = cur->next;
  }
  if (!cur) {
      fprintf(stderr,"no body section!\n");
      xmlFreeDoc(doc);
      return(-1);
  }
  cur = cur->children;
  while(cur){
    if (!xmlStrcmp(cur->name, (const xmlChar *)"pdomap")) break;
    cur = cur->next;
  }
  if (!cur) {
      fprintf(stderr,"no pdomap section!\n");
      xmlFreeDoc(doc);
      return(-1);
  }
  cur = cur->children;
  while (cur) {
    //pico configuration
    if (!xmlStrcmp(cur->name,(const xmlChar *)"pico")) {
      cur1 = cur->children;
      while(cur1) {
        if (!xmlStrcmp(cur1->name,(const xmlChar *)"item")) {
          uloi_ciddes_t *ciddes;
          ciddes=pdomap_load_parse_ciddes_item(cur1);
          if (ciddes)
            pdotool_ciddes_array_insert(&pdomap->pico,ciddes);
        }
        cur1 = cur1->next;
      }
    }
    //poco configuration
    if (!xmlStrcmp(cur->name,(const xmlChar *)"poco")) {
      cur1 = cur->children;
      while(cur1) {
        if (!xmlStrcmp(cur1->name,(const xmlChar *)"item")) {
          uloi_ciddes_t *ciddes;
          ciddes=pdomap_load_parse_ciddes_item(cur1);
          if (ciddes)
            pdotool_ciddes_array_insert(&pdomap->poco,ciddes);
        }
        cur1 = cur1->next;
      }
    }
    //piom
    if (!xmlStrcmp(cur->name,(const xmlChar *)"piom")) {
      cur1 = cur->children;
      while(cur1) {
        if (!xmlStrcmp(cur1->name,(const xmlChar *)"data")) {
          xmlChar *value;
          value=xmlGetProp(cur1, (const xmlChar *)"value");
          if (value) {
            xmlChar *pbeg,*pit;
            int cnt,v;
            pbeg=value;pit=value;
            cnt=0;
            while(1) {
              if (*pit==0) {
                cnt++;
                break;
              }
              if (*pit==',') {
                cnt++;
                pit++;
                pbeg=pit;
              } else 
                pit++;
            }
            pdomap->meta_base=malloc(cnt);
            cnt=0;
            if (pdomap->meta_base) {
              pbeg=value;pit=value;
              while(1) {
                if (*pit==0) {
                  v=strtol((char*)pbeg,NULL,0);
                  ((uchar*)pdomap->meta_base)[cnt]=v;
                  cnt++;
                  break;
                }
                if (*pit==',') {
                  *pit=0;
                  v=strtol((char*)pbeg,NULL,0);
                  ((uchar*)pdomap->meta_base)[cnt]=v;
                  cnt++;
                  pit++;
                  pbeg=pit;
                } else {
                  pit++;
                }
              }
            }
            pdomap->meta_len=cnt;
            free(value);
          }
        }
        cur1 = cur1->next;
      }
    }
    //pev2c configuration
    if (!xmlStrcmp(cur->name,(const xmlChar *)"pev2c")) {
      cur1 = cur->children;
      while(cur1) {
        if (!xmlStrcmp(cur1->name,(const xmlChar *)"item")) {
          xmlChar *evnum,*cid,*dadr,*flg;
          uloi_pev2cdes_t *pev2cdes;
          evnum= xmlGetProp(cur1, (const xmlChar *)"evnum");
          cid= xmlGetProp(cur1, (const xmlChar *)"cid");
          dadr= xmlGetProp(cur1, (const xmlChar *)"dadr");
          flg= xmlGetProp(cur1, (const xmlChar *)"flg");
          pev2cdes=malloc(sizeof(uloi_pev2cdes_t));
          if (pev2cdes) {
            /*printf("PEV2C item: evnum:%s, cid: %s, dadr: %s, flg:%s\n",evnum,cid,dadr,flg);*/
            pev2cdes->evnum=strtol((char*)evnum,NULL,0);
            pev2cdes->cid=strtol((char*)cid,NULL,0);
            pev2cdes->dadr=strtol((char*)dadr,NULL,0);
            pev2cdes->flg=strtol((char*)flg,NULL,0);
            pdotool_pev2cdes_array_insert(&pdomap->pev2c,pev2cdes);
            pev2cdes=NULL;
          }
          if (pev2cdes)
            free(pev2cdes);
          free(evnum);
          free(cid);
          free(dadr);
          free(flg);
        }
        cur1 = cur1->next;
      }
    }
    //pico gmask
    if (!xmlStrcmp(cur->name,(const xmlChar *)"pico_gmask")) {
      cur1 = cur->children;
      while(cur1) {
        if (!xmlStrcmp(cur1->name,(const xmlChar *)"data")) {
          xmlChar *value;
          value=xmlGetProp(cur1, (const xmlChar *)"value");
          if (value) {
            xmlChar *pbeg,*pit;
            int cnt,v;
            pbeg=value;pit=value;
            cnt=0;
            while(1) {
              if (*pit==0) {
                cnt++;
                break;
              }
              if (*pit==',') {
                cnt++;
                pit++;
                pbeg=pit;
              } else 
                pit++;
            }
            pdomap->pico_gmask=malloc(cnt*sizeof(uloi_gmask_t));
            cnt=0;
            if (pdomap->pico_gmask) {
              pbeg=value;pit=value;
              while(1) {
                if (*pit==0) {
                  v=strtol((char*)pbeg,NULL,0);
                  ((uloi_gmask_t*)pdomap->pico_gmask)[cnt]=v;
                  cnt++;
                  break;
                }
                if (*pit==',') {
                  *pit=0;
                  v=strtol((char*)pbeg,NULL,0);
                  ((uloi_gmask_t*)pdomap->pico_gmask)[cnt]=v;
                  cnt++;
                  pit++;
                  pbeg=pit;
                } else {
                  pit++;
                }
              }
            }
            pdomap->pico_gmask_len=cnt;
            free(value);
          }
        }
        cur1 = cur1->next;
      }
    }
    //pev2c gmask
    if (!xmlStrcmp(cur->name,(const xmlChar *)"pev2c_gmask")) {
      cur1 = cur->children;
      while(cur1) {
        if (!xmlStrcmp(cur1->name,(const xmlChar *)"data")) {
          xmlChar *value;
          value=xmlGetProp(cur1, (const xmlChar *)"value");
          if (value) {
            xmlChar *pbeg,*pit;
            int cnt,v;
            pbeg=value;pit=value;
            cnt=0;
            while(1) {
              if (*pit==0) {
                cnt++;
                break;
              }
              if (*pit==',') {
                cnt++;
                pit++;
                pbeg=pit;
              } else 
                pit++;
            }
            pdomap->pev2c_gmask=malloc(cnt*sizeof(uloi_gmask_t));
            cnt=0;
            if (pdomap->pev2c_gmask) {
              pbeg=value;pit=value;
              while(1) {
                if (*pit==0) {
                  v=strtol((char*)pbeg,NULL,0);
                  ((uloi_gmask_t*)pdomap->pev2c_gmask)[cnt]=v;
                  cnt++;
                  break;
                }
                if (*pit==',') {
                  *pit=0;
                  v=strtol((char*)pbeg,NULL,0);
                  ((uloi_gmask_t*)pdomap->pev2c_gmask)[cnt]=v;
                  cnt++;
                  pit++;
                  pbeg=pit;
                } else {
                  pit++;
                }
              }
            }
            pdomap->pev2c_gmask_len=cnt;
            free(value);
          }
        }
        cur1 = cur1->next;
      }
    }
    cur = cur->next;
  }
  xmlFreeDoc(doc);
  return 0;
}

/*******************************************************************/
int
pdomap_save(char *filename, ul_pdomap_t *pdomap) 
{
  xmlNodePtr root, node_body, node_pdomap, node_pico, node_poco, node_piom, node_pev2c, node;
  xmlNodePtr node_pico_gmask,node_pev2c_gmask;
  xmlDocPtr doc;
  uloi_ciddes_t *ciddes;
  uloi_pev2cdes_t *pev2cdes;
  unsigned indx;
  xmlChar value[256];
  unsigned short flg_loc;

  if (!filename) 
    filename="ul_pdomap.xml";

  doc = xmlNewDoc ((const xmlChar *)"1.0");
  root = xmlNewDocNode (doc, NULL, (const xmlChar *)"xml", NULL);
  xmlDocSetRootElement (doc, root);

  node_body = xmlNewChild (root, NULL, (const xmlChar *)"body", NULL);
  node_pdomap = xmlNewChild (node_body, NULL, (const xmlChar *)"pdomap", NULL);

  /* pico */
  node_pico = xmlNewChild (node_pdomap, NULL, (const xmlChar *)"pico", NULL);
  for(indx=pdotool_ciddes_array_first_indx(&pdomap->pico);
      (ciddes=pdotool_ciddes_array_indx2item(&pdomap->pico,indx)) != NULL;
      indx++) {
    node = xmlNewChild (node_pico, NULL, (const xmlChar *)"item", NULL);
    sprintf((char*)value,"%u",ciddes->cid);
    xmlSetProp (node, (const xmlChar *)"cid", value);
    flg_loc=ciddes->flg&ULOI_CIDFLG_META_LOC;
    if (flg_loc==ULOI_CIDFLG_META_OID) {
      sprintf((char*)value,"%u",ciddes->meta.oid);
      xmlSetProp (node, (const xmlChar *)"oid", value);
      sprintf((char*)value,"%u",ciddes->flg);
      xmlSetProp (node, (const xmlChar *)"flg", value);
      sprintf((char*)value,"%u",ciddes->meta_len);
      xmlSetProp (node, (const xmlChar *)"len", value);
    } else if (flg_loc==ULOI_CIDFLG_META_OFFS) {
      sprintf((char*)value,"%u",ciddes->meta.oid);
      xmlSetProp (node, (const xmlChar *)"meta", value);
      sprintf((char*)value,"%u",ciddes->flg);
      xmlSetProp (node, (const xmlChar *)"flg", value);
      sprintf((char*)value,"%u",ciddes->meta_len);
      xmlSetProp (node, (const xmlChar *)"len", value);
    } else if ((flg_loc==ULOI_CIDFLG_META_EM3B) ||
               (flg_loc==ULOI_CIDFLG_META_EM4B)) {
      sprintf((char*)value,"%u,%u",ciddes->meta.oid, ciddes->meta_len);
      xmlSetProp (node, (const xmlChar *)"metadata", value);
      sprintf((char*)value,"%u",ciddes->flg);
      xmlSetProp (node, (const xmlChar *)"flg", value);
      sprintf((char*)value,"%u",flg_loc==ULOI_CIDFLG_META_EM4B? 4: 3);
      xmlSetProp (node, (const xmlChar *)"len", value);
    } else {
      /* todo - an error ??? */
    }
  }

  /* poco */
  node_poco = xmlNewChild (node_pdomap, NULL, (const xmlChar *)"poco", NULL);
  for(indx=pdotool_ciddes_array_first_indx(&pdomap->poco);
      (ciddes=pdotool_ciddes_array_indx2item(&pdomap->poco,indx)) != NULL;
      indx++) {
    node = xmlNewChild (node_poco, NULL, (const xmlChar *)"item", NULL);
    sprintf((char*)value,"%u",ciddes->cid);
    xmlSetProp (node, (const xmlChar *)"cid", value);
    flg_loc=ciddes->flg&ULOI_CIDFLG_META_LOC;
    if (flg_loc==ULOI_CIDFLG_META_OID) {
      sprintf((char*)value,"%u",ciddes->meta.oid);
      xmlSetProp (node, (const xmlChar *)"oid", value);
      sprintf((char*)value,"%u",ciddes->flg);
      xmlSetProp (node, (const xmlChar *)"flg", value);
      sprintf((char*)value,"%u",ciddes->meta_len);
      xmlSetProp (node, (const xmlChar *)"len", value);
    } else if (flg_loc==ULOI_CIDFLG_META_OFFS) {
      sprintf((char*)value,"%u",ciddes->meta.oid);
      xmlSetProp (node, (const xmlChar *)"meta", value);
      sprintf((char*)value,"%u",ciddes->flg);
      xmlSetProp (node, (const xmlChar *)"flg", value);
      sprintf((char*)value,"%u",ciddes->meta_len);
      xmlSetProp (node, (const xmlChar *)"len", value);
    } else if ((flg_loc==ULOI_CIDFLG_META_EM3B) ||
               (flg_loc==ULOI_CIDFLG_META_EM4B)) {
      sprintf((char*)value,"%u,%u",ciddes->meta.oid, ciddes->meta_len);
      xmlSetProp (node, (const xmlChar *)"metadata", value);
      sprintf((char*)value,"%u",ciddes->flg);
      xmlSetProp (node, (const xmlChar *)"flg", value);
      sprintf((char*)value,"%u",flg_loc==ULOI_CIDFLG_META_EM4B? 4: 3);
      xmlSetProp (node, (const xmlChar *)"len", value);
    } else {
      /* todo - an error ??? */
    }
  }

  /* piom */
  node_piom = xmlNewChild (node_pdomap, NULL, (const xmlChar *)"piom", NULL);
  if (pdomap->meta_len>0) {
    uchar *meta_base=pdomap->meta_base;
    xmlChar *tvalue;
    tvalue=xmlCharStrdup("");
    node = xmlNewChild (node_piom, NULL, (const xmlChar *)"data", NULL);
    for(indx=0;indx<pdomap->meta_len;indx++) {
      sprintf((char*)value,"%d%s",meta_base[indx],indx!=(pdomap->meta_len-1) ? "," : "");
      tvalue=xmlStrcat(tvalue,value);
    }
    xmlSetProp (node, (const xmlChar *)"value", tvalue);
    free(tvalue);
  }

  /* pev2c */
  node_pev2c = xmlNewChild (node_pdomap, NULL, (const xmlChar *)"pev2c", NULL);
  for(indx=pdotool_pev2cdes_array_first_indx(&pdomap->pev2c);
      (pev2cdes=pdotool_pev2cdes_array_indx2item(&pdomap->pev2c,indx)) != NULL;
      indx++) {
    node = xmlNewChild (node_pev2c, NULL, (const xmlChar *)"item", NULL);
    sprintf((char*)value,"%u",pev2cdes->evnum);
    xmlSetProp (node, (const xmlChar *)"evnum", value);
    sprintf((char*)value,"%u",pev2cdes->cid);
    xmlSetProp (node, (const xmlChar *)"cid", value);
    sprintf((char*)value,"%u",pev2cdes->dadr);
    xmlSetProp (node, (const xmlChar *)"dadr", value);
    sprintf((char*)value,"%u",pev2cdes->flg);
    xmlSetProp (node, (const xmlChar *)"flg", value);
  }

  /* pico gmask */
  node_pico_gmask = xmlNewChild (node_pdomap, NULL, (const xmlChar *)"pico_gmask", NULL);
  if (pdomap->pico_gmask!=NULL) {
    uloi_gmask_t *gmask_base=pdomap->pico_gmask;
    xmlChar *tvalue;
    tvalue=xmlCharStrdup("");
    node = xmlNewChild (node_pico_gmask, NULL, (const xmlChar *)"data", NULL);
    for(indx=0;indx<pdomap->pico_gmask_len;indx++) {
      sprintf((char*)value,"%li%s",gmask_base[indx],indx!=(pdomap->pico_gmask_len-1) ? "," : "");
      tvalue=xmlStrcat(tvalue,value);
    }
    xmlSetProp (node, (const xmlChar *)"value", tvalue);
    free(tvalue);
  }

  /* pev2c gmask */
  node_pev2c_gmask = xmlNewChild (node_pdomap, NULL, (const xmlChar *)"pev2c_gmask", NULL);
  if (pdomap->pev2c_gmask!=NULL) {
    uloi_gmask_t *gmask_base=pdomap->pev2c_gmask;
    xmlChar *tvalue;
    tvalue=xmlCharStrdup("");
    node = xmlNewChild (node_pev2c_gmask, NULL, (const xmlChar *)"data", NULL);
    for(indx=0;indx<pdomap->pev2c_gmask_len;indx++) {
      sprintf((char*)value,"%li%s",gmask_base[indx],indx!=(pdomap->pev2c_gmask_len-1) ? "," : "");
      tvalue=xmlStrcat(tvalue,value);
    }
    xmlSetProp (node, (const xmlChar *)"value", tvalue);
    free(tvalue);
  }

  xmlKeepBlanksDefault(0);
  xmlSaveFormatFile (filename, doc, 1);
  xmlFreeDoc (doc);

  return 0;
}

int uloi_tb_buff_rd_ciddes(uloi_ciddes_t *pval, const void *buff)
{
  unsigned short flg_loc;
  uchar *p = (uchar *) buff;
  pval->cid=p[0]+(p[1]<<8);
  pval->flg=p[2]+(p[3]<<8);
  flg_loc=pval->flg&ULOI_CIDFLG_META_LOC;
  if (flg_loc==ULOI_CIDFLG_META_OID)
    pval->meta.oid=p[4]+(p[5]<<8);
  else if (flg_loc==ULOI_CIDFLG_META_OFFS)
    pval->meta.offs=p[4]+(p[5]<<8);
  else if ((flg_loc==ULOI_CIDFLG_META_EM3B) ||
        (flg_loc==ULOI_CIDFLG_META_EM4B))
    pval->meta.oid=p[4]+(p[5]<<8);
  else 
    return -1;
  pval->meta_len=p[6]+(p[7]<<8);
  return 0;
}

int uloi_tb_buff_wr_ciddes(void *buff, int size, uloi_ciddes_t *pval)
{
  unsigned short flg_loc;
  uchar *p = (uchar *) buff;
  if (size<8)
    return -1;
  p[0]=pval->cid;
  p[1]=pval->cid>>8;
  p[2]=pval->flg;
  p[3]=pval->flg>>8;
  flg_loc=pval->flg&ULOI_CIDFLG_META_LOC;
  if (flg_loc==ULOI_CIDFLG_META_OID) {
    p[4]=pval->meta.oid;
    p[5]=pval->meta.oid>>8;
  } else if (flg_loc==ULOI_CIDFLG_META_OFFS) {
    p[4]=pval->meta.offs;
    p[5]=pval->meta.offs>>8;
  } else if ((flg_loc==ULOI_CIDFLG_META_EM3B) ||
        (flg_loc==ULOI_CIDFLG_META_EM4B)) {
    p[4]=pval->meta.oid;
    p[5]=pval->meta.oid>>8;
  } else 
    return -1;
  p[6]=pval->meta_len;
  p[7]=pval->meta_len>>8;
  return 0;
}

int uloi_tb_buff_rd_pev2cdes(uloi_pev2cdes_t *pval, const void *buff)
{
  uchar *p = (uchar *) buff;
  pval->evnum=p[0]+(p[1]<<8);
  pval->cid=p[2]+(p[3]<<8);
  pval->dadr=p[4]+(p[5]<<8);
  pval->flg=p[6]+(p[7]<<8);
  return 0;
}

int uloi_tb_buff_wr_pev2cdes(void *buff, int size, uloi_pev2cdes_t *pval)
{
  uchar *p = (uchar *) buff;
  if (size<8)
    return -1;
  p[0]=pval->evnum;
  p[1]=pval->evnum>>8;
  p[2]=pval->cid;
  p[3]=pval->cid>>8;
  p[4]=pval->dadr;
  p[5]=pval->dadr>>8;
  p[6]=pval->flg;
  p[7]=pval->flg>>8;
  return 0;
}

int uloi_get_var_array(uloi_coninfo_t *coninfo,int list, int len, int idx, 
  uchar *obuff, int obuff_size, int *ofrom_idx, int *oitem_cnt, int *odata_begin_idx)
{
  int i;
  unsigned long arg;
  uchar metadata[4];
  int metadata_len;
  
  *ofrom_idx = 0;
  *oitem_cnt = 0;
  *odata_begin_idx = 0;
  if (len==0)
    return -1;
  if (len==1) {
    uloi_tb_buff_wr_unsigned(&metadata[2],2,idx);
    metadata_len=2;
  } else {
    uloi_tb_buff_wr_unsigned(&metadata[0],2,len | 0x8000);
    uloi_tb_buff_wr_unsigned(&metadata[2],2,idx);
    metadata_len=4;
  }
  i=uloi_get_var(coninfo, list, metadata, metadata_len, obuff, obuff_size);
  if(i<=0) {
    return i==0?0:-1;
  } 
  uloi_tb_buff_rd_unsigned(&arg,&obuff[0],2);
  *odata_begin_idx = 2;
  if(!(arg & ULOI_ARRAY_USE_ITEM_CNT)) {
    *ofrom_idx = arg;
    *oitem_cnt = 1;
  } else {
    arg &= ~ULOI_ARRAY_USE_ITEM_CNT;
    if(!(arg & ULOI_ARRAY_USE_COMMAND)) {
      *oitem_cnt = arg;
      uloi_tb_buff_rd_unsigned(&arg,&obuff[2],2);
      *odata_begin_idx = 4;
      *ofrom_idx = arg;
    } else
      return -1;
  }
  return 0;
}

int uloi_get_ciddes(uloi_coninfo_t *coninfo,int list,uloi_ciddes_t **ciddes_list)
{
  int indx,i;
  int at_once=64,ciddes_len=8;
  uloi_ciddes_t *ciddes=NULL;
  int ciddes_cnt=0;
  int from_idx,item_cnt,buff_data_begin;
  uchar *buff;

  buff=malloc(at_once*ciddes_len+4);
  if (!buff) return -1;
  indx=0;
  do {
    i=uloi_get_var_array(coninfo,list,at_once,indx,buff,at_once*ciddes_len,&from_idx,&item_cnt,&buff_data_begin);
    if(i<0) {
      if(ciddes!=NULL) free(ciddes);
      free(buff);
      return i==0?0:-1;
    } 
    if(ciddes==NULL) ciddes=malloc((item_cnt+1)*sizeof(uloi_ciddes_t));
    else ciddes=realloc(ciddes,(ciddes_cnt+item_cnt+1)*sizeof(uloi_ciddes_t));
    i=0;
    while(item_cnt!=i) {
      uloi_tb_buff_rd_ciddes(&ciddes[ciddes_cnt], &buff[i*ciddes_len+buff_data_begin]);
      if (ciddes[ciddes_cnt].cid!=0)
        ciddes_cnt++;
      i++;
    }
    memset(&ciddes[ciddes_cnt],0,sizeof(uloi_ciddes_t));
    indx+=at_once;
  } while (item_cnt>=at_once);
  *ciddes_list=ciddes;
  free(buff);
  return ciddes_cnt;
}

int uloi_set_ciddes(uloi_coninfo_t *coninfo,int list,pdotool_ciddes_array_t *array)
{
  int indx,array_indx;
  int at_once=64,ciddes_len=8;
  uchar *buff;
  uloi_ciddes_t *ciddes;
  int cnt,ret;

  buff=malloc(at_once*ciddes_len+4);
  if (!buff) return -1;
  indx=0;
  array_indx=pdotool_ciddes_array_first_indx(array);
  do {
    cnt=0;
    while(cnt<at_once) {
      ciddes=pdotool_ciddes_array_indx2item(array,array_indx);
      if (!ciddes) break;
      uloi_tb_buff_wr_ciddes(&buff[cnt*ciddes_len+4],8,ciddes);
      array_indx++;
      cnt++;
    }
    if (cnt==0) break;
    uloi_tb_buff_wr_unsigned(&buff[0],2,cnt | 0x8000);
    uloi_tb_buff_wr_unsigned(&buff[2],2,indx);
    ret=uloi_set_var(coninfo, list, buff, 8*cnt+4);
    if(ret<0) {
      free(buff);
      return -1;
    }
    indx+=cnt;
  } while (cnt>=at_once);
  free(buff);
  return array_indx;
}

int uloi_get_pdometa(uloi_coninfo_t *coninfo,int list,uchar **meta_list)
{
  int indx,i;
  int at_once=64;
  uchar *buff;
  uchar *meta=NULL;
  int meta_cnt=0;
  int from_idx,item_cnt,buff_data_begin;

  buff=malloc(at_once+4);
  if (!buff) return -1;
  indx=0;
  do {
    i=uloi_get_var_array(coninfo,list,at_once,indx,buff,at_once,&from_idx,&item_cnt,&buff_data_begin);
    if(i<0) {
      if(meta!=NULL) free(meta);
      free(buff);
      return -1;
    }
    if(meta==NULL) meta=malloc((item_cnt+1)*sizeof(uchar));
    else meta=realloc(meta,(meta_cnt+item_cnt+1)*sizeof(uchar));
    i=0;
    while(item_cnt!=i) {
      unsigned long val;
      uloi_tb_buff_rd_unsigned(&val, &buff[i+buff_data_begin],1);
      meta[meta_cnt]=val;
      meta_cnt++;
      i++;
    }
    meta[meta_cnt]=0;
    indx+=at_once;
  } while (item_cnt>=at_once);
  *meta_list=meta;
  free(buff);
  return meta_cnt;
}

int uloi_set_pdometa(uloi_coninfo_t *coninfo,int list,uchar *meta_list,int meta_len)
{
  int indx,array_indx;
  int at_once=64;
  uchar *buff;
  int cnt,ret;

  buff=malloc(at_once+4);
  if (!buff) return -1;
  indx=0;
  array_indx=0;
  do {
    cnt=0;
    while(cnt<at_once) {
      if (array_indx>=meta_len) break;
      uloi_tb_buff_wr_unsigned(&buff[cnt+4],1,meta_list[array_indx]);
      array_indx++;
      cnt++;
    }
    if (cnt==0) break;
    uloi_tb_buff_wr_unsigned(&buff[0],2,cnt | 0x8000);
    uloi_tb_buff_wr_unsigned(&buff[2],2,indx);
    ret=uloi_set_var(coninfo, list, buff, cnt+4);
    if(ret<0) {
      free(buff);
      return -1;
    }
    indx+=cnt;
  } while (cnt>=at_once);
  free(buff);
  return array_indx;
}

int uloi_get_pev2cdes(uloi_coninfo_t *coninfo,int list,uloi_pev2cdes_t **pev2cdes_list)
{
  int indx,i;
  int at_once=64,pev2cdes_len=8;
  uchar *buff;
  uloi_pev2cdes_t *pev2cdes=NULL;
  int pev2cdes_cnt=0;
  int from_idx,item_cnt,buff_data_begin;

  buff=malloc(at_once*pev2cdes_len+4);
  if (!buff) return -1;
  indx=0;
  do {
    i=uloi_get_var_array(coninfo,list,at_once,indx,buff,at_once*pev2cdes_len,&from_idx,&item_cnt,&buff_data_begin);
    if(i<0) {
      if(pev2cdes!=NULL) free(pev2cdes);
      free(buff);
      return -1;
    }
    if(pev2cdes==NULL) pev2cdes=malloc((item_cnt+1)*sizeof(uloi_pev2cdes_t));
    else pev2cdes=realloc(pev2cdes,(pev2cdes_cnt+item_cnt+1)*sizeof(uloi_pev2cdes_t));
    i=0;
    while(item_cnt!=i) {
      uloi_tb_buff_rd_pev2cdes(&pev2cdes[pev2cdes_cnt], &buff[i*pev2cdes_len+buff_data_begin]);
      if (pev2cdes[pev2cdes_cnt].evnum!=0)
        pev2cdes_cnt++;
      i++;
    }
    memset(&pev2cdes[pev2cdes_cnt],0,sizeof(uloi_pev2cdes_t));
    indx+=at_once;
  } while (item_cnt>=at_once);
  *pev2cdes_list=pev2cdes;
  free(buff);
  return pev2cdes_cnt;
}

int uloi_set_pev2cdes(uloi_coninfo_t *coninfo,int list,pdotool_pev2cdes_array_t *array)
{
  int indx,array_indx;
  int at_once=64,pev2cdes_len=8;
  uchar *buff;
  uloi_pev2cdes_t *pev2cdes;
  int cnt,ret;

  buff=malloc(at_once*pev2cdes_len+4);
  if (!buff) return -1;
  indx=0;
  array_indx=pdotool_pev2cdes_array_first_indx(array);
  do {
    cnt=0;
    while(cnt<at_once) {
      pev2cdes=pdotool_pev2cdes_array_indx2item(array,array_indx);
      if (!pev2cdes) break;
      uloi_tb_buff_wr_pev2cdes(&buff[cnt*pev2cdes_len+4],8,pev2cdes);
      array_indx++;
      cnt++;
    }
    if (cnt==0) break;
    uloi_tb_buff_wr_unsigned(&buff[0],2,cnt | 0x8000);
    uloi_tb_buff_wr_unsigned(&buff[2],2,indx);
    ret=uloi_set_var(coninfo, list, buff, 8*cnt+4);
    if(ret<0) {
      free(buff);
      return -1;
    }
    indx+=cnt;
  } while (cnt>=at_once);
  free(buff);
  return array_indx;
}

int uloi_get_gmask(uloi_coninfo_t *coninfo,int list,uloi_gmask_t **meta_list)
{
  int indx,i;
  int at_once=64,gmask_len=sizeof(uloi_gmask_t);
  uchar *buff;
  uloi_gmask_t *meta=NULL;
  int meta_cnt=0;
  int from_idx,item_cnt,buff_data_begin;

  buff=malloc(at_once*gmask_len+4);
  if (!buff) return -1;
  indx=0;
  do {
    i=uloi_get_var_array(coninfo,list,at_once,indx,buff,at_once,&from_idx,&item_cnt,&buff_data_begin);
    if(i<0) {
      if(meta!=NULL) free(meta);
      free(buff);
      return -1;
    }
    if(meta==NULL) meta=malloc((item_cnt+1)*sizeof(uloi_gmask_t));
    else meta=realloc(meta,(meta_cnt+item_cnt+1)*sizeof(uloi_gmask_t));
    i=0;
    while(item_cnt!=i) {
      unsigned long val;
      uloi_tb_buff_rd_unsigned(&val, &buff[i+buff_data_begin],sizeof(uloi_gmask_t));
      meta[meta_cnt]=val;
      meta_cnt++;
      i++;
    }
    meta[meta_cnt]=0;
    indx+=at_once;
  } while (item_cnt>=at_once);
  *meta_list=meta;
  free(buff);
  return meta_cnt;
}

int uloi_set_gmask(uloi_coninfo_t *coninfo,int list,uloi_gmask_t *meta_list,int meta_len)
{
  int indx,array_indx;
  int at_once=64,gmask_len=sizeof(uloi_gmask_t);
  uchar *buff;
  int cnt,ret;

  buff=malloc(at_once*gmask_len+4);
  if (!buff) return -1;
  indx=0;
  array_indx=0;
  do {
    cnt=0;
    while(cnt<at_once) {
      if (array_indx>=meta_len) break;
      uloi_tb_buff_wr_unsigned(&buff[cnt*gmask_len+4],gmask_len,meta_list[array_indx]);
      array_indx++;
      cnt++;
    }
    if (cnt==0) break;
    uloi_tb_buff_wr_unsigned(&buff[0],2,cnt | 0x8000);
    uloi_tb_buff_wr_unsigned(&buff[2],2,indx);
    ret=uloi_set_var(coninfo, list, buff, cnt*gmask_len+4);
    if(ret<0) {
      free(buff);
      return -1;
    }
    indx+=cnt;
  } while (cnt>=at_once);
  free(buff);
  return array_indx;
}

//global variables
ul_pdomap_t pdomap_state;
char *ul_dev_name = UL_DEV_NAME;
char *upload_filename;
char *download_filename;
int module = 1;
int upload_flg =0;
int download_flg =0;
int clear_flg=0;
int save_flg=0;

int 
pdo_download(int module, ul_pdomap_t *pdomap) 
{
  uloi_coninfo_t *coninfo;
  int *oids;
  int i,ret;
  int pico_fl=0,poco_fl=0,piom_fl=0,pev2c_fl=0;
  int pico_gmask_fl=0,pev2c_gmask_fl=0;

  coninfo=uloi_open(ul_dev_name,module,0x10,0,10);
  if(!coninfo) { perror("oi_var_test : uLan OI open failed");return -1;};

  printf("=== PDO DONWLOAD - module: %d ===\n",module);
  printf("Checking for PDO communication state ...\n");
  ret=uloi_get_oids(coninfo,ULOI_QOII,&oids);
  if(ret<0)
  { fprintf(stderr,"pdo_download : uloi_get_oids returned %d\n",ret);
    return -1;
  };
  for(i=0;i<ret;i++) {
    if (oids[i]==ULOI_PICO) pico_fl=1;
    if (oids[i]==ULOI_POCO) poco_fl=1;
    if (oids[i]==ULOI_PIOM) piom_fl=1;
    if (oids[i]==ULOI_PEV2C) pev2c_fl=1;
    if (oids[i]==ULOI_PICO_GMASK) pico_gmask_fl=1;
    if (oids[i]==ULOI_PEV2C_GMASK) pev2c_gmask_fl=1;
  }
  free(oids);
  printf("PDO module %d state:\n  PICO:%d\n  POCO:%d\n  PIOM:%d\n  PEV2C:%d\n  PICO GMASK:%d\n  PEV2C GMASK:%d\n",
          module,pico_fl,poco_fl,piom_fl,pev2c_fl,pico_gmask_fl,pev2c_gmask_fl);

  if (pico_fl) {
    printf("Writing PICO table ...\n");
    ret=uloi_set_ciddes(coninfo,ULOI_PICO,&pdomap->pico);
    if(ret<0)
    { fprintf(stderr,"pdo_download : uloi_set_ciddes returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
  }

  if (poco_fl) {
    printf("Writing POCO table ...\n");
    ret=uloi_set_ciddes(coninfo,ULOI_POCO,&pdomap->poco);
    if(ret<0)
    { fprintf(stderr,"pdo_download : uloi_set_ciddes returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
  }

  if (piom_fl) {
    printf("Writing PIOM table ...\n");
    ret=uloi_set_pdometa(coninfo,ULOI_PIOM,(uchar*)pdomap->meta_base,pdomap->meta_len);
    if(ret<0)
    { fprintf(stderr,"pdo_download : uloi_set_pdometa returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    pdomap->meta_len=ret;
  }

  if (pev2c_fl) {
    printf("Writing PEV2C table ...\n");
    ret=uloi_set_pev2cdes(coninfo,ULOI_PEV2C,&pdomap->pev2c);
    if(ret<0)
    { fprintf(stderr,"pdo_download : uloi_set_pev2cdes returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
  }

  if (pico_gmask_fl) {
    printf("Writing PICO GMASK table ...\n");
    ret=uloi_set_gmask(coninfo,ULOI_PICO_GMASK,pdomap->pico_gmask,pdomap->pico_gmask_len);
    if(ret<0)
    { fprintf(stderr,"pdo_download : uloi_set_gmask returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    pdomap->pico_gmask_len=ret;
  }

  if (pev2c_gmask_fl) {
    printf("Writing PEV2C GMASK table ...\n");
    ret=uloi_set_gmask(coninfo,ULOI_PEV2C_GMASK,pdomap->pev2c_gmask,pdomap->pev2c_gmask_len);
    if(ret<0)
    { fprintf(stderr,"pdo_download : uloi_set_gmask returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    pdomap->pev2c_gmask_len=ret;
  }

  uloi_close(coninfo);
  return 0;
}

int 
pdo_upload(int module, ul_pdomap_t *pdomap) 
{
  uloi_coninfo_t *coninfo;
  int pico_fl=0,poco_fl=0,piom_fl=0,pev2c_fl=0;
  int pico_gmask_fl=0,pev2c_gmask_fl=0;
  int *oids;
  int i, ret;
  uloi_ciddes_t *ciddes,*ciddes_list;
  uloi_pev2cdes_t *pev2cdes,*pev2cdes_list;

  coninfo=uloi_open(ul_dev_name,module,0x10,0,10);
  if(!coninfo) { perror("oi_var_test : uLan OI open failed");return -1;};

  printf("=== PDO UPLOAD - module: %d ===\n",module);
  printf("Checking for PDO communication state ...\n");
  ret=uloi_get_oids(coninfo,ULOI_QOII,&oids);
  if(ret<0)
  { fprintf(stderr,"pdo_upload : uloi_get_oids returned %d\n",ret);
    return -1;
  };
  for(i=0;i<ret;i++) {
    if (oids[i]==ULOI_PICO) pico_fl=1;
    if (oids[i]==ULOI_POCO) poco_fl=1;
    if (oids[i]==ULOI_PIOM) piom_fl=1;
    if (oids[i]==ULOI_PEV2C) pev2c_fl=1;
    if (oids[i]==ULOI_PICO_GMASK) pico_gmask_fl=1;
    if (oids[i]==ULOI_PEV2C_GMASK) pev2c_gmask_fl=1;
  }
  free(oids);
  printf("PDO module %d state:\n  PICO:%d\n  POCO:%d\n  PIOM:%d\n  PEV2C:%d\n  PICO GMASK:%d\n  PEV2C GMASK:%d\n",
          module,pico_fl,poco_fl,piom_fl,pev2c_fl,pico_gmask_fl,pev2c_gmask_fl);

  if (pico_fl) {
    printf("Reading PICO table ...\n");
    ret=uloi_get_ciddes(coninfo,ULOI_PICO,&ciddes_list);
    if(ret<0)
    { fprintf(stderr,"pdo_upload : uloi_get_ciddes returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    i=0;
    while(i!=ret) {
      ciddes=malloc(sizeof(uloi_ciddes_t));
      if (!ciddes)
      { fprintf(stderr,"malloc - no memory\n");
        return -1;
      };
      memcpy(ciddes,&ciddes_list[i],sizeof(uloi_ciddes_t));
      pdotool_ciddes_array_insert(&pdomap->pico,ciddes);
      i++;
    }
    free(ciddes_list);
  }

  if (poco_fl) {
    printf("Reading POCO table ...\n");
    ret=uloi_get_ciddes(coninfo,ULOI_POCO,&ciddes_list);
    if(ret<0)
    { fprintf(stderr,"pdo_upload : uloi_get_ciddes returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    i=0;
    while(i!=ret) {
      ciddes=malloc(sizeof(uloi_ciddes_t));
      if (!ciddes)
      { fprintf(stderr,"malloc - no memory\n");
        return -1;
      };
      memcpy(ciddes,&ciddes_list[i],sizeof(uloi_ciddes_t));
      pdotool_ciddes_array_insert(&pdomap->poco,ciddes);
      i++;
    }
    free(ciddes_list);
  }

  if (piom_fl) {
    printf("Reading PIOM table ...\n");
    ret=uloi_get_pdometa(coninfo,ULOI_PIOM,(uchar**)&pdomap->meta_base);
    if(ret<0)
    { fprintf(stderr,"pdo_upload : uloi_get_pdometa returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    pdomap->meta_len=ret;
  }

  if (pev2c_fl) {
    printf("Reading PEV2C table ...\n");
    ret=uloi_get_pev2cdes(coninfo,ULOI_PEV2C,&pev2cdes_list);
    if(ret<0)
    { fprintf(stderr,"pdo_upload : uloi_get_pev2cdes returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    i=0;
    while(i!=ret) {
      pev2cdes=malloc(sizeof(uloi_pev2cdes_t));
      if (!pev2cdes)
      { fprintf(stderr,"malloc - no memory\n");
        return -1;
      };
      memcpy(pev2cdes,&pev2cdes_list[i],sizeof(uloi_pev2cdes_t));
      pdotool_pev2cdes_array_insert(&pdomap->pev2c,pev2cdes);
      i++;
    }
    free(pev2cdes_list);
  }

  if (pico_gmask_fl) {
    printf("Reading PICO GMASK table ...\n");
    ret=uloi_get_gmask(coninfo,ULOI_PICO_GMASK,&pdomap->pico_gmask);
    if(ret<0)
    { fprintf(stderr,"pdo_upload : uloi_get_gmask returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    pdomap->pico_gmask_len=ret;
  }

  if (pev2c_gmask_fl) {
    printf("Reading PEV2C GMASK table ...\n");
    ret=uloi_get_gmask(coninfo,ULOI_PEV2C_GMASK,&pdomap->pev2c_gmask);
    if(ret<0)
    { fprintf(stderr,"pdo_upload : uloi_get_gmask returned %d\n",ret);
      return -1;
    };
    printf("%d items\n",ret);
    pdomap->pev2c_gmask_len=ret;
  }

  uloi_close(coninfo);
  return 0;
}

int uloi_exec(int module, int oid)
{
  uloi_coninfo_t *coninfo;

  coninfo=uloi_open(ul_dev_name,module,0x10,0,10);
  if(!coninfo) { perror("oi_var_test : uLan OI open failed");return -1;};
  uloi_set_var(coninfo, oid, NULL, 0);
  uloi_close(coninfo);
  return 0;
}

static void
usage(void)
{
  printf("Usage: ul_pdotool <parameters>\n");
  printf("  -d, --uldev <name>         name of uLan device [%s]\n",ul_dev_name);
  printf("  -m, --module <num>         messages from/to module [%d]\n",module);
  printf("  -U, --upload <filename>    upload PDO configuration(from module)\n");
  printf("  -D, --download <filename>  download PDO configuration(to module)\n");
  printf("  -c, --clear                clear PDO configuration\n");
  printf("  -s, --save                 save PDO configuration\n");
  printf("  -V, --version              show version\n");
  printf("  -h, --help                 this usage screen\n");
}

int main(int argc, char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module", 1, 0, 'm' },
    { "upload", 1, 0, 'U' },
    { "download", 1, 0, 'D' },
    { "save", 0, 0, 's' },
    { "clear", 0, 0, 'c' },
    { "version", 0, 0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int ret,opt;

  pdomap_init_items(&pdomap_state);

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:U:D:scVh")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:U:D:scVh",
                            &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 'U':
      upload_flg = 1;
      upload_filename=optarg;
      break;
    case 'D':
      download_flg = 1;
      download_filename=optarg;
      break;
    case 's':
      save_flg = 1;
      break;
    case 'c':
      clear_flg = 1;
      break;
    case 'V':
      fputs("uLan pdotool v0.1\n", stdout);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }

  if (upload_flg) {
    ret=pdo_upload(module, &pdomap_state);
    if (ret<0) {
      printf("pdo tables upload error\n");
      return 1;
    }
    pdomap_save(upload_filename, &pdomap_state);
    pdomap_free_items(&pdomap_state);
  }

  if (clear_flg)
    uloi_exec(module,ULOI_PMAP_CLEAR);

  if (download_flg) {
    ret=pdomap_load(download_filename,&pdomap_state);
    if (ret<0) {
      printf("loading file '%s' error\n",download_filename);
      return 1;
    }
    ret=pdo_download(module, &pdomap_state);
    if (ret<0) {
      printf("pdo tables download error\n");
      return 1;
    }
    pdomap_free_items(&pdomap_state);
  }

  if (save_flg)
    uloi_exec(module,ULOI_PMAP_SAVE);

  return 0;
}
