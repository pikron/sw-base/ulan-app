/*******************************************************************
  uLan Communication - simple test client

  ul_lcscan.c	- procceed wavelength scanning with LCD5000

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <ul_lib/ulan.h>
#include <uloi_com.h>

#ifdef _WIN32
#define sleep Sleep
#endif

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int module= 3;
int uloi_con_outflg;
int uloi_con_outflg_set_flg = 0;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int wl_begin   = 200;
int wl_end     = 300;
int wl_step    = 1;
int wl_pause   = 1000;
int zero_fl    = 0;
int time_co_fl = 0;
int time_co    = 0;
int adc_chan_fl= 0;
int adc_chan   = 0;
int lampctrl_fl= 0;
int lampctrl   = 0;

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("send_cmd_go : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

float IEEE4tofloat(void *p)
{
 uchar *up=p;
 long mantisa;
 int exponent;
 mantisa=up[0]|((unsigned)up[1]<<8)|
         ((unsigned long)up[2]<<16)|0x800000;
 if(up[3]&0x80) mantisa=-mantisa;
 exponent=(up[2]>>7)|(((int)up[3]&0x7f)<<1);
 return ldexp((float)mantisa,exponent-0x7f-23);
};

uchar test_cmd[]={0,0,0,204,0,234,0,20,0,204,0,0,0};

int wl_set_wlen(uloi_coninfo_t *coninfo,unsigned wlen)
{ unsigned u;
  int ret;
  ret=uloi_set_var_u2(coninfo,204,wlen);
  if(ret<0) return ret;
  while(1)
  { sleep(1);
    ret=uloi_get_var_u2(coninfo,204,&u);
    if(ret<0) return ret;
    if(u==0x8000) continue;
    if(u&0x8000) return -1;
    if(u!=wlen) return -1;
    return wlen;
  }
}

int wl_scan(void)
{
  int ret;
  ul_fd_t ul_fd;
  ul_msginfo msginfo;
  uloi_coninfo_t *coninfo;
  uchar  buf[10];
  struct timeval timeout;
  unsigned wlen;
  fd_set set;
  float  abs;

  coninfo=uloi_open(ul_dev_name,module,0x10,0,10);
  if(!coninfo) { perror("wl_scan : uLan OI open failed");return -1;};
  
  if(time_co_fl)
  { uloi_set_var_u2(coninfo,208,time_co);
  }

  if(adc_chan_fl)
  { uloi_set_var_u2(coninfo,209,adc_chan);
  }

  if(lampctrl_fl)
  { uloi_set_var_u2(coninfo,207,lampctrl);
  }

  wlen=wl_begin;
  if(!wl_step) wl_end=wlen;
  if(((wl_begin<=wl_end)&&(wl_step>=0))
   ||((wl_begin>=wl_end)&&(wl_step<0))) while(1)
  { ret=wl_set_wlen(coninfo,wlen);
    fprintf(stderr,"ul_set_wlen returned %d\n",ret);
    sleep(wl_pause/1000);
    if(zero_fl)
    { ret=uloi_send_cmd(coninfo,255);
      fprintf(stderr,"uloi_send_cmd returned %d\n",ret);
    
    }
    ret=uloi_get_var(coninfo,220,NULL,0,buf,4);
    fprintf(stderr,"uloi_get_var returned %d\n",ret);
    printf("%d %f\n",wlen,IEEE4tofloat(buf));
    
    if((wlen==wl_end)||!wl_step) break;
    wlen+=wl_step;
    if(((wl_step>0)&&(wlen>wl_end))
     ||((wl_step<0)&&(wlen<wl_end))) wlen=wl_end;
  }

  uloi_close(coninfo);
  return 0;

  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("wl_scan : uLan open failed");return -1;};
  memset(&msginfo,0,sizeof(msginfo));
  msginfo.sadr=module;
  msginfo.cmd=UL_CMD_LCDABS;
  ret=ul_addfilt(ul_fd,&msginfo);
  if(ret<0) { printf("wl_scan : add filter failed\n");return ret;};
  msginfo.cmd=UL_CMD_LCDMRK;
  ret=ul_addfilt(ul_fd,&msginfo);
  if(ret<0) { printf("wl_scan : add filter failed\n");return ret;};

  for(;;)
  {
  #ifndef WITHOUT_SYS_SELECT
    FD_ZERO (&set);
    FD_SET (ul_fd2sys_fd(ul_fd), &set);
    FD_SET (0, &set);
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
            &&errno==-EINTR);
    if(FD_ISSET(0,&set))
      {printf("\nwl_scan : break by user\n");break;};
    if(ret<0) return ret;
    if(!ret) continue;
  #else /* WITHOUT_SYS_SELECT */
    while (ul_fd_wait(ul_fd,1)<=0);
  #endif /* WITHOUT_SYS_SELECT */
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) 
    { printf("wl_scan : problem to accept message\n");
      return ret;
    };
    if(msginfo.flg&UL_BFL_FAIL) 
      {ul_freemsg(ul_fd);continue;};
    if(msginfo.cmd==UL_CMD_LCDABS)
    { while((ret=ul_read(ul_fd,buf,4))==4)
      {
        abs=IEEE4tofloat(buf);
        printf("%9.6f\n",abs);
      };
    }else 
    if(msginfo.cmd==UL_CMD_LCDMRK)
    {       
      memset(buf,0,sizeof(buf));
      ul_read(ul_fd,buf,4);
      printf("MARK %d %d\n",buf[0],buf[1]);
    };
    ul_freemsg(ul_fd);
  };
  ul_close(ul_fd);
  uloi_close(coninfo);
  return 0;
};

static void
usage(void)
{
  printf("Usage: ul_lcscan <parameters> <hex_file>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       messages from/to module\n");
  printf("  -f, --outflg <num>       flags for ULOI message retry and confirmation\n");
  printf("  -p, --print <max>        print modules to max address\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -b, --begin <wlen>       initial wavelength\n");
  printf("  -e, --end <wlen>         final wavelength\n");
  printf("  -s, --step <wlen>        wavelength step\n");
  printf("  -z, --zero               zero absorbance\n");
  printf("  -t, --time-const <s>     ADC filter time constant\n");
  printf("  -c, --adc-chan <c>       imput chanel of ADC\n");
  printf("  -l, --lamp <l>           lamp control\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "outflg", 1, 0, 'f' },
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { "begin", 1, 0, 'b' },
    { "end",   1, 0, 'e' },
    { "step",  1, 0, 's' },
    { "pause", 1, 0, 'P' },
    { "zero",  0, 0, 'z' },
    { "time-const", 1, 0, 't' },
    { "adc-chan", 1, 0, 'c' },
    { "lanmp", 1, 0, 'l' },
    { 0, 0, 0, 0}
  };
  int  opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:p:Vhb:e:s:P:zt:c:l:")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:p:Vhb:e:s:P:zt:c:l:",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 'f':
      uloi_con_outflg = strtol(optarg,NULL,0);
      uloi_con_outflg_set_flg = 1;
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'V':
      fputs("uLan lcscan v0.7\n", stdout);
      exit(0);
    case 'b':
      wl_begin = strtol(optarg,NULL,0);
      break;
    case 'e':
      wl_end = strtol(optarg,NULL,0);
      break;
    case 's':
      wl_step = strtol(optarg,NULL,0);
      break;
    case 'P':
      wl_pause = strtol(optarg,NULL,0);
      break;
    case 'z':
      zero_fl=1;
      break;
    case 't':
      time_co_fl=1;
      time_co=strtod(optarg,NULL)*100;
      break;
    case 'c':
      adc_chan_fl=1;
      adc_chan=strtol(optarg,NULL,0);
      break;
    case 'l':
      lampctrl_fl=1;
      lampctrl=strtol(optarg,NULL,0);
      break;
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if(debugk_flg) debug_kernel(debugk);

  if(prt_modules) print_modules(prt_modules);

  if(!debugk_flg&&!prt_modules)
    wl_scan();

  return 0;
}
