/*******************************************************************
  uLan Communication - addresss server 

  ul_asd.c	- 

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/
 
#include "ul_asd_headers.h"

extern pthread_mutex_t ul_net_mutex;

/*******************************************************************/
int 
xml_conf_load(uchar *filename, ul_net_info_t *ul_net) 
{
    xmlDocPtr doc;
    xmlNodePtr cur,cur1;

    xmlKeepBlanksDefault(1);
    doc = xmlParseFile(filename);
    if (doc == NULL) return(-1);
    cur = xmlDocGetRootElement(doc);
    if (cur == NULL) {
        fprintf(stderr,"empty document\n");
	xmlFreeDoc(doc);
	return(-1);
    }
    cur = cur->children;
    while(cur){
      if (!strcmp(cur->name, "body")) break;
      cur = cur->next;
    }
    if (!cur) {
        fprintf(stderr,"no body section!\n");
	xmlFreeDoc(doc);
	return(-1);
    }
    cur = cur->children;
    while (cur) {
      //configuration section
      if (!strcmp(cur->name,"configuration")) {
        cur1 = cur->children;
	while(cur1) {
	  char *value = xmlGetProp(cur1, "value");
          if (!strcmp(cur1->name,"status_period")) {
	    ul_net->dy_state->period=strtol(value,NULL,0);
	  } else if (!strcmp(cur1->name,"first_dynamic_address")) {
            ul_net->dy_state->first_dy_adr=strtol(value,NULL,0);
	  } else if (!strcmp(cur1->name,"cycle_start")) {
	    ul_dy_cycle_subfn=strtol(value,NULL,0);
	  }
          cur1 = cur1->next;
	}
      }
      //static_addresses section
      if (!strcmp(cur->name,"static_addresses")) {
        cur1 = cur->children;
	while(cur1) {
          if (!strcmp(cur1->name,"module")) {
  	    char *adr,*sn;
	    adr= xmlGetProp(cur1, "address");
	    sn= xmlGetProp(cur1, "sn");
	    if (ul_mod_static_add(ul_net,strtol(adr,NULL,0),strtol(sn,NULL,0))<0) {
   	      printf("fail adding static module:%li,%li\n",strtol(adr,NULL,0),strtol(sn,NULL,0));
	    }	    
	  }
          cur1 = cur1->next;
	}
        
      }
      cur = cur->next;
    }
    xmlFreeDoc(doc);
    return 0;
}

/*******************************************************************/
int 
xml_conf_save(uchar *filename, ul_net_info_t *ul_net) 
{
  xmlNodePtr root, node_body, node_conf, node_statica, node;
  xmlDocPtr doc;
  ul_mod_info_t *mod;
  char value[20];
  int mindx;
  
  doc = xmlNewDoc ("1.0");
  root = xmlNewDocNode (doc, NULL, "xml", NULL);
  xmlDocSetRootElement (doc, root);
			
  node_body = xmlNewChild (root, NULL, "body", NULL);
  node_conf = xmlNewChild (node_body, NULL, "configuration", NULL);
  node_statica = xmlNewChild (node_body, NULL, "static_addresses", NULL);
  
  //configuration
  node = xmlNewChild (node_conf, NULL, "status_period", NULL);
  sprintf(value,"%d",ul_net->dy_state->period);
  xmlSetProp (node, "value", value);
  node = xmlNewChild (node_conf, NULL, "first_dynamic_address", NULL);
  sprintf(value,"%d",ul_net->dy_state->first_dy_adr);
  xmlSetProp (node, "value", value);
  node = xmlNewChild (node_conf, NULL, "cycle_start", NULL);
  sprintf(value,"%d",ul_dy_cycle_subfn);
  xmlSetProp (node, "value", value);

  //static addresses
  for(mindx=0;mindx<ul_net->modules.count;mindx++){
    mod=ul_net->modules.items[mindx];
    if (mod->flg&ULMI_STATIC) {
      node = xmlNewChild (node_statica, NULL, "module", NULL);
      sprintf(value,"%d",mod->mod_adr);
      xmlSetProp (node, "address", value);
      sprintf(value,"0x%lx",mod->mod_sn);
      xmlSetProp (node, "sn", value);
    }
  }
  
  xmlKeepBlanksDefault(0);
  xmlSaveFormatFile (filename, doc, 1);
  xmlFreeDoc (doc);
  return 0;
}

