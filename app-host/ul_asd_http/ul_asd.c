/*******************************************************************
  uLan Communication - addresss server 

  ul_asd.c	- 

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include "ul_asd_headers.h"

#define GSA_OFFSET(_type,_member) \
		((int)&(((_type*)0)->_member))

int ul_dy_cycle_subfn;
int daemon_flg=0;
long int print_ip_status_period=2000;
char *ul_dev_name = UL_DEV_NAME;
char *ul_asd_conf = "ul_asd_conf.xml";
int short_flg	= 0;
int log_all_flg = 0;
ul_net_info_t *ul_net;
pthread_mutex_t ul_net_mutex;

int xml_conf_load(uchar *filename, ul_net_info_t *ul_net);
int xml_conf_save(uchar *filename, ul_net_info_t *ul_net);

#define WITHOUT_SYS_SELECT
#define UL_DRV_IN_LIB

void
ul_as_loop(void)
{
  ul_msdiff_t ms_sleep;

  while(1){
    ul_htimer_queue_t *root_htimer;
    ul_htim_time_t actual_time;
    ul_htim_time_t next_expire;

    pthread_mutex_lock(&ul_net_mutex);
    ul_root_htimer_current_time(0, &actual_time);
    root_htimer = ul_root_htimer_get(0, NULL);
    ul_htimer_run_expired(root_htimer,&actual_time);
    if(!ul_htimer_next_expire(root_htimer,&next_expire))
      next_expire=actual_time+0x10000000;
    ul_root_htimer_put(root_htimer);
    pthread_mutex_unlock(&ul_net_mutex);

    ul_root_htimer_current_time(0, &actual_time);

    ul_htime_sub2ms(&ms_sleep, &next_expire, &actual_time);
    if(ms_sleep<0) ms_sleep=0;
    /* we do not want to overflow usec field of timeout.tv_usec */
    if(ms_sleep>(~(ul_mstime_t)0/2001)) ms_sleep=~(ul_mstime_t)0/2001;

    if(ul_inepoll(ul_net->ul_fd)>0){
	int loopcnt=100;
	do {
          pthread_mutex_lock(&ul_net_mutex);
          ul_net_do_rec_msg(ul_net);
          pthread_mutex_unlock(&ul_net_mutex);
	} while((ul_inepoll(ul_net->ul_fd)>0)&&loopcnt--);
    }else {
    #ifdef WITHOUT_SYS_SELECT
     #ifdef _WIN32
      if(ms_sleep>100) ms_sleep=100;
      Sleep(ms_sleep);
     #elif defined(UL_DRV_IN_LIB)
      if(ms_sleep>100) ms_sleep=100;
      usleep(ms_sleep*1000);
     #endif /* UL_DRV_IN_LIB */
    #else /* !WITHOUT_SYS_SELECT */
      {
	int ret;
	struct timeval timeout;
	fd_set set;

	FD_ZERO (&set);
	FD_SET (ul_net->ul_fd, &set);
	FD_SET (0, &set);
	timeout.tv_sec = 0;
	timeout.tv_usec = ms_sleep*1000;
	while ((ret=select(FD_SETSIZE,&set, NULL, NULL,&timeout))==-1
        	&&errno==-EINTR);
	/* printf("select %d ",ret); */
        if(FD_ISSET(0,&set))
        { char ch;
          read(0,&ch,1);
          switch(ch) {
            case 'q' : 
              printf("\nUser requested quit\n");
              return;
          }
        }
      }
    #endif /* !WITHOUT_SYS_SELECT */
    }
  }
}


static void
usage(void)
{
  printf("usage: ul_asd <parameters>\n");
  printf("  -d, --uldev  <name>      name of uLan device [%s]\n",UL_DEV_NAME);
  printf("  -c, --conf  <name>       name of configuration file [%s]\n",ul_asd_conf);
#ifdef _WIN32
  printf("  -i, --install_service    install service into service manager on Windows\n");
  printf("  -r, --remove_service     remove service from service manager on Windows\n");
#else
  printf("  -e, --daemon             starting like deamon\n");
#endif  /* _WIN32 */
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main_options(int argc, char *argv[])
{

  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "conf", 1, 0, 'c' },
#ifdef _WIN32
    { "install_service",0,0, 'i' },
    { "remove_service",  0, 0, 'r' },
#else
    { "daemon",  0, 0, 'e' },
#endif /* _WIN32 */
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { 0, 0, 0, 0}
  };
  int  opt;

 #ifndef HAS_GETOPT_LONG
  #ifdef _WIN32
    while ((opt = getopt(argc, argv, "d:c:irVh")) != EOF)
  #else
    while ((opt = getopt(argc, argv, "d:c:eVh")) != EOF)
  #endif /* _WIN32 */
 #else
  #ifdef _WIN32
    while ((opt = getopt_long(argc, argv, "d:irVh",
			    &long_opts[0], NULL)) != EOF)
  #else
    while ((opt = getopt_long(argc, argv, "d:eVh",
			    &long_opts[0], NULL)) != EOF)
  #endif /* _WIN32 */
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'c':
      ul_asd_conf=optarg;
      break;
    #ifdef _WIN32
    case 's':
//      service_dispatchTable();
      exit(0);
      break;
    case 'i':
//      install_service();
      exit(0);
      break;
    case 'r':
//      remove_service();
      exit(0);
      break;
    #else
    case 'e':
      daemon_flg=1;
      break;  
    #endif  /* _WIN32 */
    case 'V':
      //printf("uLan address server - version %s\n", UL_ASD_PACKAGE_VERSION);
      exit(0);
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
  }
  
  return 1;
}


int main(int argc, char *argv[])
{
  pthread_t http_thread;  

  main_options(argc,argv);

  //httpd init and load configuration for nullhttpd
  httpd_init(argv[0]);

  //uLan init
  pthread_mutex_init(&ul_net_mutex,NULL);
  ul_root_htimer_init(0, NULL);
  /* FIXME: the check that ul_sn_array has been initialized
     was there in past */
  if(ul_net_new(&ul_net,ul_dev_name)<0) {
    perror("ul_net_new failed");
    return 1;  
  }
  if(ul_net_dy_init(ul_net)<0) {
    perror("ul_net_dy_init failed");
    return 1;  
  }

  //load configuration for uLan
  if (xml_conf_load(ul_asd_conf,ul_net)<0) {
    perror("error reading xml configuration file!\n");
    return 1;  
  }

  //daemonize
//  if (daemon_flg) 
//    daemon(0,0);

  pthread_create(&http_thread, NULL, (void*)&accept_loop, NULL);
  ul_as_loop();
  
  return 0;
}
