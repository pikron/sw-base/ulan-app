/*********************
*  Global variables  *
*********************/

var bBrowserIsMSIE     = false;
var bBrowserIsNetscape = false;
var bBrowserIsOther    = false;
var sButtonClass       = "buttonO"

/***************************
*  Browser type detection  *
***************************/

 if (navigator.appName == "Microsoft Internet Explorer")
 {
  bBrowserIsMSIE     = true;
  bBrowserIsNetscape = false;
  bBrowserIsOther    = false;
 }
 else if (navigator.appName == "Netscape")
 {
  bBrowserIsMSIE     = false;
  bBrowserIsNetscape = true;
  bBrowserIsOther    = false;
 }
 else
 {
  bBrowserIsMSIE     = false;
  bBrowserIsNetscape = false;
  bBrowserIsOther    = true;
 }

 if (bBrowserIsMSIE)
 {
  sButtonClass = "buttonM";
 }

 if (bBrowserIsNetscape)
 {
  sButtonClass = "buttonO";
 }

 if (bBrowserIsOther)
 {
  sButtonClass = "buttonO";
 }

function GoBack()
{
 self.history.back(1);
}

function ResetHistory()
{
 var nLength = self.history.length;
 self.history.length = 0;
}

function LoadURL(HtmlPage)
{
 top.ShowWindow.location.href = HtmlPage;
}

function LoadLeftBarURL(HtmlPage)
{
 if (HtmlPage=='TreeMenu1')
 {
  top.Code.location.href = "treemenu/code1.html";
  top.LeftBar.location.href = "treemenu/menu1.html";
 }
 else if (HtmlPage=='TreeMenu2')
 {
  top.Code.location.href = "treemenu/code2.html";
  top.LeftBar.location.href = "treemenu/menu2.html";
 }
 else if (HtmlPage=='TreeMenu3')
 {
  top.Code.location.href = "treemenu/code3.html";
  top.LeftBar.location.href = "treemenu/menu3.html";
 }
 else if (HtmlPage=='TreeMenuDial')
 {
  top.Code.location.href = "treemenu/code3";
  top.LeftBar.location.href = "treemenu/menu3";
 }
 else
   top.LeftBar.location.href = HtmlPage;
}

function CheckAllButton()
{
 for (var i=0; i<=document.FormQueueList.QueueViewItem.length-1; i++)
 {
  if (document.FormQueueList.QueueViewAllItem.checked)
  {
   document.FormQueueList.QueueViewItem[i].checked=true;
  }
  else
  {
   document.FormQueueList.QueueViewItem[i].checked=false;
  }
 }
}

function SendSubmitFirewall()
{
 document.FormKonfiguraceFirewall.submit();
}

function SendDirectionComboChange()
{
 document.FormFirewallPacketPermission.submit();
}

function CharCount(form)
{
 var memo=form.AntivirAddText.value
 var memolength=memo.length
 if (memolength > 512 ) 
 {
//  alert("Text je p��li� dlouh�! Bude zkr�cen na prvn�ch 1000 znak�!");
  form.AntivirAddText.value=memo.substring(0,512);
  var charleft = "!!!";
 }
 else 
  var charleft = 512 - memolength
}

function SetDefaultExtensions(form)
{
  form.UndeservedExtensions.value=form.TempUndeservedExtensions.value;
}

function LoadURLToLeftBar(HtmlPage)
{
 top.LeftBar.location.href = HtmlPage;
}


function WriteText(Text, Name)
{
 document.write('<span id="' + Name + '">'+Text+'</span>');
}


function DisableText(Name)
{ 
 document.getElementById(Name).style.color='#c0c0c0';
} 

function EnableText(Name)
{ 
 document.getElementById(Name).style.color='#0063A4';
} 

