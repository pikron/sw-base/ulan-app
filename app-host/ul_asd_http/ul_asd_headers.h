#ifndef _UL_ASD_HEADERS_H
#define _UL_ASD_HEADERS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "nullhttpd.h"

#ifdef HAVE_CONFIG_H
  #include "ul_asd_config.h"
#else
  #define CONFIG_UNIX 1
  #define HAVE_ERRNO_H 1
  #define HAVE_GETHOSTBYNAME 1
  #define HAVE_GETOPT_H 1
  #define HAVE_GETOPT_LONG 1
  #define HAVE_LIBNSL 1
  #define HAVE_LIBPTHREAD 1
  #define HAVE_LIBXML2 1
  #define HAVE_LIBXML_PARSER_H 1
  #define HAVE_PTHREAD_H 1
  #define HAVE_STDIO_H 1
  #define HAVE_STDLIB_H 1
  #define HAVE_STRDUP 1
  #define HAVE_STRING_H 1
  #define HAVE_TIME_H 1
  #define HAVE_UNISTD_H 1
  #define PACKAGE_BUGREPORT "petr.smolik@wo.cz"
  #define PACKAGE_NAME "ul_asd"
  #define PACKAGE_STRING "ul_asd 0.0.1"
  #define PACKAGE_TARNAME "ul_asd"
  #define PACKAGE_VERSION "0.0.1"
  #define STDC_HEADERS 1
  #ifndef _GNU_SOURCE
    #define _GNU_SOURCE
  #endif
#endif

#ifdef HAVE_STDIO_H
  #include <stdio.h>
#endif
#ifdef HAVE_STDLIB_H
  #include <stdlib.h>
#endif
#ifdef HAVE_UNISTD_H
  #include <unistd.h>
#endif
#ifdef HAVE_GETOPT_H
  #include <getopt.h>
#endif
#ifdef HAVE_ERRNO_H
  #include <errno.h>
#endif
#ifdef HAVE_TIME_H
  #include <time.h>
#endif
#ifdef HAVE_STRING_H
  #include <string.h>
#endif
#ifdef HAVE_PTHREAD_H
  #include <pthread.h>
#endif
#ifdef HAVE_WINDOWS_H
  #include <windows.h>
#endif
#ifdef HAVE_LIBXML_PARSER_H
  #include <libxml/parser.h>
#endif

#include <ul_netbase.h>
#include <ul_lib/ulan.h>

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_ASD_HEADERS_H */

