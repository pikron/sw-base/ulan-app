/* ul_asd/config.h.  Generated from config.h.in by configure.  */
/* ul_asd/config.h.in.  Generated from configure.in by autoheader.  */

/* Define if is target CYGWIN */
/* #undef CONFIG_CYGWIN */

/* Define if is target MINGW */
/* #undef CONFIG_MINGW */

/* Define if is target a unix system */
#define CONFIG_UNIX 1

/* Define if is target windows */
/* #undef CONFIG_WIN */

/* Define to 1 if you have the <errno.h> header file. */
#define HAVE_ERRNO_H 1

/* Define to 1 if you have the `gethostbyname' function. */
#define HAVE_GETHOSTBYNAME 1

/* Define to 1 if you have the <getopt.h> header file. */
#define HAVE_GETOPT_H 1

/* Define to 1 if you have the `getopt_long' function. */
#define HAVE_GETOPT_LONG 1

/* Define for getopt_long self implemetation */
/* #undef HAVE_GETPOT_LONG */

/* Define to 1 if you have the `c_r' library (-lc_r). */
/* #undef HAVE_LIBC_R */

/* Define to 1 if you have the `nsl' library (-lnsl). */
#define HAVE_LIBNSL 1

/* Define to 1 if you have the `pthread' library (-lpthread). */
#define HAVE_LIBPTHREAD 1

/* Define to 1 if you have the `socket' library (-lsocket). */
/* #undef HAVE_LIBSOCKET */

/* Define to 1 if you have the `ulan' library (-lulan). */
/* #undef HAVE_LIBULAN */

/* Define to 1 if you have the `ul_cintf' library (-lul_cintf). */
/* #undef HAVE_LIBUL_CINTF */

/* Define to 1 if you have the `ws2_32' library (-lws2_32). */
/* #undef HAVE_LIBWS2_32 */

/* Define to 1 if you have the `xml2' library (-lxml2). */
#define HAVE_LIBXML2 1

/* Define to 1 if you have the <libxml/parser.h> header file. */
#define HAVE_LIBXML_PARSER_H 1

/* Define to 1 if you have the <pthread.h> header file. */
#define HAVE_PTHREAD_H 1

/* Define to 1 if you have the <stdio.h> header file. */
#define HAVE_STDIO_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the `strdup' function. */
#define HAVE_STRDUP 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <time.h> header file. */
#define HAVE_TIME_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the <windows.h> header file. */
/* #undef HAVE_WINDOWS_H */

/* Define to the address where bug reports for this package should be sent. */
#define UL_ASD_PACKAGE_BUGREPORT "petr.smolik@wo.cz"

/* Define to the full name of this package. */
#define UL_ASD_PACKAGE_NAME "ul_asd"

/* Define to the full name and version of this package. */
#define UL_ASD_PACKAGE_STRING "ul_asd 0.0.1"

/* Define to the one symbol short name of this package. */
#define UL_ASD_PACKAGE_TARNAME "ul_asd"

/* Define to the version of this package. */
#define UL_ASD_PACKAGE_VERSION "0.0.1"

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define to 1 if your processor stores words with the most significant byte
   first (like Motorola and SPARC, unlike Intel and VAX). */
/* #undef WORDS_BIGENDIAN */


#ifndef _GNU_SOURCE
  #define _GNU_SOURCE
#endif


/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */

/* Define to `__inline__' or `__inline' if that's what the C compiler
   calls it, or to nothing if 'inline' is not supported under any name.  */
#ifndef __cplusplus
/* #undef inline */
#endif
