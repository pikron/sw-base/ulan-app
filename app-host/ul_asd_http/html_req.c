/*******************************************************************
  uLan Communication - address server 

  ul_asd.c	- 

  (C) Copyright 1996-2004 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2004 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2004 Petr Smolik
  

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <stdio.h>
#include <string.h>
#include <ul_netbase.h>
#include <ul_lib/ulan.h>
#include <nullhttpd_fnc_tab.h>

extern ul_net_info_t *ul_net;
extern pthread_mutex_t ul_net_mutex;
extern char *ul_asd_conf;

int xml_conf_save(uchar *filename, ul_net_info_t *ul_net);

int
get_query_param_long(char *query_string,char *param,unsigned long *value) {
  char *begin;
  begin=strstr(query_string,param);
  if(begin) {
    *value=strtol(begin+strlen(param)+1,NULL,0);
    return 0;
  }
  return -1;
}

int 
html_input_action(char *buf,char *query_string) {
  buf[0]=0;
  if (!query_string) return 0;
  if(strstr(query_string,"submitNewItem")) 
    sprintf(buf,"New item");
  if(strstr(query_string,"submitEditItem")) 
    sprintf(buf,"Edit item");
  if(strstr(query_string,"submitDeleteItem")) 
    sprintf(buf,"Delete item");
  return strlen(buf);
}

int 
html_auto_submit_on_delete(char *buf,char *query_string) {
  if(!strstr(query_string,"submitDeleteItem")) return 0;
  sprintf(buf,"<script type=\"text/javascript\">\
window.onload = function(){document.forms[0].submit()}\
</script>\n");
  return strlen(buf);
}

int 
html_restore_form_params(char *buf,char *query_string) {
  char *e,*v;
  int ret=0,len;

  if (!query_string) return 0;
  while(query_string[0]) {
    buf[0]=0;
    e=strchr(query_string,'&');
    if (e) 
      e[0]=0;
    v=strchr(query_string,'=');
    if (v) {
      v[0]=0;
      sprintf(buf,"<INPUT name=\"%s\" type=\"hidden\" value=\"%s\">\n",
        query_string,v+1);
      v[0]='=';
    } else
      sprintf(buf,"<INPUT name=\"%s\" type=\"hidden\">\n",query_string);
    if (e) {
      e[0]='&';
      query_string=e+1;
    } else
      query_string+=strlen(query_string);
    len=strlen(buf);
    ret+=len;buf+=len;    
  }
  return ret;
}

int 
html_perform_staticsni_action(char *buf,char *query_string) {
  unsigned long mod_sn,imod_sn,iadr;
  ul_mod_info_t *mod;

  if (!query_string) return 0;
  if(sscanf(query_string,"sn=%lu",&mod_sn)!=1) return 0;
  pthread_mutex_lock(&ul_net_mutex);
  if(strstr(query_string,"submitNewItem")) {
    get_query_param_long(query_string,"editAddress",&iadr);
    get_query_param_long(query_string,"editSN",&imod_sn);
    if (iadr<1 || iadr>64) goto err_addr;
    if (!(mod=ul_mod_sn_find(imod_sn))) 
      ul_mod_static_add(ul_net,(int)iadr,imod_sn);
  }
  if(strstr(query_string,"submitEditItem")) {
    if ((mod=ul_mod_sn_find(mod_sn))) 
      ul_mod_free(mod);
    get_query_param_long(query_string,"editAddress",&iadr);
    get_query_param_long(query_string,"editSN",&imod_sn);
    if (iadr<1 || iadr>64) goto err_addr;
    if (!(mod=ul_mod_sn_find(imod_sn))) 
      ul_mod_static_add(ul_net,(int)iadr,imod_sn);
  }
  if(strstr(query_string,"submitDeleteItem")) {
    if ((mod=ul_mod_sn_find(mod_sn))) 
      ul_mod_free(mod);
  }
  xml_conf_save(ul_asd_conf,ul_net);
err_addr:
  pthread_mutex_unlock(&ul_net_mutex);
  return 0;
}

int 
html_node_information(char *buf,char *query_string) {
    ul_mod_info_t *mod;
    int mindx,ret=0,len;

    pthread_mutex_lock(&ul_net_mutex);
    for(mindx=0;mindx<ul_net->modules.count;mindx++){
      mod=ul_net->modules.items[mindx];
      if (mod->flg&ULMI_PRESENT) {
        sprintf(buf,"<tr><td class=\"tbi\" nowrap>%d</td>\
<td class=\"tbi\" nowrap>%lu</td><td class=\"tbi\" nowrap>%s</td>\
<td class=\"tbi\" nowrap>%X</td></tr>\n",
		   mod->mod_adr,mod->mod_sn,
		   mod->mod_des ? mod->mod_des:"",mod->flg);
        len=strlen(buf);
        ret+=len;buf+=len;
      }
    }
    pthread_mutex_unlock(&ul_net_mutex);
    return ret;
}


int 
html_get_status_period(char *buf,char *query_string) {
    pthread_mutex_lock(&ul_net_mutex);
    sprintf(buf,"%d",ul_net->dy_state->period);
    pthread_mutex_unlock(&ul_net_mutex);
    return strlen(buf);
}

int 
html_get_cycle_start(char *buf,char *query_string) {
    pthread_mutex_lock(&ul_net_mutex);
    sprintf(buf,"%d",ul_dy_cycle_subfn);
    pthread_mutex_unlock(&ul_net_mutex);    
    return strlen(buf);
}

int 
html_get_first_dy_adr(char *buf,char *query_string) {
    pthread_mutex_lock(&ul_net_mutex);
    sprintf(buf,"%d",ul_net->dy_state->first_dy_adr);
    pthread_mutex_unlock(&ul_net_mutex);
    return strlen(buf);
}


int 
html_get_staticsn(char *buf,char *query_string) {
    ul_mod_info_t *mod;
    int mindx,ret=0,len;

    pthread_mutex_lock(&ul_net_mutex);
    for(mindx=0;mindx<ul_net->modules.count;mindx++){
      mod=ul_net->modules.items[mindx];
      if (mod->flg&ULMI_STATIC) {
        sprintf(buf,"<TR><TD class=\"tbh\"><INPUT name=\"sn\"\
type=\"radio\" class=\"tbh\" value=\"%li\" %s></TD><TD class=\"tbi\" nowrap>%i</TD>\
<TD class=\"tbi\" nowrap>0x%lx</TD></TR>",
            mod->mod_sn,!mindx ? "checked":"",
	    mod->mod_adr,mod->mod_sn);
        len=strlen(buf);
        ret+=len;buf+=len;
      }
    }
    pthread_mutex_unlock(&ul_net_mutex);    
    return ret;
}

int 
html_get_staticsn_address(char *buf,char *query_string) {
  unsigned long mod_sn;
  ul_mod_info_t *mod;
  int ret=0;
  
  if(strstr(query_string,"submitNewItem")) return 0;
  if(sscanf(query_string,"sn=%lu",&mod_sn)!=1) return 0;
  pthread_mutex_lock(&ul_net_mutex);
  if ((mod=ul_mod_sn_find(mod_sn))) {
    sprintf(buf,"%d",mod->mod_adr);
    ret=strlen(buf);
  }
  pthread_mutex_unlock(&ul_net_mutex);
  return ret;
}

int 
html_get_staticsn_sn(char *buf,char *query_string) {
  unsigned long mod_sn;
  ul_mod_info_t *mod;
  int ret=0;
  
  if(strstr(query_string,"submitNewItem")) return 0;
  if(sscanf(query_string,"sn=%lu",&mod_sn)!=1) return 0;
  pthread_mutex_lock(&ul_net_mutex);
  if ((mod=ul_mod_sn_find(mod_sn))) {
    sprintf(buf,"0x%lx",mod->mod_sn);
    ret=strlen(buf);
  }
  pthread_mutex_unlock(&ul_net_mutex);
  return ret;
}

int
html_server_statistics(char *buf,char *query_string) {
    char time_string[40];
    struct tm* ptm;
    int ret=0,len;

    pthread_mutex_lock(&ul_net_mutex);
    //start time    
    ptm = localtime (&ul_net->statistics->start_time);
    strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);
    sprintf(buf,"<tr><TD align=\"left\" nowrap>Start time</td>\
<td align=\"left\" nowrap></td><TD align=\"left\" nowrap>%s</td></tr>",time_string);
    len=strlen(buf);
    ret+=len;buf+=len;
    //dynamic address assigment
    sprintf(buf,"<tr><TD align=\"left\" nowrap>Dynamic address assigment</td>\
<td align=\"left\" nowrap>Count</td><TD align=\"left\" nowrap>%d</td></tr>\
",ul_net->statistics->dy_adr_asg_cnt);
    len=strlen(buf);
    ret+=len;buf+=len;
    if (ul_net->statistics->dy_adr_asg_cnt) {
      ptm = localtime (&ul_net->statistics->dy_adr_asg_last);
      strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);
      sprintf(buf,"<tr><TD align=\"left\" nowrap></td>\
<td align=\"left\" nowrap>Last</td><TD align=\"left\" nowrap>%s</td></tr>",time_string);
      len=strlen(buf);
      ret+=len;buf+=len;
    }
    len=strlen(buf);
    ret+=len;buf+=len;
    //static address request
    sprintf(buf,"<tr><TD align=\"left\" nowrap>Statics address request</td>\
<td align=\"left\" nowrap>Count</td><TD align=\"left\" nowrap>%d</td></tr>\
",ul_net->statistics->st_adr_req_cnt);
    len=strlen(buf);
    ret+=len;buf+=len;
    if (ul_net->statistics->st_adr_req_cnt) {
      ptm = localtime (&ul_net->statistics->st_adr_req_last);
      strftime (time_string, sizeof (time_string), "%Y-%m-%d %H:%M:%S", ptm);
      sprintf(buf,"<tr><TD align=\"left\" nowrap></td>\
<td align=\"left\" nowrap>Last</td><TD align=\"left\" nowrap>%s</td></tr>",time_string);
      len=strlen(buf);
      ret+=len;buf+=len;
    }
    pthread_mutex_unlock(&ul_net_mutex);
    return ret;
}

int 
html_save_server_configuration(char *buf,char *query_string) {
  int p,c,a;
  
  if (!query_string) return 0;
  if(sscanf(query_string,"status_period=%d&cycle_start=%d&first_dynamic_address=%d",&p,&c,&a)==3) {
    pthread_mutex_lock(&ul_net_mutex);
    ul_net->dy_state->period=p;
    ul_net->dy_state->first_dy_adr=a;
    ul_dy_cycle_subfn=c;
    xml_conf_save(ul_asd_conf,ul_net);
    pthread_mutex_unlock(&ul_net_mutex);
  }
  return 0;
}

static struct fnc_tab_s fnc_tab[] = {
    { "INPUT_ACTION", html_input_action},
    { "AUTO_SUBMIT_ON_DELETE", html_auto_submit_on_delete},
    { "RESTORE_FORM_PARAMS", html_restore_form_params},
    { "PERFORM_STATICSNI_ACTION", html_perform_staticsni_action},
    { "NODE_INFORMATIONS", html_node_information },
    { "GET_STATUS_PERIOD", html_get_status_period },
    { "GET_CYCLE_START", html_get_cycle_start },
    { "GET_FIRST_DY_ADR", html_get_first_dy_adr },
    { "GET_STATICSN", html_get_staticsn },
    { "GET_STATICSN_ADDRESS",html_get_staticsn_address },
    { "GET_STATICSN_SN",html_get_staticsn_sn },
    { "SERVER_STATISTICS", html_server_statistics },
    { "SAVE_SERVER_CONFIGURATION", html_save_server_configuration},
    { 0, 0}
  };

struct fnc_tab_s *nullhttpd_fnc_tab = fnc_tab;