#include "ul_curl2dbuff.h"

CURLcode ul_curl_set_url_from_dbuff(CURL *curl, ul_dbuff_t *dbuf)
{
  unsigned long len = dbuf->len;
  if((len==0) || (dbuf->data[len-1]!=0)) {
    if(ul_dbuff_set_capacity(dbuf, len+1) != len+1)
      return CURLE_OUT_OF_MEMORY;
    dbuf->data[len] = 0;
  }
  return curl_easy_setopt(curl, CURLOPT_URL, dbuf->data);
}

static size_t ul_curl_write_data_to_dbuff(void *ptr, size_t size, size_t nmemb, void *writedata)
{
  ul_dbuff_t *dbuf = (ul_dbuff_t *)writedata;
  size_t bytes = size * nmemb;
  size_t old_len = dbuf->len;

  return ul_dbuff_cat(dbuf, ptr, bytes) - old_len;
}

#ifndef CURLOPT_WRITEDATA
#define CURLOPT_WRITEDATA CURLOPT_FILE
#endif

CURLcode ul_curl_set_write_to_dbuff(CURL *curl, ul_dbuff_t *dbuf)
{
  CURLcode res;
  res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, ul_curl_write_data_to_dbuff);
  if(res != CURLE_OK)
    return res;
  return curl_easy_setopt(curl, CURLOPT_WRITEDATA, dbuf);
}

CURLcode ul_curl_perform_dbuff(ul_dbuff_t *dbuf_out, ul_dbuff_t *dbuf_url, int options)
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if(curl == NULL)
    return CURLE_OUT_OF_MEMORY;

  res = ul_curl_set_url_from_dbuff(curl, dbuf_url);
  if(res != CURLE_OK)
    goto curl_error;

  res = ul_curl_set_write_to_dbuff(curl, dbuf_out);
  if(res != CURLE_OK)
    goto curl_error;

#ifdef SKIP_PEER_VERIFICATION
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
#endif

#ifdef SKIP_HOSTNAME_VERFICATION
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
#endif

  if(options & UL_CURLOPT_NOCACHE) {
    struct curl_slist *chunk = NULL;

    chunk = curl_slist_append(chunk, "Pragma: no-cache");
    chunk = curl_slist_append(chunk, "Cache-Control: no-cache");

    res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
  }

  res = curl_easy_perform(curl);

  do {
    if(dbuf_out->len)
      if(dbuf_out->data[dbuf_out->len-1]==0)
        break;
    if(dbuf_out->capacity > dbuf_out->len) {
      if(dbuf_out->data[dbuf_out->len]==0)
        break;
      else
        dbuf_out->data[dbuf_out->len]=0;
    } else {
      size_t len = dbuf_out->len;
      if(ul_dbuff_set_capacity(dbuf_out, len+1) != len+1)
        res=CURLE_OUT_OF_MEMORY;
      else
        dbuf_out->data[len] = 0;
    }
  } while(0);

curl_error:
  curl_easy_cleanup(curl);
  return res;
}
