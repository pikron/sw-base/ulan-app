#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ul_dbuff.h>
#include <ul_dbuffprintf.h>

#include "local_config.h"

#include "ul_dysndefs.h"
#include "ul_curl2dbuff.h"

int ul_dysn_extsimpledb_features(struct ul_dysn_state_t *dysnst)
{
  return UL_DYSN_FEA_RIGID_PRERALLOC;
}

int ul_dysn_dbuff_apped_param(ul_dbuff_t *dbuf, const char *arg_name, const char *arg_val)
{
  if(!arg_name || !arg_val)
    return 0;
  ul_dbuff_append_byte(dbuf, '&');
  ul_dbuff_cat(dbuf, arg_name, strlen(arg_name));
  ul_dbuff_append_byte(dbuf, '=');
  ul_dbuff_cat(dbuf, arg_val, strlen(arg_val));
  return 0;
}

int ul_dysn_extsimpledb_db_url_and_cmd(struct ul_dysn_state_t *dysnst, ul_dbuff_t *dbuf, const char *command)
{
  if(dbuf->len)
    ul_dbuff_set_len(dbuf, 0);

  if(dysnst->database_url!=NULL) {
    ul_dbuff_cat(dbuf, dysnst->database_url, strlen(dysnst->database_url));
  } else {
    ul_dbuff_cat(dbuf, "http://", strlen("http://"));
    ul_dbuff_cat(dbuf, "ulan.sourceforge.net", strlen("ulan.sourceforge.net"));
    ul_dbuff_cat(dbuf, "/sn_reg/", strlen("/sn_reg/"));
  }
  if(dbuf->data[dbuf->len-1]!='/')
    ul_dbuff_append_byte(dbuf, '/');

  ul_dbuff_cat(dbuf, command, strlen(command));

  ul_dbuff_cat(dbuf, "?name=", strlen("?name="));
  ul_dbuff_cat(dbuf, dysnst->vendor_name, strlen(dysnst->vendor_name));
  ul_dbuff_cat(dbuf, "&pass=", strlen("&pass="));
  ul_dbuff_cat(dbuf, dysnst->vendor_password, strlen(dysnst->vendor_password));

  return 0;
}

int ul_dysn_extsimpledb_sn_prealloc(struct ul_dysn_state_t *dysnst, unsigned long *sn, const char *mod_type)
{
  int res = -1;
  ul_dbuff_t dbuf_url, dbuf_res;
  char *p, *r;
  int i;

  ul_dbuff_init(&dbuf_url, 0);
  ul_dbuff_init(&dbuf_res, 0);
  ul_dysn_extsimpledb_db_url_and_cmd(dysnst, &dbuf_url, "sn_prealloc.php");

  if(mod_type && *mod_type) {
    ul_dysn_dbuff_apped_param(&dbuf_url, "module_name", mod_type);
  }

  if(ul_curl_perform_dbuff(&dbuf_res, &dbuf_url, UL_CURLOPT_NOCACHE) != CURLE_OK) {
    printf("CURL request = \"%s\"\n", dbuf_url.data);
    printf("CURL transfer failed\n");
    goto curl_error;
  }

  p = (char*)dbuf_res.data;
  i = strlen("Done,ret=0,sn=");
  if((dbuf_res.len<i)||memcmp(p, "Done,ret=0,sn=", i)) {
    printf("Request for new SN preallocation failed\n");
    printf("  CURL request = \"%s\"\n", dbuf_url.data);
    printf("  CURL result = \"%s\"\n", dbuf_res.data);
    goto curl_error;
  }
  p+=i;
  *sn=strtoul(p,&r, 10);
  if(r>p)
    res = 0;

curl_error:
  ul_dbuff_destroy(&dbuf_url);
  ul_dbuff_destroy(&dbuf_res);

  return res;
}

int ul_dysn_extsimpledb_sn_check(struct ul_dysn_state_t *dysnst, unsigned long sn)
{
  return 0;
}

int ul_dysn_extsimpledb_sn_confirm(struct ul_dysn_state_t *dysnst, ul_dysn_module_t *mod)
{
  int res = -1;
  ul_dbuff_t dbuf_url, dbuf_res;
  char *p;
  int i;
  char s[20];

  ul_dbuff_init(&dbuf_url, 0);
  ul_dbuff_init(&dbuf_res, 0);
  ul_dysn_extsimpledb_db_url_and_cmd(dysnst, &dbuf_url, "sn_confirm.php");

  snprintf(s, sizeof(s), "%lu", mod->sn);
  ul_dysn_dbuff_apped_param(&dbuf_url, "sn", s);

  if(mod->mod_type && *mod->mod_type) {
    ul_dysn_dbuff_apped_param(&dbuf_url, "mt", mod->mod_type);
  }
  ul_dysn_dbuff_apped_param(&dbuf_url, "flash_time", mod->flash_time);
  ul_dysn_dbuff_apped_param(&dbuf_url, "version_application", mod->ver_appl);
  ul_dysn_dbuff_apped_param(&dbuf_url, "version_bootloader", mod->ver_boot);
  ul_dysn_dbuff_apped_param(&dbuf_url, "git_identification", mod->git_id);

  if(ul_curl_perform_dbuff(&dbuf_res, &dbuf_url, UL_CURLOPT_NOCACHE) != CURLE_OK) {
    printf("CURL request = \"%s\"\n", dbuf_url.data);
    printf("CURL transfer failed\n");
    goto curl_error;
  }

  p = (char*)dbuf_res.data;
  i = strlen("Done,ret=0");
  if((dbuf_res.len<i)||memcmp(p, "Done,ret=0", i)) {
    printf("Request for SN confirmation failed\n");
    printf("  CURL request = \"%s\"\n", dbuf_url.data);
    printf("  CURL result = \"%s\"\n", dbuf_res.data);
    goto curl_error;
  }
  res = 0;

curl_error:
  ul_dbuff_destroy(&dbuf_url);
  ul_dbuff_destroy(&dbuf_res);

  return res;
}

int ul_dysn_extsimpledb_sn_modify_new_application(struct ul_dysn_state_t *dysnst, ul_dysn_module_t *mod)
{
  return -1;
}

int ul_dysn_extsimpledb_sn_modify_dead(struct ul_dysn_state_t *dysnst, unsigned long sn)
{
  return -1;
}

ul_dysn_ops_t ul_dysn_extsimpledb_ops = {
  .features = ul_dysn_extsimpledb_features,
  .sn_prealloc = ul_dysn_extsimpledb_sn_prealloc,
  .sn_check = ul_dysn_extsimpledb_sn_check,
  .sn_confirm = ul_dysn_extsimpledb_sn_confirm,
  .sn_modify_new_application = ul_dysn_extsimpledb_sn_modify_new_application,
  .sn_modify_dead = ul_dysn_extsimpledb_sn_modify_dead,
};
