#ifndef _UL_RDWRMEM_H
#define _UL_RDWRMEM_H

#include <ul_lib/ulan.h>

#ifdef __cplusplus
extern "C" {
#endif

int ul_new_memrq_head(ul_fd_t ul_fd, int dadr, int cmd, 
                      int mtype, int start, int len);

int ul_new_memrq_read(ul_fd_t ul_fd, int dadr,
                      int mtype, int start, int len);

int ul_new_memrq_write(ul_fd_t ul_fd, int dadr,
                       int mtype, int start, int len, void *buf);

int ul_mem_read_wait(ul_fd_t ul_fd, int dadr,
                     int mtype, int start, int len, void *ptr);


#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_RDWRMEM_H */
