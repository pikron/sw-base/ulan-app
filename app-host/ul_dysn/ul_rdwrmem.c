#include <ul_lib/ulan.h>

#include "local_config.h"

#ifdef CONFIG_OC_ULUT
#include <ul_log.h>
UL_LOG_CUST(ulogd_rdwrmem)
#else
#include <ul_lib/ul_l_log.h>
#define UL_LDOMAIN NULL
#endif

int ul_new_memrq_head(ul_fd_t ul_fd, int dadr, int cmd, 
                      int mtype, int start, int len)
{
  int ret;
  int i;
  ul_msginfo msginfo;
  uchar buf[8];
  msginfo.dadr=dadr;
  msginfo.cmd=cmd;
  msginfo.flg=UL_BFL_ARQ|UL_BFL_PRQ|UL_BFL_M2IN;
  ret=ul_newmsg(ul_fd,&msginfo);
  if(ret<0) return ret;
  i=0;
  buf[i++]=(uchar)mtype; buf[i++]=mtype>>8;
  buf[i++]=(uchar)start; buf[i++]=start>>8;
  if(mtype&0x100)
    {buf[i++]=start>>16; buf[i++]=start>>24;};
  buf[i++]=(uchar)len;   buf[i++]=len>>8;
  if(ul_write(ul_fd,buf,i)!=i)
  { ul_abortmsg(ul_fd);
    ret=-1;
  };
  return ret;
};

int ul_new_memrq_read(ul_fd_t ul_fd, int dadr,
                      int mtype, int start, int len)
{
  int ret;
  ul_msginfo msginfo;
  ret=ul_new_memrq_head(ul_fd,dadr,UL_CMD_RDM,mtype,start,len);
  if (ret<0) return ret;
  msginfo.dadr=dadr;
  msginfo.cmd=UL_CMD_RDM&0x7F;
  msginfo.flg=UL_BFL_REC|UL_BFL_LNMM|UL_BFL_M2IN;
  msginfo.len=len;
  ret=ul_tailmsg(ul_fd,&msginfo);
  if (ret<0) {ul_abortmsg(ul_fd);return ret;};
  ret=ul_freemsg(ul_fd);
  return ret;
};

int ul_new_memrq_write(ul_fd_t ul_fd, int dadr,
                       int mtype, int start, int len, void *buf)
{
  int ret;
  ul_msginfo msginfo;
  ret=ul_new_memrq_head(ul_fd,dadr,UL_CMD_WRM,mtype,start,len);
  if(ret<0) return ret;
  msginfo.dadr=dadr;
  msginfo.cmd=UL_CMD_WRM&0x7F;
  msginfo.flg=UL_BFL_SND|UL_BFL_LNMM|UL_BFL_ARQ|UL_BFL_M2IN;
  msginfo.len=len;
  ret=ul_tailmsg(ul_fd,&msginfo);
  if(ret<0) {ul_abortmsg(ul_fd);return ret;};
  if(buf)
  { ret=ul_write(ul_fd,buf,len);
    if(ret<0) {ul_abortmsg(ul_fd);return ret;};
    ret=ul_freemsg(ul_fd);
  };
  return ret;
};

int ul_mem_read_wait(ul_fd_t ul_fd, int dadr,
                     int mtype, int start, int len, void *ptr)
{
  int ret, stamp;
  ul_msginfo msginfo;

  stamp=ret=ul_new_memrq_read(ul_fd,dadr,mtype,start,len);
  if(ret<0) return ret;

  /* ioctl(ul_fd,UL_KLOGBLL); */

  /* ioctl(ul_fd,UL_STROKE); */

  do {
    ul_freemsg(ul_fd);
    ret=ul_fd_wait(ul_fd,20);
    if(ret<0) {ul_logerr("Select returned %d\n",ret);return ret;};
    if(!ret) {ul_logerr("Select - timeout\n");return -1;};
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0)
    { ul_logerr("Accept msg returned %d\n",ret);
      return ret;
    };
  } while(msginfo.stamp!=stamp);
  if(msginfo.flg&UL_BFL_FAIL) 
  { ul_logerr("Failed msg flg=0x%X\n",msginfo.flg);
    ul_freemsg(ul_fd);return -1;
  };

  ret=ul_actailmsg(ul_fd,&msginfo);
  if(ret<0) 
  { ul_logerr("Accept tail returned %d\n",ret);
    ul_freemsg(ul_fd);
    return ret;
  };
  if((ret=ul_read(ul_fd,ptr,len))!=len)
  { ul_logerr("Bad read len %d, rq %d, msg %d\n",ret,len,msginfo.len);
    ul_freemsg(ul_fd);return -1;
  };

  ul_freemsg(ul_fd);
  return len;
};
