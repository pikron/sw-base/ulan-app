#ifndef _UL_CURL2DBUFF_H
#define _UL_CURL2DBUFF_H

#include <stdio.h>
#include <ul_dbuff.h>
#include <curl/curl.h>

#define UL_CURLOPT_NOCACHE 1

CURLcode ul_curl_set_url_from_dbuff(CURL *curl, ul_dbuff_t *dbuf);
CURLcode ul_curl_set_write_to_dbuff(CURL *curl, ul_dbuff_t *dbuf);
CURLcode ul_curl_perform_dbuff(ul_dbuff_t *dbuf_out, ul_dbuff_t *dbuf_url, int options);

#endif /*_UL_CURL2DBUFF_H*/
