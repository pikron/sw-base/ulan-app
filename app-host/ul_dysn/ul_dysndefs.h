#ifndef _UL_DYSNDEFS_H
#define _UL_DYSNDEFS_H

struct ul_dysn_state_t;

typedef struct ul_dysn_module_t {
  unsigned long sn;
  char *mod_des;	/* whole module descriptor obtained by ULNCS_SID_RQ service */
  char *mod_type;	/* .mt module type*/
  char *ver_appl;
  char *ver_boot;
  char *git_id;
  char *flash_time;
} ul_dysn_module_t;

#define UL_DYSN_FEA_RIGID_PRERALLOC 1

typedef struct ul_dysn_ops_t {
  int (*features)(struct ul_dysn_state_t *dysnst);
  int (*sn_prealloc)(struct ul_dysn_state_t *dysnst, unsigned long *sn, const char *mod_type);
  int (*sn_check)(struct ul_dysn_state_t *dysnst, unsigned long sn);
  int (*sn_confirm)(struct ul_dysn_state_t *dysnst, ul_dysn_module_t *mod);
  int (*sn_modify_new_application)(struct ul_dysn_state_t *dysnst, ul_dysn_module_t *mod);
  int (*sn_modify_dead)(struct ul_dysn_state_t *dysnst, unsigned long sn);
} ul_dysn_ops_t;

typedef struct ul_dysn_state_t {
  ul_dysn_ops_t *ops;
  char *vendor_name;
  char *vendor_password;
  char *database_url;
} ul_dysn_state_t;

typedef int (ul_dysn_setsn_t)(ul_dysn_module_t *mod, int module, unsigned long new_sn);

#endif /*_DYSNDEFS_H*/
