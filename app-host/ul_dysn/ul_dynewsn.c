/*******************************************************************
  uLan Communication - simple test client

  ul_lcscan.c	- procceed wavelength scanning with LCD5000

  (C) Copyright 1996,1999 by Pavel Pisa 

  The uLan driver is distributed under the Gnu General Public Licence. 
  See file COPYING for details.
 *******************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <ul_lib/ulan.h>

#include "ul_dysndefs.h"

extern char *ul_dev_name;

int ul_dysn_setsn_default(ul_dysn_module_t *mod, int module, unsigned long new_sn)
{ 
  int ret;
  ul_fd_t ul_fd;
  uchar buf_out[10];
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("set_new_sn : uLan open failed");return -1;};
  buf_out[0]=ULNCS_SET_SN;	/* SN0 SN1 SN2 SN3 */
  buf_out[1]=new_sn>>0;
  buf_out[2]=new_sn>>8;
  buf_out[3]=new_sn>>16;
  buf_out[4]=new_sn>>24;
  ret=ul_send_command(ul_fd,module,UL_CMD_NCS,
  		      module?UL_BFL_ARQ:0,buf_out,5);
  ul_close(ul_fd);
  if(ret==0)
    ret=1;
  return ret;
}

