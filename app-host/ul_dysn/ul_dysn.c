/*******************************************************************
  uLan Object Communication

  ul_dysn.c	- uLAN modules serial number examination and assignment

  (C) Copyright 1996-2012 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2012 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2009 Petr Smolik

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#define _GNU_SOURCE

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <ul_lib/ulan.h>

#include "local_config.h"

#include "ul_dysndefs.h"

#ifdef CONFIG_APP_UL_DYSN_WITH_LIBCURL
extern ul_dysn_ops_t ul_dysn_extsimpledb_ops;
#endif /*CONFIG_APP_UL_DYSN_WITH_LIBCURL*/

extern ul_dysn_setsn_t ul_dysn_setsn_default;

#ifdef CONFIG_APP_UL_DYSN_SETSN_METHODS
extern ul_dysn_setsn_t CONFIG_APP_UL_DYSN_SETSN_METHODS;
#endif /*CONFIG_APP_UL_DYSN_SETSN_METHODS*/

ul_dysn_setsn_t *ul_dysn_setsn_methods[] = {
 #ifdef CONFIG_APP_UL_DYSN_SETSN_METHODS
  CONFIG_APP_UL_DYSN_SETSN_METHODS,
 #endif /*CONFIG_APP_UL_DYSN_SETSN_METHODS*/
  ul_dysn_setsn_default,
  NULL
};

#if !defined( __GNUC__)||defined(DJGPP)||defined(NO_GET_DELIM)||defined (__MINGW32__)
int getdelim(char **line,size_t *linelen,char delim,FILE *F)
{
 char c;
 int  l=0;
 do{
  if(l+2>=*linelen)
  {
   *linelen=l+20;
   if(!*line) *line=(char *)malloc(*linelen);
    else *line=(char *)realloc(*line,*linelen);
  }
  c=fgetc(F);
  if(feof(F)) {if(l) break; else return -1;}
  if(c!='\r') (*line)[l++]=c;
 } while(c!=delim);
 (*line)[l]=0;
 return l;
};
#endif

/*******************************************************************/

char *sn_hist_fname="ulan_sn.his";

int check_sn_in_sn_hist(unsigned long new_sn, unsigned long *pmax_sn)
{ 
  int    ret=1;
  char   *line=NULL;
  size_t linelen=0;
  unsigned long old_sn, max_sn=0;
  FILE   *F;

  if((F=fopen(sn_hist_fname,"r"))==NULL){
    return errno==ENOENT?0:-3;
  }
  while(getdelim(&line,&linelen,'\n',F)>=0){
    if(sscanf(line,"%lX",&old_sn)>0) {
      if(old_sn>max_sn) max_sn=old_sn;
      if(new_sn&&(old_sn==new_sn)) ret=-1;
    }
  }
  if(line) free(line);
  if(pmax_sn) *pmax_sn=max_sn;
  return ret;
}


int ul_dysn_hist_file_features(struct ul_dysn_state_t *dysnst)
{
  return 0;
}

int ul_dysn_hist_file_sn_prealloc(struct ul_dysn_state_t *dysnst, unsigned long *sn, const char *mod_type)
{
  int res;
  unsigned long new_sn = 0;
  res = check_sn_in_sn_hist(0, &new_sn);
  if(res<0)
    return res;
  *sn = new_sn+1;
  return 0;
}

int ul_dysn_hist_file_sn_check(struct ul_dysn_state_t *dysnst, unsigned long sn)
{
  return check_sn_in_sn_hist(sn, NULL);
}

int ul_dysn_hist_file_sn_confirm(struct ul_dysn_state_t *dysnst, ul_dysn_module_t *mod)
{
  FILE   *F;

  if((F=fopen(sn_hist_fname,"a"))==NULL)
  {
    printf("Cannot open uLan SN history file\n");
    return -1;
  }
  fprintf(F,"%08lX\t%s\t%s\n",mod->sn,mod->flash_time,mod->mod_des);
  fclose(F);

  return 0;
}

int ul_dysn_hist_file_sn_modify_new_application(struct ul_dysn_state_t *dysnst, ul_dysn_module_t *mod)
{
  return -1;
}

int ul_dysn_hist_file_sn_modify_dead(struct ul_dysn_state_t *dysnst, unsigned long sn)
{
  return -1;
}

ul_dysn_ops_t ul_dysn_hist_file_ops = {
  .features = ul_dysn_hist_file_features,
  .sn_prealloc = ul_dysn_hist_file_sn_prealloc,
  .sn_check = ul_dysn_hist_file_sn_check,
  .sn_confirm = ul_dysn_hist_file_sn_confirm,
  .sn_modify_new_application = ul_dysn_hist_file_sn_modify_new_application,
  .sn_modify_dead = ul_dysn_hist_file_sn_modify_dead,
};

/*******************************************************************/

void ul_dysn_module_clear(ul_dysn_module_t *mod)
{
  if(mod->mod_des) free(mod->mod_des);
  if(mod->mod_type) free(mod->mod_type);
  if(mod->ver_appl) free(mod->ver_appl);
  if(mod->ver_boot) free(mod->ver_boot);
  if(mod->git_id) free(mod->git_id);
  if(mod->flash_time) free(mod->flash_time);
  mod->sn = 0;
  mod->mod_des = NULL;
  mod->mod_type = NULL;
  mod->ver_appl = NULL;
  mod->ver_boot = NULL;
  mod->git_id = NULL;
  mod->flash_time = NULL;
}

int ul_dysn_module_fill_fields(ul_dysn_module_t *mod)
{
  time_t sn_time;
  struct tm *sn_tm;
  char s[30];
  char *p;
  int i;

  if(mod->flash_time==NULL) {
    time(&sn_time);
    sn_tm=localtime(&sn_time);
    snprintf(s, sizeof(s),"%04d-%02d-%02d", (int)sn_tm->tm_year+1900,
             (int)sn_tm->tm_mon+1, sn_tm->tm_mday);
    mod->flash_time = strdup(s);
    if(mod->flash_time == NULL)
      return -1;
  }

  do {
    if(mod->mod_des==NULL)
      break;
    p=strstr(mod->mod_des,".mt ");
    if(!p)
      break;
    p+=4;
    for(i=0;p[i]>' ';i++);
    if(mod->mod_type==NULL) {
      mod->mod_type=malloc(i+1);
      if(mod->mod_type==NULL)
        return -1;
      memcpy(mod->mod_type, p, i);
      mod->mod_type[i]=0;
    }
    p+=i;
    if(*p!=' ')
      break;
    p++;
    if((*p=='v')||(*p=='V'))
      p++;
    if((*p<'0')||(*p>'9'))
      break;
    for(i=0;p[i]>' ';i++);
    if(mod->ver_appl==NULL) {
      mod->ver_appl=malloc(i+1);
      if(mod->ver_appl==NULL)
        return -1;
      memcpy(mod->ver_appl, p, i);
      mod->ver_appl[i]=0;
    }
  } while(0);

  if(mod->mod_type==NULL)
    mod->mod_type=strdup("unknown");
  if(mod->mod_des==NULL)
    mod->mod_des=strdup("unknown");
  if(mod->ver_appl==NULL)
    mod->ver_appl=strdup("0");
  if(mod->ver_boot==NULL)
    mod->ver_boot=strdup("0");
  if(mod->git_id==NULL)
    mod->git_id=strdup("0");

  return 0;
}

int ul_dysn_module_setsn(ul_dysn_module_t *mod, int module, unsigned long new_sn)
{
  int metidx;
  ul_dysn_setsn_t *setsn;
  int res;

  for(metidx=0; (setsn=ul_dysn_setsn_methods[metidx]) != NULL; metidx++) {
    res = setsn(mod, module, new_sn);
    if(res>=1)
      return res;
    if(res!=0)
      return res;
  }

  printf("No method to set SN suitable for module found\n");
  return -4;
}

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int module= 0;
int uloi_con_outflg;
int uloi_con_outflg_set_flg = 0;
int prt_modules= 0;
int prt_modules_dyn= 0;
int debugk     = 0;
int debugk_flg = 0;
int new_sn_flg = 0;
unsigned long new_sn;
int no_permanent_flg=0;
int new_adr_flg = 0;
int new_adr = 0;
int new_adrnvsv_flg = 0;
int interactive_flg = 0;
char *vendor_name = NULL;
char *vendor_password = NULL;

ul_dysn_module_t ul_dysn_module;

ul_dysn_state_t ul_dysn_state = {
  .ops = &ul_dysn_hist_file_ops,
  .vendor_name = "unknown",
  .vendor_password = "",
};

int print_modules(int max_addr)
{ int ret;
  int adr;
  ul_fd_t ul_fd;
  uchar *buf=NULL;
  int buf_len;
  int count=0; 
  uchar buf_out[10];
  ul_msginfo msginfo;
  int ul_ncs_filter_on=0;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("print_modules : uLan open failed");return -1;};
  for(adr=1;adr<=max_addr;adr++)
  {
    ret=ul_send_query_wait(ul_fd,adr,UL_CMD_SID,
  			   UL_BFL_NORE|UL_BFL_PRQ,NULL,0,(void**)&buf,&buf_len);
    if(ret>=0)
    { count++;
      for(ret=0;(ret<buf_len)&&buf[ret];ret++);
      printf("%2d:  ",adr);
      fwrite(buf,ret,1,stdout);
      printf("\n");
      free(buf);
      buf=NULL;

      /* NCS information request */
      if(!ul_ncs_filter_on){
        memset(&msginfo,0,sizeof(ul_msginfo));
        msginfo.cmd=UL_CMD_NCS;
        ul_addfilt(ul_fd,&msginfo);
        ul_ncs_filter_on=1;
      }
      buf_out[0]=ULNCS_SID_RQ;
      ret=ul_send_command(ul_fd,adr,UL_CMD_NCS,
  		          UL_BFL_NORE|UL_BFL_PRQ,buf_out,1);
      while(1)
      { ret=ul_fd_wait(ul_fd,10);
        if(ret<0) return -1;
        if(ret==0)
        { printf("cannot retrieve SN info for %d\n",adr);
          break;
        }
        ret=ul_acceptmsg(ul_fd,&msginfo);
        if(ret<0) return ret;
        if((msginfo.cmd!=UL_CMD_NCS)
          ||(msginfo.sadr!=adr)
          ||(msginfo.len<3))
        { ul_freemsg(ul_fd);
          continue;
        }
        buf=malloc(msginfo.len);
        ret=ul_read(ul_fd,buf,msginfo.len);
        ul_freemsg(ul_fd);
        if(buf[0]==ULNCS_SID_RPLY)
        { if(msginfo.len>=5)
          { 
	    printf("%2d %8lX:  ",msginfo.sadr,(buf[1])+((long)buf[2]<<8)+
	    		         ((long)buf[3]<<16)+((long)buf[4]<<24));
      	    fwrite(buf+5,msginfo.len-5,1,stdout);
      	    printf("\n");
            free(buf);
            buf=NULL;
            break;
          }
        }
        free(buf);
        buf=NULL;
      }

      /*  */

    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int print_modules_dyn(void)
{ int ret;
  ul_fd_t ul_fd;
  uchar *buf=NULL;
  int count=0; 
  uchar buf_out[10];
  ul_msginfo msginfo;
  int ul_ncs_filter_on=0;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("print_modules_dyn : uLan open failed");return -1;};

  if(!ul_ncs_filter_on){
    memset(&msginfo,0,sizeof(ul_msginfo));
    msginfo.cmd=UL_CMD_NCS;
    ul_addfilt(ul_fd,&msginfo);
    ul_ncs_filter_on=1;
  }
  buf_out[0]=ULNCS_SID_RQ;
  ret=ul_send_command(ul_fd,0,UL_CMD_NCS,
  		      UL_BFL_NORE*0,buf_out,1);
  while(1)
  { ret=ul_fd_wait(ul_fd,3);
    if(ret<0) return -1;
    if(ret==0)
    { printf("no more SN info\n");
      break;
    }
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) return ret;
    if((msginfo.cmd!=UL_CMD_NCS)
       ||(msginfo.len<3))
    { ul_freemsg(ul_fd);
      continue;
    }
    buf=malloc(msginfo.len);
    ret=ul_read(ul_fd,buf,msginfo.len);
    ul_freemsg(ul_fd);
    if(buf[0]==ULNCS_SID_RPLY)
    { if(msginfo.len>=5)
      { 
	printf("%2d %08lX:  ",msginfo.sadr,(buf[1])+((long)buf[2]<<8)+
			    ((long)buf[3]<<16)+((long)buf[4]<<24));
      	fwrite(buf+5,msginfo.len-5,1,stdout);
      	printf("\n");
        free(buf);
        buf=NULL;
      }
    }
    free(buf);
    buf=NULL;
  }
  if(buf) free(buf);
  buf=NULL;
  ul_close(ul_fd);
  return count;
};

int set_new_adr(int module,unsigned long module_sn,int new_adr)
{ int ret;
  ul_fd_t ul_fd;
  uchar buf_out[10];
  if(module_sn!=0) module=0;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("set_new_adr : uLan open failed");return -1;};
  buf_out[0]=ULNCS_SET_ADDR;	/* SN0 SN1 SN2 SN3 NEW_ADR */
  buf_out[1]=module_sn>>0;
  buf_out[2]=module_sn>>8;
  buf_out[3]=module_sn>>16;
  buf_out[4]=module_sn>>24;
  buf_out[5]=new_adr;
  ret=ul_send_command(ul_fd,module,UL_CMD_NCS,
  		      module?UL_BFL_ARQ:0,buf_out,6);
  ul_close(ul_fd);
  return 1;
}

int adr_nv_save(int module,unsigned long module_sn)
{ int ret;
  ul_fd_t ul_fd;
  uchar buf_out[10];
  ul_fd=ul_open(ul_dev_name, NULL);  
  if(ul_fd==UL_FD_INVALID) { perror("adr_nv_save : uLan open failed");return -1;};
  buf_out[0]=ULNCS_ADDR_NVSV;
  buf_out[1]=module_sn>>0;
  buf_out[2]=module_sn>>8;
  buf_out[3]=module_sn>>16;
  buf_out[4]=module_sn>>24;
  ret=ul_send_command(ul_fd,module,UL_CMD_NCS,
  		      module?UL_BFL_ARQ:0,buf_out,5);
  ul_close(ul_fd);
  return 1;
}

int read_module_sn(int module,unsigned long *read_sn, char **read_info)
{ int ret=1;
  ul_fd_t ul_fd;
  ul_msginfo msginfo;
  uchar buf_out[10];
  uchar buf_in[10];
  int i;
  if(read_info) *read_info=NULL;
  /* Open device */
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("read_module_sn : uLan open failed");return -1;};
  /* Set filter */
  memset(&msginfo,0,sizeof(ul_msginfo));
  msginfo.cmd=UL_CMD_NCS;
  msginfo.sadr=module;
  msginfo.flg=UL_BFL_NORE;
  ul_addfilt(ul_fd,&msginfo);
  /* Send identification request */
  buf_out[0]=ULNCS_SID_RQ; /* SN0 SN1 SN2 SN3 */
  ret=ul_send_command(ul_fd,module,UL_CMD_NCS,
  		      module?UL_BFL_ARQ:0,buf_out,1);
  /* wait for request */
  while(1)
  { ret=ul_fd_wait(ul_fd,10);
    if(ret<0) break;
    if(ret==0) { ret=-1; break;}
    ret=ul_acceptmsg(ul_fd,&msginfo);
    if(ret<0) break;
    if((msginfo.cmd!=UL_CMD_NCS)
       ||(msginfo.len<5))
    { ul_freemsg(ul_fd);
      continue;
    }
    ret=ul_read(ul_fd,buf_in,5);
    if(ret!=5) break;
    if(read_info) {
      if((i=msginfo.len-5)&&(*read_info=malloc(i+1))){
        ret=ul_read(ul_fd,*read_info,i);
        if(ret>=0) (*read_info)[ret]=0;
	else {
	  free(*read_info);
	  *read_info=NULL;
	}
      }
    }
    ul_freemsg(ul_fd);
    if(buf_in[0]!=ULNCS_SID_RPLY) continue;
    *read_sn=(buf_in[1])+((long)buf_in[2]<<8)+
            ((long)buf_in[3]<<16)+((long)buf_in[4]<<24);
    break;
  }
  ul_close(ul_fd);
  return ret;
}

int interactive_sn_set(void)
{
  ul_dysn_state_t *dysnst = &ul_dysn_state;
  ul_dysn_module_t *mod = &ul_dysn_module;
  unsigned long new_sn=0, prealloc_sn;
  int    ret;
  char   *p,*line=NULL;
  size_t linelen=0;
  int rigid_prealloc_fl = 0;

  while(1){
    printf("\nListing conected modules\n");
    print_modules_dyn();
    printf("select module address: ");
    if(getdelim(&line,&linelen,'\n',stdin)<0) break; 
    if(sscanf(line,"%d",&module)<1) break;
    printf("\n");
    ul_dysn_module_clear(mod);
    if(read_module_sn(module,&mod->sn,&mod->mod_des)<0){
      printf("Cannot connect module\n");
      continue;
    }
    ret=ul_dysn_module_fill_fields(mod);
    if(ret < 0) {
      printf("Module fields fill failed (code %d)  - press a key\n", ret);
      if(getdelim(&line,&linelen,'\n',stdin)<0) break;
      continue;
    }

    printf("Module %d with old SN %08lX info : %s\n",
            module,mod->sn,mod->mod_des);
    printf("  mod_type=%s ver_appl=%s ver_boot=%s git_id=%s flash_time=%s\n",
       mod->mod_type, mod->ver_appl, mod->ver_boot,  mod->git_id, mod->flash_time);

    rigid_prealloc_fl = dysnst->ops->features(dysnst) & UL_DYSN_FEA_RIGID_PRERALLOC? 1: 0;
    ret=dysnst->ops->sn_prealloc(dysnst, &prealloc_sn, mod->mod_type);
    if(ret < 0) {
      printf("Serial number preallocation failed (code %d)  - press a key\n", ret);
      if(getdelim(&line,&linelen,'\n',stdin)<0) break;
      continue;
    }

    while(1){
      new_sn=prealloc_sn;
      if(!rigid_prealloc_fl)
        printf("New SN [%08lX]: ",new_sn);
      else
        printf("Allocated new SN [%08lX] - confirm: ",new_sn);
      if(getdelim(&line,&linelen,'\n',stdin)<0) break;
      if(!rigid_prealloc_fl) {
        for(p=line;*p&&(*p<=' ');p++);
        if(*p){
	  if(sscanf(line,"%lX",&new_sn)<0){
            printf("Bad SN format - press a key\n");
	    continue;
	  }
        }
      }
      ret=dysnst->ops->sn_check(dysnst, new_sn);
      if(ret==-1){
          printf("SN already taken\n");
	  continue;
      }
      if(ret<0){
          printf("Problem with SN history file or database\n");
	  continue;
      }
      break;
    }
    printf("\n");
    if(ul_dysn_module_setsn(mod, module,new_sn)<0) {
      printf("Cannot write SN - press a key\n");
      if(getdelim(&line,&linelen,'\n',stdin)<0) break;
      continue;
    }

    mod->sn=new_sn;

    ret=dysnst->ops->sn_confirm(dysnst, mod);
    if(ret < 0) {
      printf("Serial number confirmation error\n");
      if(getdelim(&line,&linelen,'\n',stdin)<0) break;
      continue;
    }

    printf("SN changed\n");
    printf("Reset device and press a key\n");
    if(getdelim(&line,&linelen,'\n',stdin)<0) break;
  }
  if(line) free(line);
  return 0;
}


int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("send_cmd_go : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

static void
usage(void)
{
  printf("Usage: ul_dysn <parameters>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       messages from/to module\n");
  printf("  -f, --outflg <num>       flags for ULOI message retry and confirmation\n");
  printf("  -p, --print <max>        print modules to max address\n");
  printf("  -P, --print-dyn          print dynamic address capable modules\n");
  printf("  -D  --debug-kernel <m>   flags to debug kernel\n");
  printf("  -S, --set-sn <ser-num>   set serial number\n");
  printf("  -n, --no-permanent	     no permanent SN change\n");
  printf("  -a, --set-adr <adr>      set new module address\n");
  printf("  -A, --set-adrnvsv <adr>  set and store address to EEPROM\n");
  printf("  -i, --interactive        interactive SN setup\n");
 #ifdef CONFIG_APP_UL_DYSN_WITH_LIBCURL
  printf("  -N, --vendor-name        vendor name for SN database access\n");
  printf("  -W, --vendor-password    vendor password for SN database access\n");
 #endif /*CONFIG_APP_UL_DYSN_WITH_LIBCURL*/
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "outflg", 1, 0, 'f' },
    { "print", 1, 0, 'p' },
    { "print-dyn", 0, 0, 'P' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { "set-sn",1, 0, 'S' },
    { "no-permanent",1, 0, 'n' },
    { "set-adr",1, 0, 'a' },
    { "set-adrnvsv",1, 0, 'A' },
    { "interactive",0, 0, 'i' },
   #ifdef CONFIG_APP_UL_DYSN_WITH_LIBCURL
    { "vendor-name",1, 0, 'N' },
    { "vendor-password",1, 0, 'W' },
   #endif /*CONFIG_APP_UL_DYSN_WITH_LIBCURL*/
    { 0, 0, 0, 0}
  };
  int  opt;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:f:p:PVhS:na:A:iN:W:D:")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:f:p:PVhS:na:A:iN:W:D:",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 'f':
      uloi_con_outflg = strtol(optarg,NULL,0);
      uloi_con_outflg_set_flg = 1;
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'P':
      prt_modules_dyn = 1;
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'V':
      fputs("uLan Dynamic and Serial Number Management pre alpha\n", stdout);
      exit(0);
    case 'S':
      new_sn_flg=1;
      new_sn=strtol(optarg,NULL,16);
      break;
    case 'n':
      no_permanent_flg=1;
      break;
    case 'a':
      new_adr_flg=1;
      new_adr=strtol(optarg,NULL,0);
      break;
    case 'A':
      new_adr_flg=1;
      new_adr=strtol(optarg,NULL,0);
      new_adrnvsv_flg=1;
      break;
    case 'i':
      interactive_flg=1;
      break;
   #ifdef CONFIG_APP_UL_DYSN_WITH_LIBCURL
    case 'N':
      vendor_name=optarg;
      break;
    case 'W':
      vendor_password=optarg;
      break;
   #endif /*CONFIG_APP_UL_DYSN_WITH_LIBCURL*/
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if(vendor_name!=NULL) {
    if(vendor_password==NULL) {
      printf("Password has to be set when wendor database is used\n");
      return 1;
    }
   #ifndef CONFIG_APP_UL_DYSN_WITH_LIBCURL
    printf("Remote access to vendor SN database is not compiled in - exitting\n");
    return 1;
   #else /*CONFIG_APP_UL_DYSN_WITH_LIBCURL*/
    ul_dysn_state.vendor_name=vendor_name;
    ul_dysn_state.vendor_password = vendor_password;
    ul_dysn_state.ops=&ul_dysn_extsimpledb_ops;
   #endif /*CONFIG_APP_UL_DYSN_WITH_LIBCURL*/
  }

  if(new_adr_flg||new_adrnvsv_flg) {
    if(!module&&!new_sn_flg) {
      printf("Cannot set address when old address and SN undefined\n");
      return 1;
    }
    if(!new_sn&&!new_sn_flg) {
      if(read_module_sn(module,&new_sn,NULL)<0){
        printf("Cannot read module %d serial number\n",module);
        return 1;
      }
      printf("Module %d, SN %8lX will be readdressed to %d\n",
             module,new_sn,new_adr);
    }
    set_new_adr(module,new_sn,new_adr);
    if(new_adrnvsv_flg) {
      adr_nv_save(new_adr,new_sn);
    }
    new_sn_flg=0;
  }

  if(debugk_flg) debug_kernel(debugk);

  if(new_sn_flg) {
    ul_dysn_module_t *mod = &ul_dysn_module;
    ul_dysn_module_clear(mod);
    if(read_module_sn(module,&mod->sn,&mod->mod_des)<0) {
      printf("Cannot connect module\n");
      return 1;
    }
    if(ul_dysn_module_fill_fields(mod)<0){
      printf("Fill module fields failed\n");
      return 1;
    }
    if(ul_dysn_module_setsn(mod, module, new_sn) <= 0) {
      printf("Set module SN failed\n");
      return 1;
    }
    mod->sn=new_sn;
  }

  if(prt_modules) print_modules(prt_modules);

  if(prt_modules_dyn) print_modules_dyn();

  if(interactive_flg) interactive_sn_set();

  return 0;
}
