/*******************************************************************
  uLan Object Communication

  ul_oitool.c	- simple oi test program

  (C) Copyright 1996-2009 by Pavel Pisa - project originator
        http://cmp.felk.cvut.cz/~pisa
  (C) Copyright 1996-2009 PiKRON Ltd.
        http://www.pikron.com
  (C) Copyright 2002-2009 Petr Smolik
  (C) 2009 DCE FEE CTU Prague
        http://dce.fel.cvut.cz

  The uLan driver project can be used and distributed 
  in compliance with any of next licenses
   - GPL - GNU Public License
     See file COPYING for details.
   - LGPL - Lesser GNU Public License
   - MPL - Mozilla Public License
   - and other licenses added by project originator

  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  WARNING: if you delete all lines, you are not allowed to
  distribute code or sources in any form.
 *******************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <ul_lib/ulan.h>
#include <uloi_com.h>
#include <ul_l_oitarr.h>

#ifdef _WIN32
#define sleep Sleep
#endif

/*******************************************************************/

char *ul_dev_name = UL_DEV_NAME;
int module= 3;
int uloi_con_outflg;
int uloi_con_outflg_set_flg = 0;
int prt_modules= 0;
int debugk     = 0;
int debugk_flg = 0;
int oi_var_fl  = 0;
int oi_var_rd  = 0;
char *oi_var_aoid=NULL;
char *oi_var_val=NULL;

typedef int(oi_var_rdvalue_fnc_t)(ul_msgbuff_t *msgbuff, int vallen);
typedef int(oi_var_wrvalue_fnc_t)(char *data, ul_msgbuff_t *msgbuff, int vallen);

int si_long(const uchar *type_start,int num_len, int *val)
{
  char buff[10];
  if (num_len>sizeof(buff)-1) 
    return -1;
  memset(buff,0,sizeof(buff));
  strncpy(buff,(char*)type_start,num_len);
  *val=strtol(buff,NULL,0);
  return 0;
}

int val_list_add(void ***pdata,int *plen,int base,char *str,char *sep,char *element)
{
  char *s=str,*se,*vs;
  int vallen;
  
  do{
    while(*s && strchr(sep,*s)) s++;
    if(!*s) break;
    se=s;
    if (element!=NULL) {
      if (strchr(element,*se)) {
        se++;
        while(*se && !strchr(element,*se)) se++;
      }
    }
    while(*se && !strchr(sep,*se)) se++;
    if(!*se) {
      vallen=strlen(s);
    } else {
      vallen=se-s;
    }
    vs=malloc(vallen+1);
    if (!vs) return -1;
    strncpy(vs,s,vallen);
    vs[vallen]=0;
    s=se;
    if(*pdata==NULL){
      *plen=0;
      *pdata=malloc(sizeof(char*));
    }else{
      *pdata=realloc(*pdata,(*plen+1)*sizeof(char*));
      if(*pdata==NULL) return -1;
    }
    ((char**)(*pdata))[*plen]=vs;
    (*plen)++;
  } while(1);
  return 1;
}

int val_list_destroy(void ***pdata,int *plen)
{
  int i;
  for(i=0;i<*plen;i++) {
    if (((char**)(*pdata))[i]!=NULL) {
      free(((char**)(*pdata))[i]);
    }
  }
  free(*pdata);
  *plen=0;
  return 0;
}

int uloi_array_parse_dim_et(uchar *type, int len, int *dim, uchar **element_type, int *element_type_len)
{
  int array_string_parse_state,i;
  uchar ch,*et;

  *dim=0;
  array_string_parse_state=0;
  et=NULL;
  for(i=0;i<len;i++) {
    ch=type[i];
    if (array_string_parse_state==0) {
      if (ch!='[') {
        if (et==NULL) {
          et=&type[i];
         if (element_type_len!=NULL)
          *element_type_len=len-i;
        }
      } else 
        array_string_parse_state=1;
    } else {
      if (ch==']') {
        array_string_parse_state=0;
        (*dim)++;
      }
    }
  }
  if (element_type!=NULL)
    *element_type=et;
  return 0;
}

int uloi_array_parse_ranges_string(uchar *type, int len, uchar **element_type,
                                   uloi_array_range_t **pranges, int *pdim,
                                   int force_range)
{
  uloi_array_range_t *range_array;
  int dim,array_string_parse_state,array_num_start,num;
  int i,arg1,arg2;
  unsigned int start,count;
  char ch;

  uloi_array_parse_dim_et(type,len,pdim,element_type,NULL);
  if (*pdim==0)
    return 0;

  range_array=malloc(sizeof(uloi_array_range_t)*(*pdim));
  if (range_array==NULL)
    return -1;

  array_string_parse_state=0;
  array_num_start=-1;
  arg1=-1;
  arg2=-1;
  dim=0;
  for(i=0;i<len;i++) {
    ch=type[i];
    if (array_string_parse_state==0) {
      if (ch=='[')
        array_string_parse_state=1;
    } else if (array_string_parse_state==1) {
      if (ch==']') {
        /* convert num */
        if ((array_num_start>=0) && (array_num_start<i)) {
          si_long(&type[array_num_start],i-array_num_start,&num);
          if (arg1<0) arg1=num;
          else if (arg2<0) arg2=num;
        }
        /* */
        if (arg1<0) {
          start=0;
          count=ULOI_ARRAY_COUNT_UNBOUND;
        } else if (arg2<0) {
          if (!force_range) {
            start=arg1;
            count=1;
          } else {
            start=0;
            count=arg1>=0? arg1: ULOI_ARRAY_COUNT_UNBOUND;
          }
        } else {
          start=arg1;
          count=arg2-arg1+1;
        }
        /* store result */
        range_array[dim].start=start;
        range_array[dim].count=count;
        /* prepare variables for new parsing */
        array_num_start=-1;
        arg1=-1;
        arg2=-1;
        array_string_parse_state=0;
        dim++;
      } else {
        if (((ch>='0') && (ch<='9')) || (ch=='x') || (ch=='X')) {
          if (array_num_start<0)
            array_num_start=i;
        } else {
          /* convert num */
          if ((array_num_start>=0) && (array_num_start<i)) {
            si_long(&type[array_num_start],i-array_num_start,&num);
            if (arg1<0) arg1=num;
            else if (arg2<0) arg2=num;
          }
          array_num_start=-1;
        }
      }
    }
  }

  *pranges=range_array;
  return 0;
}

int oi_var_oiddes_print(int oid, uchar *despack)
{
  char *desname=uloi_oiddespack_strdup(despack,0);
  char *destype=uloi_oiddespack_strdup(despack,1);

  if(desname){
    printf(" %5d deslen=%d name=\"%s\" type=\"%s\"\n",
           oid,despack[0],desname?desname:"",destype?destype:"");
  }else{
    printf(" %5d\n",oid);
  }

  if(desname) free(desname);
  if(destype) free(destype);
  return 0;
}

int oi_var_wrvalue_exec_buff(char *data, ul_msgbuff_t *msgbuff, int vallen)
{
  return 0;
}

int oi_var_rdvalue_float_print(ul_msgbuff_t *msgbuff, int vallen)
{
  float val;
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;
  ret = uloi_tb_buff_rd_float(&val, &msgbuff->data[msgbuff->idx], vallen);
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  printf("%f",val);
  return 0;
}

int oi_var_wrvalue_float_buff(char *data, ul_msgbuff_t *msgbuff, int vallen)
{
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;

  ret = uloi_tb_buff_wr_float(&msgbuff->data[msgbuff->idx], vallen, strtof(data,NULL));
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  return 0;
}

int oi_var_rdvalue_signed_print(ul_msgbuff_t *msgbuff, int vallen)
{
  signed long val;
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;
  ret = uloi_tb_buff_rd_signed(&val, &msgbuff->data[msgbuff->idx], vallen);
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  printf("%ld",val);
  return 0;
}

int oi_var_wrvalue_signed_buff(char *data, ul_msgbuff_t *msgbuff, int vallen)
{
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;

  ret = uloi_tb_buff_wr_signed(&msgbuff->data[msgbuff->idx], vallen, strtol(data,NULL,0));
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  return 0;
}

int oi_var_rdvalue_unsigned_print(ul_msgbuff_t *msgbuff, int vallen)
{
  unsigned long val;
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;
  ret = uloi_tb_buff_rd_unsigned(&val, &msgbuff->data[msgbuff->idx], vallen);
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  printf("%lu",val);
  return 0;
}

int oi_var_wrvalue_unsigned_buff(char *data, ul_msgbuff_t *msgbuff, int vallen)
{
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;

  ret = uloi_tb_buff_wr_unsigned(&msgbuff->data[msgbuff->idx], vallen, strtoul(data,NULL,0));
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  return 0;
}

int oi_var_rdvalue_string_print(ul_msgbuff_t *msgbuff, int vallen)
{
  uchar *p=&msgbuff->data[msgbuff->idx];
  char val[257];
  int sl;
  sl=p[0];
  if ((msgbuff->size - msgbuff->idx -1) < (sl+1))
    return -1;
  memcpy(&val,&p[1],sl);
  val[sl]=0;
  msgbuff->idx+=sl+1;
  printf("%s",val);
  return 0;
}

int oi_var_wrvalue_string_buff(char *data, ul_msgbuff_t *msgbuff, int vallen)
{
  uchar *p=&msgbuff->data[msgbuff->idx];
  int sl;
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;

  sl=strlen(data);
  if (sl>(vallen-1)) sl=vallen-1;
  p[0]=sl;
  memcpy(&p[1],data,sl);
  msgbuff->idx+=sl+1;
  return 0;
}

int oi_var_rdvalue_u2_4x_print(ul_msgbuff_t *msgbuff, int vallen)
{
  unsigned long val1,val2,val3,val4;
  int ret;
  if ((msgbuff->size - msgbuff->idx -1) < 8)
    return -1;
  ret = uloi_tb_buff_rd_unsigned(&val1, &msgbuff->data[msgbuff->idx], 2);
  ret = uloi_tb_buff_rd_unsigned(&val2, &msgbuff->data[msgbuff->idx+2], 2);
  ret = uloi_tb_buff_rd_unsigned(&val3, &msgbuff->data[msgbuff->idx+4], 2);
  ret = uloi_tb_buff_rd_unsigned(&val4, &msgbuff->data[msgbuff->idx+6], 2);
  if (ret<0)
    return ret;
  msgbuff->idx+=8;
  printf("{%lu,%lu,%lu,%lu}",val1,val2,val3,val4);
  return 0;
}

int oi_var_wrvalue_u2_4x_buff(char *data, ul_msgbuff_t *msgbuff, int vallen)
{
  int ret;
  char **val = NULL;
  int val_len = 0;
  
  if ((msgbuff->size - msgbuff->idx -1) < vallen)
    return -1;
  
  if(val_list_add((void***)&val,&val_len,0,data,",{}",NULL)<0){
    return -1;
  }

  if (val_len!=4) {
    val_list_destroy((void***)&val,&val_len);
    return -1;
  }

  ret = uloi_tb_buff_wr_unsigned(&msgbuff->data[msgbuff->idx], 2, strtoul(val[0],NULL,0));
  ret = uloi_tb_buff_wr_unsigned(&msgbuff->data[msgbuff->idx+2], 2, strtoul(val[1],NULL,0));
  ret = uloi_tb_buff_wr_unsigned(&msgbuff->data[msgbuff->idx+4], 2, strtoul(val[2],NULL,0));
  ret = uloi_tb_buff_wr_unsigned(&msgbuff->data[msgbuff->idx+6], 2, strtoul(val[3],NULL,0));

  val_list_destroy((void***)&val,&val_len);
  if (ret<0)
    return ret;
  msgbuff->idx+=vallen;
  return 0;
}

int oi_var_rdarray_print(int dim, ul_msgbuff_t *msgbuff, 
                         int vallen, oi_var_rdvalue_fnc_t *fnc_print)
{
  unsigned int idx;
  unsigned int cnt;
  int ret;

  ret=uloi_array_rdrange_buff(msgbuff, &idx, &cnt);
  if(ret < 0)
    return ret;

  printf("[");
  while (cnt--) {
    if (dim<=1) {
      if (fnc_print!=NULL)
        fnc_print(msgbuff,vallen);
      if (cnt!=0) 
        printf(",");
    } else
      oi_var_rdarray_print(dim-1, msgbuff, vallen, fnc_print);
  }

  printf("]");
  return 0;
}

int oi_var_wrarray_buff(uloi_array_range_t *ranges, int dim, uchar ***data, ul_msgbuff_t *msgbuff,
                        int vallen, oi_var_wrvalue_fnc_t *fnc_buff)
{
  unsigned int idx;
  unsigned int cnt;
  int ret;

  idx=ranges[0].start;
  cnt=ranges[0].count;
  ret=uloi_array_wrrange_buff(msgbuff, &idx, &cnt);
  if(ret < 0)
    return ret;

  while (cnt--) {
    if (dim<=1) {
      if (fnc_buff!=NULL) {
        if (*data!=NULL) {
          fnc_buff((char*)(*data)[0],msgbuff,vallen);
          (*data)++;
        } else
          fnc_buff(NULL,msgbuff,vallen);
      }
    } else
      oi_var_wrarray_buff(ranges+1, dim-1, data, msgbuff, vallen, fnc_buff);
  }

  return 0;
}

int oi_var_rd_print(int dim, ul_msgbuff_t *msgbuff, int vallen, 
                    uchar *element_type, int element_type_len)
{
  oi_var_rdvalue_fnc_t *fnc_print=NULL;

  if(element_type[0] == 'f') {
    fnc_print=oi_var_rdvalue_float_print;
  } else if(element_type[0] == 's') {
    fnc_print=oi_var_rdvalue_signed_print;
  } else if(element_type[0] == 'u') {
    fnc_print=oi_var_rdvalue_unsigned_print;
  } else if ((element_type_len>=2) && (element_type[0] == 'v') && (element_type[1] == 's')) {
    fnc_print=oi_var_rdvalue_string_print;
  } else if ((element_type_len>=4) && (element_type[0] == 'p') && (element_type[1] == 'i') &&
             (element_type[2] == 'c') && (element_type[3] == 'o')) {
    fnc_print=oi_var_rdvalue_u2_4x_print;
  } else if ((element_type_len>=4) && (element_type[0] == 'p') && (element_type[1] == 'o') &&
             (element_type[2] == 'c') && (element_type[3] == 'o')) {
    fnc_print=oi_var_rdvalue_u2_4x_print;
  } else if ((element_type_len>=5) && (element_type[0] == 'p') && (element_type[1] == 'e') &&
             (element_type[2] == 'v') && (element_type[3] == '2') && (element_type[4] == 'c')) {
    fnc_print=oi_var_rdvalue_u2_4x_print;
  } else {
    return -1;
  }

  if (fnc_print==NULL)
    return -1;
  
  if (dim==0) {
    fnc_print(msgbuff,vallen);    
  } else {
    oi_var_rdarray_print(dim, msgbuff, vallen, fnc_print); 
  }
  return 0;
}


int oi_var_wr_buff(uloi_array_range_t *ranges, int dim, uchar **data, ul_msgbuff_t *msgbuff,
                   int vallen, uchar *element_type, int element_type_len)
{
  oi_var_wrvalue_fnc_t *fnc_buff=NULL;

  if(element_type[0] == 'f') {
    fnc_buff=oi_var_wrvalue_float_buff;
  } else if(element_type[0] == 's') {
    fnc_buff=oi_var_wrvalue_signed_buff;
  } else if(element_type[0] == 'u') {
    fnc_buff=oi_var_wrvalue_unsigned_buff;
  } else if(element_type[0] == 'e') {
    fnc_buff=oi_var_wrvalue_exec_buff;
  } else if ((element_type_len>=2) && (element_type[0] == 'v') && (element_type[1] == 's')) {
    fnc_buff=oi_var_wrvalue_string_buff;
  } else if ((element_type_len>=4) && (element_type[0] == 'p') && (element_type[1] == 'i') &&
             (element_type[2] == 'c') && (element_type[3] == 'o')) {
    fnc_buff=oi_var_wrvalue_u2_4x_buff;
  } else if ((element_type_len>=4) && (element_type[0] == 'p') && (element_type[1] == 'o') &&
             (element_type[2] == 'c') && (element_type[3] == 'o')) {
    fnc_buff=oi_var_wrvalue_u2_4x_buff;
  } else if ((element_type_len>=5) && (element_type[0] == 'p') && (element_type[1] == 'e') &&
             (element_type[2] == 'v') && (element_type[3] == '2') && (element_type[4] == 'c')) {
    fnc_buff=oi_var_wrvalue_u2_4x_buff;
  } else {
    return -1;
  }

  if (fnc_buff==NULL)
    return -1;
  
  if (dim==0) {
    if (data!=NULL) 
      fnc_buff((char*)data[0],msgbuff,vallen);    
    else
      fnc_buff(NULL,msgbuff,vallen);    
  } else {
    oi_var_wrarray_buff(ranges, dim, &data, msgbuff, vallen, fnc_buff); 
  }
  return 0;
}

int uloi_get_var_array(uloi_coninfo_t *coninfo, int oid, uloi_array_range_t *pranges, int dim, void *val, int size)
{ 
  int i, meta_len;
  uchar *meta;

  meta_len=0;
  for(i=dim-1; i>=0 ;i--) {
    if (pranges[i].count==1) meta_len=(pranges[i].count * meta_len) + 2;
    else meta_len=(pranges[i].count * meta_len) + 4;
  }

  meta=malloc(meta_len);
  if(!meta) return -1;
  uloi_tb_buff_wr_array_ranges(dim,pranges,meta,meta_len);
  uloi_get_var(coninfo,oid,meta,meta_len, val, size);

  if (meta) 
    free(meta);
  return 0;
}

int oi_var_test(char *aoid, char *valin, char **valout, char *type, int dir_rd)
{
  int ret;
  int oid;
  ul_msgbuff_t msgbuff;
  uchar valbuff[4096];
  int vallen=-1;
  int i;
  int aoid_dim, dim = 0;
  int element_type_len;
  uloi_coninfo_t *coninfo;
  uchar *oiddespack = NULL;
  uchar *element_type;
  uloi_array_range_t *ranges = NULL;
  uchar **msgdata = NULL;
  int msgdata_len;

  msgbuff.data=valbuff;
  msgbuff.size=sizeof(valbuff);
  msgbuff.idx=0;
  element_type=(uchar*)"u2";
  element_type_len=2;
  vallen = 2;
  msgdata_len=0;

  coninfo=uloi_open(ul_dev_name,module,0x10,0,10);
  if(uloi_con_outflg_set_flg) {
    uloi_con_ulan_set_outflg(coninfo,uloi_con_outflg);
  }
  if(!coninfo) { perror("oi_var_test : uLan OI open failed");return -1;};

  if((aoid!=NULL)&&(*aoid)) {
    if(isdigit(*aoid)) {
      oid=strtol(aoid,NULL,0);
      ret=uloi_get_oiddes(coninfo,dir_rd?ULOI_DOIO:ULOI_DOII,oid,&oiddespack);
      if(ret<0) {
        fprintf(stderr,"oi_var_test : uloi_get_oiddes returned %d for oid %d\n",ret,oid);
      }
    } else {
      /* fix for array */
      ret=uloi_get_aoiddes(coninfo,dir_rd?ULOI_DOIO:ULOI_DOII,aoid,&oiddespack);
      if(ret<0) {
        fprintf(stderr,"oi_var_test : uloi_get_aoiddes returned %d for aoid %s\n",ret,aoid);
        goto error_ret;
      }
      oid=ret;
    }

    ret=uloi_array_parse_ranges_string((uchar*)aoid, strlen(aoid), NULL, &ranges, &aoid_dim, 0);
    if(ret<0) {
      fprintf(stderr,"oi_var_test : uloi_array_parse_ranges_string returned %d\n",ret);
      goto error_ret;
    }

    if(oiddespack) {
      uchar *destypeloc = uloi_oiddespack_getloc(oiddespack, 1);
      if(destypeloc) {
        uloi_array_parse_dim_et((destypeloc + 1), destypeloc[0], &dim, &element_type, &element_type_len);
        if (dim!=aoid_dim) {
          if (ranges)
            free(ranges);
          ret=uloi_array_parse_ranges_string((destypeloc + 1), destypeloc[0], NULL, &ranges, &dim, 1);
          if(ret<0) {
            fprintf(stderr,"oi_var_test : uloi_array_parse_ranges_string returned %d\n",ret);
            goto error_ret;
          }
        }

        /* todo - check range count undefined -1 */

        vallen = uloi_tb_type2size(element_type, element_type_len);
        if(vallen == -1) {
          fprintf(stderr,"oi_var_test : uloi_tb_type2size reports bad size\n");
          goto error_ret;
        }
      }
    }

    /* write/read actions */
    if(valout==NULL&&!dir_rd) {
      if(valin&&*valin) {
        int items_count;
        if(vallen == 0) {
          fprintf(stderr,"oi_var_test : oid %d does not expect argument\n", oid);
          goto error_ret;
        }
        if(val_list_add((void***)&msgdata,&msgdata_len,0,valin,",[]","{}")<0){
          fprintf(stderr,"oi_var_test : incorrect message data \"%s\"\n",aoid);
          goto error_ret;
        }

        items_count=1;
        for(i=0;i<dim;i++)
          items_count*=ranges[i].count;

        if (items_count!=msgdata_len) {
          fprintf(stderr,"oi_var_test : wrong number of input arguments, expect %d, written %d\n",items_count,msgdata_len);
          goto error_ret;
        }
      } else {
        if(vallen>0) {
          fprintf(stderr,"oi_var_test : oid %d expects argument\n", oid);
          goto error_ret;
        }
        vallen=0;
      }

      oi_var_wr_buff(ranges, dim, msgdata, &msgbuff, vallen, element_type, element_type_len);

      ret=uloi_set_var(coninfo,oid,msgbuff.data,msgbuff.idx);
      if(ret<0) {
        fprintf(stderr,"oi_var_test : uloi_set_var failed\n");
        goto error_ret;
      }
    } else {
      msgbuff.idx=0;
      if (dim==0) {
        ret=uloi_get_var(coninfo,oid,NULL,0,msgbuff.data,msgbuff.size);
        if(ret<0) {
          fprintf(stderr,"oi_var_test : uloi_get_var failed\n");
          goto error_ret;
        }
      } else {
        ret=uloi_get_var_array(coninfo, oid, ranges, dim, msgbuff.data, msgbuff.size);
        if(ret<0) {
          fprintf(stderr,"oi_var_test : uloi_get_var failed\n");
          goto error_ret;
        }
      }
      /* print value */
      printf("%s=",aoid);
      oi_var_rd_print(dim, &msgbuff, vallen, element_type, element_type_len);
      printf("\n");
    }
    if(oiddespack)
      free(oiddespack);
    oiddespack = NULL;
  } else {
    int *oids;
    ret=uloi_get_oids(coninfo,ULOI_QOII,&oids);
    if(ret<0)
    { fprintf(stderr,"oi_var_test : uloi_get_oids returned %d\n",ret);
      return -1;
    };
    printf("module recognized commands and set vars:\n");
    for(i=0;i<ret;i++)
    { uchar *despack=NULL;
      if(uloi_get_oiddes(coninfo,ULOI_DOII,oids[i],&despack)>=0){
        oi_var_oiddes_print(oids[i],despack);
        if(despack)
          free(despack);
      }else{
        printf(" %5d no reply\n",oids[i]);
      }
    }
    free(oids);
    ret=uloi_get_oids(coninfo,ULOI_QOIO,&oids);
    if(ret<0)
    { fprintf(stderr,"oi_var_test : uloi_get_oids returned %d\n",ret);
      return -1;
    };
    printf("module recognized get vars:\n");
    for(i=0;i<ret;i++)
    { uchar *despack=NULL;
      if(uloi_get_oiddes(coninfo,ULOI_DOIO,oids[i],&despack)>=0){
        oi_var_oiddes_print(oids[i],despack);
        if(despack)
          free(despack);
      }else{
        printf(" %5d no reply\n",oids[i]);
      }
    }
    free(oids);
  }

  uloi_close(coninfo);

  return 0;

 error_ret:
  if(oiddespack)
    free(oiddespack);

  if(ranges)
    free(ranges);

  val_list_destroy((void***)&msgdata,&msgdata_len);

  uloi_close(coninfo);

  return -1;

}

int print_modules(int max_addr)
{ int ret;
  int i;
  ul_fd_t ul_fd;
  uchar *p;
  void *buf=NULL;
  int buf_len;
  int count=0; 
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID)
    { perror("print_modules : uLan open failed");return -1;};
  for(i=1;i<=max_addr;i++)
  {
    ret=ul_send_query_wait(ul_fd,i,UL_CMD_SID,
  			UL_BFL_NORE|UL_BFL_PRQ,NULL,0,&buf,&buf_len);
    if(ret>=0)
    { count++;
      p=(uchar*)buf;
      for(ret=0;(ret<buf_len)&&*p;ret++,p++);
      printf("%2d:  ",i);
      fwrite(buf,ret,1,stdout);
      printf("\n");
    };
    if(buf) free(buf);
    buf=NULL;
  };
  ul_close(ul_fd);
  return count;
};

int debug_kernel(int debug_msk)
{ int ret;
  ul_fd_t ul_fd;
  ul_fd=ul_open(ul_dev_name, NULL);
  if(ul_fd==UL_FD_INVALID) { perror("send_cmd_go : uLan open failed");return -1;};
  ret=ul_drv_debflg(ul_fd,debug_msk);
  ul_close(ul_fd);
  return ret;
};

static void
usage(void)
{
  printf("Usage: ul_oitool <parameters>\n");
  printf("  -d, --uldev <name>       name of uLan device [%s]\n", ul_dev_name);
  printf("  -m, --module <num>       messages from/to module\n");
  printf("  -f, --outflg <num>       flags for ULOI message retry and confirmation\n");
  printf("  -p, --print <max>        print modules to max address\n");
  printf("      --debug-kernel <m>   flags to debug kernel\n");
  printf("  -o, --oi-var <oid>=<v>   uLan OI variable set\n");
  printf("  -o, --oi-var <oid>?      uLan OI variable read\n");
  printf("  -o, --oi-var <oid>:      uLan OI variable execute\n");
  printf("  -V, --version            show version\n");
  printf("  -h, --help               this usage screen\n");
}

int main(int argc,char *argv[])
{
  static struct option long_opts[] = {
    { "uldev", 1, 0, 'd' },
    { "module",1, 0, 'm' },
    { "outflg", 1, 0, 'f' },
    { "print", 1, 0, 'p' },
    { "version",0,0, 'V' },
    { "help",  0, 0, 'h' },
    { "debug-kernel", 1, 0, 'D' },
    { "oi-var",1, 0, 'o' },
    { 0, 0, 0, 0}
  };
  int  opt;
  char c;

 #ifndef HAS_GETOPT_LONG
  while ((opt = getopt(argc, argv, "d:m:p:Vho:")) != EOF)
 #else
  while ((opt = getopt_long(argc, argv, "d:m:p:Vho:",
			    &long_opts[0], NULL)) != EOF)
 #endif
    switch (opt) {
    case 'd':
      ul_dev_name=optarg;
      break;
    case 'm':
      module = strtol(optarg,NULL,0);
      break;
    case 'f':
      uloi_con_outflg = strtol(optarg,NULL,0);
      uloi_con_outflg_set_flg = 1;
      break;
    case 'p':
      prt_modules = strtol(optarg,NULL,0);
      break;
    case 'D':
      debugk = strtol(optarg,NULL,0);
      debugk_flg=1;
      break;
    case 'V':
      fputs("uLan oitool v0.1\n", stdout);
      exit(0);
    case 'o':
      oi_var_fl=1;
      oi_var_rd=0;
      oi_var_aoid=optarg;
      oi_var_val=strpbrk(optarg,":=?");
      if(oi_var_val)
      { c=*oi_var_val;
        *(oi_var_val++)=0;
        switch(c)
        { case '?':
	    oi_var_val=NULL;
	    oi_var_rd=1;
            break;
        }
      }
      break;
    case 'h':
    default:
      usage();
      exit(opt == 'h' ? 0 : 1);
    }

  if(debugk_flg) debug_kernel(debugk);

  if(prt_modules) print_modules(prt_modules);

  if(oi_var_fl) oi_var_test(oi_var_aoid,oi_var_val,NULL,NULL,oi_var_rd);

  return 0;
}
